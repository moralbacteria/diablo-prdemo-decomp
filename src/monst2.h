// These two functions look like they're in their own file for... some reason?
// I don't actually know what the file is called so I called it monst2 to
// complement monster and monstdat

#ifndef __MONST2_H__
#define __MONST2_H__

#include <windows.h>

void M_GetDir();
BOOL SolidLoc(int x, int y);

#endif
