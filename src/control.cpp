#include "control.h"

#include "defines.h"
#include "diablo.h"
#include "engine.h"
#include "gmenu.h"
#include "inv.h"
#include "lighting.h"
#include "player.h"
#include "spells.h"

#include <windows.h>

//
// initialized vars (.data:004A3450)
//

// wtf is this? nobody references it
static char registration_block[128] = "REGISTRATION_BLOCK";
// ASCII char -> frame in bigtgold, etc
BYTE fontframe[128] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 54, 44, 57, 0, 56, 55, 47, 40, 41, 0, 39, 50, 37, 51, 52,
    36, 27, 28, 29, 30, 31, 32, 33, 34, 35, 48, 49, 58, 38, 59, 53,
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
    16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 42, 0, 43, 0, 0,
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
    16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 40, 0, 41, 0, 0};
// Frame (bigtgold, etc) -> total character width in px
BYTE fontkern[] = {
    8, 10, 7, 9, 8, 7, 6, 8, 8, 3, 3, 8, 6, 11, 9, 10, 6,
    9, 9, 6, 9, 11, 10, 13, 10, 11, 7, 5, 7, 7, 8, 7, 7,
    7, 7, 7, 10, 4, 5, 6, 3, 3, 4, 3, 6, 6, 3, 3, 3, 3,
    3, 2, 7, 6, 3, 10, 10, 6, 8, 8, 0, 0, 0, 0};
// TODO: Use SCREENXY macros
int lineOffsets[4][4] = {
    {0x70BF1, 0x6000, 0x6000, 0x6000},
    {0x6EDF1, 0x72CF1, 0x6000, 0x6000},
    {0x6E4F1, 0x70BF1, 0x735F1, 0x6000},
    {0x6DBF1, 0x6FCF1, 0x71DF1, 0x73EF1}};
BOOL spselflag = FALSE;
char SpellITbl[MAX_SPELLS] = {
    1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 37, 13, 12, 17, 16, 14,
    18, 19, 11, 20, 15, 21, 23, 24, 25, 22, 34, 38, 46};
// TODO rect struct
int PanBtnPos[6][4] = {
    {15, 367, 52, 24},
    {573, 367, 52, 24},
    {15, 404, 27, 24},
    {15, 441, 27, 24},
    {599, 404, 27, 24},
    {599, 441, 27, 24}};
const char *PanBtnStr[] = {
    "Inventory",
    "Character Information",
    "Automap",
    "Main menu",
    "Send multiplayer message",
    "Message filter"};
// TODO rect struct
int ChrBtnsRect[6][4] = {
    {336, 90, 95, 22},
    {457, 138, 41, 22},
    {457, 166, 41, 22},
    {457, 195, 41, 22},
    {457, 223, 41, 22},
    {0, 0, 0, 0}};

//
// uninitialized vars (.data:)
//

BOOL drawbtnflag;
BOOL drawhpflag;
// Index into plr[myplr].InvList.
// if -1 then there is nothing being hovered
// Set by draw_potbox string. Used to set player potbox selection in control_do_update_potbox.
int potbox_hover;
BOOL durflag;
BOOL drawinfoflag;
BYTE *pPanelText;
BYTE *pStatusPanel;
char infostr[64];
BOOL chrbtn[6];
// control panel graphics buffer; PANEL_HEIGHT+22 by PANEL_WIDTH (the +22 is for bulb overhang?)
BYTE *pBtmBuff;
BYTE *pSpellCels;
BYTE *pManaBuff;
char tempstr[64];
BOOL panbtndown;
BOOL panbtn[6];
BYTE *pLifeBuff;
int num_potbox_items;
// Level up button pressed? Action happens on release
BOOL lvlbtndown;
BOOL drawmanaflag;
int pnumlines;
// TODO
BYTE *pChrButtons;
BOOL chrbtnactive;
BYTE *pChrPanel;
BOOL drawpotboxflag;
int potbox_hover_miscid;
// TODO
BOOL talkflag;
BOOL pinfoflag;
BYTE *pPanelButtons;
BYTE *pDurIcons;
int potbox_hover_ispell;
char panelstr[4][64];
char infoclr;
BOOL pstrjust[4];
BYTE *pSpell;
BOOL chrflag;

//
// code (.text:000000000040EF80)
//

// DrawSpellCel	000000000040EF80

// .text:0040F059
void SetSpellTrans(char t)
{
    // TODO
}

// .text:0040F28D
void DrawSpell()
{
    char st;

    st = plr[myplr]._pRSplType;
    // TODO
}

// DrawSpellList	000000000040F3C8
// SetSpell	000000000040F95C
void SetSpell()
{
    // TODO
}
// SetSpeedSpell	000000000040F9D8
void SetSpeedSpell(int)
{
    // TODO
}
// ToggleSpell	000000000040FA49
void ToggleSpell(int)
{
    // TODO
}
// PrintChar	000000000040FC09
// CPrintString	000000000040FDCE
void CPrintString(int y, const char *str, BOOL center)
{
    // TODO
}

// AddPanelString	000000000040FF10
void AddPanelString(const char *str, BOOL just)
{
    // TODO
}

// ClearPanel	000000000040FF70
void ClearPanel()
{
    strcpy(panelstr[0], " ");
    pstrjust[0] = FALSE;
    strcpy(panelstr[1], " ");
    pstrjust[1] = FALSE;
    strcpy(panelstr[2], " ");
    pstrjust[2] = FALSE;
    strcpy(panelstr[3], " ");
    pstrjust[3] = FALSE;

    pnumlines = 0;
    drawinfoflag = TRUE;
    pinfoflag = FALSE;
}

// .text:0041001E
// Draws a rectangle of (x, y, w, h) of the control panel to the screen at (sx, sy)
// You may want to draw only select regions to avoid overdraw, esp for the text in the center.
// This blits from `pBtmBuff`, not the CEL file!
void DrawPanelBox(int x, int y, int w, int h, int sx, int sy)
{
    int nSrcOff, nDstOff;

    nSrcOff = x + PANEL_WIDTH * y;
    nDstOff = sx + BUFFER_WIDTH * sy;

    __asm {
		mov		esi, pBtmBuff; // pBtmBuff is the pre-rendered control panel
		add		esi, nSrcOff
		mov		edi, gpBuffer
		add		edi, nDstOff
		xor		ebx, ebx
		mov		bx, word ptr w
		xor		edx, edx
		mov		dx, word ptr h
	label1:
		mov		ecx, ebx
		shr		ecx, 1
		jnb		label2
		movsb
		jecxz	label4
	label2:
		shr		ecx, 1
		jnb		label3
		movsw
		jecxz	label4
	label3:
		rep movsd
	label4:
		add		esi, PANEL_WIDTH
		sub		esi, ebx
		add		edi, BUFFER_WIDTH
		sub		edi, ebx
		dec		edx
		jnz		label1
    }
}

// .text:004100A7
// Blits the panel text background and prints all text lines.
void PrintInfo()
{
    int i;
    DrawPanelBox(177, 78, 288, 45, 177 + BORDER_LEFT, 574);
    for (i = 0; i < pnumlines; i++)
    {
        CPrintString(i, panelstr[i], pstrjust[i]);
    }
    pnumlines = 0;
}

// .text:00410125
// Clears the panel text then adds some introductory text.
void InitPanelStr()
{
    ClearPanel();
    AddPanelString("Welcome to Diablo", TRUE);
    AddPanelString("Press F1 for help", TRUE);
    pinfoflag = FALSE;
}

// BuffCopy	0000000000410162
// TransBuffCopy	00000000004101CC
// DrawHealthTop	000000000041021C
// UpdateLifeFlask	00000000004102D5
void UpdateLifeFlask()
{
    // TODO
}
// DrawManaTop	00000000004103E6
// UpdateManaFlask	000000000041049F
void UpdateManaFlask()
{
    // TODO
}

// text:004105F9
// Loads control panel graphics and initializes related state.
void InitControlPan()
{
    int i;

    // BUG these are never initialized to 0! which could explain #26?
    pBtmBuff = (BYTE *)DiabloAllocPtr((PANEL_HEIGHT + 22) * PANEL_WIDTH);
    pManaBuff = (BYTE *)DiabloAllocPtr(88 * 88);
    pLifeBuff = (BYTE *)DiabloAllocPtr(88 * 88);
    pPanelText = LoadFileInMem("CtrlPan\\SmalText.CEL");
    pChrPanel = LoadFileInMem("Data\\Char.CEL");
    pSpellCels = LoadFileInMem("CtrlPan\\SpelIcon.CEL");
    SetSpellTrans(RSPLTYPE_SKILL);

    // Render Panel7 into the intermediate buffer `pBtmBuff`
    pStatusPanel = LoadFileInMem("CtrlPan\\Panel7.CEL");
    CelBlitWidth(pBtmBuff, 0, 143, 640, pStatusPanel, 1, 640);
    MemFreeDbg(pStatusPanel);

    // Render P7Bulbs into `pLifeBuff` and `pManaBuff`
    pStatusPanel = LoadFileInMem("CtrlPan\\P7Bulbs.CEL");
    CelBlitWidth(pLifeBuff, 0, 87, 88, pStatusPanel, 1, 88);
    CelBlitWidth(pManaBuff, 0, 87, 88, pStatusPanel, 2, 88);
    MemFreeDbg(pStatusPanel);

    talkflag = FALSE;
    lvlbtndown = FALSE;

    pPanelButtons = LoadFileInMem("CtrlPan\\PanBtns.CEL");
    for (i = 0; i < 6; i++)
    {
        panbtn[i] = FALSE;
    }
    panbtndown = FALSE;

    pChrButtons = LoadFileInMem("Data\\CharBut.CEL");
    for (i = 0; i < 6; i++)
    {
        chrbtn[i] = FALSE;
    }
    chrbtnactive = FALSE;

    pDurIcons = LoadFileInMem("Items\\DurIcons.CEL");
    durflag = FALSE;

    strcpy(infostr, "");
    InitPanelStr();

    drawhpflag = TRUE;
    drawmanaflag = TRUE;
    chrflag = FALSE;
    spselflag = FALSE;
    drawpotboxflag = FALSE;

    if (plr[myplr].potbox_item != -1)
    {
        i = plr[myplr].potbox_item;
        potbox_hover_ispell = plr[myplr].InvList[i]._iSpell;
        potbox_hover_miscid = plr[myplr].InvList[i]._iMiscId;
    }
    else
    {
        potbox_hover_ispell = 0;
        potbox_hover_miscid = 0;
    }
}

// control_416813	000000000041090E

// .text:00410946
void DrawCtrlPan()
{
    DrawPanelBox(0, 16, PANEL_WIDTH, PANEL_HEIGHT, PANEL_X, PANEL_Y);
    DrawSpell();
    // draw_potion_box();
    //  TODO
}

// DrawCtrlBtns	000000000041097D
void DrawCtrlBtns()
{
    // TODO
}

// DoSpeedBook	0000000000410A61
void DoSpeedBook()
{
    // TODO
}
// draw_potion_box	0000000000410C67
// control_potbox_checkitem	0000000000410D66
// draw_potbox_string	0000000000410E94
// control_do_update_potbox	00000000004111C5
void control_do_update_potbox()
{
    // TODO
}
// DeterminePotionBoxItems	000000000041121E
// control_check_btn_press	000000000041147C
void control_check_btn_press()
{
    // TODO
}
// CheckPanelInfo	00000000004115F1
// CheckBtnUp	0000000000411D75
void CheckBtnUp()
{
    // TODO
}
// FreeControlPan	0000000000411F94
void FreeControlPan()
{
    // TODO
}
// control_WriteStringToBuffer	00000000004120FA
// control_print_info_str	0000000000412188
// DrawInfoBox	00000000004122A1
// ADD_PlrStringXY	00000000004125E4
// DrawChr	0000000000412719
// CheckLvlBtn	0000000000413704
void CheckLvlBtn()
{
    // TODO
}
// ReleaseLvlBtn	0000000000413770
void ReleaseLvlBtn()
{
    // TODO
}
// DrawLevelUpIcon	00000000004137D4
// CheckChrBtns	000000000041383D
void CheckChrBtns()
{
    // TODO
}
// ReleaseChrBtns	0000000000413B08
void ReleaseChrBtns()
{
    // TODO
}

// .text:00413D0C
void DrawDurIcon()
{
    int x;
    int c;

    x = 80;
    if (chrflag || invflag)
    {
        return;
    }

    if (invflag)
    {
        x = 320;
    }

    if (plr[myplr].InvBody[INVLOC_HEAD]._itype != ITYPE_NONE && plr[myplr].InvBody[INVLOC_HEAD]._iDurability <= 5)
    {
        c = 4;
        if (plr[myplr].InvBody[INVLOC_HEAD]._iDurability > 2)
        {
            // The first 8 icons are red, the last 8 are yellow
            c += 8;
        }
        CelDraw(x, 495, pDurIcons, c, 32);
        x += 40;
    }

    if (plr[myplr].InvBody[INVLOC_CHEST]._itype != ITYPE_NONE && plr[myplr].InvBody[INVLOC_CHEST]._iDurability <= 5)
    {
        c = 3;
        if (plr[myplr].InvBody[INVLOC_CHEST]._iDurability > 2)
        {
            c += 8;
        }
        CelDraw(x, 495, pDurIcons, c, 32);
        x += 40;
    }

    if (plr[myplr].InvBody[INVLOC_HAND_LEFT]._itype != ITYPE_NONE && plr[myplr].InvBody[INVLOC_HAND_LEFT]._iDurability <= 5)
    {
        if (plr[myplr].InvBody[INVLOC_HAND_LEFT]._iClass == ICLASS_WEAPON)
        {
            switch (plr[myplr].InvBody[INVLOC_HAND_LEFT]._itype)
            {
            case ITYPE_SWORD:
                c = 2;
                break;
            case ITYPE_AXE:
                c = 6;
                break;
            case ITYPE_BOW:
                c = 7;
                break;
            case ITYPE_MACE:
                c = 5;
                break;
            case ITYPE_STAFF:
                c = 8;
                break;
            }
        }
        else
        {
            // Shield
            c = 1;
        }
        if (plr[myplr].InvBody[INVLOC_HAND_LEFT]._iDurability > 2)
        {
            c += 8;
        }
        CelDraw(x, 495, pDurIcons, c, 32);
        x += 40;
    }

    if (plr[myplr].InvBody[INVLOC_HAND_RIGHT]._itype != ITYPE_NONE && plr[myplr].InvBody[INVLOC_HAND_RIGHT]._iDurability <= 5)
    {
        if (plr[myplr].InvBody[INVLOC_HAND_RIGHT]._iClass == ICLASS_WEAPON)
        {
            switch (plr[myplr].InvBody[INVLOC_HAND_RIGHT]._itype)
            {
            case ITYPE_SWORD:
                c = 2;
                break;
            case ITYPE_AXE:
                c = 6;
                break;
            case ITYPE_BOW:
                c = 7;
                break;
            case ITYPE_MACE:
                c = 5;
                break;
            case ITYPE_STAFF:
                c = 8;
                break;
            }
        }
        else
        {
            // Shield
            c = 1;
        }
        if (plr[myplr].InvBody[INVLOC_HAND_RIGHT]._iDurability > 2)
        {
            c += 8;
        }
        CelDraw(x, 495, pDurIcons, c, 32);
        x += 40;
    }
}

// .text:0041414F
// Modifies the game viewport to turn the game view shades of red. Doesn't touch the control panel.
void RedBack()
{
    int idx;

    idx = light4flag ? 0x600 : 0x1200;
    __asm {
			mov		edi, gpBuffer
			add		edi, SCREENXY(0, 0)
			mov		ebx, pLightTbl
			add		ebx, idx
			mov		edx, SCREEN_HEIGHT - PANEL_HEIGHT
		lx_label1:
			mov		ecx, SCREEN_WIDTH
		lx_label2:
			mov		al, [edi]
			xlat
			stosb
			loop	lx_label2
			add		edi, BORDER_LEFT + BORDER_RIGHT
			dec		edx
			jnz		lx_label1
    }
}

// .text:004141B4
// Draws the red pause screen overtop the rest of the game.
void DrawPause()
{
    RedBack();
    gmenu_print_text(316, 336, "Pause");
}
