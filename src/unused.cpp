// This segment contains 3 functions with an empty implementation.
// There are also no callers.

// .text:00489810
void __dc_sub_489810(int i)
{
    // This space intentionally left blank
}

// .text:00489826
void __dc_sub_489826()
{
    // This space intentionally left blank
}

// .text:00489836
void __dc_sub_489836()
{
    // This space intentionally left blank
}