#ifndef __PALETTE_H__
#define __PALETTE_H__

#include <windows.h>

#include <DDRAW.H>

//
// defines
//

#define PALETTE_NO_FADE 0
#define PALETTE_FADE_IN 1
#define PALETTE_FADE_OUT 2

// standard palette for all levels
// 8 or 16 shades per color
// example (dark blue): PAL16_BLUE+14, PAL8_BLUE+7
// example (light red): PAL16_RED+2, PAL8_RED
// example (orange): PAL16_ORANGE+8, PAL8_ORANGE+4
#define PAL8_BLUE 128
#define PAL8_RED 136
#define PAL8_YELLOW 144
#define PAL8_ORANGE 152
#define PAL16_BEIGE 160
#define PAL16_BLUE 176
#define PAL16_YELLOW 192
#define PAL16_ORANGE 208
#define PAL16_RED 224
#define PAL16_GRAY 240

//
// variables
//

extern PALETTEENTRY progress_palette[256];
extern PALETTEENTRY system_palette[256];
extern PALETTEENTRY orig_palette[256];
extern PALETTEENTRY logical_palette[256];
extern DWORD fade_state;
extern UINT delayed_Msg;
extern double gamma_correction;
extern double gamma_backup;

//
// functions
//

void LoadPalette(char *pszFilename, PALETTEENTRY *dest);
void DoFadeIn();
void DoFadeOut();
void PaletteFadeIn(int delta);
void PaletteFadeOut(int delta);
void CopyPalette(PALETTEENTRY *dest, PALETTEENTRY *src);
void ApplyGamma(PALETTEENTRY *pal, int start, int end);
void LoadRndLvlPal(int l);
void palette_init_caves();
void palette_update_caves();
void ResetPal();

#endif