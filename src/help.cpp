#include "help.h"

#include "control.h"
#include "minitext.h"
#include "stores.h"

//
// initialized variables (.data:004B3F10)
//

// This is the help text that is shown when the user presses F1 in-game.
//
// This array of string literals is treated as one big block of text. This works
// since the compiler lays out the string literals contiguously in the order
// that they appear. NUL character padding will occur to reach 4-byte alignment.
// Additionally, each string literal is NUL terminated. However, these
// characters are skipped in DrawHelp.
//
// There are several special markup characters that affect behavior but are not
// drawn:
//
// - The pipe `|` indicates a new paragraph.
// - The dollar sign `$` indicates that this is a header (drawn in red)
// - The ampersand `&` marks the end of the text.
const char *gszHelpText[] = {
    "Welcome mortals! Diablo is real-time action in a dark, gothic role-playing game. ",
    "Prepare to be taken into the very depths of hell on your quest to destroy the Lord ",
    "of all Evil - Diablo.|",
    "|",
    "This Help section is designed to make your playing experience as easy as possible. ",
    "Let's get started...|",
    "|",
    "$Keyboard Shortcuts:|",
    "Diablo can be played exclusively by using the mouse controls. There are times, however, ",
    "when you may want to use shortcuts to some commands by using the keyboard.  These ",
    "shortcuts are listed below:|",
    "|",
    "F1: Open the Help Screen|",
    "Esc: Displays the main menu|",
    "Tab: Displays the Auto-map|",
    "Space: Removes all screens and map from the play area|",
    "S: Opens Spell selection pop-up menu|",
    "I: Opens the Inventory screen|",
    "C: Opens the Character screen|",
    "Z: Zooms the game screen in and out|",
    "F: Reduces the brightness of the screen|",
    "G: Increases the brightness of the screen|",
    "Shift + Left Click: Fire any Bow without moving|",
    "1,2,3: While the Spell selection pop-up menu is active, these keys will ",
    "set that number as a hot-key selection (pressing the number will ",
    "automatically ready that spell).|",
    "|",
    "$Movement:|",
    "Movement is controlled by the mouse.  The gauntlet on the screen is your cursor. ",
    "Use this to indicate the destination of your character and then left-click to move ",
    "to that area.|",
    "|",
    "$Selecting Items:|",
    "What you can interact with within the game is easily identifiable.  Simply move the ",
    "cursor over any object or creature.  If the object can be picked up, attacked, activated ",
    "or used in any way, it will be immediately outlined and highlighted with a description of ",
    "the object appearing in the text area on the control panel.|",
    "|",
    "Example: If you select a door and then left-click the character will walk to the door and ",
    "open it.  A second click closes the door.  If you left-click on a selected weapon, the ",
    "character will walk over to it and put it in his inventory. If you left-click on a selected  ",
    "creature....|",
    "|",
    "$Combat:|",
    "Combat is initiated by left-clicking on a creature that has been selected.  Left-clicking ",
    "on a creature that you are next to will cause your character to attack it immediately. ",
    "Left-clicking on a distant creature will cause the character to walk to the creature and ",
    "then attack. This will work if your character has a melee weapon (sword, mace, club, etc.), ",
    "just a shield or is unarmed.|",
    "|",
    "If your character is equipped with a bow, left-clicking will fire an arrow at a selected ",
    "creature. Holding down the shift key and then left-clicking allows the character to fire ",
    "the bow without accidentally moving.|",
    "|",
    "$Picking up Objects:|",
    "If you left-click an item - such as a weapon, armor, scroll or book - your character will ",
    "move to that item and add it to his inventory automatically.  If you do not have enough ",
    "room in your inventory for the item, it will fall from your grasp and you will receive a ",
    "message that more room is needed.  Open your inventory screen and try re-arranging or ",
    "removing items to carry what you really want or need.|",
    "|",
    "$Inventory:|",
    "You can toggle the Inventory screen on and off by clicking the INV> button on the control ",
    "panel.  Items may be moved around in your inventory by selecting them and then left-clicking ",
    "to pick them up.  When you pick up an item while in the inventory screen, your cursor ",
    "changes into the item. You can then place this item into empty spaces in your inventory, ",
    "swap them with other items in your inventory or equip them.|",
    "|",
    "If you have an item that you no longer wish to carry, simply grab the item from your ",
    "inventory and then left-click in the play area to drop it.|",
    "|",
    "$Equipping Items:|",
    "Equipping items is done by picking up an item from your inventory and placing it in the ",
    "appropriate boxes on the figure in the Inventory screen. Weapons and shields go into the ",
    "large spaces to the  right or left of the figure. Two-handed weapons such as bows and ",
    "axes preclude the use of a shield and will take up both of these large spaces.|",
    "|",
    "Cloaks, robes, capes and all other armor must go in the central torso slot of the figure.|",
    "|",
    "Helmets and caps  go in the head box and rings go into the small boxes at the hands of ",
    "the figure.|",
    "|",
    "To change items that your character has equipped, pick up a new item and place it on top ",
    "of the item you wish to remove.  Your character will automatically swap the items and the ",
    "cursor will now change into the item that was in that box.|",
    "|",
    "$Usable Items:|",
    "Potions, elixirs, oils and books are classified as usable items.  These items can be used ",
    "by right-clicking on them in the inventory screen.  Any one item can also be 'readied' in ",
    "the same way as spells. To ready an item, left-click the 'select current item' button to ",
    "the left of the red health ball on the control panel. ",
    "A pop-up menu will activate that you can scroll through to choose an item to be readied. ",
    "Left-click when the desired item is selected and you will see that item displayed on the ",
    "button.  To use a readied item, simply right-click on the 'select current item' button or ",
    "press the Enter key on your keyboard.|",
    "|",
    "$Item Information:|",
    "All items in Diablo share certain common attributes.  These are damage, durability, charges ",
    "and minimum requirements.|",
    "|",
    "Damage: This is represented by a range that shows the minimum and maximum damage that item ",
    "can inflict. An sword that has a (2-6) after its name does a minimum of two damage and a ",
    "maximum of six when it hits. Damage can be modified by the quality of the weapon, the ",
    "characters strength and magic.|",
    "|",
    "Durability: This is how long an item can last before it is rendered useless. This is ",
    "represented by a ratio of current durability to maximum durability. A shield that has a ",
    "durability of 15/20 would have 15 points of damage it could take from use before it was ",
    "rendered useless. Maximum durability can be affected by the quality of the item, magic or ",
    "repairs upon the item. Minimum durability can be raised by repairing an item.|",
    "When the durability of an item you are wearing or wielding drops to 5 points or below, ",
    "an icon of the image appears in the lower left corner above the control panel. These icons ",
    "warn the player that the item is about to break and needs to be repaired soon.|",
    "|",
    "Charges: Some items have charges associated with. These represent how many times that item ",
    "can be used to cast the spell or affect indicated in its description.  Charges are ",
    "represented by a ratio of charges left to maximum charges. A staff that has charges of ",
    "2/5 could be used to cast 2 more spells before it was rendered powerless (it could still ",
    "be used to attack with physically). Maximum charges can be affected by the magic or ",
    "recharges cast upon the item. Minimum charges can be raised by recharging the item.|",
    "|",
    "Minimum Requirements: These are the minimum requirements that a character must meet to ",
    "wield the item. The more powerful an item is, the higher the minimum requirements will be. ",
    "If a character does not meet these requirements, he will be unable to equip the item and ",
    "its name and information will be displayed in red.  The item artwork will also have a red ",
    "tint to it in the Inventory screen.|",
    "|",
    "There are also three classes of items in Diablo.  These are mundane, magic and Unique:|",
    "|",
    "Mundane items have no special attributes and their information is displayed in white text.|",
    "|",
    "Magic Items are represented by blue text descriptions. Use the Identify spell to determine ",
    "their exact properties and attributes.|",
    "|",
    "Unique items are fully identified when you obtain them. Information on these items is ",
    "displayed in gold.|",
    "|",
    "$Skills:|",
    "Important Note - Skills do not work in the town in this pre-release demo. Left-clicking ",
    "on the 'select current spell' button will open a pop-up menu that will allow you to ready ",
    "a skill or spell for use.  To use a readied skill or spell, simply right-click in the main ",
    "play area.|",
    "|",
    "Skills are the innate abilities of your character. These skills are different depending on ",
    "what class you choose and they require no mana to use.|",
    "|",
    "The Warrior starts the game with the Repair skill.  The Repair Skill icon appears as the ",
    "'select current spell' button found next to the blue Mana sphere on the control panel. ",
    "When the skill is useable, the button will be gold in color.  The game starts with the ",
    "button grayed out as you cannot repair or cast spells in the town.|",
    "|",
    "The Repair skill allows the Warrior to fix an item that has been worn by use or is damaged ",
    "in combat.  To repair an item, have the Repair Skill selected (your character starts the ",
    "game this way) and right-click the mouse as if you were casting a spell.  Your cursor will ",
    "change into a hammer shaped cursor that you will use to select the item to be repaired. ",
    "Repairing an item will decrease the maximum durability of that item, but can be done while ",
    "in the labyrinth.|",
    "|",
    "The Blacksmith can repair items, but it will cost you gold. When the Blacksmith repairs ",
    "an item, it does not decrease the maximum durability of the item.|",
    "|",
    "$Spells:|",
    "Important Note - Spells do not work in the town in this pre-release demo.|",
    "|",
    "Spells are magical effects that can be cast from a scroll, a staff or memorized from a ",
    "book.  Spells may or may not require mana to use and are available to all classes.|",
    "|",
    "Spells cast from an item (a scroll or staff) cost no mana to use, but are limited by ",
    "the number of charges available on the item.  Scrolls always have one charge while staves ",
    "may have multiple charges. To use spells from a staff, you must equip the staff to cast ",
    "the spell. Spells that are cast from staves are represented by an orange icon button in ",
    "the 'select current spell' button area.  Scrolls are represented by a red button.|",
    "|",
    "Memorized spells cost mana to cast,, but they can be used as long as the character has ",
    "mana to power them.  The Warrior starts the game with no memorized spells.  If the ",
    "character finds a book in the labyrinth, he can memorize the spell written in that ",
    "book by opening the Inventory screen and right-clicking on the book.  This will make ",
    "the spell always available to the character for casting.  Memorized spells are denoted ",
    "by a blue button.|",
    "|",
    "While some spells affect the caster, some spells require a target. These targeted spells ",
    "are cast in the direction that you indicate with your cursor on the play area.  If you ",
    "highlight a creature, you will cast that spell at that creature.  Not all items within ",
    "the labyrinth can be targeted.|",
    "|",
    "Example: A fireball spell will travel at the creature or to the location you right-click ",
    "on.  A Healing spell will simply add health to your character while diminishing his ",
    "available mana and requires no targeting.|",
    "|",
    "You can also set a spell or scroll as a Hot Key position for instant selection.  Start ",
    "by opening the pop-up menu as described in the skill section above.  Assign Hot Keys by ",
    "hitting the 1, 2 or 3 keys on your keyboard after scrolling through the available spells ",
    "and skills and stopping on the one you wish to assign.|",
    "|",
    "$Health and Mana:|",
    "The two spheres in the control panel display your health and mana.  Your health is a ",
    "measure of how much life force your character has.  The red sphere of fluid on the left ",
    "side of the control panel represents the health of your character.  When the fluid is ",
    "gone - your character is dead.|",
    "|",
    "The blue fluid on the right side of the control panel represents your character's ",
    "available mana. Mana is the magical force used by your character to cast spells.  When ",
    "the liquid in the sphere is low or depleted, you may be unable to cast some (or all) of ",
    "your spells.|",
    "|",
    "$Control Panel:|",
    "The control panel is how you receive detailed information in Diablo and interact with ",
    "much of your surroundings.  Here is a quick run-down of the control panel areas and their ",
    "use:|",
    "|",
    "INV:  This button is used to access your Inventory screen|",
    "CHAR: This button is used to access your Character Statistics screen|",
    "Current Item: This is the item that has been readied for immediate use|",
    "Current Spell: This is the spell that has been readied for immediate casting|",
    "Health Sphere: This is the amount of health your character currently has|",
    "Mana Sphere: This is the amount of mana your character currently has|",
    "Automap: This button activates the mapping overlay|",
    "Main Menu: This activates the main menu screen|",
    "Multiplayer Message: Unavailable in the pre-release demo|",
    "Message Filter: Unavailable in the pre-release demo|",
    "Description Area: This is where any important information about creatures or items ",
    "you can interact with is displayed.|",
    "|",
    "Character Info:|",
    "Toggle the Character Statistics Screen on and off by clicking the <CHAR button on the ",
    "control panel.  This screen shows the 'nuts and bolts' of your character.  There are ",
    "four major statistics that dictate how your character progresses in the game.|",
    "|",
    "STRENGTH: This is how physically powerful your character is. This statistic plays a ",
    "large part in  determining how much damage you can do when attacking creatures.|",
    "|",
    "MAGIC: This is how attuned your character is with the arcane powers of the world. ",
    "This statistic plays a large part in  determining how much mana you have available ",
    "for casting spells.|",
    "|",
    "DEXTERITY: This is how quick of foot and hand your character is. This statistic plays ",
    "a large part in  determining the chance you have to hit creatures in combat.|",
    "|",
    "VITALITY: This is how physically fit your character is. This statistic plays a large ",
    "part in  determining how much health your character has when he is undamaged.|",
    "|",
    "$Main Menu:|",
    "To activate the Game Menu, click the button with the pentagram symbol on the left ",
    "of the control panel.  You have three options in this menu.|",
    "|",
    "Save: This saves the game you are currently playing. Please note that this pre-release ",
    "demo will only save one game at a time.  Saving overwrites any previously saved game.|",
    "|",
    "Load: This loads the last game that you have saved.|",
    "|",
    "Quit: This exits the program. Please note that this does not automatically save your ",
    "game. If you wish to save the game you are currently playing, select save game before ",
    "quitting.|",
    "|",
    "Auto-map: Diablo will automatically map where you have been in the labyrinth. To access ",
    "the auto-map, click the button with the compass symbol on the left of the control panel. ",
    "You can also hit the TAB key on your keyboard to activate the auto-map.  You can zoom in ",
    "and out of the map with the + and - keys.  Scrolling the map is done with the arrow keys.|",
    "|",
    "$Tech support:|",
    "This is a pre-release version of Diablo.  We have tried to make this as bug free as possible. ",
    "There will be no patches or updates of this pre-release demo available. The version will ",
    "not be supported by our tech support lines.|",
    "|",
    "$For maximum performance:|",
    "We recommend copying the Diablo folder onto your hard drive.|",
    "&",
};

//
// uninitialized variables (.data:005FD0B0)
//

BOOL helpflag; // is showing F1 text
int help_select_line;
// ...
BOOL helpmodeflag; // "helpmode" is a reactive overlay that says how to play the game. kinda like system shock
int HelpTop;
// ...

//
// code (.text:0044AEA0)
//

// .text:0044AEA0
void InitHelp()
{
    helpflag = FALSE;
    helpmodeflag = FALSE;
    // TODO
    // mov dword_5FD0B8, 0
    // mov     dword_5FD580, 0
}

// .text:0044AED8
// Draw a line of help text.
static void DrawHelpLine(int x, int y, char *text, char color)
{
    // TODO
}

// .text:0044AFA6
// Draw help text to the screen in a scrolling text window. A lot of the store
// functions are reused.
void DrawHelp()
{
    const char *s; // help string pointer (index)
    int i;         // help line index
    int j;
    int c;    // tempstr index
    int w;    // width in px of string in tempstr
    BYTE tc;  // animation frame for ASCII char
    BYTE col; // the color of text to draw (palette index)

    DrawSTextHelp();
    DrawQTextBack();

    PrintSString(0, 2, TRUE, "Diablo Help", 3, 0);
    DrawSLine(5);

    // help_select_line is the first line that appears on the screen. Since the
    // game performs text wrap dynamically, everything up until help_select_line
    // must be "reflowed". This gets more expensive the further we scroll down.
    // Feels like something they could have cached...
    s = gszHelpText[0];
    for (i = 0; i < help_select_line; i++)
    {
        c = 0;
        w = 0;
        // Skip NUL characters. This is an artifact of how gszHelpText is
        // constructed of contiguous string literals.
        //
        // This was "fixed" in retail by having one long string. Although, this
        // code does remain!
        while (*s == '\0')
        {
            s++;
        }

        // Skip header markup
        while (*s == '$')
        {
            s++;
        }

        // Stop when we reach the end
        if (*s == '&')
        {
            continue;
        }

        // Construct a string that fills the screen or up to a paragraph break
        while (*s != '|' && w < 577) // TODO magic number
        {
            // Skip NUL chars within a paragraph (see prior note)
            while (*s == '\0')
            {
                s++;
            }

            // Copy data into a buffer to be referenced later for reflow.
            // BUG: Well-crafted input could cause an overflow here
            tempstr[c] = *s;

            // Add to running string width in px based on font frame width px.
            // If this exceeds 577 then this loop exits
            tc = fontframe[(BYTE)tempstr[c]];
            w += fontkern[tc] + 1;

            c++; // go to next character in output (tempstr)
            s++; // go to next character in input (gszHelpText)
        }

        // Text overflow never splits a word; backtrack to the last space.
        if (w >= 577)
        {
            while (1)
            {
                c--;
                // BUG: Well-crafted input could cause underflow
                if (tempstr[c] == ' ')
                {
                    break;
                }
                s--;
            }
        }

        // Could be at the end of a paragraph. Skip the end-of-paragraph marker
        if (*s == '|')
        {
            s++;
        }
    }

    // Render 15 lines of text with wrapping starting at help_select_line
    for (j = 7; j < 22; j++) // TODO magic numbers
    {
        c = 0;
        w = 0;

        // Skip NUL characters (de rigueur by now)
        while (*s == '\0')
        {
            s++;
        }

        // $ at the beginning means draw the text in red, instead of white
        if (*s == '$')
        {
            s++;
            col = 2; // TODO magic number
        }
        else
        {
            col = 0; // TODO magic number
        }

        // Store the final line in HelpTop and stop rendering. This will be used
        // when scrolling the text area
        if (*s == '&')
        {
            HelpTop = help_select_line;
            continue;
        }

        // Reflow the text to render
        while (*s != '|' && w < 577)
        {
            while (*s == '\0')
            {
                s++;
            }
            tempstr[c] = *s;
            tc = fontframe[(BYTE)tempstr[c]];
            w += fontkern[tc] + 1;
            c++;
            s++;
        }
        if (w >= 577)
        {
            while (1)
            {
                c--;
                if (tempstr[c] == ' ')
                {
                    break;
                }
                s--;
            }
        }

        // If there's a string to draw then draw it
        if (c != 0)
        {
            tempstr[c] = '\0';
            DrawHelpLine(0, j, tempstr, col);
        }

        // Skip paragraph markup (de rigueur by now)
        if (*s == '|')
        {
            s++;
        }
    }

    PrintSString(0, 23, TRUE, "Press ESC to end or the arrow keys to scroll.", 3, 0);
}
// PrintHelpModeStr	000000000044B265
// AddHText	000000000044B357
// helpmode_add_line	000000000044B3FA
// DrawHelpLines	000000000044B487
// helpmode_set_pos	000000000044B530

// DrawHelpMode	000000000044B58F
void DrawHelpMode()
{
    // TODO
    // mov dword_5FD0B8, 0
    // mov     dword_5FD580, 0
}

// .text:0044B78F
void DisplayHelp()
{
    helpflag = TRUE;
    help_select_line = 0;
    HelpTop = 5000; // This will be overwritten during DrawHelp
}

// .text:0044B7BD
void HelpScrollUp()
{
    if (help_select_line > 0)
    {
        help_select_line--;
    }
}

// .text:0044B7E0
void HelpScrollDown()
{
    if (help_select_line < HelpTop)
    {
        help_select_line++;
    }
}
