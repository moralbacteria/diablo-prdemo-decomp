#ifndef __STORM_H__
#define __STORM_H__

#include <windows.h>

#include <DDRAW.H>

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef STORMAPI
#define STORMAPI __stdcall
#endif

     //     SBltDestroy @2
     //     SBltGetSCode @3
     //     SBltROP3 @4
     //     SBltROP3Clipped @5
     //     SBltROP3Tiled @6
     //     SBmpDestroy @7
     //     SBmpLoadImage @8
     //     SBmpSaveImage @9
     //     SCodeCompile @10
     //     SCodeDelete @11
     //     SCodeDestroy @12
     //     SCodeExecute @13
     //     SCodeGetPseudocode @14
     //     SDlgCreateDialog @15
     //     SDlgCreateDialogIndirect @16
     //     SDlgCreateDialogIndirectParam @17
     //     SDlgCreateDialogParam @18
     //     SDlgDefDialogProc @19
     //     SDlgDestroy @20
     //     SDlgDialogBox @21
     //     SDlgDialogBoxIndirect @22
     //     SDlgDialogBoxIndirectParam @23
     //     SDlgDialogBoxParam @24
     //     SDlgEndDialog @25
     //     SDlgSetBackgroundBitmap @26
     //     SDrawAutoInitialize @27
     //     SDrawCaptureScreen @28
     //     SDrawClearSurface @29
     //     SDrawDestroy @30
     //     SDrawFlipPage @31
     //     SDrawGetFrameWindow @32
     //     SDrawGetObjects @33
     //     SDrawGetScreenSize @34
     //     SDrawGetServiceLevel @35
     //     SDrawLockSurface @36
     BOOL STORMAPI SDrawManualInitialize(HWND hWnd, LPDIRECTDRAW ddInterface, LPDIRECTDRAWSURFACE primarySurface, LPDIRECTDRAWSURFACE surface2, LPDIRECTDRAWSURFACE backSurface, LPDIRECTDRAWPALETTE ddPalette, HPALETTE hPalette);
     //     SDrawMessageBox @38
     //     SDrawPostClose @39
     //     SDrawSelectGdiSurface @40
     //     SDrawUnlockSurface @41
     //     SDrawUpdatePalette @42
     //     SDrawUpdateScreen @43
     //     SEvtDestroy @44
     //     SEvtDispatch @45
     //     SEvtRegisterHandler @46
     //     SEvtUnregisterHandler @47
     //     SEvtUnregisterType @48
     BOOL STORMAPI SFileCloseArchive(HANDLE hArchive);
     BOOL STORMAPI SFileCloseFile(HANDLE hFile);
     BOOL STORMAPI SFileDdaBegin(HANDLE hFile, DWORD flags, DWORD mask);
     BOOL STORMAPI SFileDdaDestroy();
     BOOL STORMAPI SFileDdaEnd(HANDLE hFile);
     BOOL STORMAPI SFileDdaInitialize(HANDLE directsound);
     //     SFileDestroy @55
     LONG STORMAPI SFileGetFileSize(HANDLE hFile, LPDWORD lpFileSizeHigh);
     BOOL STORMAPI SFileOpenArchive(const char *szMpqName, DWORD dwPriority, DWORD dwFlags, HANDLE *phMpq);
     BOOL STORMAPI SFileOpenFile(const char *filename, HANDLE *phFile);
     BOOL STORMAPI SFileReadFile(HANDLE hFile, void *buffer, DWORD nNumberOfBytesToRead, DWORD *read, LONG *lpDistanceToMoveHigh);
     //     SFileSetBasePath @60
     int __stdcall SFileSetFilePointer(HANDLE, int, HANDLE, int);
     //     SFileSetLocale @62
     //     SGdiBitBlt @63
     //     SGdiCreateFont @64
     //     SGdiDeleteObject @65
     //     SGdiDestroy @66
     //     SGdiExtTextOut @67
     //     SGdiLoadFont @68
     //     SGdiRectangle @69
     //     SGdiSelectObject @70
     //     SGdiSetPitch @71
     //     SGdiTextOut @72
     //     SMemAlloc @73
     //     SMemDestroy @74
     //     SMemFree @75
     //     SMsgDestroy @76
     //     SMsgDispatchMessage @77
     //     SMsgDoMessageLoop @78
     //     SMsgRegisterCommand @79
     //     SMsgRegisterKeyDown @80
     //     SMsgRegisterKeyUp @81
     //     SMsgRegisterMessage @82
     //     SNetCreateGame @83
     //     SNetDestroy @84
     //     SNetEnumGames @85
     //     SNetEnumProviders @86
     //     SNetGetNetworkLatency @87
     //     SNetGetNumPlayers @88
     //     SNetGetPlayerCaps @89
     //     SNetGetPlayerName @90
     //     SNetGetTurnsInTransit @91
     //     SNetInitializeProvider @92
     //     SNetJoinGame @93
     //     SNetLeaveGame @94
     //     SNetReceiveMessage @95
     //     SNetReceiveTurns @96
     //     SNetRegisterEventHandler @97
     //     SNetResetLatencyMeasurements @98
     //     SNetSendMessage @99
     //     SNetSendTurn @100
     //     SNetStartGame @101
     //     SNetUiDestroy @102
     //     SNetUiInitialize @103
     //     SNetUiOnPaint @104
     //     SNetUiSelectGame @1
     //     SNetUiSelectProvider @105
     //     SNetUnregisterEventHandler @106
     //     SSyncDeleteLock @107
     //     SSyncEnterLock @108
     //     SSyncInitializeLock @109
     //     SSyncLeaveLock @110
     //     STransBlt @111
     //     STransBltUsingMask @112
     //     STransCreate @113
     //     STransDelete @114
     //     STransDestroy @115
     //     STransDuplicate @116
     //     STransIntersectDirtyArray @117
     //     STransInvertMask @118
     //     STransLoad @119
     //     STransSetDirtyArrayInfo @120
     //     STransUpdateDirtyArray @121
     BOOL STORMAPI SVidDestroy();
     //     SVidGetPerformanceData @123
     //     SVidGetSize @124
     //     SVidInitialize @125
     BOOL STORMAPI SVidPlayBegin(const char *filename, int a2, int a3, int a4, int flags, HANDLE *video);
     BOOL __cdecl SVidPlayContinue(void);
     //     SVidPlayContinueSingle @128
     BOOL STORMAPI SVidPlayEnd(HANDLE video);
     //     StormDestroy @130

#ifdef __cplusplus
}
#endif

#endif
