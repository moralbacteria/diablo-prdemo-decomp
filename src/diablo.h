#ifndef __DIABLO_H__
#define __DIABLO_H__

#include <windows.h>
#include <DDRAW.H>

#include "defines.h"

//
// defines
//

// Causes a repaint. E.g. progress fade, draw screen, etc
#define WM_DIABPAINT WM_USER
#define WM_401 WM_USER + 1
// Take down stairs; progress from currlevel to curlevel + 1
#define WM_DIABNEXTLVL WM_USER + 2
// Take up stairs; progress from currlevel to curlevel + 1
#define WM_DIABPREVLVL WM_USER + 3
// Town portal?
#define WM_DIABRTNLVL WM_USER + 4
// Load set level like the Bone Chamber
#define WM_DIABSETLVL WM_USER + 5
#define WM_406 WM_USER + 6
#define WM_407 WM_USER + 7
// Run some logic after the screen has faded in.
// Typically, fade in/out are handled by the interac.cpp "framework" and actual logic happens afterwards during WM_DIABMENUTICK
#define WM_DIABDONEFADEIN WM_USER + 8
// Run some logic after the screen has faded out.
// Typically, fade in/out are handled by the interac.cpp "framework" and actual logic happens afterwards during WM_DIABMENUTICK
#define WM_DIABDONEFADEOUT WM_USER + 9
#define WM_40A WM_USER + 10
#define WM_40B WM_USER + 11
#define WM_DIABNEWGAME WM_USER + 12
#define WM_DIABMENUTICK WM_USER + 12
#define WM_DIABLOADGAME WM_USER + 13

//
// enums
//

// Which screen the user is seeing. Largely used for controlling menus, or game vs non-game logic.
// Typical transitions look like MODE_BLIZ_LOGO -> MODE_INTRO_VID -> MODE_MAINMENU -> MODE_NEWGAME -> MODE_PROGRESS -> MODE_GAME -> MODE_PROGRESS -> MODE_GAME -> ... -> MODE_DEMO_END
enum game_mode
{
    // Quotes and Blizzard logo animation
    MODE_BLIZ_LOGO = 0x0,
    // New game, load game, quit
    MODE_MAINMENU = 0x1,
    // Choose class, enter name
    MODE_NEWGAME = 0x2,
    // Actual gameplay
    MODE_GAME = 0x7,
    // Intro movie (same video as retail)
    MODE_INTRO_VID = 0xC,
    // Promotional slideshow for the full game, typically shown on quit
    MODE_DEMO_END = 0xD,
    // Level transition screens (i.e. dungeon to town, etc)
    MODE_PROGRESS = 0xE,
};

// Bitmap of possible drawing options
enum force_redraw_values
{
    // Draw the top 352 pixels. Respects `draw*flag`.
    FORCE_REDRAW_VIEWPORT = 1,
    // Draw the bottom 128 pixels. Respects `draw*flag`.
    FORCE_REDRAW_PANEL = 2,
    // Draw everything, unconditionally. Ignores `draw*flag`.
    FORCE_REDRAW_ALL = 4,
};

// TODO: Copied from devilution, verify
enum lvl_entry
{
    ENTRY_MAIN = 0, // Stairs down
    ENTRY_PREV = 1, // stairs up
    ENTRY_SETLVL = 2,
    ENTRY_RTNLVL = 3,
    ENTRY_LOAD = 4,
    ENTRY_WARPLVL = 5,
    ENTRY_TWARPDN = 6,
    ENTRY_TWARPUP = 7,
};

//
// variables
//

extern BYTE *gpBuffer;
extern char fileLoadPrefix[64];
extern BOOL cheat_mode;
extern BOOL gbActive;
extern BOOL shouldStopPaintTimer;
extern BOOL demo_mode;
extern BOOL debug_mode;
extern BOOL light4flag;
extern char savedir_abspath[64];
extern int force_redraw;
extern BOOL debugMusicOn;
extern HANDLE sghMusic;
extern LPDIRECTDRAWSURFACE lpDDSBackBuf;
extern LPDIRECTDRAWSURFACE lpDDSPrimary;
extern LPDIRECTDRAWPALETTE lpDDPalette;
extern int gMode;
extern BOOL shouldStopPaintTimer;
extern UINT paint_event_timer_resolution;
extern volatile BOOL paint_mutex;
extern BOOL outstanding_paint_event;
extern int MouseX;
extern int MouseY;
extern BOOL pause_paint_timer;
extern BOOL zoomflag;
extern BOOL dd_use_backbuffer;
extern BOOL calculate_offsets_in_DrawMain;
extern int DDPitchTbl[SCREEN_HEIGHT];

//
// functions
//

void FreeGameMem();
void LoadGameLevel(BOOL firstflag, int lvldir, BOOL inc_progress);
void set_outstanding_paint_event(BOOL b);
void InitLevels();

#endif
