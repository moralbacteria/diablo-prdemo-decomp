#ifndef __MULTI_H__
#define __MULTI_H__

#include <windows.h>

//
// variables
//

extern BOOL isSinglePlayer;
extern BYTE byte_4A3AB4;
extern DWORD dword_5DE7B4;

//
// functions
//

BOOL DPlayHandleMessage();
BOOL unknown_dplay_get_plractive();
void unknown_dplay_send();
void unknown_dplay_case_plr();
void unknown_dplay_game_send(DWORD);
void unknown_dplay_get_plrs(DWORD);

#endif