#ifndef __TRIGS_H__
#define __TRIGS_H__

//
// structs
//

struct TriggerStruct {
    int _tx;
    int _ty;
    int _tmsg;
};

//
// functions
//

void InitTownTriggers();
void InitL1Triggers();
void InitL2Triggers();
void InitL3Triggers();
void InitL4Triggers();
void InitSKingTriggers();
void InitSChambTriggers();
void CheckTrigForce();
void CheckTriggers();

#endif