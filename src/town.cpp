#include "town.h"

#include "diablo.h"
#include "effects.h"
#include "engine.h"
#include "gendung.h"
#include "inv.h"
#include "items.h"
#include "minitext.h"
#include "player.h"
#include "quests.h"
#include "stores.h"

//
// initialized data (.data:004B3CD8)
//

/** Specifies the start X-coordinates of the cows in Tristram. */
int TownCowX[] = {58, 56, 59};
/** Specifies the start Y-coordinates of the cows in Tristram. */
int TownCowY[] = {16, 14, 20};
/** Specifies the start directions of the cows in Tristram. */
int TownCowDir[] = {DIR_SW, DIR_NW, DIR_N};
/** Maps from direction to X-coordinate delta, which is used when
 * placing cows in Tristram. A single cow may require space of up
 * to three tiles when being placed on the map.
 */
int cowoffx[8] = {-1, 0, -1, -1, -1, 0, -1, -1};
/** Maps from direction to Y-coordinate delta, which is used when
 * placing cows in Tristram. A single cow may require space of up
 * to three tiles when being placed on the map.
 */
int cowoffy[8] = {-1, -1, -1, 0, -1, -1, -1, 0};

//
// uninitialized data (.data:005FC8A0)
//

int numtowners;          // Upper bound for towner
BYTE *pCowCels;          // "Towners\\Animals\\Cow.CEL"
TownerStruct towner[10]; // Contains all data for townsfolk

//
// Code (.text:00443430)
//

// town_world_sub_upper    0000000000443430
// town_world_sub_lower    0000000000443710
// town_clear_upper_buf    0000000000443A6D
// town_clear_low_buf    0000000000443AEF
// town_lower_44A643    0000000000443B80
// town_upper_44A6F5    0000000000443C32
// town_draw_clipped_e_flag    0000000000443CDC
// town_draw_clipped_town    0000000000443DD3
// town_draw_clipped_e_flag_2    0000000000445451
// town_draw_clipped_town_2    000000000044558A
// town_draw_e_flag    0000000000446CEC
// town_draw_town_all    0000000000446DEE
// T_DrawGame    0000000000448502
// T_DrawZoom    00000000004487E6
// T_DrawView    0000000000448B2C
void T_DrawView(int StartX, int StartY)
{
    // TODO
}

// .text:00448CDD
// Basically the town version of ObjSetMicro
static void SetTownMicros()
{
    int x;
    int y;
    int lv;
    int i;
    MICROS *pMap;
    WORD *pPiece;
    MICROS micro;
    WORD map_mt;

    for (y = 0; y < MAXDUNY; y++)
    {
        for (x = 0; x < MAXDUNX; x++)
        {
            lv = dPiece[x][y];
            pMap = &dpiece_defs_map_1[IsometricCoord(x, y)];
            if (lv != 0)
            {
                lv--;
                pPiece = (WORD *)&pLevelPieces[32 * lv];
                // Town micros are 16 high, compared to dungen which is 10
                for (i = 0; i < 16; i++) // TODO magic numer
                {
                    pMap->mt[i] = pPiece[(i & 1) + 16 - 2 - (i & 0xE)];
                }
            }
            else
            {
                for (i = 0; i < 16; i++)
                {
                    pMap->mt[i] = 0;
                }
            }
        }
    }

    // TODO: wtf does this do
    // for (y = 0; y < MAXDUNY; y++)
    // {
    //     for (x = 0; x < MAXDUNX; x++)
    //     {
    //         pMap = &dpiece_defs_map_1[IsometricCoord(x, y)];
    //         for (i = 0; i < 16; i++) // TODO magic numer
    //         {
    //             micro.mt[i] = 0;
    //         }
    //         map_mt = pMap->mt[0];
    //         if (map_mt & 0x70 == 0x40)
    //         {
    //             micro.mt[0] = 1;
    //         }
    //         map_mt = pMap->mt[1];
    //         if (map_mt & 0x70 == 0x50)
    //         {
    //             micro.mt[1] = 1;
    //         }
    //         for (i = 2; i < 16; i++) // TODO magic numer
    //         {
    //             map_mt = pMap->mt[i];
    //             if ((BYTE*)(&map_mt)[1] == 0x70 || ) {

    //             }
    //         }
    //         // TODO
    //     }
    // }

    // TODO

    if (zoomflag)
    {
        ViewDX = 640;
        ViewDY = 352;
        ViewBX = 10;
        ViewBY = 11;
    }
    else
    {
        ViewDX = 384;
        ViewDY = 224;
        ViewBX = 6;
        ViewBY = 7;
    }
}

// .text:0044911F
// Load tiles from .DUN and use .TIL to unpack into dPiece
static void T_FillSector(BYTE *P3Tiles, BYTE *pSector, int xi, int yi, int w, int h)
{
    int i;
    int j;
    int xx;
    int yy;
    int ii;
    int v1;
    int v2;
    int v3;
    int v4;

    ii = 4;
    yy = yi;
    for (j = 0; j < h; j++)
    {
        xx = xi;
        for (i = 0; i < w; i++)
        {
            // Custom asm that unpacks the .DUN and .TIL via LODSW
            __asm {
                ; // Look at a word of the .DUN
                mov esi, pSector
                mov eax, ii
                add esi, eax
                xor eax, eax
                lodsw
                ; // If it's 0 then set all v* to 0
                or eax, eax
                jz dun_is_zero
                ; // Otherwise, unpack the .TIL for that value
                dec eax
                mov esi, P3Tiles
                shl eax, 3
                add esi, eax
                xor eax, eax
                lodsw
                inc eax
                mov v1, eax
                lodsw
                inc eax
                mov v2, eax
                lodsw
                inc eax
                mov v3, eax
                lodsw
                inc eax
                mov v4, eax
                jmp done
            dun_is_zero:
                mov v1, eax
                mov v2, eax
                mov v3, eax
                mov v4, eax
            done:
                nop
            }
            dPiece[xx][yy] = v1;
            dPiece[xx + 1][yy] = v2;
            dPiece[xx][yy + 1] = v3;
            dPiece[xx + 1][yy + 1] = v4;
            xx += 2;
            ii += 2;
        }
        yy += 2;
    }
}

// .text:0044924C
// Loads town sectors into dPiece
static void T_Pass3()
{
    int xx;
    int yy;
    BYTE *P3Tiles;
    BYTE *pSector;

    // clear dPiece
    for (yy = 0; yy < MAXDUNY; yy += 2)
    {
        for (xx = 0; xx < MAXDUNX; xx += 2)
        {
            dPiece[xx][yy] = 0;
            dPiece[xx + 1][yy] = 0;
            dPiece[xx][yy + 1] = 0;
            dPiece[xx + 1][yy + 1] = 0;
        }
    }

    // The town is split into 4 parts ("sectors") and joined together. I can
    // imagine that this way uses 1/4 of the memory for pSector loading
    P3Tiles = LoadFileInMem("Levels\\TownData\\Town.TIL");
    pSector = LoadFileInMem("Levels\\TownData\\Sector1s.DUN");
    T_FillSector(P3Tiles, pSector, 46, 46, 25, 25);
    MemFreeDbg(pSector);
    pSector = LoadFileInMem("Levels\\TownData\\Sector2s.DUN");
    T_FillSector(P3Tiles, pSector, 46, 0, 25, 23);
    MemFreeDbg(pSector);
    pSector = LoadFileInMem("Levels\\TownData\\Sector3s.DUN");
    T_FillSector(P3Tiles, pSector, 0, 46, 23, 25);
    MemFreeDbg(pSector);
    pSector = LoadFileInMem("Levels\\TownData\\Sector4s.DUN");
    T_FillSector(P3Tiles, pSector, 0, 0, 23, 23);
    MemFreeDbg(pSector);
}

// .text:00449442
// Create a new town (not load!) and set as current dungeon
void CreateTown(int entry)
{
    int x, y;

    dminx = 10;
    dminy = 10;
    dmaxx = 84;
    dmaxy = 84;

    // Set initial player starting point. (_px, _py) are set during InitPlayer
    if (entry == ENTRY_MAIN) // New game
    {
        // lolwut
        if (debug_mode)
        {
            ViewX = 75;
            ViewY = 68;
        }
        else
        {
            ViewX = 75;
            ViewY = 68;
        }
    }
    else // Return from cathedral
    {
        ViewX = 25;
        ViewY = 31;
    }

    T_Pass3();

    for (y = 0; y < MAXDUNY; y++)
    {
        for (x = 0; x < MAXDUNX; x++)
        {
            dLight[x][y] = 0;
            dFlags[x][y] = 0;
            dPlayer[x][y] = 0;
            dMonster[x][y] = 0;
            dObject[x][y] = 0;
            dItem[x][y] = 0;
            dSpecial[x][y] = 0;
            dLight[x][y] = 0;

            if (dPiece[x][y] == 360)
            {
                dSpecial[x][y] = 1;
            }
            if (dPiece[x][y] == 358)
            {
                dSpecial[x][y] = 2;
            }
            if (dPiece[x][y] == 129)
            {
                dSpecial[x][y] = 6;
            }
            if (dPiece[x][y] == 130)
            {
                dSpecial[x][y] = 7;
            }
            if (dPiece[x][y] == 128)
            {
                dSpecial[x][y] = 8;
            }
            if (dPiece[x][y] == 117)
            {
                dSpecial[x][y] = 9;
            }
            if (dPiece[x][y] == 157)
            {
                dSpecial[x][y] = 10;
            }
            if (dPiece[x][y] == 158)
            {
                dSpecial[x][y] = 11;
            }
            if (dPiece[x][y] == 156)
            {
                dSpecial[x][y] = 12;
            }
            if (dPiece[x][y] == 162)
            {
                dSpecial[x][y] = 13;
            }
            if (dPiece[x][y] == 160)
            {
                dSpecial[x][y] = 14;
            }
            if (dPiece[x][y] == 214)
            {
                dSpecial[x][y] = 15;
            }
            if (dPiece[x][y] == 212)
            {
                dSpecial[x][y] = 16;
            }
            if (dPiece[x][y] == 217)
            {
                dSpecial[x][y] = 17;
            }
            if (dPiece[x][y] == 216)
            {
                dSpecial[x][y] = 18;
            }
        }
    }

    SetTownMicros();
}

// .text:00449922
// Parses pData for 8 directions of animation and stores them into pAnim.
void SetTownerGPtrs(BYTE *pData, BYTE **pAnim)
{
    int i;
    BYTE *src;

    for (i = 0; i < 8; i++)
    {
        // Start at top of frame table
        src = pData;
        __asm {
            ; // EBX := src
            mov        eax, src
            mov        ebx, eax
            ; // Turn i into a byte offset
            mov        edx, i
            shl        edx, 2
            ; // Read the frame table for the starting offset of direction `i`
            add        ebx, edx
            ; // Set src to the start of the animation for direction `i`
            mov        edx, [ebx]
            add        eax, edx
            mov        src, eax
        }
        // Store the animation for direction i
        pAnim[i] = src;
    }
}

// .text:0044997F
// Sets current towner animation
void NewTownerAnim(int tnum, BYTE *pAnim, int numFrames, int Delay)
{
    towner[tnum]._tAnimData = pAnim;
    towner[tnum]._tAnimLen = numFrames;
    towner[tnum]._tAnimFrame = 1;
    towner[tnum]._tAnimCnt = 0;
    towner[tnum]._tAnimDelay = Delay;
}

// .text:00449A19
void InitTownerInfo(int i, int w, BOOL sel, BOOL freegfx)
{
    towner[i]._tmode = 0;
    towner[i]._tx = 0;
    towner[i]._ty = 0;
    towner[i]._txoff = 0;
    towner[i]._tyoff = 0;
    towner[i]._txvel = 0;
    towner[i]._tyvel = 0;
    towner[i]._tdir = 0;
    towner[i]._tVar1 = 0;
    towner[i]._tVar2 = 0;
    towner[i]._tVar3 = 0;
    towner[i]._tVar4 = 0;
    towner[i]._tSelFlag = sel;
    towner[i]._tAnimWidth = w;
    towner[i]._tAnimWidth2 = (w - 64) / 2;
    towner[i].freeGfx = freegfx;
}

// .text:00449BD5
// Adds Grisworld to town.
void InitSmith()
{
    InitTownerInfo(TOWN_SMITH, /*w=*/96, /*sel=*/TRUE, /*freegfx=*/TRUE);
    towner[TOWN_SMITH]._tNData = LoadFileInMem("Towners\\Smith\\SmithN.CEL");
    SetTownerGPtrs(towner[TOWN_SMITH]._tNData, towner[TOWN_SMITH]._tNAnim);
    towner[TOWN_SMITH]._tNFrames = 16;
    NewTownerAnim(TOWN_SMITH, towner[TOWN_SMITH]._tNAnim[DIR_SW], towner[TOWN_SMITH]._tNFrames, /*Delay=*/3);
    towner[TOWN_SMITH]._tx = 62;
    towner[TOWN_SMITH]._ty = 63;
    towner[TOWN_SMITH]._tbtcnt = 0;
    strcpy(towner[TOWN_SMITH]._tName, "Blacksmith");
    dMonster[62][63] = TOWN_SMITH + 1; // +1 since that's the convention for "single 'monster' on this tile"
    numtowners++;
}

// .text:00449C78
// Adds Ogden to town.
void InitBarOwner()
{
    InitTownerInfo(TOWN_TAVERN, /*w=*/96, /*sel=*/TRUE, /*freegfx=*/TRUE);
    towner[TOWN_TAVERN]._tNData = LoadFileInMem("Towners\\TwnF\\TwnFN.CEL");
    SetTownerGPtrs(towner[TOWN_TAVERN]._tNData, towner[TOWN_TAVERN]._tNAnim);
    towner[TOWN_TAVERN]._tNFrames = 16;
    NewTownerAnim(TOWN_TAVERN, towner[TOWN_TAVERN]._tNAnim[DIR_SW], towner[TOWN_TAVERN]._tNFrames, /*Delay=*/3);
    towner[TOWN_TAVERN]._tx = 55;
    towner[TOWN_TAVERN]._ty = 62;
    towner[TOWN_TAVERN]._tbtcnt = 0;
    strcpy(towner[TOWN_TAVERN]._tName, "Tavern owner");
    dMonster[55][62] = TOWN_TAVERN + 1;
    numtowners++;
}

// .text:00449D23
// Adds the dead guy outside the cathedral to town.
void InitTownDead()
{
    int i;
    InitTownerInfo(TOWN_DEADGUY, /*w=*/96, /*sel=*/TRUE, /*freegfx=*/TRUE);
    towner[TOWN_DEADGUY]._tNData = LoadFileInMem("Towners\\Butch\\Deadguy.CEL");
    // Dead guy only has one animation so copy it to each direction.
    for (i = 0; i < 8; i++)
    {
        towner[TOWN_DEADGUY]._tNAnim[i] = towner[TOWN_DEADGUY]._tNData;
    }
    towner[TOWN_DEADGUY]._tNFrames = 8;
    // Why DIR_N? They're all the same lol
    NewTownerAnim(TOWN_DEADGUY, towner[TOWN_DEADGUY]._tNAnim[DIR_N], towner[TOWN_DEADGUY]._tNFrames, /*Delay=*/6);
    towner[TOWN_DEADGUY]._tx = 24;
    towner[TOWN_DEADGUY]._ty = 32;
    towner[TOWN_DEADGUY]._tbtcnt = 0;
    strcpy(towner[TOWN_DEADGUY]._tName, "Wounded Townsman");
    dMonster[24][32] = TOWN_DEADGUY + 1;
    numtowners++;
}

// .text:00449DED
// Adds Adria to town
void InitWitch()
{
    int i;
    InitTownerInfo(TOWN_WITCH, /*w=*/96, /*sel=*/TRUE, /*freegfx=*/TRUE);
    towner[TOWN_WITCH]._tNData = LoadFileInMem("Towners\\TownWmn1\\WmnN.CEL");
    // Adria only has one animation so copy it to each direction.
    for (i = 0; i < 8; i++)
    {
        towner[TOWN_WITCH]._tNAnim[i] = towner[TOWN_WITCH]._tNData;
    }
    towner[TOWN_WITCH]._tNFrames = 19;
    NewTownerAnim(TOWN_WITCH, towner[TOWN_WITCH]._tNAnim[DIR_S], towner[TOWN_WITCH]._tNFrames, /*Delay=*/6);
    towner[TOWN_WITCH]._tx = 80;
    towner[TOWN_WITCH]._ty = 19;
    towner[TOWN_WITCH]._tbtcnt = 0;
    strcpy(towner[TOWN_WITCH]._tName, "The Witch");
    dMonster[80][19] = TOWN_WITCH + 1;
    numtowners++;
}

// .text:00449EB7
// Adds Wirt to town.
void InitBoy()
{
    int i;
    InitTownerInfo(TOWN_PEGBOY, /*w=*/96, /*sel=*/TRUE, /*freegfx=*/TRUE);
    towner[TOWN_PEGBOY]._tNData = LoadFileInMem("Towners\\TownBoy\\PegKid1.CEL");
    // Wirt only has one animation so copy it to each direction.
    for (i = 0; i < 8; i++)
    {
        towner[TOWN_PEGBOY]._tNAnim[i] = towner[TOWN_PEGBOY]._tNData;
    }
    towner[TOWN_PEGBOY]._tNFrames = 20;
    NewTownerAnim(TOWN_PEGBOY, towner[TOWN_PEGBOY]._tNAnim[DIR_S], towner[TOWN_PEGBOY]._tNFrames, /*Delay=*/6);
    towner[TOWN_PEGBOY]._tx = 11;
    towner[TOWN_PEGBOY]._ty = 53;
    towner[TOWN_PEGBOY]._tbtcnt = 0;
    strcpy(towner[TOWN_PEGBOY]._tName, "Pegged leg boy");
    dMonster[80][19] = TOWN_PEGBOY + 1;
    numtowners++;
}

// .text:00449F81
// Adds cows to town.
void InitCows()
{
    int i, dir;
    int x, y, xo, yo;

    pCowCels = LoadFileInMem("Towners\\Animals\\Cow.CEL");
    for (i = 0; i < 3; i++)
    {
        x = TownCowX[i];
        y = TownCowY[i];
        dir = TownCowDir[i];
        InitTownerInfo(numtowners, 128, FALSE, FALSE);
        towner[numtowners]._tNData = pCowCels;
        SetTownerGPtrs(towner[numtowners]._tNData, towner[numtowners]._tNAnim);
        towner[numtowners]._tNFrames = 12;
        NewTownerAnim(numtowners, towner[numtowners]._tNAnim[dir], towner[numtowners]._tNFrames, 3);
        towner[numtowners]._tAnimFrame = random_(11) + 1;
        towner[numtowners]._tx = x;
        towner[numtowners]._ty = y;
        towner[numtowners]._tbtcnt = 0;
        towner[numtowners]._tSelFlag = FALSE;

        xo = x + cowoffx[dir];
        yo = y + cowoffy[dir];
        if (dMonster[x][yo] == 0)
            dMonster[x][yo] = -(numtowners + 1);
        if (dMonster[xo][y] == 0)
            dMonster[xo][y] = -(numtowners + 1);
        if (dMonster[xo][yo] == 0)
            dMonster[xo][yo] = -(numtowners + 1);

        numtowners++;
    }
}

// .text:0044A23D
// Adds all towsfolk to town.
void InitTowners()
{
    numtowners = 0;
    InitSmith();
    InitBarOwner();
    InitTownDead();
    InitWitch();
    // Wirt has 1 in 3 chance of spawning (unless you're cheating >:( )
    if (random_(3) == 0 || debug_mode)
    {
        InitBoy();
    }
    InitCows();
}

// .text:0044A294
// Deallocate all graphics loaded for towners
void FreeTownerGFX()
{
    int i;
    for (i = 0; i < numtowners; i++)
    {
        if (towner[i].freeGfx)
        {
            // Ignore cows since their graphics are loaded once and shared 3 times. Otherwise this would triple free...
            MemFreeDbg(towner[i]._tNData);
        }
    }
    MemFreeDbg(pCowCels);
}

// .text:0044A355
// Stops the townfolk from talking when the player walks away.
void TownCtrlMsg(int i)
{
    int p;
    int dx, dy;

    if (towner[i]._tbtcnt != 0)
    {
        p = towner[i]._tVar1;
        dx = abs(towner[i]._tx - plr[p]._px);
        dy = abs(towner[i]._ty - plr[p]._py);
        if (dx >= 2 || dy >= 2)
            towner[i]._tbtcnt = 0;
        if (!towner[i]._tbtcnt)
        {
            qtextflag = FALSE;
            sfx_stop();
        }
    }
}

// .text:0044A475
// Performs Grisworld per-tick logic, like checking for quest completion.
void TownBlackSmith()
{
    int x, y;

    TownCtrlMsg(TOWN_SMITH);
    // Give infravision ring when completing The Magic Rock.
    if (!qtextflag && quests[Q_ROCK]._qactive == QUEST_DONE)
    {
        // Find a suitable location for the ring to drop.
        x = towner[TOWN_SMITH]._tx;
        y = towner[TOWN_SMITH]._ty + 1;
        if (dPlayer[x][y])
        {
            x++;
        }
        // Drop the ring.
        CreateItem(7, x, y); // TODO magic number
        // Complete the quest.
        quests[Q_ROCK]._qactive = QUEST_NOTAVAIL;
    }
}

// .text:0044A4F8
void TownBarOwner()
{
    TownCtrlMsg(TOWN_TAVERN);
}

// .text:0044A512
void TownDead()
{
    TownCtrlMsg(TOWN_DEADGUY);
    if (!qtextflag && quests[Q_BUTCHER]._qactive != QUEST_INIT)
    {
        // "Kills" the dead guy by freezing him on one frame.
        towner[TOWN_DEADGUY]._tAnimDelay = 1000;
        towner[TOWN_DEADGUY]._tAnimFrame = 1;
        strcpy(towner[TOWN_DEADGUY]._tName, "Slain Townsman");
    }
    if (quests[Q_BUTCHER]._qactive != QUEST_INIT)
    {
        // Stay dead please :)
        towner[TOWN_DEADGUY]._tAnimCnt = 0;
    }
}

// .text:0044A58F
// Performs per-tick logic for all townsfolk. This involves checking for quests and playing animations.
void ProcessTowners()
{
    int i;
    for (i = 0; i < numtowners; i++)
    {
        // Only look at townsfolk that can give quests
        switch (i)
        {
        case TOWN_SMITH:
            TownBlackSmith();
            break;
        case TOWN_TAVERN:
            TownBarOwner();
            break;
        case TOWN_DEADGUY:
            TownDead();
            break;
        }

        towner[i]._tAnimCnt++;
        if (towner[i]._tAnimCnt >= towner[i]._tAnimDelay)
        {
            towner[i]._tAnimCnt = 0;
            towner[i]._tAnimFrame++;
            if (towner[i]._tAnimFrame > towner[i]._tAnimLen)
                towner[i]._tAnimFrame = 1;
        }
    }
}

// .text:0044A6D9
// `p` is plr
// `t` is towner
void TalkToTowner(int p, int t)
{
    int dx, dy;
    int i;

    dx = abs(plr[p]._px - towner[t]._tx);
    dy = abs(plr[p]._py - towner[t]._ty);
    // Allow talking to townsfolk from across the screen with cheats.
    if (!debug_mode)
    {
        // Otherwise, the player needs to be standing in the proximity of the townsfolk.
        if (dx >= 2 || dy >= 2)
        {
            return;
        }
    }

    if (t == TOWN_TAVERN)
    {
        // Play voiced lines in the demo!
        if (demo_mode)
        {
            if (!plr[p]._pLvlVisited[1])
            {
                if (towner[TOWN_TAVERN]._tVar4 == 0)
                {
                    // "Thank goodness you've returned! ..."
                    InitQTextMsg(12); // tODO magicn umber
                    PlayTownerSFX(3); // TODO magic number
                    ++towner[TOWN_TAVERN]._tVar4;
                }
                else
                {
                    // "Continue on your quest ..."
                    InitQTextMsg(14); // tODO magicn umber
                    PlayTownerSFX(5); // TODO magic number
                }
            }
            else
            {
                if (towner[TOWN_TAVERN]._tVar4 == 0)
                {
                    // "There is an old woman ..."
                    InitQTextMsg(13); // tODO magicn umber
                    PlayTownerSFX(4); // TODO magic number
                    ++towner[TOWN_TAVERN]._tVar4;
                }
                else
                {
                    // "Continue on your quest ..."
                    InitQTextMsg(14); // tODO magicn umber
                    PlayTownerSFX(5); // TODO magic number
                }
            }
        }
        else
        {
            if (quests[Q_BUTCHER]._qactive != QUEST_DONE)
            {
                towner[TOWN_TAVERN]._tbtcnt = 150; // Arbitrary, could be anything > 0
                towner[TOWN_TAVERN]._tVar1 = p;
                // "Thank goodness you've returned! ..."
                InitQTextMsg(12); // tODO magicn umber
            }
            else
            {
                if (quests[Q_SKELKING]._qactive == QUEST_ACTIVE)
                {
                    quests[Q_SKELKING]._qmsg = 1;      // TODO MAGIC NUMBER
                    towner[TOWN_TAVERN]._tbtcnt = 150; // Arbitrary, could be anything > 0
                    towner[TOWN_TAVERN]._tVar1 = p;
                    // "The Skeleton Kings undead minions still plague this land! ..."
                    InitQTextMsg(1); // TODO MAGIC NUMBER
                }
                if (quests[Q_SKELKING]._qactive == QUEST_DONE)
                {
                    quests[Q_SKELKING]._qmsg = 2;      // TODO MAGIC NUMBER
                    towner[TOWN_TAVERN]._tbtcnt = 150; // Arbitrary, could be anything > 0
                    towner[TOWN_TAVERN]._tVar1 = p;
                    // "You have saved this land from a terrible evil ..."
                    InitQTextMsg(2); // TODO MAGIC NUMBER
                }
                if (quests[Q_SKELKING]._qactive == QUEST_INIT)
                {
                    quests[Q_SKELKING]._qactive = QUEST_ACTIVE;
                    quests[Q_SKELKING]._qmsg = 0;      // TODO MAGIC NUMBER
                    towner[TOWN_TAVERN]._tbtcnt = 150; // Arbitrary, could be anything > 0
                    towner[TOWN_TAVERN]._tVar1 = p;
                    // "I would like to ask your help ..."
                    InitQTextMsg(0); // TODO MAGIC NUMBER
                }
            }
        }
    }
    if (t == TOWN_DEADGUY)
    {
        if (quests[Q_BUTCHER]._qactive == QUEST_ACTIVE)
        {
            towner[TOWN_DEADGUY]._tbtcnt = 150;
            towner[TOWN_DEADGUY]._tVar1 = p;
            // "The man's body appears to be twisted in agony ..."
            InitQTextMsg(4);
        }
        if (quests[Q_BUTCHER]._qactive == QUEST_DONE)
        {
            towner[TOWN_DEADGUY]._tbtcnt = 150;
            towner[TOWN_DEADGUY]._tVar1 = p;
            // "The man appears to be in peace ..."
            InitQTextMsg(5);
        }
        if (quests[Q_BUTCHER]._qactive == QUEST_INIT)
        {
            quests[Q_BUTCHER]._qactive = QUEST_ACTIVE;
            quests[Q_BUTCHER]._qmsg = 3; // TODO magic number
            towner[TOWN_DEADGUY]._tbtcnt = 50;
            towner[TOWN_DEADGUY]._tVar1 = p;
            towner[TOWN_DEADGUY]._tVar2 = 3;
            // "Please help....! I barely escaped from ....... the Butcher...! ..."
            InitQTextMsg(3);
            PlayTownerSFX(2);
        }
    }
    if (t == TOWN_SMITH)
    {
        // The Magic Rock requires beating the skeleton king.
        if (quests[Q_ROCK]._qactive == QUEST_INIT && quests[Q_SKELKING]._qactive == QUEST_DONE)
        {
            quests[Q_ROCK]._qactive = QUEST_ACTIVE;
            quests[Q_ROCK]._qmsg = 6;
            towner[TOWN_DEADGUY]._tbtcnt = 150;
            towner[TOWN_DEADGUY]._tVar1 = p;
            // "I have a story that I think you might be interested in ..."
            InitQTextMsg(6);
        }
        else
        {
            if (quests[Q_ROCK]._qactive == QUEST_ACTIVE)
            {
                // If the player has the rock in their inventory then complete the quest.
                for (i = 0; i < plr[p]._pNumInv; ++i)
                {
                    if (plr[i].InvList[i]._iCurs == 51) // TODO magic number
                    {
                        quests[Q_ROCK]._qactive = QUEST_DONE;
                        RemoveInvItem(p, i);
                        i = plr[p]._pNumInv;
                    }
                }
            }
            if (quests[Q_ROCK]._qactive == QUEST_DONE)
            {
                quests[Q_ROCK]._qmsg = 8; // The player never sees this???
                towner[TOWN_SMITH]._tbtcnt = 150;
                towner[TOWN_SMITH]._tVar1 = p;
                InitQTextMsg(8);
            }
        }
        if (!qtextflag)
        {
            StartStore(1);
            PlayTownerSFX(0);
        }
    }
    if (t == TOWN_WITCH)
    {
        StartStore(5);    // TODO magic number
        PlayTownerSFX(1); // TODO magic number
    }
    if (t == TOWN_PEGBOY)
    {
        StartStore(12);   // TODO magic number
        PlayTownerSFX(6); // TODO magic number
    }
}
