#ifndef __DRLG_L2_H__
#define __DRLG_L2_H__

#include <windows.h>

//
// functions
//

void CreateL2Dungeon(DWORD rseed, int entry);
void LoadL2Dungeon(const char *sFileName, int vx, int vy);
void LoadPreL2Dungeon(const char *sFileName, int vx, int vy);

#endif
