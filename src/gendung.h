// Common dungeon generation functions to all algorithms.
//
// Nomenclature:
//
// - mini tile: space that an entity can occupy (player, monster, etc). Each level contains 80x80. It's made up of 10/16 micro tiles
// - micro tile: a 32x32 pixel piece of a mini tile. A mini tile is divided into a 2x5 or 2x8 grid of microtiles which are drawn bottom up.
// - mega tile: a 2x2 colletion of mini tiles. Forms a logical unit, e.g. a wall corner.
//
// The drawing routine cares about micro tiles. Player interaction cares about mini tiles. Dungeon generation cares about mega tiles. 

#ifndef __GENDUNG_H__
#define __GENDUNG_H__

#include <windows.h>

//
// defines
//

// Number of unique tiles
#define MAXTILES 2048

// Size of level in each direction in mini tiles.
// I think `MAXDUNX = MINITILE_PADDING_X + DMAXX * 2 + MINITILE_PADDING_X`.
// MINITILE_PADDING_X describes empty, unused mini tiles on all sides (maybe to make the drawing routines happy?).
// DMAXX * 2 takes care of the fact that a mega tile is made of 2x2 mini tiles.
// The same goes for MAXDUNY.
#define MAXDUNX 112
#define MAXDUNY 112

// While there are 112x112 mini tiles allocation, there is 16 mini tile padding on all sides.
// This effectively means the player interacts with only 80x80 mini tile area.
#define MINITILE_PADDING_X 16
#define MINITILE_PADDING_Y 16

// Size of level in each direction in mega tiles (2x2 tiles).
// This is what the DRLG mainly interfaces with.
#define DMAXX 40
#define DMAXY 40

//
// enums
//

// TODO: Copied from Devilution, verify
enum dflag
{
    BFLAG_MISSILE = 0x01,
    BFLAG_VISIBLE = 0x02,
    BFLAG_DEAD_PLAYER = 0x04,
    // This space is occupied by a setpiece (e.g. Butcher Chamber)
    BFLAG_POPULATED = 0x08,
    BFLAG_MONSTLR = 0x10,
    BFLAG_PLAYERLR = 0x20,
    BFLAG_LIT = 0x40,
    BFLAG_EXPLORED = 0x80,
};

enum dungeon_type
{
    DTYPE_TOWN = 0x0,
    DTYPE_OLD_CATHEDRAL = 0x1,
    DTYPE_CATACOMBS = 0x2,
    DTYPE_CAVES = 0x3,
    DTYPE_HELL = 0x4,
    DTYPE_CATHEDRAL = 0x5
};


// TODO so this is straight from Deviltion but it's misspelled...
// should be drlg_flags and DRLG_*
// TODO should this be the same as MAPFLAG?
enum dlrg_flag
{
    // This megatile represents a door
    DLRG_HDOOR = 0x01,
    // This megatile represents a door
    DLRG_VDOOR = 0x02,
    DLRG_CHAMBER = 0x40,
    // This megatile already has something special generated on it that shouldn't be overridden, e.g. a door.
    DLRG_PROTECTED = 0x80,
};

//
// structs
//

// A tile is made up of 16 smaller CEL pieces (defined by the .MIN file).
// (For reference, the player occupies the space of a tile.)
// Tiles are then grouped into 2x2 to form megatiles (defined by the .TIL file).
// Only the first 10 can be used in the dungeon. All 16 can be used in town.
struct MICROS
{
    WORD mt[16];
};

struct ScrollStruct
{
    int _sxoff;
    int _syoff;
    int _sdx;
    int _sdy;
    int _sdir; // enum _scroll_direction
};

struct ShadowStruct
{
    unsigned char strig;
    unsigned char s1;
    unsigned char s2;
    unsigned char s3;
    unsigned char nv1;
    unsigned char nv2;
    unsigned char nv3;
};

//
// variables
//

extern BYTE dungeon[DMAXX][DMAXY];
extern BYTE pdungeon[DMAXX][DMAXY];
extern char dObject[MAXDUNX][MAXDUNY];
extern int dPiece[MAXDUNX][MAXDUNY];
extern char dFlags[MAXDUNX][MAXDUNY];
extern int dMonster[MAXDUNX][MAXDUNY];
extern char dSpecial[MAXDUNX][MAXDUNY];
extern char dPlayer[MAXDUNX][MAXDUNY];
extern char dMissile[MAXDUNX][MAXDUNY];
extern char dLight[MAXDUNX][MAXDUNY];
extern char dDead[MAXDUNX][MAXDUNY];
extern char dItem[MAXDUNX][MAXDUNY];
extern char dTransVal[MAXDUNX][MAXDUNY];
extern char dPreLight[MAXDUNX][MAXDUNY];
extern BOOL nSolidTable[MAXTILES + 1];
extern BOOL nTrapTable[MAXTILES + 1];
extern BYTE *pMegaTiles;
extern int currlevel;
extern int leveltype;
extern MICROS dpiece_defs_map_1[MAXDUNX * MAXDUNY];
extern MICROS dpiece_defs_map_2[MAXDUNX][MAXDUNY];
extern int setpc_x;
extern int setpc_y;
extern int setpc_w;
extern int setpc_h;
extern int setlvlnum;
extern BOOL setlevel;
extern BYTE *pDungeonCels;
extern BYTE *pMegaTiles;
extern BYTE *pLevelPieces;
extern BYTE *pSpecialCels;
extern BYTE *pSpeedCels;
extern int ViewX;
extern int ViewY;
extern int MouseOffX; // This value is used but never initalized :X
extern int MouseOffY; // This value is used but never initalized :X
extern ScrollStruct ScrollInfo;
extern int portal_leveltype;
extern BOOL portal_setlevel;
extern int portal_currlevel_or_setlvlnum;
extern int dmaxx;
extern int dmaxy;
extern int dminx;
extern int dminy;
extern int ViewBX;
extern int ViewBY;
extern int ViewDX;
extern int ViewDY;
extern int LvlViewX;
extern int LvlViewY;
extern char TransVal;
extern BOOL TransList[256];
extern BOOL nBlockTable[MAXTILES + 1];
extern BOOL nTransTable[MAXTILES + 1];
extern BYTE *pDungeonCels;

//
// functions
//

int IsometricCoord(int x, int y);
void DRLG_InitTrans();
void DRLG_ListTrans(int num, BYTE *List);
void DRLG_AreaTrans(int num, BYTE *List);
void DRLG_RectTrans(int x1, int y1, int x2, int y2);
void DRLG_SetPC();
void DRLG_CopyTrans(int sx, int sy, int dx, int dy);
void DRLG_MRectTrans(int, int, int, int);

#endif