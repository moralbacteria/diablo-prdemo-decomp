FROM ubuntu:22.04 AS builder

# Install dependencies for unzipping installing
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        unrar \
        p7zip-full \
    && rm -rf /var/lib/apt/lists/*

# Get the files from host
COPY [ "GAMESDK.rar", "Microsoft Visual C++ 4.2 Professional (ISO).7z", "/root/" ]

WORKDIR /root

# I can't figure out how to run SETUP.EXE in headless mode so just use the extracted folder
RUN unrar x GAMESDK.rar \
    && mv GAMESDK/SDK /GAMESDK

# I can't figure out how to mount the ISO so just extract it and run setup locally
# (Nor can I figure out how to run MSDEV/SETUP.EXE in headless mode)
RUN 7z x "Microsoft Visual C++ 4.2 Professional (ISO).7z" \
    && 7z x "Microsoft Visual C++ 4.2 Professional (ISO)/VCPP-4.20.iso" \
    && mv MSDEV /

FROM ubuntu:22.04

# Install build and package dependencies
RUN dpkg --add-architecture i386 \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
        wine \
        wine32 \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /MSDEV /MSDEV
COPY --from=builder /GAMESDK /GAMESDK

# Tell Wine where the compiler is
ENV WINEPATH=/MSDEV/BIN
# Tell LINK where to look for libs (there is no /LIBPATH!)
# https://learn.microsoft.com/en-us/cpp/build/reference/linking?view=msvc-170
ENV LIB=/GAMESDK/LIB;/MSDEV/LIB
# Tell CL where to find headers (easier than /I)
# https://learn.microsoft.com/en-us/cpp/build/reference/cl-environment-variables?view=msvc-170
ENV INCLUDE=/GAMESDK/INC;/MSDEV/INCLUDE