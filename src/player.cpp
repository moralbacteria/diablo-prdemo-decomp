#include "player.h"

#include <stdio.h>

#include "control.h"
#include "effects.h"
#include "engine.h"
#include "gendung.h"
#include "inv.h"
#include "monster.h"
#include "multi.h"
#include "objects.h"
#include "town.h"

//
// Initialized Variables (.data:004B9690)
//

// TODO ...
char ArmourChar[] = {
    'L', // light (early game gear)
    'M', // medium (mid game gear, e.g. catacombs+)
    'H', // heavy (late game gear, e.g. late caves or hell)
};
const char WepChar[] = {
    'N', // No weapon
    'U', // Unarmed (only shield)
    'S', // Sword (no shield)
    'D', // sworD with shielD
    'B', // Bow
    'A', // Axe
    'M', // Mace (no shield)
    'H', // mace with shield
    'T'  // sTaff
};
// Base animation data per class per animation type
// Might be overridden by SetPlrAnims
char PlrGFXAnimLens[3][11] = {
    // Warrior
    {
        10, // Stand (N)
        16, // Attack (A)
        8,  // Walk (W)
        2,  // Block (B)
        15, // Death (D) (NOTE: Devilution is 20)
        20, // Spell (fire/lightning/magic) (S)
        6,  // Got hit (H)
        20, // Stand in town (N)
        8,  // Walk in town (W)
        9,  // AFNum (frame of animation to deal attack damage)
        14  // SFNum (frame of animatin to cast spell)
    },
    // Rogue
    {
        8,  // N
        18, // A
        6,  // W (NOTE: Devilution is 8. See ParseCmd for faster walk speed)
        4,  // B
        20, // D
        16, // S
        7,  // H
        20, // town N
        8,  // town W
        10, // AFNum
        12  // SFNum
    },
    // Sorcerer (copy of Warrior)
    {10, 16, 8, 2, 15, 20, 6, 20, 8, 9, 14},
};
// Baes class strength
int StrengthTbl[] = {
    30, // Warrior
    25, // Rogue (NOTE: Devilution is 20)
    15, // Sorcerer
};
// Base class magic
int MagicTbl[] = {
    15, // Warrior (NOTE: Devilution is 10)
    20, // Rogue
    30, // Sorcerer (NOTE: Devilution is 35)
};
// Base class dexterity
int DexterityTbl[] = {
    20, // Warrior
    30, // Rogue
    25, // Sorcerer (Note: Devilution is 15)
};
// Base class vitality
int VitalityTbl[] = {
    30, // Warrior (NOTE: Devilution is 25)
    20, // Rogue
    20, // Sorcerer
};
// Base class to-hit (not in Devilution!)
int ToHitTbl[] = {
    60, // Warrior
    50, // Rogue
    40, // Sorcerer
};
// Base chance to block
int ToBlkTbl[] = {
    30, // Warrior
    20, // Rogue
    10, // Sorcerer
};
const char *ClassStrTbl[] = {
    "Warrior",
    "Rogue",
    "Sorceror", // Misspelling of sorcerer
};
// Maximum trainable stats per class
int MaxStats[3][4] = {
    // Warrior
    {
        100, // Strength (NOTE: Devilution is 250)
        40,  // Magic (NOTE: Devilution is 50)
        65,  // Dexterity (NOTE: Devilution is 60)
        100, // Vitality
    },
    // Rogue
    {
        65,  // Strength (NOTE: Devilution is 55)
        60,  // Magic (NOTE: Devilution is 70)
        100, // Dexterity (NOTE: Devilution is 250)
        80,  // Vitality
    },
    // Sorcerer
    {
        40,  // Strength (NOTE: Devilution is 45)
        100, // Magic (NOTE: Devilution is 250)
        85,  // Dexterity
        80,  // Vitality
    },
};
// Experience required to go from clvl i to i+1
int ExpLvlsTbl[] = {
    0,
    540, // 1 -> 2
    1512,
    3213,
    6104,
    10875,
    18509,
    30341,
    48090,
    73826,
    111143, // 10 -> 11
    165253,
    243713,
    357481,
    522444,
    761641,
    1108476,
    1611388,
    2340610,
    3397982,
    4931172, // 20 -> 21
    7154298,
    10377831,
    15051954,
    21829432,
    31656775,
    45906423,
    66568413,
    96528299,
    139970134,
    202960795, // 30 -> 31
    // Max clvl is 31
};

//
// uninitialized vars (.data:)
//

DWORD dword_605390;
// ...
int gbActivePlayers;
// ...
DWORD dword_605474; // UNUSED; set to 0, never read
PlayerStruct plr[MAX_PLRS];
int myplr;
BOOL deathflag;
int deathdelay;
// ...
DWORD dword_615F14; // UNUSED; set to 0, never read
DWORD dword_615F18; // UNUSED; set to 0, never read
DWORD dword_615F1C; // UNUSED; set to 0, never read
DWORD dword_615F20; // UNUSED; set to 0, never read
DWORD dword_615F24; // UNUSED; set to 0, never read
DWORD dword_615F28; // UNUSED; set to 0, never read
DWORD dword_615F2C; // UNUSED; set to 0, never read
DWORD dword_615F30; // UNUSED; set to 0, never read
DWORD dword_615F34; // UNUSED; set to 0, never read
DWORD dword_615F38; // UNUSED; set to 0, never read

//
// code
//

// .text:000464BB0
// Initialize the pointers for all directions of an animation given a CEL with groups.
// For example:
//
//    SetPlayerGPtrs(plr[pnum]._pNData, plr[pnum]._pNAnim);
//
// @param pData[in] CEL data with 8 groups
// @param pAnim[out] 8 element array to store CEL data
static void SetPlayerGPtrs(BYTE *pData, BYTE **pAnim)
{
    BYTE *pDirData;
    int i;

    // There are 8 directions, starting with S moving clockwise
    for (i = 0; i < 8; i++)
    {
        pDirData = pData;                // Start with group table
        pDirData += ((DWORD *)pData)[i]; // Use the group table to find the CEL for that direction
        pAnim[i] = pDirData;             // Remember where the CEL is for that direction. This is now the start of the frame table.
    }
}

// .text:00464C0D
// Call SetPlayerGPtrs for all relevant animations
// @param pnum Index to plr
void SetPlayerAllGPtrs(int pnum)
{
    if (leveltype == DTYPE_TOWN)
    {
        SetPlayerGPtrs(plr[pnum]._pNData, plr[pnum]._pNAnim);
        SetPlayerGPtrs(plr[pnum]._pWData, plr[pnum]._pWAnim);
    }
    else
    {
        SetPlayerGPtrs(plr[pnum]._pNData, plr[pnum]._pNAnim);
        SetPlayerGPtrs(plr[pnum]._pWData, plr[pnum]._pWAnim);
        SetPlayerGPtrs(plr[pnum]._pAData, plr[pnum]._pAAnim);
        SetPlayerGPtrs(plr[pnum]._pHData, plr[pnum]._pHAnim);
        SetPlayerGPtrs(plr[pnum]._pLData, plr[pnum]._pLAnim);
        SetPlayerGPtrs(plr[pnum]._pFData, plr[pnum]._pFAnim);
        SetPlayerGPtrs(plr[pnum]._pTData, plr[pnum]._pTAnim);
        SetPlayerGPtrs(plr[pnum]._pDData, plr[pnum]._pDAnim);
        if (plr[pnum]._pBlockFlag)
        {
            SetPlayerGPtrs(plr[pnum]._pBData, plr[pnum]._pBAnim);
        }
    }
}

// .void:00464EE0
// Load graphics into the appropriate _p*Data and _p*Anim
// @param pnum Index into plr
// @param gfxflag One of PFILE_*
void LoadPlrGFX(int pnum, int gfxflag)
{
    char cClass;
    char pszName[256];
    char cWeapon;
    char prefix[16];
    char szClass[16];
    char cArmour;

    switch (plr[pnum]._pClass)
    {
    case PC_WARRIOR:
        strcpy(szClass, "Warrior");
        cClass = 'W';
        break;
    case PC_ROGUE:
        strcpy(szClass, "Rogue");
        cClass = 'R';
        break;
    case PC_SORCERER:
        strcpy(szClass, "Warrior");
        cClass = 'W';
        break;
    }

    cArmour = ArmourChar[plr[pnum]._pgfxnum >> 4];
    cWeapon = WepChar[plr[pnum]._pgfxnum & 0xF];
    sprintf(prefix, "%c%c%c", cClass, cArmour, cWeapon);

    if (leveltype == DTYPE_TOWN)
    {
        if (gfxflag == PFILE_STAND)
        {
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sST.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pNData);
            SetPlayerGPtrs(plr[pnum]._pNData, plr[pnum]._pNAnim);
        }
        else
        {
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sWL.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pWData);
            SetPlayerGPtrs(plr[pnum]._pWData, plr[pnum]._pWAnim);
        }
    }
    else
    {
        switch (gfxflag)
        {
        case PFILE_STAND:
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sAS.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pNData);
            SetPlayerGPtrs(plr[pnum]._pNData, plr[pnum]._pNAnim);
            break;
        case PFILE_WALK:
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sAW.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pWData);
            SetPlayerGPtrs(plr[pnum]._pWData, plr[pnum]._pWAnim);
            break;
        case PFILE_ATTACK:
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sAT.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pAData);
            SetPlayerGPtrs(plr[pnum]._pAData, plr[pnum]._pAAnim);
            break;
        case PFILE_HIT:
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sHT.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pHData);
            SetPlayerGPtrs(plr[pnum]._pHData, plr[pnum]._pHAnim);
            break;
        case PFILE_LIGHTNING:
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sLM.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pLData);
            SetPlayerGPtrs(plr[pnum]._pLData, plr[pnum]._pLAnim);
            break;
        case PFILE_FIRE:
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sFM.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pFData);
            SetPlayerGPtrs(plr[pnum]._pFData, plr[pnum]._pFAnim);
            break;
        case PFILE_MAGIC:
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sQM.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pTData);
            SetPlayerGPtrs(plr[pnum]._pTData, plr[pnum]._pTAnim);
            break;
        case PFILE_DEATH:
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sDT.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pDData);
            SetPlayerGPtrs(plr[pnum]._pDData, plr[pnum]._pDAnim);
            break;
        case PFILE_BLOCK:
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sBL.CEL", szClass, prefix, prefix);
            LoadFileWithMem(pszName, plr[pnum]._pBData);
            SetPlayerGPtrs(plr[pnum]._pBData, plr[pnum]._pBAnim);
            break;
        }
    }

    plr[pnum]._pGFXLoad |= gfxflag;
}

// .text:00465887
// Load required player animation CELs and store in plr[pnum]._p*Data.
// Called on level load.
// Lots of overlap with LoadPlrGFX
void InitPlayerGFX(int pnum)
{
    char cClass;
    char cs[16];
    char cArmour;
    char cWeapon;
    char pszName[256];
    char Type[16];

    switch (plr[pnum]._pClass)
    {
    case PC_WARRIOR:
        strcpy(cs, "Warrior");
        cClass = 'W';
        break;
    case PC_ROGUE:
        strcpy(cs, "Rogue");
        cClass = 'R';
        break;
    case PC_SORCERER:
        strcpy(cs, "Warrior");
        cClass = 'W';
        break;
    }

    cArmour = ArmourChar[plr[pnum]._pgfxnum >> 4];
    cWeapon = WepChar[plr[pnum]._pgfxnum & 0xF];
    sprintf(Type, "%c%c%c", cClass, cArmour, cWeapon);

    if (leveltype == DTYPE_TOWN)
    {
        // Town only uses stand and walk animation
        sprintf(pszName, "PlrGFX\\%s\\%s\\%sST.CEL", cs, Type, Type);
        LoadFileWithMem(pszName, plr[pnum]._pNData);
        sprintf(pszName, "PlrGFX\\%s\\%s\\%sWL.CEL", cs, Type, Type);
        LoadFileWithMem(pszName, plr[pnum]._pWData);
    }
    else
    {
        sprintf(pszName, "PlrGFX\\%s\\%s\\%sAS.CEL", cs, Type, Type);
        LoadFileWithMem(pszName, plr[pnum]._pNData);
        sprintf(pszName, "PlrGFX\\%s\\%s\\%sAW.CEL", cs, Type, Type);
        LoadFileWithMem(pszName, plr[pnum]._pWData);
        sprintf(pszName, "PlrGFX\\%s\\%s\\%sAT.CEL", cs, Type, Type);
        LoadFileWithMem(pszName, plr[pnum]._pAData);
        sprintf(pszName, "PlrGFX\\%s\\%s\\%sHT.CEL", cs, Type, Type);
        LoadFileWithMem(pszName, plr[pnum]._pHData);
        sprintf(pszName, "PlrGFX\\%s\\%s\\%sLM.CEL", cs, Type, Type);
        LoadFileWithMem(pszName, plr[pnum]._pLData);
        sprintf(pszName, "PlrGFX\\%s\\%s\\%sFM.CEL", cs, Type, Type);
        LoadFileWithMem(pszName, plr[pnum]._pFData);
        sprintf(pszName, "PlrGFX\\%s\\%s\\%sQM.CEL", cs, Type, Type);
        LoadFileWithMem(pszName, plr[pnum]._pTData);
        sprintf(pszName, "PlrGFX\\%s\\%s\\%sDT.CEL", cs, Type, Type);
        LoadFileWithMem(pszName, plr[pnum]._pDData);

        if (plr[pnum]._pBlockFlag)
        {
            sprintf(pszName, "PlrGFX\\%s\\%s\\%sBL.CEL", cs, Type, Type);
            LoadFileWithMem(pszName, plr[pnum]._pBData);
        }
    }

    SetPlayerAllGPtrs(pnum);
    plr[pnum]._pGFXLoad = PFILE_ALL;
}

// .text:00465DC2
// Allocate memory for _p*Data
void InitPlrGFXMem(int pnum)
{
    // BUG! Some of these buffer sizes are too small for the actual graphics.
    plr[pnum]._pNData = (BYTE *)(DiabloAllocPtr(0x45800)); // TODO magic number
    plr[pnum]._pWData = (BYTE *)(DiabloAllocPtr(0x1D000)); // TODO magic number
    plr[pnum]._pAData = (BYTE *)(DiabloAllocPtr(0x57000)); // TODO magic number
    plr[pnum]._pHData = (BYTE *)(DiabloAllocPtr(0x16C00)); // TODO magic number
    plr[pnum]._pLData = (BYTE *)(DiabloAllocPtr(0x5A000)); // TODO magic number
    plr[pnum]._pFData = (BYTE *)(DiabloAllocPtr(0x78800)); // TODO magic number
    plr[pnum]._pTData = (BYTE *)(DiabloAllocPtr(0x91800)); // TODO magic number
    plr[pnum]._pDData = (BYTE *)(DiabloAllocPtr(0x41C00)); // TODO magic number
    plr[pnum]._pBData = (BYTE *)(DiabloAllocPtr(0xB400));  // TODO magic number
}

// .text:00465F7F
// Free all _p*Data
void FreePlayerGFX(int pnum)
{
    if (leveltype == DTYPE_TOWN)
    {
        MemFreeDbg(plr[pnum]._pNData);
        MemFreeDbg(plr[pnum]._pWData);
    }
    else
    {
        MemFreeDbg(plr[pnum]._pNData);
        MemFreeDbg(plr[pnum]._pWData);
        MemFreeDbg(plr[pnum]._pAData);
        MemFreeDbg(plr[pnum]._pHData);
        MemFreeDbg(plr[pnum]._pLData);
        MemFreeDbg(plr[pnum]._pFData);
        MemFreeDbg(plr[pnum]._pTData);
        MemFreeDbg(plr[pnum]._pDData);
        MemFreeDbg(plr[pnum]._pBData);
    }
}

// .text:0046632D
// Get an animation ready to be played
void NewPlrAnim(int pnum, BYTE *Peq, int numFrames, int Delay, int width)
{
    plr[pnum]._pAnimData = Peq;
    plr[pnum]._pAnimLen = numFrames;
    plr[pnum]._pAnimFrame = 1;
    plr[pnum]._pAnimCnt = 0;
    plr[pnum]._pAnimDelay = Delay;
    plr[pnum]._pAnimWidth = width;
    plr[pnum]._pAnimWidth2 = (width - 64) >> 1;
}

// .text:00466421
// Set _pVar* to 0
void ClearPlrVars(int pnum)
{
    plr[pnum]._pVar1 = 0;
    plr[pnum]._pVar2 = 0;
    plr[pnum]._pVar3 = 0;
    plr[pnum]._pVar4 = 0;
    plr[pnum]._pVar5 = 0;
    plr[pnum]._pVar6 = 0;
    plr[pnum]._pVar7 = 0;
    plr[pnum]._pVar8 = 0;
}

// .text:0046652F
void SetPlrAnims(int pnum)
{
    // Player class (one of PC_*)
    int pc;
    // Lower 4 bits of _pgfxnum, one of ANIM_ID_*
    int gn;

    // Load animation widths, used across weapons and armor types
    plr[pnum]._pNWidth = 96;
    plr[pnum]._pWWidth = 96;
    plr[pnum]._pAWidth = 128;
    plr[pnum]._pHWidth = 96;
    plr[pnum]._pSWidth = 96;
    plr[pnum]._pDWidth = 128;
    plr[pnum]._pBWidth = 96;

    // Load default animation lengths based on PlrGFXAnimLens
    pc = plr[pnum]._pClass;

    if (leveltype == DTYPE_TOWN)
    {
        plr[pnum]._pNFrames = PlrGFXAnimLens[pc][7];
        plr[pnum]._pWFrames = PlrGFXAnimLens[pc][8];
    }
    else
    {
        plr[pnum]._pNFrames = PlrGFXAnimLens[pc][0];
        plr[pnum]._pWFrames = PlrGFXAnimLens[pc][2];
        plr[pnum]._pAFrames = PlrGFXAnimLens[pc][1];
        plr[pnum]._pHFrames = PlrGFXAnimLens[pc][6];
        plr[pnum]._pSFrames = PlrGFXAnimLens[pc][5];
        plr[pnum]._pDFrames = PlrGFXAnimLens[pc][4];
        plr[pnum]._pBFrames = PlrGFXAnimLens[pc][3];
        plr[pnum]._pAFNum = PlrGFXAnimLens[pc][9];
    }
    plr[pnum]._pSFNum = PlrGFXAnimLens[pc][10];

    // Override animation lengths for specific class/weapon/armor combos
    gn = plr[pnum]._pgfxnum & 0xF;

    if (pc == PC_WARRIOR || pc == PC_SORCERER)
    {
        if (plr[pnum]._pgfxnum == ANIM_ID_LIGHT_ARMOR | ANIM_ID_SWORD_SHIELD)
        {
            // wlddt should have 21 frames
            plr[pnum]._pDFrames = 21;
            // (wmddt and whddt should have 15 frames)
        }
        if (gn == ANIM_ID_UNARMED)
        {
            // wlndt and whndt should have 20 frames
            plr[pnum]._pDFrames = 20;
            if (plr[pnum]._pgfxnum == ANIM_ID_MEDIUM_ARMOR | ANIM_ID_UNARMED)
            {
                // wmndt should have 15 frames
                plr[pnum]._pDFrames = 15;
            }
        }
        if (gn == ANIM_ID_UNARMED_SHIELD)
        {
            // wludt and wmudt should have 20 frames
            plr[pnum]._pDFrames = 20;
            if (plr[pnum]._pgfxnum == ANIM_ID_HEAVY_ARMOR | ANIM_ID_UNARMED_SHIELD)
            {
                // whudt should have 15 frames
                plr[pnum]._pDFrames = 15;
            }
        }
        if (gn == ANIM_ID_SWORD)
        {
            // wlsdt and wmsdt should have 21 frames
            plr[pnum]._pDFrames = 21;
            if (plr[pnum]._pgfxnum == ANIM_ID_HEAVY_ARMOR | ANIM_ID_SWORD)
            {
                // whsdt should have 15 frames
                plr[pnum]._pDFrames = 15;
            }
        }
        if (gn == ANIM_ID_BOW)
        {
            if (leveltype != DTYPE_TOWN)
            {
                // wlbas, wmbas, and whbas should have 8 frames
                plr[pnum]._pNFrames = 8;
            }
            // wlbat, wmbat, and whbat are 96 pixels wide
            plr[pnum]._pAWidth = 96;
            // Shoot an arrow on frame 12 of wlbat, wmbat, and whbat
            plr[pnum]._pAFNum = 12;

            // NOTE: wlbdt has 21 frames but _pDFrames is never set here
        }
        if (gn == ANIM_ID_AXE)
        {
            // wlaat, wmaat, and whaat should have 20 frames
            plr[pnum]._pAFrames = 20;
            // Register hit on frame 10 of wlaat, wmaat, and whaat
            plr[pnum]._pAFNum = 10;
        }
        if (gn == ANIM_ID_MACE)
        {
            // wlmdt, wmmdt, and whmdt should have 16 frames
            plr[pnum]._pDFrames = 16;
        }
        if (gn == ANIM_ID_MACE_SHIELD && plr[pnum]._pgfxnum != (ANIM_ID_HEAVY_ARMOR | ANIM_ID_MACE_SHIELD))
        {
            // wlhdt and wmhdt should have 16 frames
            plr[pnum]._pDFrames = 16;
        }
        if (gn == ANIM_ID_STAFF)
        {
            // wltat, wmtad, and whtat should have 16 frames
            plr[pnum]._pAFrames = 16;
            plr[pnum]._pAFNum = 11;
            // wltdt, wmtdt, and whtdt should have 20 frames
            plr[pnum]._pDFrames = 20;
        }
    }

    if (pc == PC_ROGUE)
    {
        if (gn == ANIM_ID_AXE)
        {
            plr[pnum]._pAFrames = 22;
            plr[pnum]._pAFNum = 13;
        }
        else if (gn == ANIM_ID_BOW)
        {
            plr[pnum]._pAFrames = 12;
            plr[pnum]._pAFNum = 7;
            plr[pnum]._pDFrames = 21;
        }
        else if (gn == ANIM_ID_STAFF)
        {
            plr[pnum]._pAFrames = 16;
            plr[pnum]._pAFNum = 11;
        }
    }
}

// .text:00466C74
// Initialize a player with the chosen class. Sets default stats, etc.
// `pnum` is an index into `plr`. `c` is one of `enum plr_class`.
// Player still has to enter a name.
void CreatePlayer(int pnum, char c)
{
    int i;
    char val;

    plr[pnum]._pClass = c;

    val = StrengthTbl[c];
    if (val < 0)
    {
        val = 0;
    }
    plr[pnum]._pStrength = val;
    plr[pnum]._pBaseStr = val;

    val = MagicTbl[c];
    if (val < 0)
    {
        val = 0;
    }
    plr[pnum]._pMagic = val;
    plr[pnum]._pBaseMag = val;

    val = DexterityTbl[c];
    if (val < 0)
    {
        val = 0;
    }
    plr[pnum]._pDexterity = val;
    plr[pnum]._pBaseDex = val;

    val = VitalityTbl[c];
    if (val < 0)
    {
        val = 0;
    }
    plr[pnum]._pVitality = val;
    plr[pnum]._pBaseVit = val;

    plr[pnum]._pStatPts = 0;
    plr[pnum]._pBaseToHit = ToHitTbl[c];
    plr[pnum]._pDamageMod = plr[pnum]._pStrength * plr[pnum]._pLevel / 100;
    plr[pnum]._pBaseToBlk = ToBlkTbl[c];

    plr[pnum]._pHitPoints = plr[pnum]._pVitality << 6;
    if (plr[pnum]._pClass == PC_WARRIOR)
    {
        plr[pnum]._pHitPoints <<= 1;
    }
    if (plr[pnum]._pClass == PC_ROGUE)
    {
        plr[pnum]._pHitPoints += plr[pnum]._pHitPoints >> 1;
    }

    plr[pnum]._pMaxHP = plr[pnum]._pHitPoints;
    plr[pnum]._pHPBase = plr[pnum]._pHitPoints;
    plr[pnum]._pMaxHPBase = plr[pnum]._pHitPoints;

    plr[pnum]._pMana = plr[pnum]._pMagic << 6;
    if (plr[pnum]._pClass == PC_SORCERER)
    {
        plr[pnum]._pMana <<= 1;
    }
    if (plr[pnum]._pClass == PC_ROGUE)
    {
        plr[pnum]._pMana += plr[pnum]._pMana >> 1;
    }

    plr[pnum]._pMaxMana = plr[pnum]._pMana;
    plr[pnum]._pManaBase = plr[pnum]._pMana;
    plr[pnum]._pMaxManaBase = plr[pnum]._pMana;

    plr[pnum]._pLevel = 1;
    plr[pnum]._pMaxLvl = plr[pnum]._pLevel;
    plr[pnum]._pExperience = 0;
    plr[pnum]._pMaxExp = plr[pnum]._pExperience;
    plr[pnum]._pNextExper = ExpLvlsTbl[1];
    plr[pnum]._pArmorClass = 0;
    plr[pnum]._pMagResist = 0;
    plr[pnum]._pFireResist = 0;
    plr[pnum]._pLghtResist = 0;
    plr[pnum]._pLightRad = 10;
    plr[pnum]._pInfraFlag = FALSE;

    if (c == PC_WARRIOR)
    {
        plr[pnum]._pAblSpells = SPELLBIT(SPL_REPAIR);
    }
    if (c == PC_ROGUE)
    {
        plr[pnum]._pAblSpells = SPELLBIT(SPL_DISARM) | SPELLBIT(SPL_IDENTIFY);
    }
    if (c == PC_SORCERER)
    {
        plr[pnum]._pAblSpells = SPELLBIT(SPL_RECHARGE);
    }

    if (c == PC_SORCERER)
    {
        plr[pnum]._pMemSpells = SPELLBIT(SPL_FIREBOLT);
    }
    else
    {
        plr[pnum]._pMemSpells = 0;
    }

    for (i = 0; i < 32; i++) // TODO magic number
    {
        plr[pnum]._pSplLvl[i] = 0;
    }

    if (plr[pnum]._pClass == PC_SORCERER)
    {
        plr[pnum]._pSplLvl[SPL_FIREBOLT] = 3; // TODO magic number
    }

    for (i = 0; i < 3; i++) // TODO magic number
    {
        plr[pnum]._pSplHotKey[i] = -1;
    }

    if (c == PC_WARRIOR)
    {
        plr[pnum]._pgfxnum = ANIM_ID_SWORD_SHIELD;
    }
    if (c == PC_ROGUE)
    {
        plr[pnum]._pgfxnum = ANIM_ID_BOW;
    }
    if (c == PC_SORCERER)
    {
        plr[pnum]._pgfxnum = ANIM_ID_STAFF;
    }

    for (i = 0; i < NUMLEVELS; i++)
    {
        plr[pnum]._pLvlVisited[i] = FALSE;
    }

    for (i = 0; i < 10; i++) // TODO magic number
    {
        plr[pnum]._pSLvlVisited[i] = FALSE;
    }

    plr[pnum]._WarpActive = 0;

    CreatePlrItems(pnum);
}

// __dc_FillHealthAndMana	0000000000467601
// NextPlrLevel	0000000000467683

// AddPlrExperience	0000000000467988
void AddPlrExperience(int pnum, int lvl, int exp)
{
    // TODO
}

// ClearPlrRVars	0000000000467B02

// .text:00467C48
void InitPlayer(int pnum, BOOL FirstTime)
{
    // TODO
}

// InitMultiView	000000000046841D

// .text:0046845E
void CheckEFlag(int pnum, BOOL flag)
{
    int i;
    MICROS *pieces;
    int bitflags;
    int x;
    int y;

    x = plr[pnum]._px - 1;
    y = plr[pnum]._py + 1;
    bitflags = 0;
    pieces = &dpiece_defs_map_2[x][y];

    // i < 10 beacuse there are 10 microtiles per tile in dungeon?
    // i = 2 to ignore the first two floor microtiles?
    for (i = 2; i < 10; i++)
    {
        bitflags |= pieces->mt[i];
    }

    if (bitflags | dSpecial[x][y] | nSolidTable[dPiece[x][y]])
    {
        plr[pnum]._peflag = 1;
    }
    else
    {
        plr[pnum]._peflag = 0;
    }

    if (flag == TRUE && plr[pnum]._peflag == 1)
    {
        x = plr[pnum]._px;
        y = plr[pnum]._py + 2;
        bitflags = 0;
        pieces = &dpiece_defs_map_2[x][y];

        for (i = 2; i < 10; i++)
        {
            bitflags |= pieces->mt[i];
        }

        if (bitflags | dSpecial[x][y] == 0)
        {
            x = plr[pnum]._px - 2;
            y = plr[pnum]._py + 1;
            bitflags = 0;
            pieces = &dpiece_defs_map_2[x][y];

            for (i = 2; i < 10; i++)
            {
                bitflags |= pieces->mt[i];
            }

            if (bitflags | dSpecial[x][y])
            {
                plr[pnum]._peflag = 2;
            }
        }
    }
}

// PlayerGetDirection	0000000000468739

// .text:004688DC
BOOL PlrDirOK(int pnum, int dir)
{
    int px;
    int py;
    BOOL isOk;

    isOk = TRUE;
    px = plr[pnum]._px + offset_x[dir];
    py = plr[pnum]._py + offset_y[dir];

    // TODO

    return isOk;
}

// PlrClrTrans	0000000000468A82
// PlrDoTrans	0000000000468B06

// .text:00468BF1
void StartStand(int pnum, int dir)
{
    if (!plr[pnum]._pInvincible && plr[pnum]._pHitPoints == 0)
    {
        StartPlayerKill(pnum);
        return;
    }

    if (!(plr[pnum]._pGFXLoad & PFILE_STAND))
    {
        LoadPlrGFX(pnum, PFILE_STAND);
    }
    NewPlrAnim(pnum, plr[pnum]._pNAnim[dir], plr[pnum]._pNFrames, 3, plr[pnum]._pNWidth);

    plr[pnum]._pmode = PM_STAND;
    plr[pnum]._pfutx = plr[pnum]._px;
    plr[pnum]._pfuty = plr[pnum]._py;
    plr[pnum]._pxoff = 0;
    plr[pnum]._pyoff = 0;
    CheckEFlag(pnum, FALSE);
    plr[pnum]._pdir = dir;
    if (pnum == myplr)
    {
        ScrollInfo._sxoff = 0;
        ScrollInfo._syoff = 0;
        ScrollInfo._sdir = SDIR_NONE;
    }
}

// PM_ChangeLightOff	0000000000468E08
// PM_ChangeOffset	0000000000468F49

// .text:0046911D
void StartWalk(int pnum, int xvel, int yvel, int xadd, int yadd, int EndDir, int sdir)
{
    int px, py;

    if (plr[pnum]._pInvincible && plr[pnum]._pHitPoints == 0)
    {
        StartPlayerKill(pnum);
        return;
    }

    px = xadd + plr[pnum]._px;
    py = yadd + plr[pnum]._py;

    if (PlrDirOK(pnum, EndDir))
    {
        plr[pnum]._pfutx = px;
        plr[pnum]._pfuty = py;

        if (pnum == myplr)
        {
            ScrollInfo._sdx = plr[pnum]._px - ViewX;
            ScrollInfo._sdy = plr[pnum]._py - ViewY;
        }

        dPlayer[px][py] = -1 - pnum;
        plr[pnum]._pmode = PM_WALK;
        plr[pnum]._pxvel = xvel;
        plr[pnum]._pyvel = yvel;
        plr[pnum]._pxoff = 0;
        plr[pnum]._pyoff = 0;
        plr[pnum]._pVar1 = xadd;
        plr[pnum]._pVar2 = yadd;
        plr[pnum]._pVar3 = EndDir;

        if (!(plr[pnum]._pGFXLoad & PFILE_WALK))
        {
            LoadPlrGFX(pnum, PFILE_WALK);
        }

        NewPlrAnim(pnum, plr[pnum]._pWAnim[EndDir], plr[pnum]._pWFrames, 0, plr[pnum]._pWWidth);

        plr[pnum]._pdir = EndDir;
        plr[pnum]._pVar6 = 0;
        plr[pnum]._pVar7 = 0;
        plr[pnum]._pVar8 = 0;

        CheckEFlag(pnum, FALSE);

        if (pnum == myplr)
        {
            // TODO
        }
    }
    // TODO
}

// StartWalk2	0000000000469548
void StartWalk2(int pnum, int xvel, int yvel, int xoff, int yoff, int xadd, int yadd, int EndDir, int sdir)
{
    // TODO
}

// StartWalk3	0000000000469AD2
void StartWalk3(int pnum, int xvel, int yvel, int xoff, int yoff, int xadd, int yadd, int mapx, int mapy, int EndDir, int sdir)
{
    // TODO
}

// StartAttack	000000000046A03C
void StartAttack(int pnum, int d)
{
    // TODO
}

// StartRangeAttack	000000000046A253
void StartRangeAttack(int pnum, int d, int cx, int cy)
{
    // TODO
}

// StartPlrBlock	000000000046A4A9
void StartPlrBlock(int pnum, int dir)
{
    // TODO
}

// StartSpell	000000000046A6CA
void StartSpell(int pnum, int d, int spl, int cx, int cy)
{
    // TODO
}

// .text:0046AAD5
void RemovePlrFromMap(int pnum)
{
    int x, y;

    x = plr[pnum]._px;
    y = plr[pnum]._py;

    dPlayer[x + 1][y - 1] = 0;
    dPlayer[x][y - 1] = 0;
    dPlayer[x - 1][y - 1] = 0;

    dPlayer[x + 1][y] = 0;
    dPlayer[x][y] = 0;
    dPlayer[x - 1][y] = 0;

    dPlayer[x + 1][y + 1] = 0;
    dPlayer[x][y + 1] = 0;
    dPlayer[x - 1][y + 1] = 0;

    dFlags[x + 1][y] &= ~BFLAG_PLAYERLR;
    dFlags[x][y + 1] &= ~BFLAG_PLAYERLR;
}

// StartPlrHit	000000000046AC51
void StartPlrHit(int pnum)
{
    // TODO
}

// .text:0046AF2C
void StartPlayerKill(int pnum)
{
    PlayRndSFX(IS_DEAD);

    if (!(plr[pnum]._pGFXLoad & PFILE_DEATH))
    {
        LoadPlrGFX(pnum, PFILE_DEATH);
    }

    NewPlrAnim(pnum, plr[pnum]._pDAnim[plr[pnum]._pdir], plr[pnum]._pDFrames, 1, plr[pnum]._pDWidth);

    // Some of this was moved in Devilution to FixPlayerLocation
    plr[pnum]._pmode = PM_DEATH;
    plr[pnum]._pfutx = plr[pnum]._px;
    plr[pnum]._pfuty = plr[pnum]._py;
    plr[pnum]._pxoff = 0;
    plr[pnum]._pyoff = 0;
    CheckEFlag(pnum, FALSE);
    plr[pnum]._pInvincible = TRUE;
    plr[pnum]._pVar8 = 1;
    if (pnum == myplr)
    {
        ScrollInfo._sxoff = 0;
        ScrollInfo._syoff = 0;
        ScrollInfo._sdir = SDIR_NONE;
        ViewX = plr[pnum]._px;
        ViewY = plr[pnum]._py;
    }

    RemovePlrFromMap(pnum);
    dPlayer[plr[pnum]._px][plr[pnum]._py] = pnum + 1;
    drawhpflag = TRUE;
    deathdelay = 30;
}

// StartNewLvl	000000000046B1F5
void StartNewLvl(int pnum, int fom, int lvl)
{
    // TODO
}

// StartWarpLvl	000000000046B451
void StartWarpLvl(int pnum)
{
    // TODO
}

// StartWarpLvl_2	000000000046B91D
void StartWarpLvl_2(int pnum)
{
    // TODO
}

// StartNewGame	000000000046BBC6
void StartNewGame(int pnum)
{
    // TODO
}

// PM_DoStand	000000000046BDE9
// PM_DoWalk	000000000046BE01
// PM_DoWalk2	000000000046C1C6
// PM_DoWalk3	000000000046C4D3
// WeaponDur	000000000046C929
// PlrHitMonst	000000000046CCD4
// PlrHitObj	000000000046D48D
// PM_DoAttack	000000000046D541
// PM_DoRangeAttack	000000000046D858
// ShieldDur	000000000046DA04
// PM_DoBlock	000000000046DB94
// PM_DoSpell	000000000046DC2A
// ArmorDur	000000000046DEE5
// PM_DoGotHit	000000000046E12C
// PM_DoDeath	000000000046E1E2
// PM_DoNewLvl	000000000046E319

// ParseCmd	000000000046E331
void ParseCmd(int pnum)
{
    int i;

    if (plr[pnum].recv_ready)
    {
        // This is the code that causes Rogue to walk faster.
        // Note that it's not a class check...
        // The Rogue is the only class that happens to have 6 (instead of 8) walk frames.
        // If the Warrior happened to have 6 walk frames then he'd be faster too!
        switch (plr[pnum].recv_action)
        {
        case ACTION_STAND:
            StartStand(pnum, plr[pnum].recv_v1);
            break;
        case ACTION_WALK_N:
            if (plr[pnum]._pmode != PM_STAND)
            {
                return;
            }
            if (plr[pnum]._pWFrames == 6)
            {
                StartWalk(pnum, 0, -1362, -1, -1, DIR_N, SDIR_N);
            }
            else
            {
                StartWalk(pnum, 0, -1024, -1, -1, DIR_N, SDIR_N);
            }
            break;
        case ACTION_WALK_NE:
            if (plr[pnum]._pmode != PM_STAND)
            {
                return;
            }
            if (plr[pnum]._pWFrames == 6)
            {
                StartWalk(pnum, 1364, -682, 0, -1, DIR_NE, SDIR_NE);
            }
            else
            {
                StartWalk(pnum, 1024, -512, 0, -1, DIR_NE, SDIR_NE);
            }
            break;
        case ACTION_WALK_E:
            if (plr[pnum]._pmode != PM_STAND)
            {
                return;
            }
            if (plr[pnum]._pWFrames == 6)
            {
                StartWalk3(pnum, 2730, 0, -32, -16, 1, -1, 1, 0, DIR_E, SDIR_E);
            }
            else
            {
                StartWalk3(pnum, 2048, 0, -32, -16, 1, -1, 1, 0, DIR_E, SDIR_E);
            }
            break;
        case ACTION_WALK_SE:
            if (plr[pnum]._pmode != PM_STAND)
            {
                return;
            }
            if (plr[pnum]._pWFrames == 6)
            {
                StartWalk2(pnum, 1364, 682, -32, -16, 1, 0, DIR_SE, SDIR_SE);
            }
            else
            {
                StartWalk2(pnum, 1024, 512, -32, -16, 1, 0, DIR_SE, SDIR_SE);
            }
            break;
        case ACTION_WALK_S:
            if (plr[pnum]._pmode != PM_STAND)
            {
                return;
            }
            if (plr[pnum]._pWFrames == 6)
            {
                StartWalk2(pnum, 0, 1364, 0, -32, 1, 1, DIR_S, SDIR_S);
            }
            else
            {
                StartWalk2(pnum, 0, 1024, 0, -32, 1, 1, DIR_S, SDIR_S);
            }
            break;
        case ACTION_WALK_SW:
            if (plr[pnum]._pmode != PM_STAND)
            {
                return;
            }
            if (plr[pnum]._pWFrames == 6)
            {
                StartWalk2(pnum, -1364, 682, 32, -16, 0, 1, DIR_SW, SDIR_SW);
            }
            else
            {
                StartWalk2(pnum, -1024, 512, 32, -16, 0, 1, DIR_SW, SDIR_SW);
            }
            break;
        case ACTION_WALK_W:
            if (plr[pnum]._pmode != PM_STAND)
            {
                return;
            }
            if (plr[pnum]._pWFrames == 6)
            {
                StartWalk3(pnum, -2730, 0, 32, -16, -1, 1, 0, 1, DIR_W, SDIR_W);
            }
            else
            {
                StartWalk3(pnum, -2048, 0, 32, -16, -1, 1, 0, 1, DIR_W, SDIR_W);
            }
            break;
        case ACTION_WALK_NW:
            if (plr[pnum]._pmode != PM_STAND)
            {
                return;
            }
            if (plr[pnum]._pWFrames == 6)
            {
                StartWalk(pnum, -1364, -682, -1, 0, DIR_NW, SDIR_NW);
            }
            else
            {
                StartWalk(pnum, -1024, -512, -1, 0, DIR_NW, SDIR_NW);
            }
            break;
        case ACTION_ATTACK:
            StartAttack(pnum, plr[pnum].recv_v1);
            break;
        case ACTION_RATTACK:
            StartRangeAttack(pnum, plr[pnum].recv_v1, plr[pnum].recv_v2, plr[pnum].recv_v3);
            break;
        case ACTION_BLOCK:
            StartPlrBlock(pnum, plr[pnum].recv_v1);
            break;
        case ACTION_SPELL:
            StartSpell(pnum, plr[pnum].recv_v1, plr[pnum]._pRSpell, plr[pnum].recv_v2, plr[pnum].recv_v3);
            break;
        case ACTION_OPERATE:
            i = plr[pnum].recv_v2;
            if (object[i]._oBreak == TRUE)
            {
                StartAttack(pnum, plr[pnum].recv_v1);
            }
            else
            {
                OperateObject(pnum, plr[pnum].recv_v2);
            }
            break;
        case ACTION_NEWLVL:
            StartNewLvl(pnum, plr[pnum].recv_v2, plr[pnum].recv_v3);
            break;
        case ACTION_AGETITEM:
            AutoGetItem(pnum, plr[pnum].recv_v2);
            break;
        case ACTION_PUTITEM:
            InvPutItem(pnum, plr[pnum].recv_v1, plr[pnum].recv_v2, plr[pnum].recv_v3);
            break;
        case ACTION_NEWGAME:
            StartNewGame(pnum);
            break;
        case ACTION_TALK:
            TalkToTowner(pnum, plr[pnum].recv_v2);
            break;
        case ACTION_USEITEM:
            UseInvItem(plr[pnum].recv_v2);
            break;
        case ACTION_WARP1:
            StartWarpLvl(pnum);
            break;
        case ACTION_WARP2:
            portal_setlevel = plr[pnum].recv_v1;
            portal_leveltype = plr[pnum].recv_v2;
            portal_currlevel_or_setlvlnum = plr[pnum].recv_v3;
            StartWarpLvl_2(pnum);
            break;
        case ACTION_UNUSED:
            // Nothing :)
            break;
        case ACTION_SEED:
            sprintf(tempstr, "Level seed = %i", plr[myplr]._pSeedTbl[currlevel]);
            break;
        }

        plr[pnum]._nextdir = plr[pnum]._pdir;
        plr[pnum].recv_ready = FALSE;
        if (pnum == myplr)
        {
            plr[pnum].to_send_ready = FALSE;
        }
    }
}

// .text:0046ED1E
// TODO: When are these actually read and reacted to (or sent over the network)?
void NetSendCmd(int pnum, char action, int v1, int v2, int v3)
{
    plr[pnum].to_send_action = action;
    plr[pnum].to_send_v1 = v1;
    plr[pnum].to_send_v2 = v2;
    plr[pnum].to_send_v3 = v3;
}

// .text:0046EDD0
void Player_SerializeNextAction()
{
    int i;

    i = plr[myplr].send_buffer_idx;
    plr[myplr].recv_buffer_idx = i;

    plr[myplr].mp_buffer[i][1] = plr[myplr].to_send_message_id;
    plr[myplr].mp_buffer[i][2] = myplr;
    plr[myplr].mp_buffer[i][3] = currlevel;
    plr[myplr].mp_buffer[i][4] = byte_4A3AB4;
    if (plr[myplr].to_send_ready)
    {
        plr[myplr].mp_buffer[i][0] = 11; // message size
        plr[myplr].mp_buffer[i][5] = plr[myplr].to_send_action;
        plr[myplr].mp_buffer[i][6] = plr[myplr].to_send_v1;
        plr[myplr].mp_buffer[i][7] = plr[myplr].to_send_v2 >> 8;
        plr[myplr].mp_buffer[i][8] = plr[myplr].to_send_v2 & 0xFF;
        plr[myplr].mp_buffer[i][9] = plr[myplr].to_send_v3 >> 8;
        plr[myplr].mp_buffer[i][10] = plr[myplr].to_send_v3 & 0xFF;
    }
    else
    {
        plr[myplr].mp_buffer[i][0] = 6;    // message size
        plr[myplr].mp_buffer[i][5] = 0xFF; // TODO magic nuber
    }

    plr[myplr].send_buffer_idx = (plr[myplr].send_buffer_idx + 1) % 4;
}

// Player_DeserializeNextAction	000000000046F14A
void Player_DeserializeNextAction()
{
    // TODO
}
// player_action_dequeue	000000000046F425
// PlrDeathModeOK	000000000046FF2B

// .text:0046FFC7
void ProcessPlayers()
{
    int pnum;

    // Play a delayed sound effect.
    // Only ever "The spirits of the dead are now avenged". Both from killing Butcher and Leoric.
    if (sfxdelay > 0)
    {
        sfxdelay--;
        if (sfxdelay == 0)
        {
            PlayRndSFX(sfxdnum);
        }
    }

    // player_action_dequeue();

    // for (pnum = 0; pnum < gbActivePlayers; pnum++)
    // {
    //     if (!PlrDeathModeOK(pnum) && FP_FLOOR(plr[pnum]._pHitPoints) <= FP_FLOOR(0))
    //     {
    //         plr[pnum]._pHitPoints = 0;
    //         if (gbActivePlayers == 1)
    //         {
    //             StartPlrKill(pnum);
    //         }
    //     }

    //     tplayer = 0;
    //     if (plr[p]._pIFlags &)
    // }

    // TODO
}
// PosOkPlayer	0000000000470372
// MakePlrPath	0000000000470514
// player_4708D4	00000000004708D4

// .text:00470C20
// TODO what is param?
void PlayerAppendPathCommand(int action, int param)
{
    int i;

    // Find an empty slot. If full, override the last action.
    for (i = 0; i < MAX_PATH_LENGTH - 1; ++i)
    {
        if (!plr[myplr].walkpath[i])
        {
            break;
        }
    }

    // Insert the action.
    plr[myplr].walkpath[i] = action;
    plr[myplr].walkPathRelated = param;
}

// CheckPlrSpell	0000000000470CC8
void CheckPlrSpell()
{
    // TODO
}
// SyncPlrAnim	0000000000470F44

// SyncInitPlr	000000000047130D
void SyncInitPlr(int pnum)
{
    // TODO
}

// ModifyPlrStr	0000000000471414
void ModifyPlrStr(int p, int l)
{
    // TODO
}

// ModifyPlrMag	00000000004714CC
void ModifyPlrMag(int p, int l)
{
    // TODO
}

// ModifyPlrDex	00000000004715CD
void ModifyPlrDex(int p, int l)
{
    // TODO
}

// ModifyPlrVit	000000000047162A
void ModifyPlrVit(int p, int l)
{
    // TODO
}
