#ifndef __AUTOMAP_H__
#define __AUTOMAP_H__

#include <windows.h>

//
// variables
//

extern BOOL automapflag;

//
// functions
//

void InitAutomapOnce();
void InitAutomap();
void StartAutomap();
void AutomapUp();
void AutomapDown();
void AutomapLeft();
void AutomapRight();
void AutomapZoomIn();
void AutomapZoomOut();
void AutomapZoomReset();

#endif
