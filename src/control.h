#ifndef __CONTROL_H__
#define __CONTROL_H__

#include <windows.h>

//
// variables
//

extern BYTE fontframe[128];
extern BYTE fontkern[128];
extern char infostr[64];
extern char tempstr[64];
extern char infoclr;
extern BOOL drawhpflag;
extern BOOL drawmanaflag;
extern char infoclr;
extern BOOL chrflag;
extern BOOL drawpotboxflag;
extern BOOL pinfoflag;
extern BOOL spselflag;
extern BOOL lvlbtndown;
extern BOOL panbtndown;
extern BOOL chrbtnactive;
extern BOOL durflag;
extern BOOL drawinfoflag;
extern BOOL drawbtnflag;

//
// functions
//

void AddPanelString(const char *str, BOOL just);
void ClearPanel();
void DoSpeedBook();
void SetSpeedSpell(int);
void ToggleSpell(int);
void SetSpell();
void control_do_update_potbox();
void CheckChrBtns();
void CheckLvlBtn();
void control_check_btn_press();
void CheckBtnUp();
void ReleaseChrBtns();
void ReleaseLvlBtn();
void FreeControlPan();
void DrawCtrlPan();
void PrintInfo();
void UpdateLifeFlask();
void UpdateManaFlask();
void DrawCtrlBtns();

#endif