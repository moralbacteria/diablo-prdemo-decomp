#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <windows.h>

#include "items.h"
#include "spells.h"

//
// defines
//

#define MAX_PLRS 4
#define NUMLEVELS 17
#define PLR_NUM_MP_BUFFERS 4
#define MAX_PATH_LENGTH 25

//
// enums
//

enum inv_body_loc
{
    INVLOC_HEAD,
    INVLOC_CHEST,
    INVLOC_RING_LEFT,
    INVLOC_RING_RIGHT,
    INVLOC_HAND_LEFT,
    INVLOC_HAND_RIGHT,
    NUM_INVLOC,
};

enum plr_class
{
    PC_WARRIOR = 0x0,
    PC_ROGUE = 0x1,
    PC_SORCERER = 0x2,
};

// TODO this was just copied from Devilution, double-check the values!
enum PLR_MODE
{
    PM_STAND = 0,
    PM_WALK = 1,
    PM_WALK2 = 2,
    PM_WALK3 = 3,
    PM_ATTACK = 4,
    PM_RATTACK = 5,
    PM_BLOCK = 6,
    PM_GOTHIT = 7,
    PM_DEATH = 8,
    PM_SPELL = 9,
    PM_NEWLVL = 10,
    PM_QUIT = 11,
};

enum action_id
{
    ACTION_STAND = 0,
    ACTION_WALK_NE = 1,
    ACTION_WALK_NW = 2,
    ACTION_WALK_SE = 3,
    ACTION_WALK_SW = 4,
    ACTION_WALK_N = 5,
    ACTION_WALK_E = 6,
    ACTION_WALK_S = 7,
    ACTION_WALK_W = 8,
    ACTION_ATTACK = 9,
    ACTION_RATTACK = 10,
    ACTION_BLOCK = 11,
    ACTION_SPELL = 12,
    ACTION_NEWLVL = 13,
    ACTION_AGETITEM = 14,
    ACTION_PUTITEM = 15,
    ACTION_NEWGAME = 16,
    ACTION_OPERATE = 17,
    ACTION_TALK = 18,
    ACTION_USEITEM = 19,
    ACTION_WARP1 = 20,
    ACTION_WARP2 = 21,
    ACTION_UNUSED = 22, // TODO used by MI_Town
    ACTION_SEED = 100,
};

// TODO: Should this be here? or in scrollrt.h?
enum _scroll_direction
{
    SDIR_NONE = 0x0,
    SDIR_N = 0x1,
    SDIR_NE = 0x2,
    SDIR_E = 0x3,
    SDIR_SE = 0x4,
    SDIR_S = 0x5,
    SDIR_SW = 0x6,
    SDIR_W = 0x7,
    SDIR_NW = 0x8,
};

// TODO: This is copied from Devilution, verify that it's correct here!
enum player_graphic
{
    PFILE_STAND = 1 << 0,
    PFILE_WALK = 1 << 1,
    PFILE_ATTACK = 1 << 2,
    PFILE_HIT = 1 << 3,
    PFILE_LIGHTNING = 1 << 4,
    PFILE_FIRE = 1 << 5,
    PFILE_MAGIC = 1 << 6,
    PFILE_DEATH = 1 << 7,
    PFILE_BLOCK = 1 << 8,
    // everything except PFILE_DEATH
    // 0b1_0111_1111
    PFILE_NONDEATH = 0x17F,
    // PFILE_STAND | PFILE_WALK | ...
    PFILE_ALL = 0x1FF
};

// The lower 4 bits of _pgfxnum
enum anim_weapon_id
{
    ANIM_ID_UNARMED = 0x00,        // **N
    ANIM_ID_UNARMED_SHIELD = 0x01, // **U
    ANIM_ID_SWORD = 0x02,          // **S
    ANIM_ID_SWORD_SHIELD = 0x03,   // **D
    ANIM_ID_BOW = 0x04,            // **B
    ANIM_ID_AXE = 0x05,            // **A
    ANIM_ID_MACE = 0x06,           // **M
    ANIM_ID_MACE_SHIELD = 0x07,    // **H
    ANIM_ID_STAFF = 0x08,          // **T
};

// The upper 4 bits of _pgfxnum
enum anim_armor_id
{
    ANIM_ID_LIGHT_ARMOR = 0x00,  // *L*
    ANIM_ID_MEDIUM_ARMOR = 0x10, // *M*
    ANIM_ID_HEAVY_ARMOR = 0x20   // *H*
};

enum player_weapon_type
{
    WT_MELEE = 0,
    WT_RANGED = 1,
};

//
// structs
//

struct PlayerStruct
{
    int _pmode;
    BOOL plractive;
    char to_send_message_id;
    // padding
    int anonymous_1;
    char anonymous_2; // this is used a lot in multiplayer communication
                      // observed values: 0, 1, 2, 3
                      // is this the pnum of the multiplayer host?
    // padding
    // TODO is to_send_ready to to_send_v3 a struct? it could be reused for the recv stuff
    BOOL to_send_ready;
    char to_send_action;
    // padding
    int to_send_v1; // byte?
    int to_send_v2; // DWORD?
    int to_send_v3; // DWORD?
    BOOL recv_ready;
    char recv_action;
    // padding
    int recv_v1;
    int recv_v2;
    int recv_v3;
    int recv_buffer_idx;                     // round robin, 0 to PLR_NUM_MP_BUFFERS
    int send_buffer_idx;                     // round robin, 0 to PLR_NUM_MP_BUFFERS
    char mp_buffer[PLR_NUM_MP_BUFFERS][256]; // circular buffer??
    char walkpath[MAX_PATH_LENGTH];
    char anonymous_6[3]; // padding?
    int walkPathRelated; // TODO what is this
    int _px;
    int _py;
    int _pfutx;
    int _pfuty;
    int _pxoff;
    int _pyoff;
    int _pxvel;
    int _pyvel;
    int _pdir;
    int _nextdir;
    int _pgfxnum;     // Weapon and armor of currently playing animation
                      // The first 8 bits are the weapon (see WepChar)
                      // the next 2 are the armor (See ArmourChar)
    BYTE *_pAnimData; // Currently playing animation
    int _pAnimDelay;  // Number of ticks before advancing to a new frame
    int _pAnimCnt;    // Progress into Delay
    int _pAnimLen;    // How many total frames are in the animation
    int _pAnimFrame;  // The current frame of animation
    int _pAnimWidth;  // Pixel width of frame data
    int _pAnimWidth2;
    int _peflag;
    int _plid; // The player casts light, this is the light object associated with that.
    int _pvid; // How far the player can see (and which monsters can see you!)
    int _pRSpell;
    char _pRSplType;
    char _pSplLvl[MAX_SPELLS]; // There's 35 bytes here, wondering if the difference is due to padding/packing
    // padding? 6 bytes
    int _pMemSpells;
    int _pAblSpells;
    int _pScrlSpells;
    int _pSplHotKey[3];
    char anonymous_16;
    char anonymous_17[3];
    int _pInvincible;
    int _pwtype;      // enum player_weapon_type (either melee or ranged)
    BOOL _pBlockFlag; // if TRUE then the player is capable of blocking (i.e. wearing a shield)
    char _pLightRad;
    BYTE gap509[61];
    char anonymous_19;
    char _pName[32];
    BYTE gap567[30];
    char _pClass;
    char anonymous_20[2]; // padding
    int _pStrength;
    int _pBaseStr;
    int _pMagic;
    int _pBaseMag;
    int _pDexterity;
    int _pBaseDex;
    int _pVitality;
    int _pBaseVit;
    int _pStatPts;
    int _pBaseToHit; // Not in Devilution!
    int _pDamageMod;
    int _pBaseToBlk;
    int _pHPBase;
    int _pMaxHPBase;
    int _pHitPoints;
    int _pMaxHP;
    int _pHPPer;
    int _pManaBase;
    int _pMaxManaBase;
    int _pMana;
    int _pMaxMana;
    int _pManaPer;
    char _pLevel;
    char _pMaxLvl;
    char anonymous_24[2]; // padding
    int _pExperience;
    int _pMaxExp;
    int _pNextExper;
    char _pArmorClass;
    char _pMagResist;
    char _pFireResist;
    char _pLghtResist;
    int _pGold;
    BOOL _pInfraFlag;
    int _pVar1;
    int _pVar2;
    int _pVar3;
    int _pVar4;
    int _pVar5;
    int _pVar6;
    int _pVar7;
    int _pVar8;
    int _pSeedTbl[17];      // In Devilution this is stored outside the player!
    int _pLevelTypeTbl[17]; // In Devilution this is stored outside the player!
    BOOL _pLvlVisited[17];
    BOOL _pSLvlVisited[17];
    int _pGFXLoad; // Bitmask of which graphics are loaded (see PFILE_*)
    BYTE *_pNAnim[8];
    int _pNFrames;
    int _pNWidth;
    BYTE *_pWAnim[8];
    int _pWFrames;
    int _pWWidth;
    BYTE *_pAAnim[8];
    int _pAFrames;
    int _pAWidth;
    int _pAFNum;
    BYTE *_pLAnim[8];
    BYTE *_pFAnim[8];
    BYTE *_pTAnim[8];
    int _pSFrames;
    int _pSWidth;
    int _pSFNum;
    BYTE *_pHAnim[8];
    int _pHFrames;
    int _pHWidth;
    BYTE *_pDAnim[8];
    int _pDFrames;
    int _pDWidth;
    BYTE *_pBAnim[8];
    int _pBFrames;
    int _pBWidth;
    ItemStruct InvBody[6]; // This is never treated like an array, the elements are always accessed by specific index
                           // Makes me wonder: was it an array?
    int potbox_item;
    ItemStruct InvList[40];
    int _pNumInv;
    char InvGrid[40];
    ItemStruct HoldItem;
    int _pIMinDam;
    int _pIMaxDam;
    int _pIAC;
    int _pIBonusDam;
    int _pIBonusToHit;
    int _pIBonusAC;
    int _pIBonusDamMod;
    int _pISpells;
    int _pIFlags; // Based on Item _iFlags, one of `enum item_special_effect`
    int _pIGetHit;
    // "All spell levels +X" bonus
    char _pISplLvlAdd;
    // "Spell cost -X%". Demo/beta only! Between 10% (Monk's) and 50% (Cardinal's)!
    // While this is present in Devilution (and is part of the spell cost formula), it is never set.
    char _pISplCost;
    char _pISplDur;
    char anonymous_48; // padding?
    int _WarpActive;
    int _WarpLevel;
    int _WarpLvlType;
    int _WarpSet;
    int _WarpX;
    int _WarpY;
    BYTE *_pNData; // stand
    BYTE *_pWData; // walk
    BYTE *_pAData; // attack
    BYTE *_pLData; // lightning magic
    BYTE *_pFData; // fire magic
    BYTE *_pTData; // other magic
    BYTE *_pHData; // get hit
    BYTE *_pDData; // death
    BYTE *_pBData; // block
};

//
// variables
//

extern int gbActivePlayers;
extern PlayerStruct plr[MAX_PLRS];
extern int myplr;
extern DWORD dword_605474;
extern DWORD dword_615F14;
extern DWORD dword_615F18;
extern DWORD dword_615F1C;
extern DWORD dword_615F20;
extern DWORD dword_615F24;
extern DWORD dword_615F28;
extern DWORD dword_615F2C;
extern DWORD dword_615F30;
extern DWORD dword_615F34;
extern DWORD dword_615F38;
extern DWORD dword_605390;
extern BOOL deathflag;

//
// functions
//

void LoadPlrGFX(int pnum, int gfxflag);
void SetPlrAnims(int pnum);
void AddPlrExperience(int pnum, int lvl, int exp);
void StartPlrHit(int pnum);
void StartPlayerKill(int pnum);
void ModifyPlrStr(int p, int l);
void ModifyPlrMag(int p, int l);
void ModifyPlrDex(int p, int l);
void ModifyPlrVit(int p, int l);
void NetSendCmd(int pnum, char action, int v1, int v2, int v3);
void SyncInitPlr(int pnum);
void CheckPlrSpell();
void FreePlayerGFX(int);
void Player_SerializeNextAction();
void Player_DeserializeNextAction();
void ProcessPlayers();
void CreatePlayer(int pnum, char c);
void InitPlrGFXMem(int pnum);
void PlayerAppendPathCommand(int action, int param);

#endif