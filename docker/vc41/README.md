# Visual C++ 4.1

> NOTE: This docker container is not used and only kept for historical purposes

This creates a docker container with:

- Visual C++ 4.1 available in `/MSDEV`
- DirectX 1.0 available in `/GAMESDK`
- Wine, to run Visual C++ 4.1

## Setup

Copy [Visual C++ 4.1] and [DirectX 1.0] into this directory.

[Visual C++ 4.1]: https://winworldpc.com/download/45e280a1-4d7b-18c3-9a11-c3a4e284a2ef
[DirectX 1.0]: https://archive.org/details/gamesdk

## Build

	docker build -t registry.gitlab.com/moralbacteria/diablo-prdemo-decomp/build_vc41 .

## Push

	docker login registry.gitlab.com
	docker push registry.gitlab.com/moralbacteria/diablo-prdemo-decomp/build_vc41

This image is hosted on this repo: https://gitlab.com/moralbacteria/diablo-prdemo-decomp/container_registry

For help logging into the GitLab registry: https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry