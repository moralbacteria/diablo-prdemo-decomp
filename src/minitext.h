#ifndef __MINITEXT_H__
#define __MINITEXT_H__

#include <windows.h>

//
// variables
//

extern BOOL qtextflag;

//
// functions
//

void InitQuestText();
void InitQTextMsg(int m);
void FreeQuestText();
void DrawQTextBack();

#endif