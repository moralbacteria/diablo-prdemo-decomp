#include "drlg_l4.h"

#include "diablo.h"
#include "engine.h"
#include "gendung.h"
#include "lighting.h"

//
// initialized variables (.data:004AC390)
//

/**
 * A lookup table for the 16 possible patterns of a 2x2 area,
 * where each cell either contains a SW wall or it doesn't.
 */
const BYTE L4ConvTbl[16] = {12, 3, 1, 3, 2, 3, 3, 3, 8, 3, 1, 3, 2, 3, 6, 3};

/** Miniset: Stairs up. */
BYTE L4USTAIRS[] = {
    // clang-format off
    4, 4, // width, height

    3, 3, 3, 3, // search
    3, 3, 3, 3,
    3, 3, 3, 3,
    3, 3, 3, 3,

    0,  0,  0, 0, // replace
    0, 72, 77, 0,
    0, 76,  0, 0,
    0,  0,  0, 0,
    // clang-format on
};
/** Miniset: Stairs down. */
BYTE L4DSTAIRS[] = {
    // clang-format off
    4, 4, // width, height

    3, 3, 3, 3, // search
    3, 3, 3, 3,
    3, 3, 3, 3,
    3, 3, 3, 3,

    0,  0,  0, 0, // replace
    0, 48, 71, 0,
    0, 50, 78, 0,
    0,  0,  0, 0,
    // clang-format on
};

//
// uninitialized variables (.data:005E8CC0)
//

BYTE L4dungeon[80][80];
BOOL hallok[20];
// 1/4 of a full level, in megatiles. This will be replicated in each quadrant.
BYTE dung[20][20];

//
// code (.text:00425560)
//

// .text:00425560
// Initialize `dung`, `L4dungeon`, and `dungeon`.
void L4FixRim()
{
    int x, y;

    for (y = 0; y < 20; ++y)
    {
        for (x = 0; x < 20; ++x)
        {
            dung[x][y] = 0;
        }
    }
    for (y = 0; y < 80; ++y)
    {
        for (x = 0; x < 80; ++x)
        {
            L4dungeon[x][y] = 0;
        }
    }
    for (y = 0; y < 40; ++y)
    {
        for (x = 0; x < 40; ++x)
        {
            dungeon[x][y] = 12;
        }
    }
}

// L4makeDmt    0000000000425660
void L4makeDmt()
{
    // TODO
}

// L4AddWall    000000000042574E

// L4tileFix    0000000000425E35
void L4tileFix()
{
    // TODO
}

// L4makeDungeon    000000000042696E
void L4makeDungeon()
{
    // TODO
}

// uShape    0000000000426D6D
void uShape()
{
    // TODO
}

// GetArea    00000000004270EC
int GetArea()
{
    // TODO
    return 0;
}

// .text:00427166
// Places a rectangle with the given position and dimensions as a room into `dung`.
void L4drawRoom(int x, int y, int width, int height)
{
    int i, j;

    for (j = 0; j < height; j++)
    {
        for (i = 0; i < width; i++)
        {
            dung[i + x][j + y] = 1;
        }
    }
}

// L4checkRoom    00000000004271DB

// L4roomGen    00000000004272BD
void L4roomGen(int x, int y, int w, int h, int dir)
{
    // TODO
}

// .text:0042760B
// Draw the first room and generate from there.
void L4firstRoom()
{
    int x, y, w, h, rndx, rndy, xmin, xmax, ymin, ymax, tx, ty;

    // Room size ranges from 2x2 to 7x7.
    w = random_(5) + 2;
    h = random_(5) + 2;

    // Don't cross the 20 (mega?) tile boundary.
    xmin = (20 - w) >> 1;
    xmax = 19 - w;
    rndx = random_(xmax - xmin + 1) + xmin;
    if (rndx + w > 19)
    {
        tx = w + rndx - 19;
        x = rndx - tx + 1;
    }
    else
    {
        x = rndx;
    }
    ymin = (20 - h) >> 1;
    ymax = 19 - h;
    rndy = random_(ymax - ymin + 1) + ymin;
    if (rndy + h > 19)
    {
        ty = h + rndy - 19;
        y = rndy - ty + 1;
    }
    else
    {
        y = rndy;
    }

    // Place the room.
    L4drawRoom(x, y, w, h);

    // Draw the rest of the owl.
    L4roomGen(x, y, w, h, random_(2));
}

// .text:0042772B
// Places `miniset` between `tmin` and `tmax` times.
// `cx` is the vertical line (megatiles) that separates the level into east/west. Ignored if -1.
// `cy` is the horizontal line (megatiles) that separates the level into north/south. Ignored if -1.
// Taken together, `cx` and `cy` carve the level up into quadrants.
// If `setview` is TRUE then change where the isometric view is centered.
// If `ldir` is 0 then change LvlViewX and LvlviewY
// Returns TRUE if placed, FALSE if not.
// TODO how to interpret ldir?
BOOL DRLG_L4PlaceMiniSet(const BYTE *miniset, int tmin, int tmax, int cx, int cy, BOOL setview, int ldir)
{
    int sx, sy, sw, sh, xx, yy, i, ii, numt, bailcnt;
    BOOL found;

    // Parse the miniset dimensions from the header.
    sw = miniset[0];
    sh = miniset[1];

    // Determine random number of times to place.
    if (tmax - tmin == 0)
    {
        numt = 1;
    }
    else
    {
        numt = random_(tmax - tmin) + tmin;
    }

    // Start placing!
    for (i = 0; i < numt; i++)
    {
        // Generate an initial random position for the miniset.
        sx = random_(DMAXX - sw);
        sy = random_(DMAXY - sh);

        // Search.
        found = FALSE;
        bailcnt = 0;
        while (!found && bailcnt < 200)
        {
            // Assume (sx, sy) is suitable until proven otherwise.
            found = TRUE;

            // Avoid placing minisets around the quadrant boundaries.
            // This looks like leftover from alpha cathedral DRLG.
            // In practice, cx == cy == -1 so this is never used.
            if (cx != -1 && sx >= cx - sw && sx <= cx + 12)
            {
                sx = random_(DMAXX - sw);
                sy = random_(DMAXY - sh);
                found = FALSE;
            }
            if (cy != -1 && sy >= cy - sh && sy <= cy + 12)
            {
                sx = random_(DMAXX - sw);
                sy = random_(DMAXY - sh);
                found = FALSE;
            }

            // Compare miniset search params to level gen.
            ii = 2; // +2 to skip miniset header
            for (yy = 0; yy < sh && found == TRUE; yy++)
            {
                for (xx = 0; xx < sw && found == TRUE; xx++)
                {
                    if (miniset[ii] != 0 && dungeon[xx + sx][yy + sy] != miniset[ii])
                    {
                        // Something didn't match; bail.
                        found = FALSE;
                    }
                    ii++;
                }
            }

            // If we need to try again, push the candidate location around the level (in hopes that it will be better),
            if (!found)
            {
                sx++;
                if (sx == DMAXX - sw)
                {
                    sx = 0;
                    sy++;
                    if (sy == DMAXY - sh)
                    {
                        sy = 0;
                    }
                }
            }

            bailcnt++;
        }

        // Tried too hard, give up
        if (bailcnt >= 200)
        {
            return FALSE;
        }

        // Replace.
        ii = sw * sh + 2; // +2 to skip miniset header
        for (yy = 0; yy < sh; yy++)
        {
            for (xx = 0; xx < sw; xx++)
            {
                if (miniset[ii] != 0)
                {
                    dungeon[xx + sx][yy + sy] = miniset[ii];
                }
                ii++;
            }
        }
    }

    if (setview == TRUE)
    {
        ViewX = 2 * sx + 21;
        ViewY = 2 * sy + 22;
    }
    if (ldir == 0)
    {
        LvlViewX = 2 * sx + 21;
        LvlViewY = 2 * sy + 22;
    }

    return TRUE;
}

// DRLG_L4FTVR    0000000000427A6C

// DRLG_L4FloodTVal    0000000000427DB7
void DRLG_L4FloodTVal()
{
    // TODO
}

// DRLG_L4TransFix    0000000000427E72
void DRLG_L4TransFix()
{
    // TODO
}

// .text:00428197
// `entry` is one of `enum lvl_entry`, in this case either ENTRY_MAIN or ENTRY_PREV
void DRLG_L4(int entry)
{
    BOOL doneflag;
    int i; // written but never read
    int ar;

    doneflag = FALSE;
    while (!doneflag)
    {
        i = 0;
        do
        {
            L4FixRim();
            L4firstRoom();
            ar = GetArea();
            if (ar >= 173)
            {
                uShape();
            }
            ++i;
        } while (ar < 173);
        L4makeDungeon();
        L4makeDmt();
        L4tileFix();
        DRLG_L4FloodTVal();
        DRLG_L4TransFix();

        // Place the stairs and center the view on the right one.
        if (entry == ENTRY_MAIN)
        {
            doneflag = DRLG_L4PlaceMiniSet(L4USTAIRS, 1, 1, -1, -1, TRUE, 0);
            if (doneflag)
            {
                doneflag = DRLG_L4PlaceMiniSet(L4DSTAIRS, 1, 1, -1, -1, FALSE, 1);
            }
            ++ViewX;
        }
        else
        {
            doneflag = DRLG_L4PlaceMiniSet(L4USTAIRS, 1, 1, -1, -1, FALSE, 0);
            if (doneflag)
            {
                doneflag = DRLG_L4PlaceMiniSet(L4DSTAIRS, 1, 1, -1, -1, TRUE, 1);
            }
            ++ViewX;
        }
    }
}

// DRLG_L4Pass3    00000000004282B6
void DRLG_L4Pass3()
{
    // TODO
}

// .text:004284C5
// `entry` is one of `enum lvl_entry`, in this case either ENTRY_MAIN or ENTRY_PREV
void CreateL4Dungeon(DWORD rseed, int entry)
{
    int x, y;

    srand(rseed);

    dminx = MINITILE_PADDING_X;
    dminy = MINITILE_PADDING_Y;
    dmaxx = MAXDUNX - MINITILE_PADDING_X;
    dmaxy = MAXDUNY - MINITILE_PADDING_Y;

    DRLG_InitTrans();
    DRLG_L4(entry);
    DRLG_L4Pass3();

    // Reset all gameplay gendung vars
    for (y = 0; y < MAXDUNY; ++y)
    {
        for (x = 0; x < MAXDUNX; ++x)
        {
            if (lightflag)
            {
                if (light4flag)
                {
                    dLight[x][y] = 3;
                }
                else
                {
                    dLight[x][y] = 15;
                }
            }
            else
            {
                dLight[x][y] = 0;
            }
            dFlags[x][y] = 0;
            dPlayer[x][y] = 0;
            dMonster[x][y] = 0;
            dDead[x][y] = 0;
            dObject[x][y] = 0;
            dItem[x][y] = 0;
            dMissile[x][y] = 0;
            dSpecial[x][y] = 0;

            // Fix door arches that are drawn on top?
            if (dPiece[x][y] == 13)
            {
                dSpecial[x][y] = 5;
            }
            if (dPiece[x][y] == 17)
            {
                dSpecial[x][y] = 6;
            }
        }
    }
    // TODO
}

// drlg_l4_copy_pixels    00000000004286FB
