#ifndef __ENGINE_H__
#define __ENGINE_H__

#include <windows.h>

#include "sound.h"

//
// defines
//

// This name was chosen to be consistent with Devilution
#define MemFreeDbg(x)              \
    GlobalUnlock(GlobalHandle(x)); \
    GlobalFree(GlobalHandle(x));

// This name was chosen to be consistent with Devilution
#define DiabloAllocPtr(x) \
    GlobalLock(GlobalAlloc(GMEM_FIXED, x))

// Floors a fixed point number with 6 fractional bits. E.g. player health, monster health
#define FP_FLOOR(x) (x & 0xFFFFFFC0)
// Convert an integer into a fixed point number with 6 fractional bits.
#define INT2FP(x) (x << 6)

//
// enums
//

// Eight conventional animation directions. Starts at south and moves clockwise
enum direction
{
    DIR_S = 0x0,
    DIR_SW = 0x1,
    DIR_W = 0x2,
    DIR_NW = 0x3,
    DIR_N = 0x4,
    DIR_NE = 0x5,
    DIR_E = 0x6,
    DIR_SE = 0x7,
    DIR_OMNI = 0x8,
};

//
// functions
//

BYTE *LoadFileInMem(const char *pszName);
void LoadFileWithMem(const char *pszName, BYTE *p);
int LoadWaveFileHeaderInMem(const char *filename, TSnd *pSnd, LPWAVEFORMATEX *pFormat);
int LoadWaveFileWithMem(const char *filename, TSnd *pSnd, LPWAVEFORMATEX *pFormat, void **pcm_data);
DWORD FileGetSize(const char *filename);
char *FileAddLoadPrefix(const char *filename);
void CelDraw(int sx, int sy, BYTE *pCelBuff, int nCel, int nWidth);
void CelApplyTrans(BYTE *p, BYTE *ttbl, int nCel);
int GetDirection(int x1, int y1, int x2, int y2);
int random_(int v);
void DrawLine(int x0, int y0, int x1, int y1, BYTE col);
void ENG_set_pixel(int sx, int sy, BYTE col);
void CelBlitWidth(BYTE *pBuff, int x, int y, int wdt, BYTE *pCelBuff, int nCel, int nWidth);
void CelDrawOutlineSafe(char col, int sx, int sy, BYTE *pCelBuff, int nCel, int nWidth, int CelSkip, int CelCap);
void CelDrawLightTblSafe(int sx, int sy, BYTE *pCelBuff, int nCel, int nWidth, int CelSkip, int CelCap, char light);
void CelDrawLightSafe(int sx, int sy, BYTE *pCelBuff, int nCel, int nWidth, int CelSkip, int CelCap);
void CelDrawSafe(int sx, int sy, BYTE *pCelBuff, int nCel, int nWidth, int CelSkip, int CelCap);

#endif