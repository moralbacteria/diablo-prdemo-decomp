// Parts of this code looks like it was copy-pasted from DirectX 1 SDK samples,
// like DUEL.C or CGREMOTE.CPP

// Tell windows header to actually define DIABLO_GUID, not just declare it.
// Must be before any includes. Slightly worried that there is a strict header
// ordering (to not mess DirectX headers) but so far haven't had any problems...
#define INITGUID

#include "multi.h"

#include <DPLAY.H>
#include <windows.h>

#include "diablo.h"
#include "engine.h"
#include "gendung.h"
#include "items.h"
#include "missiles.h"
#include "monster.h"
#include "objects.h"
#include "palette.h"
#include "player.h"
#include "spells.h"

//
// defines
//

// Why is 62 the max length? The largest length in the header is DPLONGNAMELEN,
// which is 52
#define MAX_STR_LEN 62
#define MAX_BUFFER_SIZE 256

#define MSGTYPE_ACK 0xF5
#define MSGTYPE_MUST_ACK 0xF6
#define MSGTYPE_SEED 0x80
#define MSGTYPE_NEW_PLR 0x83

/*
Known messages

data ACK (multi_send_and_wait)
  0 = 0xF5

data that requires ACK (multi_serialize_*)
  0 = 0xF6
  rest is domain specific data (objects, monsters, etc)

sync seed (multi_send_seed)
  0 = 0x80
  1 = seed high
  2 = seed low

new player
  0 = 0x83
  rest is player data, see multi_serialize_player

player action deltas (Player_SerializeNextAction, ...)
action/v1/v2/v3 are set by NetSendCmd
  0x0 = message size (either 0x6 or 0xB)
  0x1 = message id
  0x2 = sender pnum
  0x3 = sender currlevel
  0x4 = ???
  0x5 = action (or 0xFF of size is 6, or 0xFE???)
  --(message ends here if size is 6)--
  0x6 = v1
  0x7 = v2 high
  0x8 = v2 low
  0x9 = v3 high
  0xA = v3 low
*/

// E901EFE0-4B32-11CF-A675-444553540000
DEFINE_GUID(DIABLO_GUID, 0xE901EFE0, 0x4B32, 0x11CF, 0xA6, 0x75, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00);

//
// initialized variables (.data:004A3AA8)
//

LPDIRECTPLAY lpIDC = NULL;
HANDLE dphEvent = NULL;
// either 0, 1, 2
int sendOrRecv = 0;
BYTE byte_4A3AB4 = 0; // TODO better name, something player serialization related
BOOL isSinglePlayer = FALSE;
BOOL hasJoinedGame = FALSE;
BOOL handledSystemMessage = FALSE;

//
// uninitialized variables (.data:005DE370)
//

// Each index corresponds to dpPlayerIds
char dpPlayerNames[MAX_PLRS * MAX_STR_LEN];
// Used by DPlayCreateMyPlayer
DPID dcoID;
BYTE pnum_recipient; // TODO better name, something pnum related
// Enumerated DirectPlay session names. 6 names, 62 chars each. Indices
// correspond to dpSessions. This is set but never read.
char dpSessionNames[6 * MAX_STR_LEN];
int CommBuffCopySize;
// Enumerated DirectPlay session identifiers.
DWORD dpSessions[7];
BOOL IsHost; // set but never used, hold-over from DirectX sample code
// ...
DPSESSIONDESC dpDesc;
// Number of discovered DirectPlay sessions. Never bounds-checked but exceeding
// 6 will cause overflows in dpSessionNames
int dpNumSessions;
// index into dpPlayerIds
int wait_recv_player;
// Identifier for the chosen DirectPlay service provider.
// Either the GUID for the IPX driver, or uninitialized if not found!
LPGUID lpGUIDForSP;
BYTE CommBuff[MAX_BUFFER_SIZE];
// Each index corresponds to dpPlayerNames
DPID dpPlayerIds[MAX_PLRS];
DWORD dword_5DE7B0; // UNUSED
DWORD dword_5DE7B4; // TODO better name
// CommBuffCopy is used for packing/unpacking messages
// If the first byte is 0xF6 then it must be acknowledged with 0xF5.
static BYTE CommBuffCopy[MAX_BUFFER_SIZE];
BOOL isMpPlayerInitialized[MAX_PLRS];
int mpRandSeed;

//
// declarations
//

// I coped these signatures from the SDK samples but in reality FAR is
// unnecesary in 32bit memory models and PASCAL is just __stdcall.

static BOOL FAR PASCAL EnumSP(LPGUID lpGUID, LPSTR lpDriverDescription, DWORD dwMajorVersion, DWORD dwMinorVersion, LPVOID lpContext);
static BOOL FAR PASCAL EnumSession(LPDPSESSIONDESC lpDPSGameDesc, LPVOID lpContext, LPDWORD lpdwTimeOut, DWORD dwFlags);
static BOOL FAR PASCAL EnumPlayers(DPID pidID, LPSTR lpFriendlyName, LPSTR lpFormalName, DWORD dwFlags, LPVOID lpContex);

static void DPlayInitPlayer(DPID pidID, LPSTR lpFriendlyName);
static void DPlayFreePlayer(DPID dpId);
static void multi_sort_players_by_id();
static void multi_serialize_player(int p, int initialSendSize);
static int multi_find_player_index(DPID dpID);
static void multi_deserialize_player(int p, int initialRecvSize);

//
// Code (.text:004141E0)
//

/*
Most of these functions aren't called by anything so I don't know which order
they need to be run in. My guess is that there are some missing menus to create/
join a game, evidenced by: missing MODE_* for 3-6 and 8-11, DPlayOpenSession
requiring a session ID (which would probably have been shown to the player).

Pre-reqs: game is running on some non-MODE_GAME menu with a character in memory plr[0]

Joining a game:

1. DPlayCreate() - initializes main DirectPlay handle (lpIDC) and DirectPlay service provider (lpGUIDForSP)
2. DPlayEnumSessions() - Find and store all DirectPlay sessions (dpSessions, dpSessionNames)
3. DPlayOpenSession() - Join a given game (one of dpSessions)
4. DPlayEnumPlayers() - Find all DirectPlay players in that game
*/

// .text:004141E0
// DEAD CODE
// Initialize DirectPlay with the IPX driver. Use lpIDC for DPlay calls
BOOL DPlayCreate()
{
    int i;

    if (lpIDC == NULL)
    {
        for (i = 0; i < MAX_PLRS; ++i)
        {
            // No error checking D:
            DirectPlayEnumerate(EnumSP, NULL);
            // If it can't find the IPX driver (e.g. on modern Windows) then
            // lpGUIDForSP is uninitialized and this will probably fail
            DirectPlayCreate(lpGUIDForSP, &lpIDC, NULL);
            return (TRUE);
        }
    }

    return FALSE;
}

// .text:0041425B
// DEAD CODE
// Find all DPlay sesisons and store in dpSessions
BOOL DPlayEnumSessions()
{
    memset(&dpDesc, 0x00, sizeof(DPSESSIONDESC));
    dpDesc.dwSize = sizeof(dpDesc);
    dpDesc.guidSession = DIABLO_GUID;

    dpNumSessions = 0;

    // No error checking D:
    // Don't know where to put this but I thought it was funny: DUEL.C uses NULL
    // for the last param. This technically compiles but makes no sense
    // semantically. The function wants a bitmap of flags, not a pointer!
    lpIDC->EnumSessions(&dpDesc, /*dwTimeout=*/1000, EnumSession, NULL, /*dwFlags=*/0);

    return (TRUE);
}

// .text:004142DB
// DEAD CODE
// Join a game. If this fails then lpIDC is released, so DPlayCreateSession/
// DPlayEnumSessions needs to be called again.
BOOL DPlayOpenSession(DWORD sessionId)
{
    int i;
    HRESULT hr;

    if (sessionId != 0)
    {
        memset(&dpDesc, 0x00, sizeof(DPSESSIONDESC));
        dpDesc.dwSize = sizeof(dpDesc);
        dpDesc.guidSession = DIABLO_GUID;
        dpDesc.dwFlags = DPOPEN_OPENSESSION;
        dpDesc.dwSession = sessionId;

        hr = lpIDC->Open(&dpDesc);

        for (i = 0; i < gbActivePlayers; ++i)
        {
            isMpPlayerInitialized[i] = FALSE;
        }

        if (hr != DP_OK)
        {
            lpIDC->Close();
            lpIDC->Release();
            lpIDC = NULL;
            return (FALSE);
        }
    }

    return (TRUE);
}

// .text:004143D4
// DEAD CODE
// Create a game. If this fails then lpIDC is released, so DPlayCreateSession/
// DPlayEnumSessions needs to be called again.
BOOL DPlayCreateSession(const char *FullName)
{
    HRESULT hr;
    int i;

    // IsHost is never used. A vestige of the sample code...
    IsHost = TRUE;
    memset(&dpDesc, 0x00, sizeof(DPSESSIONDESC));
    dpDesc.dwSize = sizeof(dpDesc);
    // Why 16 and not MAX_PLRS? A vestige of the sample code...
    dpDesc.dwMaxPlayers = 16;
    dpDesc.dwFlags = DPOPEN_CREATESESSION;
    dpDesc.guidSession = DIABLO_GUID;
    strcpy(dpDesc.szSessionName, FullName);

    for (i = 0; i < gbActivePlayers; ++i)
    {
        isMpPlayerInitialized[i] = FALSE;
    }

    if ((hr = lpIDC->Open(&dpDesc)) != DP_OK)
    {
        lpIDC->Release();
        lpIDC = NULL;
        return (FALSE);
    }

    return (TRUE);
}

// .text:004144D3
// DEAD CODE
BOOL DPlayEnumPlayers()
{
    gbActivePlayers = 0;
    // No error checking D:
    lpIDC->EnumPlayers(dpDesc.dwSession, EnumPlayers, &gbActivePlayers, DPENUMPLAYERS_GROUP);
    return TRUE;
}

// .text:00414514
// DEAD CODE
// Create a player with the given name. If this fails then lpIDC is released,
// so DPlayCreateSession/etc needs to be called again.
BOOL DPlayCreateMyPlayer(LPSTR NickName)
{
    HRESULT hr;
    if ((hr = lpIDC->CreatePlayer(&dcoID, NickName,
                                  "Diablo Player", &dphEvent)) != DP_OK)
    {
        lpIDC->Close();
        lpIDC->Release();
        lpIDC = NULL;
        return (FALSE);
    }

    DPlayInitPlayer(dcoID, NickName);

    return (TRUE);
}

// .text:0041459E
// DEAD CODE
// Destroy a player by ID. If this is your player then lpIDC is released.
BOOL DPlayDestroyPlayer(DPID dpId)
{
    lpIDC->DestroyPlayer(dpId);
    if (dpId == dcoID)
    {
        lpIDC->Close();
        lpIDC->Release();
        lpIDC = NULL;
    }
    DPlayFreePlayer(dpId);
    return TRUE;
}

// .text:0041460D
BOOL DPlayHandleMessage()
{
    int buffer_index;
    DPMSG_GENERIC *pGeneric;
    int pnum;
    DPID fromID;
    int i;
    DWORD nBytes;
    HRESULT status;
    DPMSG_ADDPLAYER *pMsg;
    DPID dcoReceiveID;
    int j;
    BOOL retval;

    retval = FALSE;

    if (lpIDC != NULL)
    {
        nBytes = MAX_BUFFER_SIZE;
        status = lpIDC->Receive(&fromID, &dcoReceiveID, DPRECEIVE_ALL, CommBuff, &nBytes);
        switch (status)
        {
        case DP_OK:
            if (fromID == 0)
            {
                // Handle system messages
                pGeneric = (DPMSG_GENERIC *)CommBuff;
                switch (pGeneric->dwType)
                {
                case DPSYS_ADDPLAYER:
                    pMsg = (DPMSG_ADDPLAYER *)CommBuff;
                    if (pMsg->dwCurrentPlayers <= MAX_PLRS)
                    {
                        DPlayInitPlayer(pMsg->dpId, pMsg->szShortName);
                        handledSystemMessage = TRUE;
                    }
                    break;
                case DPSYS_DELETEPLAYER:
                    DPlayFreePlayer(fromID);
                    handledSystemMessage = TRUE;
                    break;
                }
            }
            else
            {
                if (hasJoinedGame || gMode == MODE_GAME)
                {
                    pnum = multi_find_player_index(fromID);
                    if (sendOrRecv == 1 && wait_recv_player == pnum && *CommBuff == MSGTYPE_MUST_ACK)
                    {
                        // Copy for deserialization elsewhere (see multi_wait_and_send)
                        for (i = 0; i < nBytes; i++)
                        {
                            CommBuffCopy[i] = CommBuff[i];
                        }
                        return TRUE;
                    }
                    if (sendOrRecv == 2 && wait_recv_player == pnum && *CommBuff == MSGTYPE_ACK)
                    {
                        // Return confirmation of ack (see multi_send_and_wait)
                        return TRUE;
                    }

                    retval = TRUE;

                    buffer_index = plr[pnum].send_buffer_idx;
                    for (j = 0; j < *CommBuff; j++)
                    {
                        plr[pnum].mp_buffer[buffer_index][j] = CommBuff[j];
                    }
                    plr[pnum].send_buffer_idx = (plr[pnum].send_buffer_idx + 1) % PLR_NUM_MP_BUFFERS;

                    buffer_index = plr[pnum].recv_buffer_idx;
                    // TODO magic number (both index 5 and value 0xfe)
                    // index 5 is the one of enum action_id, set by NetSendCmd.
                    // or it can be special 0xFF (-1) or 0xFE (-2)
                    if (plr[pnum].mp_buffer[buffer_index][5] == 0xFE)
                    {
                        plr[pnum].recv_buffer_idx = (plr[pnum].recv_buffer_idx + 1) % PLR_NUM_MP_BUFFERS;
                        plr[pnum].to_send_message_id = plr[pnum].mp_buffer[buffer_index][1];
                        byte_4A3AB4 = 3; // TODO magic number
                        pnum_recipient = pnum;
                    }
                }
                else
                {
                    if (*CommBuff == MSGTYPE_SEED) // I joined!
                    {
                        // Get the seed (this is used in multi_init_seed)
                        mpRandSeed = CommBuff[1] << 8 + CommBuff[2];

                        // Start transitioning to game
                        PaletteFadeOut(32);
                        delayed_Msg = WM_DIABDONEFADEOUT;
                        hasJoinedGame = TRUE;

                        // Send my character data to all other players
                        multi_serialize_player(myplr, 1);
                        *CommBuffCopy = MSGTYPE_NEW_PLR;
                        lpIDC->Send(dcoID, /*pidTo=*/0, /*dwFlags=*/0, CommBuffCopy, CommBuffCopySize);

                        // Receive the message just sent and unpack. I think
                        // this needs to happen since myplr probably starts as
                        // 0 but this changes myplr so need to resync
                        // multiplayer state
                        multi_sort_players_by_id();
                        myplr = multi_find_player_index(dcoID);
                        multi_deserialize_player(myplr, 1);
                        return TRUE;
                    }
                    else if (*CommBuff == MSGTYPE_NEW_PLR) // Someone else joined!
                    {
                        // Get their character data
                        pnum = multi_find_player_index(fromID);
                        for (i = 0; i < nBytes; i++)
                        {
                            CommBuffCopy[i] = CommBuff[i];
                        }
                        multi_deserialize_player(pnum, 1);
                        isMpPlayerInitialized[pnum] = TRUE;
                        return TRUE;
                    }
                }
            }
            break;
        case DPERR_INVALIDOBJECT:
            return retval;
        case DPERR_INVALIDPARAM:
            return retval;
        case DPERR_BUFFERTOOSMALL:
            return retval;
        case DPERR_NOMESSAGES:
            return retval;
        case DPERR_GENERIC:
            return retval;
        }
    }

    return retval;
}

// .text:00414B0E
// DEAD CODE since DPlayEnumSessions is never called
static BOOL FAR PASCAL EnumSession(LPDPSESSIONDESC lpDPSGameDesc, LPVOID lpContext, LPDWORD lpdwTimeOut, DWORD dwFlags)
{
    LPVOID ctx; // unused

    ctx = lpContext;

    // Timeout is 1s (1000ms, see DPlayEnumSessions)
    if (dwFlags & DPESC_TIMEDOUT)
    {
        // Stop enumerating
        return FALSE;
    }

    if (lpDPSGameDesc->dwSession != 0)
    {
        strncpy(&dpSessionNames[dpNumSessions * MAX_STR_LEN], lpDPSGameDesc->szSessionName, MAX_STR_LEN);
        dpSessions[dpNumSessions] = lpDPSGameDesc->dwSession;
        ++dpNumSessions;
    }

    // Continue enumerating
    return TRUE;
}

// .text:00414B8A
// DEAD CODE since DPlayCreate is never called
// Find IPX driver GUID and store in lpGUIDForSP.
static BOOL FAR PASCAL EnumSP(LPGUID lpGUID, LPSTR lpDriverDescription, DWORD dwMajorVersion, DWORD dwMinorVersion, LPVOID lpContext)
{
    LPVOID ctx; // unused

    ctx = lpContext;

    // AFAICT, IPX is a communication protocol that was used in office LANs
    // before TCP/IP took over. Good luck getting that to work again!
    //
    // https://github.com/solemnwarning/ipxwrapper could be useful here
    if (strcmp("WinSock IPX Connection For DirectPlay", lpDriverDescription) == 0)
    {
        lpGUIDForSP = lpGUID;
    }

    // Continue enumerating. Technically, they could have stopped in the `if`
    // above by returning FALSE...
    return TRUE;
}

// .text:00414BCB
// DEAD CODE since DPlayEnumPlayers is never called.
static BOOL FAR PASCAL EnumPlayers(DPID pidID, LPSTR lpFriendlyName, LPSTR lpFormalName, DWORD dwFlags, LPVOID lpContex)
{
    DPlayInitPlayer(pidID, lpFriendlyName);
    return TRUE;
}

// .text:00414BED
// Remove all player registered with the given ID. Always decreases
// gbActivePlayers so I hope dpId is actually stored!
static void DPlayFreePlayer(DPID dpId)
{
    int i;
    int j;

    for (i = 0; i < gbActivePlayers; ++i)
    {
        // Find the player
        if (dpPlayerIds[i] == dpId)
        {
            // Remove them by shifting the rest of the array over top.
            // Goal is to keep both array contiguous
            for (j = i; j < gbActivePlayers - 1; ++j)
            {
                dpPlayerIds[j] = dpPlayerIds[j + 1];
                strncpy(&dpPlayerNames[j * MAX_STR_LEN], &dpPlayerNames[(j + 1) * MAX_STR_LEN], MAX_STR_LEN);
            }

            // why no break/return?
        }
    }

    // This always decreases, regardless if dpId is a registered player...
    gbActivePlayers--;
}

// .text:00414CA3
// Store player info in next available slot. See dpPlayer*
static void DPlayInitPlayer(DPID pidID, LPSTR lpFriendlyName)
{
    dpPlayerIds[gbActivePlayers] = pidID;
    strncpy(&dpPlayerNames[gbActivePlayers * MAX_STR_LEN], lpFriendlyName, MAX_STR_LEN);
    ++gbActivePlayers;
}

// .text:00414CF4
// The only reason I can think to sort is that it keeps the array order the same
// for each player. So my dpPlayerId[i] is the same as your dpPlayerId[i]
// for any `i`. That could simplify communication or eliminate variability for
// easier debugging...
static void multi_sort_players_by_id()
{
    DPID tmpId;
    char buffer[64];
    int j;
    int k; // largest ID
    int i;
    DWORD unused;

    unused = 0;

    for (i = 0; i < gbActivePlayers; i++)
    {
        k = i;
        for (j = i + 1; j < gbActivePlayers; ++j)
        {
            if (dpPlayerIds[k] > dpPlayerIds[j])
            {
                k = j;
            }
        }
        if (i != k)
        {
            tmpId = dpPlayerIds[i];
            dpPlayerIds[i] = dpPlayerIds[k];
            dpPlayerIds[k] = tmpId;
            strcpy(buffer, &dpPlayerNames[i * MAX_STR_LEN]);
            strcpy(&dpPlayerNames[i * MAX_STR_LEN], &dpPlayerNames[k * MAX_STR_LEN]);
            strcpy(&dpPlayerNames[k * MAX_STR_LEN], buffer);
        }
    }
}

// .text:00414E1C
// DEAD CODE
static void multi_send_seed()
{
    BYTE buffer[3];

    // Serialize seed
    buffer[0] = MSGTYPE_SEED;
    mpRandSeed = rand();
    buffer[1] = mpRandSeed >> 8;
    buffer[2] = mpRandSeed & 0xFF;

    if (lpIDC != NULL)
    {
        // Send seed
        lpIDC->Send(dcoID, /*pidTo=*/0, /*dwFlags=*/0, buffer, 3);

        hasJoinedGame = TRUE;

        // Send player
        multi_serialize_player(myplr, 1);
        *CommBuffCopy = MSGTYPE_NEW_PLR;
        lpIDC->Send(dcoID, /*pidTo=*/0, /*dwFlags=*/0, CommBuffCopy, CommBuffCopySize);

        // Synchronize multiplayer state
        multi_sort_players_by_id();
        myplr = multi_find_player_index(dcoID);
        multi_deserialize_player(myplr, 1);
    }
}

// .text:00414EE5
// Find i for plr[i] given a DPID.
//
// I wonder if DPID can't be 0 or something but index
// into plr can, which si why they need to translation?
static int multi_find_player_index(DPID dpID)
{
    BOOL found;
    int i;

    found = FALSE;

    for (i = 0; i < MAX_PLRS && !found; ++i)
    {
        if (dpPlayerIds[i] == dpID)
        {
            found = TRUE;
        }
        else
        {
            found = FALSE;
        }
    }

    if (found)
    {
        return i - 1;
    }

    return -1;
}

// .text:00414F6D
// TODO what is the point of this function?
// This seems to be SUPER IMPORTANT for limiting the game_logic to 20 FPS.
BOOL unknown_dplay_get_plractive()
{
    int buffer_idx;
    int i;
    BOOL result;

    result = TRUE;

    for (i = 0; i < gbActivePlayers; ++i)
    {
        buffer_idx = plr[i].recv_buffer_idx;
        if (plr[i].plractive)
        {
            if (result && plr[i].to_send_message_id == plr[i].mp_buffer[buffer_idx][1])
            {
                result = TRUE;
            }
            else
            {
                result = FALSE;
            }
        }
    }

    return result;
}

// unknown_dplay_send	0000000000415053 TODO
void unknown_dplay_send()
{
    // TODO
}

// .text:0041515B
void multi_BSave(char v)
{
    CommBuffCopy[CommBuffCopySize++] = v;
}

// .text:00415186
void multi_WSave(WORD v)
{
    CommBuffCopy[CommBuffCopySize++] = (v >> 8);
    CommBuffCopy[CommBuffCopySize++] = (v & 0xFF);
}

// .text:004151C6
void multi_ISave(int v)
{
    CommBuffCopy[CommBuffCopySize++] = (v >> 24);
    CommBuffCopy[CommBuffCopySize++] = ((v >> 16) & 0xFF);
    CommBuffCopy[CommBuffCopySize++] = ((v >> 8) & 0xFF);
    CommBuffCopy[CommBuffCopySize++] = (v & 0xFF);
}

// .text:00415236
void multi_OSave(BOOL v)
{
    if (v)
    {
        CommBuffCopy[CommBuffCopySize++] = 1;
    }
    else
    {
        CommBuffCopy[CommBuffCopySize++] = 0;
    }
}

// .text:0041527F
// Packs player data into CommBuffCopy/CommBuffCopySize, ready to send
static void multi_serialize_player(int p, int initSize)
{
    CommBuffCopySize = initSize;
    multi_WSave(plr[p]._pmode);
    multi_OSave(plr[p].recv_ready);
    multi_BSave(plr[p].recv_action);
    multi_WSave(plr[p].recv_v1);
    multi_WSave(plr[p].recv_v2);
    multi_WSave(plr[p].recv_v3);
    multi_WSave(plr[p]._px);
    multi_WSave(plr[p]._py);
    multi_ISave(plr[p]._pxoff);
    multi_ISave(plr[p]._pyoff);
    multi_ISave(plr[p]._pxvel);
    multi_ISave(plr[p]._pxvel); // lol so this is a bug, should probably be _pyvel
    multi_WSave(plr[p]._pdir);
    multi_WSave(plr[p]._pgfxnum);
    multi_WSave(plr[p]._pAnimDelay);
    multi_WSave(plr[p]._pAnimCnt);
    multi_WSave(plr[p]._pAnimLen);
    multi_WSave(plr[p]._pAnimFrame);
    multi_ISave(plr[p]._pAnimWidth);
    multi_ISave(plr[p]._pAnimWidth2);
    multi_WSave(plr[p]._peflag);
    multi_WSave(plr[p]._pRSpell);
    multi_WSave(plr[p]._pLightRad);
    multi_BSave(plr[p]._pStrength);
    multi_BSave(plr[p]._pMagic);
    multi_BSave(plr[p]._pDexterity);
    multi_BSave(plr[p]._pVitality);
    multi_WSave(plr[p]._pBaseToHit);
    multi_WSave(plr[p]._pDamageMod);
    multi_ISave(plr[p]._pHitPoints);
    multi_ISave(plr[p]._pMaxHP);
    multi_ISave(plr[p]._pMana);
    multi_ISave(plr[p]._pMaxMana);
    multi_BSave(plr[p]._pLevel);
    multi_BSave(plr[p]._pMaxLvl);
    multi_ISave(plr[p]._pExperience);
    multi_ISave(plr[p]._pMaxExp);
    multi_BSave(plr[p]._pArmorClass);
    multi_OSave(plr[p]._pInfraFlag);
    multi_ISave(plr[p]._pVar1);
    multi_ISave(plr[p]._pVar2);
    multi_ISave(plr[p]._pVar3);
    multi_ISave(plr[p]._pVar4);
    multi_ISave(plr[p]._pVar5);
    multi_ISave(plr[p]._pVar6);
    multi_ISave(plr[p]._pVar7);
    multi_ISave(plr[p]._pVar8);
}

// .text:00415888
// Packs monster data into CommBuffCopy/CommBuffCopySize, ready to send
static void multi_serialize_monster(int m)
{
    CommBuffCopySize = 1;
    *CommBuffCopy = MSGTYPE_MUST_ACK;
    multi_WSave(monster[m]._mmode);
    multi_BSave(monster[m]._mgoal);
    multi_BSave(monster[m]._mgoalvar1);
    multi_WSave(monster[m]._mx);
    multi_WSave(monster[m]._my);
    multi_ISave(monster[m]._mxoff);
    multi_ISave(monster[m]._myoff);
    multi_ISave(monster[m]._mxvel);
    multi_ISave(monster[m]._myvel);
    multi_WSave(monster[m]._mdir);
    multi_WSave(monster[m]._menemy);
    multi_WSave(monster[m]._mAnimDelay);
    multi_WSave(monster[m]._mAnimCnt);
    multi_WSave(monster[m]._mAnimLen);
    multi_WSave(monster[m]._mAnimFrame);
    multi_WSave(monster[m]._meflag);
    multi_OSave(monster[m]._mDelFlag);
    multi_ISave(monster[m]._mVar1);
    multi_ISave(monster[m]._mVar2);
    multi_ISave(monster[m]._mVar3);
    multi_ISave(monster[m]._mVar4);
    multi_ISave(monster[m]._mVar5);
    multi_ISave(monster[m]._mVar6);
    multi_ISave(monster[m]._mVar7);
    multi_ISave(monster[m]._mVar8);
    multi_ISave(monster[m]._mhitpoints);
    multi_BSave(monster[m]._mint);
    multi_WSave(monster[m]._mFlags);
    multi_WSave(monster[m].anonymous_13);
}

// .text:00415BA5
// Packs missile data into CommBuffCopy/CommBuffCopySize, ready to send
static void multi_serialize_missile(int mi)
{
    CommBuffCopySize = 1;
    *CommBuffCopy = MSGTYPE_MUST_ACK;
    multi_WSave(missile[mi]._mitype);
    multi_WSave(missile[mi]._mix);
    multi_WSave(missile[mi]._miy);
    multi_ISave(missile[mi]._mixoff);
    multi_ISave(missile[mi]._miyoff);
    multi_ISave(missile[mi]._mixvel);
    multi_ISave(missile[mi]._miyvel);
    multi_WSave(missile[mi]._misx);
    multi_WSave(missile[mi]._misy);
    multi_ISave(missile[mi]._mitxoff);
    multi_ISave(missile[mi]._mityoff);
    multi_WSave(missile[mi]._mimfnum);
    multi_OSave(missile[mi]._miDelFlag);
    multi_OSave(missile[mi]._miAnimFlags);
    multi_WSave(missile[mi]._miAnimDelay);
    multi_WSave(missile[mi]._miAnimCnt);
    multi_WSave(missile[mi]._miAnimLen);
    multi_WSave(missile[mi]._miAnimFrame);
    multi_ISave(missile[mi]._miAnimWidth);
    multi_ISave(missile[mi]._miAnimWidth2);
    multi_WSave(missile[mi]._mirange);
    multi_WSave(missile[mi]._misource);
    multi_WSave(missile[mi]._micaster);
    multi_OSave(missile[mi]._midist);
    multi_ISave(missile[mi]._miVar1);
    multi_ISave(missile[mi]._miVar2);
    multi_ISave(missile[mi]._miVar3);
    multi_ISave(missile[mi]._miVar4);
}

// .text:00415DFC
// Packs spell data into CommBuffCopy/CommBuffCopySize, ready to send
static void multi_serialize_spell(int si)
{
    CommBuffCopySize = 1;
    *CommBuffCopy = MSGTYPE_MUST_ACK;
    multi_WSave(spells[si].type);
    multi_WSave(spells[si].x);
    multi_WSave(spells[si].y);
    multi_OSave(spells[si].animFlag);
    multi_OSave(spells[si].field_10);
    multi_WSave(spells[si].animDelay);
    multi_WSave(spells[si].animCnt);
    multi_WSave(spells[si].animLen);
    multi_WSave(spells[si].animFrame);
    multi_ISave(spells[si].animWidth);
    multi_ISave(spells[si].animWidth2);
    multi_OSave(spells[si].field_30);
    multi_WSave(spells[si].field_34);
    multi_WSave(spells[si].field_38);
    multi_WSave(spells[si].range);
    multi_OSave(spells[si].delFlag);
    multi_WSave(spells[si].source);
    multi_ISave(spells[si].var1);
    multi_ISave(spells[si].var2);
    multi_ISave(spells[si].var3);
    multi_ISave(spells[si].var4);
    multi_ISave(spells[si].var5);
    multi_ISave(spells[si].var6);
    multi_ISave(spells[si].var7);
    multi_ISave(spells[si].var8);
}

// .text:00416030
// Packs object data into CommBuffCopy/CommBuffCopySize, ready to send
static void multi_serialize_object(int oi)
{
    CommBuffCopySize = 1;
    *CommBuffCopy = MSGTYPE_MUST_ACK;
    multi_WSave(object[oi]._otype);
    multi_WSave(object[oi]._ox);
    multi_WSave(object[oi]._oy);
    multi_WSave(object[oi]._oAnimDelay);
    multi_WSave(object[oi]._oAnimCnt);
    multi_WSave(object[oi]._oAnimLen);
    multi_WSave(object[oi]._oAnimFrame);
    multi_ISave(object[oi]._oAnimWidth);
    multi_ISave(object[oi]._oAnimWidth2);
    multi_OSave(object[oi]._oDelFlag);
    multi_ISave(object[oi]._oVar1);
    multi_ISave(object[oi]._oVar2);
    multi_ISave(object[oi]._oVar3);
    multi_ISave(object[oi]._oVar4);
}

// .text:004161B5
// Packs item drop data into CommBuffCopy/CommBuffCopySize, ready to send
static void multi_serialize_item(int i)
{
    CommBuffCopySize = 1;
    *CommBuffCopy = MSGTYPE_MUST_ACK;
    multi_WSave(item[i]._itype);
    multi_WSave(item[i]._ix);
    multi_WSave(item[i]._iy);
    multi_OSave(item[i]._iAnimFlag);
    multi_WSave(item[i]._iAnimLen);
    multi_WSave(item[i]._iAnimFrame);
    multi_ISave(item[i]._iAnimWidth);
    multi_ISave(item[i]._iAnimWidth2);
    multi_OSave(item[i]._iDelFlag);
}

// .text:004162BD
static BYTE multi_BLoad()
{
    BYTE v;
    v = CommBuffCopy[++CommBuffCopySize];
    return v;
}

// .text:004162E7
static WORD multi_WLoad()
{
    WORD v;
    v = CommBuffCopy[++CommBuffCopySize] << 8;
    v += CommBuffCopy[++CommBuffCopySize];
    return v;
}

// .text:0041632C
static int multi_ILoad()
{
    int v;
    v = CommBuffCopy[CommBuffCopySize++] << 24;
    v += CommBuffCopy[CommBuffCopySize++] << 16;
    v += CommBuffCopy[CommBuffCopySize++] << 8;
    v += CommBuffCopy[CommBuffCopySize++];
    return v;
}

// text:004163A3
static BOOL multi_OLoad()
{
    int i;
    i = CommBuffCopySize;
    ++CommBuffCopySize;
    if (CommBuffCopy[i] == 1)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

// .text:004163E9
static void multi_deserialize_player(int pnum, int initSize)
{
    CommBuffCopySize = initSize;
    plr[pnum]._pmode = multi_WLoad();
    plr[pnum].recv_ready = multi_OLoad();
    plr[pnum].recv_action = multi_BLoad();
    plr[pnum].recv_v1 = multi_WLoad();
    plr[pnum].recv_v2 = multi_WLoad();
    plr[pnum].recv_v3 = multi_WLoad();
    plr[pnum]._px = multi_WLoad();
    plr[pnum]._py = multi_WLoad();
    plr[pnum]._pxoff = multi_ILoad();
    plr[pnum]._pyoff = multi_ILoad();
    plr[pnum]._pxvel = multi_ILoad();
    plr[pnum]._pyvel = multi_ILoad();
    plr[pnum]._pdir = multi_WLoad();
    plr[pnum]._pgfxnum = multi_WLoad();
    plr[pnum]._pAnimDelay = multi_WLoad();
    plr[pnum]._pAnimCnt = multi_WLoad();
    plr[pnum]._pAnimLen = multi_WLoad();
    plr[pnum]._pAnimFrame = multi_WLoad();
    plr[pnum]._pAnimWidth = multi_ILoad();
    plr[pnum]._pAnimWidth2 = multi_ILoad();
    plr[pnum]._peflag = multi_WLoad();
    plr[pnum]._pRSpell = multi_WLoad();
    plr[pnum]._pLightRad = multi_WLoad();
    plr[pnum]._pStrength = multi_BLoad();
    plr[pnum]._pMagic = multi_BLoad();
    plr[pnum]._pDexterity = multi_BLoad();
    plr[pnum]._pVitality = multi_BLoad();
    plr[pnum]._pBaseToHit = multi_WLoad();
    plr[pnum]._pDamageMod = multi_WLoad();
    plr[pnum]._pHitPoints = multi_ILoad();
    plr[pnum]._pMaxHP = multi_ILoad();
    plr[pnum]._pMana = multi_ILoad();
    plr[pnum]._pMaxMana = multi_ILoad();
    plr[pnum]._pLevel = multi_BLoad();
    plr[pnum]._pMaxLvl = multi_BLoad();
    plr[pnum]._pExperience = multi_ILoad();
    plr[pnum]._pMaxExp = multi_ILoad();
    plr[pnum]._pArmorClass = multi_BLoad();
    plr[pnum]._pInfraFlag = multi_OLoad();
    plr[pnum]._pVar1 = multi_ILoad();
    plr[pnum]._pVar2 = multi_ILoad();
    plr[pnum]._pVar3 = multi_ILoad();
    plr[pnum]._pVar4 = multi_ILoad();
    plr[pnum]._pVar5 = multi_ILoad();
    plr[pnum]._pVar6 = multi_ILoad();
    plr[pnum]._pVar7 = multi_ILoad();
    plr[pnum]._pVar8 = multi_ILoad();
    SyncInitPlr(pnum);
}

// unknown_dplay_recv_mon	00000000004169FE TODO
// unknown_dplay_recv_mis	0000000000416D1B TODO
// unknown_dplay_recv_spell	0000000000416F73 TODO TODO
// unknown_dplay_recv_obj	00000000004171A8 TODO
// unknown_dplay_recv_item	000000000041732E TODO

// .text:00417437
// TODO better ma,e
void unknown_dplay_get_plrs(DWORD)
{
    // TODO
}

// .text:0041763C
// Send CommBuffCopy to wait_recv_player, then wait for 0xF5 message.
void multi_send_and_wait(DWORD size)
{
    lpIDC->Send(dcoID, dpPlayerIds[wait_recv_player], /*dwFlags=*/0, &CommBuffCopy, size);

    while (!DPlayHandleMessage())
    {
        // This space intentionally left blank
    }

    if (*CommBuff == MSGTYPE_ACK)
    {
        *CommBuff = 0;
    }
}

// .text:004176AB
// TODO better name
void unknown_dplay_game_send(DWORD)
{
    // TODO
}

// .text:00417E07
// TODO better name
void unknown_dplay_case_plr()
{
    DWORD var_4;
    DWORD var_8;
    BYTE buffer[6];

    // Send 0xFD message
    var_8 = pnum_recipient;
    buffer[0] = 6; // message size
    buffer[1] = plr[myplr].to_send_message_id;
    buffer[2] = myplr;
    buffer[3] = currlevel;
    buffer[4] = byte_4A3AB4;
    buffer[5] = 0xFD; // TODO magic number
    lpIDC->Send(dcoID, var_8, /*dwFlags=*/0, buffer, buffer[0]);

    // wait for a response
    do
    {
        DPlayHandleMessage();
    } while (plr[var_8].recv_buffer_idx == plr[var_8].send_buffer_idx);

    // Interpret response
    var_4 = plr[var_8].recv_buffer_idx;
    switch (plr[var_8].mp_buffer[var_4][5])
    {
    case 0xFA: // TODO magic number
        byte_4A3AB4 = 0;
        plr[var_8].recv_buffer_idx = (plr[var_8].recv_buffer_idx + 1) % 4;
        plr[var_8].anonymous_1 = 0;
        break;
    case 0xFB: // TODO magic number
        byte_4A3AB4 = 2;
        dword_5DE7B4 = var_8;
        break;
    case 0xFC: // TODO magic number
        byte_4A3AB4 = 1;
        dword_5DE7B4 = var_8;
        break;
    }
}

// .text:00417FDA
// Wait for message 0xF6 then send 0xF5 to `wait_recv_player`.
void multi_wait_and_send()
{
    BYTE toSend;

    while (!DPlayHandleMessage())
    {
        // This space intentionally left blank
    }

    if (*CommBuff == MSGTYPE_MUST_ACK)
    {
        toSend = MSGTYPE_ACK;
        lpIDC->Send(dcoID, dpPlayerIds[wait_recv_player], 0, &toSend, 1);
    }
}

// unknown_dplay_game_recv	0000000000418047 TODO
// unknown_dplay_do_recv	0000000000418766 TODO

// .text:00418E25
// DEAD CODE
// This waits for all joined players to send data (MSGTYPE_NEW_PLR) then
// initializes the seed from MSGTYPE_SEED
void multi_init_seeds()
{
    int p;
    int i;
    int drlgSeed;
    BOOL allActivePlayersInitialized;

    isMpPlayerInitialized[myplr] = TRUE;

    // Wait for seed and player ready
    do
    {
        DPlayHandleMessage();

        allActivePlayersInitialized = TRUE;
        for (i = 0; i < gbActivePlayers; ++i)
        {
            allActivePlayersInitialized &= isMpPlayerInitialized[i];
        }
    } while (!allActivePlayersInitialized);

    // Apply seed
    srand(mpRandSeed);

    // Generate DRLG seeds from main seed for all levels
    for (i = 0; i < NUMLEVELS; ++i)
    {
        drlgSeed = random_(0x8000);
        for (p = 0; p < MAX_PLRS; ++p)
        {
            plr[p]._pSeedTbl[i] = drlgSeed;
        }
    }
}
