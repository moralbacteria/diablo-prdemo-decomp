# Visual C++ 4.2

This creates a docker container with:

- Visual C++ 4.2 available in `/MSDEV`
- DirectX 1.0 available in `/GAMESDK`
- Wine, to run Visual C++ 4.2

## Setup

Copy [Visual C++ 4.2] and [DirectX 1.0] into this directory.

[Visual C++ 4.2]: https://winworldpc.com/download/67c2abc3-af03-e280-93c3-9611c3a6efbf
[DirectX 1.0]: https://archive.org/details/gamesdk

## Build

	docker build -t registry.gitlab.com/moralbacteria/diablo-prdemo-decomp/build_vc42 .

## Push

	docker login registry.gitlab.com
	docker push registry.gitlab.com/moralbacteria/diablo-prdemo-decomp/build_vc42

This image is hosted on this repo: https://gitlab.com/moralbacteria/diablo-prdemo-decomp/container_registry

For help logging into the GitLab registry: https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry