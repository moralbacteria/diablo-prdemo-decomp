#include "lighting.h"

#include "diablo.h"
#include "engine.h"
#include "gendung.h"
#include "player.h"

//
// Initialized variables
//

DWORD dword_4ACBF8 = 0; // unused
// If `TRUE` then turn off dynamic lighting and use "precomputed lighting". (This typically means fullbright, max lighting)
// If `FALSE` then use dynamic lighting.
BOOL lightflag = FALSE;

// TODO: From Devilution, verify!
char CrawlTable[2749] = {
    1,
    0, 0,
    4,
    0, 1, 0, -1, -1, 0, 1, 0,
    16,
    0, 2, 0, -2, -1, 2, 1, 2,
    -1, -2, 1, -2, -1, 1, 1, 1,
    -1, -1, 1, -1, -2, 1, 2, 1,
    -2, -1, 2, -1, -2, 0, 2, 0,
    24,
    0, 3, 0, -3, -1, 3, 1, 3,
    -1, -3, 1, -3, -2, 3, 2, 3,
    -2, -3, 2, -3, -2, 2, 2, 2,
    -2, -2, 2, -2, -3, 2, 3, 2,
    -3, -2, 3, -2, -3, 1, 3, 1,
    -3, -1, 3, -1, -3, 0, 3, 0,
    32,
    0, 4, 0, -4, -1, 4, 1, 4,
    -1, -4, 1, -4, -2, 4, 2, 4,
    -2, -4, 2, -4, -3, 4, 3, 4,
    -3, -4, 3, -4, -3, 3, 3, 3,
    -3, -3, 3, -3, -4, 3, 4, 3,
    -4, -3, 4, -3, -4, 2, 4, 2,
    -4, -2, 4, -2, -4, 1, 4, 1,
    -4, -1, 4, -1, -4, 0, 4, 0,
    40,
    0, 5, 0, -5, -1, 5, 1, 5,
    -1, -5, 1, -5, -2, 5, 2, 5,
    -2, -5, 2, -5, -3, 5, 3, 5,
    -3, -5, 3, -5, -4, 5, 4, 5,
    -4, -5, 4, -5, -4, 4, 4, 4,
    -4, -4, 4, -4, -5, 4, 5, 4,
    -5, -4, 5, -4, -5, 3, 5, 3,
    -5, -3, 5, -3, -5, 2, 5, 2,
    -5, -2, 5, -2, -5, 1, 5, 1,
    -5, -1, 5, -1, -5, 0, 5, 0,
    48,
    0, 6, 0, -6, -1, 6, 1, 6,
    -1, -6, 1, -6, -2, 6, 2, 6,
    -2, -6, 2, -6, -3, 6, 3, 6,
    -3, -6, 3, -6, -4, 6, 4, 6,
    -4, -6, 4, -6, -5, 6, 5, 6,
    -5, -6, 5, -6, -5, 5, 5, 5,
    -5, -5, 5, -5, -6, 5, 6, 5,
    -6, -5, 6, -5, -6, 4, 6, 4,
    -6, -4, 6, -4, -6, 3, 6, 3,
    -6, -3, 6, -3, -6, 2, 6, 2,
    -6, -2, 6, -2, -6, 1, 6, 1,
    -6, -1, 6, -1, -6, 0, 6, 0,
    56,
    0, 7, 0, -7, -1, 7, 1, 7,
    -1, -7, 1, -7, -2, 7, 2, 7,
    -2, -7, 2, -7, -3, 7, 3, 7,
    -3, -7, 3, -7, -4, 7, 4, 7,
    -4, -7, 4, -7, -5, 7, 5, 7,
    -5, -7, 5, -7, -6, 7, 6, 7,
    -6, -7, 6, -7, -6, 6, 6, 6,
    -6, -6, 6, -6, -7, 6, 7, 6,
    -7, -6, 7, -6, -7, 5, 7, 5,
    -7, -5, 7, -5, -7, 4, 7, 4,
    -7, -4, 7, -4, -7, 3, 7, 3,
    -7, -3, 7, -3, -7, 2, 7, 2,
    -7, -2, 7, -2, -7, 1, 7, 1,
    -7, -1, 7, -1, -7, 0, 7, 0,
    64,
    0, 8, 0, -8, -1, 8, 1, 8,
    -1, -8, 1, -8, -2, 8, 2, 8,
    -2, -8, 2, -8, -3, 8, 3, 8,
    -3, -8, 3, -8, -4, 8, 4, 8,
    -4, -8, 4, -8, -5, 8, 5, 8,
    -5, -8, 5, -8, -6, 8, 6, 8,
    -6, -8, 6, -8, -7, 8, 7, 8,
    -7, -8, 7, -8, -7, 7, 7, 7,
    -7, -7, 7, -7, -8, 7, 8, 7,
    -8, -7, 8, -7, -8, 6, 8, 6,
    -8, -6, 8, -6, -8, 5, 8, 5,
    -8, -5, 8, -5, -8, 4, 8, 4,
    -8, -4, 8, -4, -8, 3, 8, 3,
    -8, -3, 8, -3, -8, 2, 8, 2,
    -8, -2, 8, -2, -8, 1, 8, 1,
    -8, -1, 8, -1, -8, 0, 8, 0,
    72,
    0, 9, 0, -9, -1, 9, 1, 9,
    -1, -9, 1, -9, -2, 9, 2, 9,
    -2, -9, 2, -9, -3, 9, 3, 9,
    -3, -9, 3, -9, -4, 9, 4, 9,
    -4, -9, 4, -9, -5, 9, 5, 9,
    -5, -9, 5, -9, -6, 9, 6, 9,
    -6, -9, 6, -9, -7, 9, 7, 9,
    -7, -9, 7, -9, -8, 9, 8, 9,
    -8, -9, 8, -9, -8, 8, 8, 8,
    -8, -8, 8, -8, -9, 8, 9, 8,
    -9, -8, 9, -8, -9, 7, 9, 7,
    -9, -7, 9, -7, -9, 6, 9, 6,
    -9, -6, 9, -6, -9, 5, 9, 5,
    -9, -5, 9, -5, -9, 4, 9, 4,
    -9, -4, 9, -4, -9, 3, 9, 3,
    -9, -3, 9, -3, -9, 2, 9, 2,
    -9, -2, 9, -2, -9, 1, 9, 1,
    -9, -1, 9, -1, -9, 0, 9, 0,
    80,
    0, 10, 0, -10, -1, 10, 1, 10,
    -1, -10, 1, -10, -2, 10, 2, 10,
    -2, -10, 2, -10, -3, 10, 3, 10,
    -3, -10, 3, -10, -4, 10, 4, 10,
    -4, -10, 4, -10, -5, 10, 5, 10,
    -5, -10, 5, -10, -6, 10, 6, 10,
    -6, -10, 6, -10, -7, 10, 7, 10,
    -7, -10, 7, -10, -8, 10, 8, 10,
    -8, -10, 8, -10, -9, 10, 9, 10,
    -9, -10, 9, -10, -9, 9, 9, 9,
    -9, -9, 9, -9, -10, 9, 10, 9,
    -10, -9, 10, -9, -10, 8, 10, 8,
    -10, -8, 10, -8, -10, 7, 10, 7,
    -10, -7, 10, -7, -10, 6, 10, 6,
    -10, -6, 10, -6, -10, 5, 10, 5,
    -10, -5, 10, -5, -10, 4, 10, 4,
    -10, -4, 10, -4, -10, 3, 10, 3,
    -10, -3, 10, -3, -10, 2, 10, 2,
    -10, -2, 10, -2, -10, 1, 10, 1,
    -10, -1, 10, -1, -10, 0, 10, 0,
    88,
    0, 11, 0, -11, -1, 11, 1, 11,
    -1, -11, 1, -11, -2, 11, 2, 11,
    -2, -11, 2, -11, -3, 11, 3, 11,
    -3, -11, 3, -11, -4, 11, 4, 11,
    -4, -11, 4, -11, -5, 11, 5, 11,
    -5, -11, 5, -11, -6, 11, 6, 11,
    -6, -11, 6, -11, -7, 11, 7, 11,
    -7, -11, 7, -11, -8, 11, 8, 11,
    -8, -11, 8, -11, -9, 11, 9, 11,
    -9, -11, 9, -11, -10, 11, 10, 11,
    -10, -11, 10, -11, -10, 10, 10, 10,
    -10, -10, 10, -10, -11, 10, 11, 10,
    -11, -10, 11, -10, -11, 9, 11, 9,
    -11, -9, 11, -9, -11, 8, 11, 8,
    -11, -8, 11, -8, -11, 7, 11, 7,
    -11, -7, 11, -7, -11, 6, 11, 6,
    -11, -6, 11, -6, -11, 5, 11, 5,
    -11, -5, 11, -5, -11, 4, 11, 4,
    -11, -4, 11, -4, -11, 3, 11, 3,
    -11, -3, 11, -3, -11, 2, 11, 2,
    -11, -2, 11, -2, -11, 1, 11, 1,
    -11, -1, 11, -1, -11, 0, 11, 0,
    96,
    0, 12, 0, -12, -1, 12, 1, 12,
    -1, -12, 1, -12, -2, 12, 2, 12,
    -2, -12, 2, -12, -3, 12, 3, 12,
    -3, -12, 3, -12, -4, 12, 4, 12,
    -4, -12, 4, -12, -5, 12, 5, 12,
    -5, -12, 5, -12, -6, 12, 6, 12,
    -6, -12, 6, -12, -7, 12, 7, 12,
    -7, -12, 7, -12, -8, 12, 8, 12,
    -8, -12, 8, -12, -9, 12, 9, 12,
    -9, -12, 9, -12, -10, 12, 10, 12,
    -10, -12, 10, -12, -11, 12, 11, 12,
    -11, -12, 11, -12, -11, 11, 11, 11,
    -11, -11, 11, -11, -12, 11, 12, 11,
    -12, -11, 12, -11, -12, 10, 12, 10,
    -12, -10, 12, -10, -12, 9, 12, 9,
    -12, -9, 12, -9, -12, 8, 12, 8,
    -12, -8, 12, -8, -12, 7, 12, 7,
    -12, -7, 12, -7, -12, 6, 12, 6,
    -12, -6, 12, -6, -12, 5, 12, 5,
    -12, -5, 12, -5, -12, 4, 12, 4,
    -12, -4, 12, -4, -12, 3, 12, 3,
    -12, -3, 12, -3, -12, 2, 12, 2,
    -12, -2, 12, -2, -12, 1, 12, 1,
    -12, -1, 12, -1, -12, 0, 12, 0,
    104,
    0, 13, 0, -13, -1, 13, 1, 13,
    -1, -13, 1, -13, -2, 13, 2, 13,
    -2, -13, 2, -13, -3, 13, 3, 13,
    -3, -13, 3, -13, -4, 13, 4, 13,
    -4, -13, 4, -13, -5, 13, 5, 13,
    -5, -13, 5, -13, -6, 13, 6, 13,
    -6, -13, 6, -13, -7, 13, 7, 13,
    -7, -13, 7, -13, -8, 13, 8, 13,
    -8, -13, 8, -13, -9, 13, 9, 13,
    -9, -13, 9, -13, -10, 13, 10, 13,
    -10, -13, 10, -13, -11, 13, 11, 13,
    -11, -13, 11, -13, -12, 13, 12, 13,
    -12, -13, 12, -13, -12, 12, 12, 12,
    -12, -12, 12, -12, -13, 12, 13, 12,
    -13, -12, 13, -12, -13, 11, 13, 11,
    -13, -11, 13, -11, -13, 10, 13, 10,
    -13, -10, 13, -10, -13, 9, 13, 9,
    -13, -9, 13, -9, -13, 8, 13, 8,
    -13, -8, 13, -8, -13, 7, 13, 7,
    -13, -7, 13, -7, -13, 6, 13, 6,
    -13, -6, 13, -6, -13, 5, 13, 5,
    -13, -5, 13, -5, -13, 4, 13, 4,
    -13, -4, 13, -4, -13, 3, 13, 3,
    -13, -3, 13, -3, -13, 2, 13, 2,
    -13, -2, 13, -2, -13, 1, 13, 1,
    -13, -1, 13, -1, -13, 0, 13, 0,
    112,
    0, 14, 0, -14, -1, 14, 1, 14,
    -1, -14, 1, -14, -2, 14, 2, 14,
    -2, -14, 2, -14, -3, 14, 3, 14,
    -3, -14, 3, -14, -4, 14, 4, 14,
    -4, -14, 4, -14, -5, 14, 5, 14,
    -5, -14, 5, -14, -6, 14, 6, 14,
    -6, -14, 6, -14, -7, 14, 7, 14,
    -7, -14, 7, -14, -8, 14, 8, 14,
    -8, -14, 8, -14, -9, 14, 9, 14,
    -9, -14, 9, -14, -10, 14, 10, 14,
    -10, -14, 10, -14, -11, 14, 11, 14,
    -11, -14, 11, -14, -12, 14, 12, 14,
    -12, -14, 12, -14, -13, 14, 13, 14,
    -13, -14, 13, -14, -13, 13, 13, 13,
    -13, -13, 13, -13, -14, 13, 14, 13,
    -14, -13, 14, -13, -14, 12, 14, 12,
    -14, -12, 14, -12, -14, 11, 14, 11,
    -14, -11, 14, -11, -14, 10, 14, 10,
    -14, -10, 14, -10, -14, 9, 14, 9,
    -14, -9, 14, -9, -14, 8, 14, 8,
    -14, -8, 14, -8, -14, 7, 14, 7,
    -14, -7, 14, -7, -14, 6, 14, 6,
    -14, -6, 14, -6, -14, 5, 14, 5,
    -14, -5, 14, -5, -14, 4, 14, 4,
    -14, -4, 14, -4, -14, 3, 14, 3,
    -14, -3, 14, -3, -14, 2, 14, 2,
    -14, -2, 14, -2, -14, 1, 14, 1,
    -14, -1, 14, -1, -14, 0, 14, 0,
    120,
    0, 15, 0, -15, -1, 15, 1, 15,
    -1, -15, 1, -15, -2, 15, 2, 15,
    -2, -15, 2, -15, -3, 15, 3, 15,
    -3, -15, 3, -15, -4, 15, 4, 15,
    -4, -15, 4, -15, -5, 15, 5, 15,
    -5, -15, 5, -15, -6, 15, 6, 15,
    -6, -15, 6, -15, -7, 15, 7, 15,
    -7, -15, 7, -15, -8, 15, 8, 15,
    -8, -15, 8, -15, -9, 15, 9, 15,
    -9, -15, 9, -15, -10, 15, 10, 15,
    -10, -15, 10, -15, -11, 15, 11, 15,
    -11, -15, 11, -15, -12, 15, 12, 15,
    -12, -15, 12, -15, -13, 15, 13, 15,
    -13, -15, 13, -15, -14, 15, 14, 15,
    -14, -15, 14, -15, -14, 14, 14, 14,
    -14, -14, 14, -14, -15, 14, 15, 14,
    -15, -14, 15, -14, -15, 13, 15, 13,
    -15, -13, 15, -13, -15, 12, 15, 12,
    -15, -12, 15, -12, -15, 11, 15, 11,
    -15, -11, 15, -11, -15, 10, 15, 10,
    -15, -10, 15, -10, -15, 9, 15, 9,
    -15, -9, 15, -9, -15, 8, 15, 8,
    -15, -8, 15, -8, -15, 7, 15, 7,
    -15, -7, 15, -7, -15, 6, 15, 6,
    -15, -6, 15, -6, -15, 5, 15, 5,
    -15, -5, 15, -5, -15, 4, 15, 4,
    -15, -4, 15, -4, -15, 3, 15, 3,
    -15, -3, 15, -3, -15, 2, 15, 2,
    -15, -2, 15, -2, -15, 1, 15, 1,
    -15, -1, 15, -1, -15, 0, 15, 0,
    (char)128,
    0, 16, 0, -16, -1, 16, 1, 16,
    -1, -16, 1, -16, -2, 16, 2, 16,
    -2, -16, 2, -16, -3, 16, 3, 16,
    -3, -16, 3, -16, -4, 16, 4, 16,
    -4, -16, 4, -16, -5, 16, 5, 16,
    -5, -16, 5, -16, -6, 16, 6, 16,
    -6, -16, 6, -16, -7, 16, 7, 16,
    -7, -16, 7, -16, -8, 16, 8, 16,
    -8, -16, 8, -16, -9, 16, 9, 16,
    -9, -16, 9, -16, -10, 16, 10, 16,
    -10, -16, 10, -16, -11, 16, 11, 16,
    -11, -16, 11, -16, -12, 16, 12, 16,
    -12, -16, 12, -16, -13, 16, 13, 16,
    -13, -16, 13, -16, -14, 16, 14, 16,
    -14, -16, 14, -16, -15, 16, 15, 16,
    -15, -16, 15, -16, -15, 15, 15, 15,
    -15, -15, 15, -15, -16, 15, 16, 15,
    -16, -15, 16, -15, -16, 14, 16, 14,
    -16, -14, 16, -14, -16, 13, 16, 13,
    -16, -13, 16, -13, -16, 12, 16, 12,
    -16, -12, 16, -12, -16, 11, 16, 11,
    -16, -11, 16, -11, -16, 10, 16, 10,
    -16, -10, 16, -10, -16, 9, 16, 9,
    -16, -9, 16, -9, -16, 8, 16, 8,
    -16, -8, 16, -8, -16, 7, 16, 7,
    -16, -7, 16, -7, -16, 6, 16, 6,
    -16, -6, 16, -6, -16, 5, 16, 5,
    -16, -5, 16, -5, -16, 4, 16, 4,
    -16, -4, 16, -4, -16, 3, 16, 3,
    -16, -3, 16, -3, -16, 2, 16, 2,
    -16, -2, 16, -2, -16, 1, 16, 1,
    -16, -1, 16, -1, -16, 0, 16, 0,
    (char)136,
    0, 17, 0, -17, -1, 17, 1, 17,
    -1, -17, 1, -17, -2, 17, 2, 17,
    -2, -17, 2, -17, -3, 17, 3, 17,
    -3, -17, 3, -17, -4, 17, 4, 17,
    -4, -17, 4, -17, -5, 17, 5, 17,
    -5, -17, 5, -17, -6, 17, 6, 17,
    -6, -17, 6, -17, -7, 17, 7, 17,
    -7, -17, 7, -17, -8, 17, 8, 17,
    -8, -17, 8, -17, -9, 17, 9, 17,
    -9, -17, 9, -17, -10, 17, 10, 17,
    -10, -17, 10, -17, -11, 17, 11, 17,
    -11, -17, 11, -17, -12, 17, 12, 17,
    -12, -17, 12, -17, -13, 17, 13, 17,
    -13, -17, 13, -17, -14, 17, 14, 17,
    -14, -17, 14, -17, -15, 17, 15, 17,
    -15, -17, 15, -17, -16, 17, 16, 17,
    -16, -17, 16, -17, -16, 16, 16, 16,
    -16, -16, 16, -16, -17, 16, 17, 16,
    -17, -16, 17, -16, -17, 15, 17, 15,
    -17, -15, 17, -15, -17, 14, 17, 14,
    -17, -14, 17, -14, -17, 13, 17, 13,
    -17, -13, 17, -13, -17, 12, 17, 12,
    -17, -12, 17, -12, -17, 11, 17, 11,
    -17, -11, 17, -11, -17, 10, 17, 10,
    -17, -10, 17, -10, -17, 9, 17, 9,
    -17, -9, 17, -9, -17, 8, 17, 8,
    -17, -8, 17, -8, -17, 7, 17, 7,
    -17, -7, 17, -7, -17, 6, 17, 6,
    -17, -6, 17, -6, -17, 5, 17, 5,
    -17, -5, 17, -5, -17, 4, 17, 4,
    -17, -4, 17, -4, -17, 3, 17, 3,
    -17, -3, 17, -3, -17, 2, 17, 2,
    -17, -2, 17, -2, -17, 1, 17, 1,
    -17, -1, 17, -1, -17, 0, 17, 0,
    (char)144,
    0, 18, 0, -18, -1, 18, 1, 18,
    -1, -18, 1, -18, -2, 18, 2, 18,
    -2, -18, 2, -18, -3, 18, 3, 18,
    -3, -18, 3, -18, -4, 18, 4, 18,
    -4, -18, 4, -18, -5, 18, 5, 18,
    -5, -18, 5, -18, -6, 18, 6, 18,
    -6, -18, 6, -18, -7, 18, 7, 18,
    -7, -18, 7, -18, -8, 18, 8, 18,
    -8, -18, 8, -18, -9, 18, 9, 18,
    -9, -18, 9, -18, -10, 18, 10, 18,
    -10, -18, 10, -18, -11, 18, 11, 18,
    -11, -18, 11, -18, -12, 18, 12, 18,
    -12, -18, 12, -18, -13, 18, 13, 18,
    -13, -18, 13, -18, -14, 18, 14, 18,
    -14, -18, 14, -18, -15, 18, 15, 18,
    -15, -18, 15, -18, -16, 18, 16, 18,
    -16, -18, 16, -18, -17, 18, 17, 18,
    -17, -18, 17, -18, -17, 17, 17, 17,
    -17, -17, 17, -17, -18, 17, 18, 17,
    -18, -17, 18, -17, -18, 16, 18, 16,
    -18, -16, 18, -16, -18, 15, 18, 15,
    -18, -15, 18, -15, -18, 14, 18, 14,
    -18, -14, 18, -14, -18, 13, 18, 13,
    -18, -13, 18, -13, -18, 12, 18, 12,
    -18, -12, 18, -12, -18, 11, 18, 11,
    -18, -11, 18, -11, -18, 10, 18, 10,
    -18, -10, 18, -10, -18, 9, 18, 9,
    -18, -9, 18, -9, -18, 8, 18, 8,
    -18, -8, 18, -8, -18, 7, 18, 7,
    -18, -7, 18, -7, -18, 6, 18, 6,
    -18, -6, 18, -6, -18, 5, 18, 5,
    -18, -5, 18, -5, -18, 4, 18, 4,
    -18, -4, 18, -4, -18, 3, 18, 3,
    -18, -3, 18, -3, -18, 2, 18, 2,
    -18, -2, 18, -2, -18, 1, 18, 1,
    -18, -1, 18, -1, -18, 0, 18, 0};
// Unused
char *pCrawlTable[19] = {
    CrawlTable,
    CrawlTable + 3,
    CrawlTable + 12,
    CrawlTable + 45,
    CrawlTable + 94,
    CrawlTable + 159,
    CrawlTable + 240,
    CrawlTable + 337,
    CrawlTable + 450,
    CrawlTable + 579,
    CrawlTable + 724,
    CrawlTable + 885,
    CrawlTable + 1062,
    CrawlTable + 1255,
    CrawlTable + 1464,
    CrawlTable + 1689,
    CrawlTable + 1930,
    CrawlTable + 2187,
    CrawlTable + 2460};
// TODO: From Devilution, verify!
// TODO: are these actually X-Y pairs? would a struct be better?
BYTE vCrawlTable[23][30] = {
    {1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0, 11, 0, 12, 0, 13, 0, 14, 0, 15, 0},
    {1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 1, 9, 1, 10, 1, 11, 1, 12, 1, 13, 1, 14, 1, 15, 1},
    {1, 0, 2, 0, 3, 0, 4, 1, 5, 1, 6, 1, 7, 1, 8, 1, 9, 1, 10, 1, 11, 1, 12, 2, 13, 2, 14, 2, 15, 2},
    {1, 0, 2, 0, 3, 1, 4, 1, 5, 1, 6, 1, 7, 1, 8, 2, 9, 2, 10, 2, 11, 2, 12, 2, 13, 3, 14, 3, 15, 3},
    {1, 0, 2, 1, 3, 1, 4, 1, 5, 1, 6, 2, 7, 2, 8, 2, 9, 3, 10, 3, 11, 3, 12, 3, 13, 4, 14, 4, 0, 0},
    {1, 0, 2, 1, 3, 1, 4, 1, 5, 2, 6, 2, 7, 3, 8, 3, 9, 3, 10, 4, 11, 4, 12, 4, 13, 5, 14, 5, 0, 0},
    {1, 0, 2, 1, 3, 1, 4, 2, 5, 2, 6, 3, 7, 3, 8, 3, 9, 4, 10, 4, 11, 5, 12, 5, 13, 6, 14, 6, 0, 0},
    {1, 1, 2, 1, 3, 2, 4, 2, 5, 3, 6, 3, 7, 4, 8, 4, 9, 5, 10, 5, 11, 6, 12, 6, 13, 7, 0, 0, 0, 0},
    {1, 1, 2, 1, 3, 2, 4, 2, 5, 3, 6, 4, 7, 4, 8, 5, 9, 6, 10, 6, 11, 7, 12, 7, 12, 8, 13, 8, 0, 0},
    {1, 1, 2, 2, 3, 2, 4, 3, 5, 4, 6, 5, 7, 5, 8, 6, 9, 7, 10, 7, 10, 8, 11, 8, 12, 9, 0, 0, 0, 0},
    {1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 5, 7, 6, 8, 7, 9, 8, 10, 9, 11, 9, 11, 10, 0, 0, 0, 0, 0, 0},
    {1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 9, 11, 10, 11, 0, 0, 0, 0, 0, 0},
    {1, 1, 2, 2, 2, 3, 3, 4, 4, 5, 5, 6, 5, 7, 6, 8, 7, 9, 7, 10, 8, 10, 8, 11, 9, 12, 0, 0, 0, 0},
    {1, 1, 1, 2, 2, 3, 2, 4, 3, 5, 4, 6, 4, 7, 5, 8, 6, 9, 6, 10, 7, 11, 7, 12, 8, 12, 8, 13, 0, 0},
    {1, 1, 1, 2, 2, 3, 2, 4, 3, 5, 3, 6, 4, 7, 4, 8, 5, 9, 5, 10, 6, 11, 6, 12, 7, 13, 0, 0, 0, 0},
    {0, 1, 1, 2, 1, 3, 2, 4, 2, 5, 3, 6, 3, 7, 3, 8, 4, 9, 4, 10, 5, 11, 5, 12, 6, 13, 6, 14, 0, 0},
    {0, 1, 1, 2, 1, 3, 1, 4, 2, 5, 2, 6, 3, 7, 3, 8, 3, 9, 4, 10, 4, 11, 4, 12, 5, 13, 5, 14, 0, 0},
    {0, 1, 1, 2, 1, 3, 1, 4, 1, 5, 2, 6, 2, 7, 2, 8, 3, 9, 3, 10, 3, 11, 3, 12, 4, 13, 4, 14, 0, 0},
    {0, 1, 0, 2, 1, 3, 1, 4, 1, 5, 1, 6, 1, 7, 2, 8, 2, 9, 2, 10, 2, 11, 2, 12, 3, 13, 3, 14, 3, 15},
    {0, 1, 0, 2, 0, 3, 1, 4, 1, 5, 1, 6, 1, 7, 1, 8, 1, 9, 1, 10, 1, 11, 2, 12, 2, 13, 2, 14, 2, 15},
    {0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 1, 8, 1, 9, 1, 10, 1, 11, 1, 12, 1, 13, 1, 14, 1, 15},
    {0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0, 11, 0, 12, 0, 13, 0, 14, 0, 15}};
// Unused. Reminds me of Dirs in GetDirection16 :thinking:
// Does this have something to do with light4flag?
BYTE byte_4AD9C8[18][18] = {
    {0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
    {0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
    {0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
    {0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
    {0, 0, 1, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
    {0, 0, 1, 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
    {0, 0, 0, 1, 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
    {0, 0, 0, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
    {0, 0, 0, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3},
    {0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3},
    {0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3},
    {0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3},
    {0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3},
    {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3},
    {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3},
    {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3},
    {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3},
    {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2}};
// TODO: From Devilution, verify!
BYTE RadiusAdj[23] = {0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 4, 3, 2, 2, 2, 1, 1, 1, 0, 0, 0, 0};

//
// Uninitialized variables (.data:005F1AB0)
//

// Current number of active entries in LightList.
int numlights;
LightListStruct VisionList[MAXVISION];
int visionid;
BYTE lightblock[64][16][16];
LightListStruct LightList[MAXLIGHTS];
char lightmax;
// 0x1B00 bytes (27*256)
// Something to do with .TRN?
BYTE *pLightTbl;
BYTE lightradius[16][128];
BOOL dovision;
int numvision;
BOOL dolighting;
// _lid to use for new lights added with AddLight; autoinc
int nextLightId;

//
// code (.text:0042E530)
//

// .text:0042E530
void RotateRadius(int *x, int *y, int *dx, int *dy, int *lx, int *ly, int *bx, int *by)
{
    int swap;

    *bx = 0;
    *by = 0;

    swap = *dx;
    *dx = 7 - *dy;
    *dy = swap;
    swap = *lx;
    *lx = 7 - *ly;
    *ly = swap;

    *x = *dx - *lx;
    *y = *dy - *ly;

    if (*x < 0)
    {
        *x += 8;
        *bx = 1;
    }
    if (*y < 0)
    {
        *y += 8;
        *by = 1;
    }
}

// .text:0042E5EF
// (`nXPos`, `nYPos`) the the light location; it forms the origin/center of the lighting calcs.

void DoLighting(int nXPos, int nYPos, int nRadius, int Lnum)
{
    int xoff, yoff; // _xoff and _yoff of ListLight[Lnum]
    int dist_x, dist_y, min_x, max_x, min_y, max_y, mult, x, y, radius_block, temp_x, temp_y, v, light_x, light_y, block_x, block_y;

    xoff = 0;
    yoff = 0;
    light_x = 0;
    light_y = 0;
    block_x = 0;
    block_y = 0;

    if (Lnum >= 0)
    {
        // Treat lights on the negative border as instead lights on the tile it's closest too but on the positive border
        xoff = LightList[Lnum]._xoff;
        yoff = LightList[Lnum]._yoff;
        if (xoff < 0)
        {
            xoff += 8;
            nXPos--;
        }
        if (yoff < 0)
        {
            yoff += 8;
            nYPos--;
        }
    }

    dist_x = xoff;
    dist_y = yoff;

    // Constrain light calcs to 16 tiles within level bounds.
    if (nXPos - 15 < 0)
    {
        min_x = nXPos + 1;
    }
    else
    {
        min_x = 15;
    }
    if (nXPos + 15 > MAXDUNX)
    {
        max_x = MAXDUNX - nXPos;
    }
    else
    {
        max_x = 15;
    }
    if (nYPos - 15 < 0)
    {
        min_y = nYPos + 1;
    }
    else
    {
        min_y = 15;
    }
    if (nYPos + 15 > MAXDUNY)
    {
        max_y = MAXDUNY - nYPos;
    }
    else
    {
        max_y = 15;
    }

    dLight[nXPos][nYPos] = 0;

    // `mult` is kinda like an index into `int foo[8][8]`
    mult = xoff + 8 * yoff;
    for (y = 0; y < min_y; y++)
    {
        for (x = 1; x < max_x; x++)
        {
            radius_block = lightblock[mult][y][x];
            if (radius_block < 128)
            {
                temp_x = nXPos + x;
                temp_y = nYPos + y;
                v = lightradius[nRadius][radius_block];
                if (v < dLight[temp_x][temp_y])
                {
                    dLight[temp_x][temp_y] = v;
                }
            }
        }
    }

    // Do it again I wasn't looking
    RotateRadius(&xoff, &yoff, &dist_x, &dist_y, &light_x, &light_y, &block_x, &block_y);
    mult = xoff + 8 * yoff;
    for (y = 0; y < max_y; y++)
    {
        for (x = 1; x < max_x; x++)
        {
            radius_block = lightblock[mult][y + block_y][x + block_x];
            if (radius_block < 128)
            {
                temp_x = nXPos + y;
                temp_y = nYPos - x;
                v = lightradius[nRadius][radius_block];
                if (v < dLight[temp_x][temp_y])
                {
                    dLight[temp_x][temp_y] = v;
                }
            }
        }
    }

    // Do it again I wasn't looking
    RotateRadius(&xoff, &yoff, &dist_x, &dist_y, &light_x, &light_y, &block_x, &block_y);
    mult = xoff + 8 * yoff;
    for (y = 0; y < max_y; y++)
    {
        for (x = 1; x < min_x; x++)
        {
            radius_block = lightblock[mult][y + block_y][x + block_x];
            if (radius_block < 128)
            {
                temp_x = nXPos - x;
                temp_y = nYPos - y;
                v = lightradius[nRadius][radius_block];
                if (v < dLight[temp_x][temp_y])
                {
                    dLight[temp_x][temp_y] = v;
                }
            }
        }
    }

    // Do it again I wasn't looking
    RotateRadius(&xoff, &yoff, &dist_x, &dist_y, &light_x, &light_y, &block_x, &block_y);
    mult = xoff + 8 * yoff;
    for (y = 0; y < min_y; y++)
    {
        for (x = 1; x < min_x; x++)
        {
            radius_block = lightblock[mult][y + block_y][x + block_x];
            if (radius_block < 128)
            {
                temp_x = nXPos - y;
                temp_y = nYPos + x;
                v = lightradius[nRadius][radius_block];
                if (v < dLight[temp_x][temp_y])
                {
                    dLight[temp_x][temp_y] = v;
                }
            }
        }
    }
}

// DoUnLight    000000000042EB01
// DoUnVision    000000000042EBF3
void DoUnVision(int nXPos, int nYPos, int nRadius)
{
    // TODO
}

// DoVision    000000000042ECE8
void DoVision(int nXPos, int nYPos, int nRadius)
{
    int k; // line length iterator?
    int v; // cardinal direction iterator
    int j; // radius iterator?
    BOOL nBlockerFlag;
    int nLineLen;
    int x1adj;
    int y1adj;
    int x2adj;
    int y2adj;
    int nCrawlX;
    int nCrawlY;

    for (k = 0; k < TransVal; k++)
    {
        TransList[k] = FALSE;
    }

    dFlags[nXPos][nYPos] |= BFLAG_LIT;

    // There are four cardinal directions: up, down, left, right (not in order)
    for (v = 0; v < 4; v++)
    {
        for (j = 0; j < 23; j++) // TODO magic number
        {
            nBlockerFlag = FALSE; // TODO this might not be the same var as devilution
            nLineLen = 2 * (nRadius - RadiusAdj[j]);
            for (k = 0; k < nLineLen; k += 2)
            {
                x1adj = 0;
                x2adj = 0;
                y1adj = 0;
                y2adj = 0;
                switch (v)
                {
                case 0:
                    nCrawlX = nXPos + vCrawlTable[j][k];
                    nCrawlY = nYPos + vCrawlTable[j][k + 1];
                    if (vCrawlTable[j][k] > 0 && vCrawlTable[j][k + 1] > 0)
                    {
                        x1adj = -1;
                        y2adj = -1;
                    }
                    break;
                case 1:
                    nCrawlX = nXPos - vCrawlTable[j][k];
                    nCrawlY = nYPos - vCrawlTable[j][k + 1];
                    if (vCrawlTable[j][k] > 0 && vCrawlTable[j][k + 1] > 0)
                    {
                        y1adj = 1;
                        x2adj = 1;
                    }
                    break;
                case 2:
                    nCrawlX = nXPos + vCrawlTable[j][k];
                    nCrawlY = nYPos - vCrawlTable[j][k + 1];
                    if (vCrawlTable[j][k] > 0 && vCrawlTable[j][k + 1] > 0)
                    {
                        x1adj = -1;
                        y2adj = 1;
                    }
                    break;
                case 3:
                    nCrawlX = nXPos - vCrawlTable[j][k];
                    nCrawlY = nYPos + vCrawlTable[j][k + 1];
                    if (vCrawlTable[j][k] > 0 && vCrawlTable[j][k + 1] > 0)
                    {
                        y1adj = -1;
                        x2adj = 1;
                    }
                    break;
                }
                if (nCrawlX >= 0 && nCrawlX <= MAXDUNX && nCrawlY >= 0 && nCrawlY <= MAXDUNY)
                {
                    // TODO this is pretty different from devilution
                }
            }
        }
    }
}

// .text:0042F267
// TODO wtf is this doing
void MakeLightTable()
{
    BYTE *tbl;
    int shade;
    int lights;
    int i;
    int j;
    int k;
    BYTE col;
    BYTE max;
    BYTE *trn;
    BYTE *trn_;

    // 0x1B00 == 6912 == 27*256
    pLightTbl = (BYTE *)DiabloAllocPtr(0x1B00);

    tbl = pLightTbl;
    shade = 0;

    if (light4flag)
    {
        lights = 3;
    }
    else
    {
        lights = 15;
    }

    for (i = 0; i < lights; i++)
    {
        *tbl++ = 0;
        // TODO magic number
        for (j = 0; j < 8; j++)
        {
            // TODO magic numbers galore
            col = 16 * j + shade;
            max = 16 * j + 15;

            // TODO magic number
            for (k = 0; k < 16; k++)
            {
                // TODO
            }
        }
    }

    // TODO

    for (j = 0; j < 256; j++)
    {
        *tbl++ = 0;
    }

    trn = LoadFileInMem("PlrGFX\\Infra.TRN");
    trn_ = trn;
    for (j = 0; j < 256; j++)
    {
        *tbl++ = *trn_++;
    }
    MemFreeDbg(trn);

    trn = LoadFileInMem("PlrGFX\\Stone.TRN");
    trn_ = trn;
    for (j = 0; j < 256; j++)
    {
        *tbl++ = *trn_++;
    }
    MemFreeDbg(trn);

    // TODO
}

// ResetLight    000000000042F8E5

// .text:0042FA32
void ToggleLighting()
{
    int x, y;

    lightflag ^= TRUE;
    if (lightflag)
    {
        for (y = 0; y < MAXDUNY; ++y)
        {
            for (x = 0; y < MAXDUNX; ++y)
            {
                dLight[x][y] = 0;
            }
        }
    }
    else
    {
        for (y = 0; y < MAXDUNY; ++y)
        {
            for (x = 0; y < MAXDUNX; ++y)
            {
                dLight[x][y] = dPreLight[x][y];
            }
        }
        for (x = 0; x <= gbActivePlayers; x++)
        {
            DoLighting(plr[x]._px, plr[x]._py, plr[x]._pLightRad, -1);
        }
    }
}

// .text:0042FB92
// Sets how many light levels are available.
void InitLightMax()
{
    if (light4flag)
    {
        lightmax = 3;
    }
    else
    {
        lightmax = 15;
    }
}

// .text:0042FBC2
// Set initial values for the lighting module.
void InitLighting()
{
    numlights = 0;
    dolighting = FALSE;
    lightflag = FALSE;
    nextLightId = 1;
}

// .text:0042FBFA
// Creates a new light.
// (`x`, `y`) is the new location for the light
// `r` is the new light radius.
int AddLight(int x, int y, int r)
{
    int lid;

    if (lightflag)
    {
        return -1;
    }

    lid = -1;

    if (numlights < MAXLIGHTS)
    {
        // Add the light to the tracking list.
        LightList[numlights]._lx = x;
        LightList[numlights]._ly = y;
        LightList[numlights]._lradius = r;
        LightList[numlights]._xoff = 0;
        LightList[numlights]._yoff = 0;
        // The index into LightList is different than the light ID. In retail, this is not the case.
        lid = nextLightId;
        ++nextLightId;
        LightList[numlights]._lid = lid;
        LightList[numlights]._ldel = FALSE;
        LightList[numlights]._lunflag = FALSE;
        ++numlights;
        // Trigger light updates.
        dolighting = TRUE;
    }

    return lid;
}

// .text:0042FCF9
// TODO what does it mean to "add" an "unlight"? This modifies `_lid == i`, it doesn't "add" anything
void AddUnLight(int i)
{
    int iter;

    if (lightflag || i == -1)
    {
        return;
    }

    for (iter = 0; iter < numlights; ++iter)
    {
        // Find the light that we're interested in.
        if (LightList[iter]._lid == i)
        {
            // Undo the lighting at the previous location.
            LightList[iter]._ldel = TRUE;
            // Trigger light update.
            dolighting = TRUE;
        }
    }
}

// .text:0042FD84
// Sets new light radius.
// `i` is the _lid to change. This is not the same as the index into LightList!
// `r` is the new radius
// TODO what are the units for radius?
void ChangeLightRadius(int i, int r)
{
    int iter;

    if (lightflag || i == -1)
    {
        return;
    }

    for (iter = 0; iter < numlights; ++iter)
    {
        // Find the light that we're interested in.
        if (LightList[iter]._lid == i)
        {
            // Undo the lighting at the previous location.
            LightList[iter]._lunflag = TRUE;
            LightList[iter]._lunx = LightList[iter]._lx;
            LightList[iter]._luny = LightList[iter]._ly;
            LightList[iter]._lunr = LightList[iter]._lradius;
            // Apply new radius.
            LightList[iter]._lradius = r;
            // Trigger light update.
            dolighting = TRUE;
        }
    }
}

// .text:0042FE70
// Sets new light location.
// `i` is the _lid to change. This is not the same as the index into LightList!
// (`x`, `y`) is the new location for the light
void ChangeLightXY(int i, int x, int y)
{
    int iter;

    if (lightflag || i == -1)
    {
        return;
    }

    for (iter = 0; iter < numlights; ++iter)
    {
        // Find the light that we're interested in.
        if (LightList[iter]._lid == i)
        {
            // Undo the lighting at the previous location.
            LightList[iter]._lunflag = TRUE;
            LightList[iter]._lunx = LightList[iter]._lx;
            LightList[iter]._luny = LightList[iter]._ly;
            LightList[iter]._lunr = LightList[iter]._lradius;
            // Apply new location.
            LightList[iter]._lx = x;
            LightList[iter]._ly = y;
            // Trigger light update.
            dolighting = TRUE;
        }
    }
}

// .text:0042FF6D
// Sets new light offset.
// `i` is the _lid to change. This is not the same as the index into LightList!
// (`x`, `y`) is the new offset for the light
// TODO what units does the offset use?
void ChangeLightOff(int i, int x, int y)
{
    int iter;

    if (lightflag || i == -1)
    {
        return;
    }

    for (iter = 0; iter < numlights; ++iter)
    {
        // Find the light that we're interested in.
        if (LightList[iter]._lid == i)
        {
            // Undo the lighting at the previous location.
            LightList[iter]._lunflag = TRUE;
            LightList[iter]._lunx = LightList[iter]._lx;
            LightList[iter]._luny = LightList[iter]._ly;
            LightList[iter]._lunr = LightList[iter]._lradius;
            // Apply new offset.
            LightList[iter]._xoff = x;
            LightList[iter]._yoff = y;
            // Trigger light update.
            dolighting = TRUE;
        }
    }
}

// .text:0043006A
// Sets new light properties.
// `i` is the _lid to change. This is not the same as the index into LightList!
// (`x`, `y`) is the new location for the light
// `r` is the new light radius.
void ChangeLight(int i, int x, int y, int r)
{
    int iter;

    if (lightflag || i == -1)
    {
        return;
    }

    for (iter = 0; iter < numlights; ++iter)
    {
        // Find the light that we're interested in.
        if (LightList[iter]._lid == i)
        {
            // Undo the lighting at the previous location.
            LightList[iter]._lunflag = TRUE;
            LightList[iter]._lunx = LightList[iter]._lx;
            LightList[iter]._luny = LightList[iter]._ly;
            LightList[iter]._lunr = LightList[iter]._lradius;
            // Apply new properties.
            LightList[iter]._lx = x;
            LightList[iter]._ly = y;
            LightList[iter]._lradius = r;
            // Trigger light update.
            dolighting = TRUE;
        }
    }
}

// ProcessLightList    0000000000430178
void ProcessLightList()
{
    // TODO
}
// SavePreLighting    0000000000430362
// InitVision    00000000004303DF
// AddVision    00000000004304FD
// AddUnVision    00000000004305B8

// ChangeVisionRadius    0000000000430622
void ChangeVisionRadius(int id, int r)
{
    int i;

    for (i = 0; i < numvision; i++)
    {
        if (VisionList[i]._lid == id)
        {
            VisionList[i]._lunflag = TRUE;
            VisionList[i]._lunx = VisionList[i]._lx;
            VisionList[i]._luny = VisionList[i]._ly;
            VisionList[i]._lunr = VisionList[i]._lradius;
            VisionList[i]._lradius = r;
            dovision = TRUE;
        }
    }
}

// ChangeVisionXY    00000000004306ED
// ChangeVision    00000000004307CA
// ProcessVisionList    00000000004308B7
void ProcessVisionList()
{
    // TODO
}