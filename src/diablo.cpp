#include "diablo.h"

#include <DDRAW.H>
#include <storm.h>
#include <direct.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <windows.h>

#include "automap.h"
#include "control.h"
#include "cursor.h"
#include "debug.h"
#include "defines.h"
#include "drlg_l1.h"
#include "drlg_l2.h"
#include "drlg_l3.h"
#include "drlg_l4.h"
#include "effects.h"
#include "engine.h"
#include "error.h"
#include "gendung.h"
#include "gmenu.h"
#include "help.h"
#include "interfac.h"
#include "inv.h"
#include "lighting.h"
#include "minitext.h"
#include "missiles.h"
#include "monster.h"
#include "mopaq.h"
#include "multi.h"
#include "objects.h"
#include "palette.h"
#include "player.h"
#include "quests.h"
#include "scrollrt.h"
#include "snd.h"
#include "sound.h"
#include "spells.h"
#include "stores.h"
#include "town.h"
#include "trigs.h"

//
// defines
//

#define PAUSE_OFF 0
// Special pause mode that advances one frame each time P is pressed. Good for inspecting animations
// Only activated by the P key in debug_mode. Use PAUSE key to break out
#define PAUSE_FRAMESTEP 1
// Normal pause mode
#define PAUSE_ON 2

// Width of inventory/character panel
#define POPOUT_WIDTH 320
#define POPOUT_HALF_WIDTH 160
// The actual graphic is 144 except there is 16px of transparency caused by the
// health globe overhang
#define CTRLPAN_HEIGHT 128

#define TIME_BETWEEN_FRAMES_MS 50

//
// forward decl (TODO declare in header?)
//

void diablo_init_screen();
BOOL init_create_window(HINSTANCE hInstance, int nShowCmd);
void game_logic();
void dx_cleanup();
void set_outstanding_paint_event(BOOL b);
void CALLBACK PaintEventTimer(UINT uTimerID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);

//
// initialized vars (.data:004BC0A8)
//

// wtf is this? nobody references it
char registration_table[128] = "REGISTRATION_TABLE";
// "Mutex" "held" duing PaintEventTimer (set to TRUE on enter, FALSE on return).
// Kind of a crude signal between main thread and PaintEventThread to coorindate
// when painting is occurring.
//
// Seems to serve double duty:
//   1. indicate that PaintEventTimer is working
//   2. used to stop PaintEventTimer from painting in that tick interval (e.g.
//      I've just issued a draw call)
volatile BOOL paint_mutex = FALSE;
// Controls which elements of the screen are drawn. See `force_redraw_values`
int force_redraw = 0;
// Used by main loop to determine when to post a WM_DIABPAINT message. Typically set by GM_Game
BOOL need_to_repaint = FALSE;
// Controlled by ShowProgress. Will be set to TRUE during certain ShowProgress
// blocks, then set to FALSE when done. This will be used as the final check in
// PaintEventTimer to block actually posting WM_DIABPAINT. I guess the idea is that there's no expectation to render at 20 FPS while we're loading.
// TODO: Rename stop_painting or something like that
BOOL pause_paint_timer = FALSE;
BOOL zoomflag = TRUE;
int unused000 = 0;
int unused001 = 0;
int unused002 = 1;
int unused003 = 1;
int unused004 = -1;
const char *version_string = "V1.0";
// If TRUE then enable a bunch of shenanigans. This shows the Cheats: On/Off option in the game menu.
BOOL debug_mode = FALSE;
// If TRUE then restrict to dlvl 1 and warrior
BOOL demo_mode = TRUE;
BOOL show_intros = TRUE;
// Enable/disable all music. Kinda intended as a compile-time toggle
BOOL debugMusicOn = TRUE;
BOOL cheat_mode = FALSE;
BOOL frameflag = FALSE;
// If TRUE then moving the mouse to the edge of the screen scrolls the view.
// (think real-time strategy controls a la Age of Empires).
// This is separate from the game_logic screen scrolling which requires cheat_mode.
BOOL scrollflag = FALSE;
// If TRUE then use 4 levels of lighting, otherwise use 16.
// I have no idea why you'd want to set this to TRUE. Maybe performance?
BOOL light4flag = FALSE;
// If TRUE then use music from the the filesystem (not the MPQ).
BOOL musicFromDisk = FALSE;
HANDLE sghMusic = 0;
// Game pause means gameplay stops (giving you time to safely go to the bathroom)
DWORD PauseMode = PAUSE_OFF;
// Menu option set by player. Compared to debugMusicOn, this is run-time
// settable by the player in the menu.
BOOL gbMusicOn = TRUE;
const char *sgszMusicTracks[] = {
    // indexed by leveltype
    "Music\\DTowne.wav",
    "Music\\DLvlA.wav",
    "Music\\DLvlA.wav",
    "Music\\DLvlA.wav",
    "Music\\DLvlA.wav",
    "Music\\DLvlA.wav",
};

//
// uninitialized vars (.data:0061B700)
//

BYTE *gpBuffer;
// How many times PaintEventTimer was run this second
// TODO rename
DWORD frames;
// How many paint events were dispatched this second
// TODO rename
DWORD frames_drawn;
// How many paint events were dispatched last second.
// TODO rename
DWORD draw_framerate;
// How many times PaintEventTimer was run last second.
// TODO rename
int framerate;
DWORD dword_61B714; // UNUSED
DWORD dword_61B718; // UNUSED; set to 0, never read
DWORD dword_61B71C; // UNUSED; set to 0, never read
DWORD dword_61B720; // UNUSED; set to 768, never read
DWORD dword_61B724; // UNUSED; set to 656, never used
// The DirectDraw surface back buffer. Given that dd_use_backbuf == FALSE, lpDDSBackBuf == lpDDSPrimary
LPDIRECTDRAWSURFACE lpDDSBackBuf;
// The Win32 window handle for the entire game. Initialized in init_create_window()
HWND ghMainWnd;
int MouseX;
int MouseY;
DWORD dword_61B738; // UNUSED
int screenshot_idx;
DWORD dword_61B740; // TODO TODO TODO
DWORD dword_61B744; // UNUSED
// If FALSE then PaintEventTimer will generate a WM_DIABPAINT event and set this to TRUE. The handling WndProc will then draw and set this to FALSE. Then the circle of life repeats.
// The "debouncing" behavior probably helps to avoid flooding the event loop with paint events that it can't handle if the game runs at < 20 FPS (the rate of the timer).
// TODO should this be volatile?
BOOL outstanding_paint_event;
// The active DirectDraw palette.
LPDIRECTDRAWPALETTE lpDDPalette;
int gMode; // enum game_mode
BOOL shouldStopPaintTimer;
DWORD dword_61B758; // UNUSED; set to 0, never read
UINT paint_event_timer_id;
DWORD dword_61B760; // UNUSED
DWORD dword_61B764; // UNUSED
UINT paint_event_timer_delay;
DWORD dword_61B76C; // UNUSED
// Path to the save directory; either $PWD/Save or $TMP/Save
char savedir_abspath[64];
TIMECAPS timecaps;
// ...
// This is like PitchTbl but for lpDDPrimary locked surface
int DDPitchTbl[SCREEN_HEIGHT];
int timer_expiry_per_second; // UNUSED; set, never read
UINT paint_event_timer_resolution;
// If TRUE then use exclusive fullscreen. If false then use windowed fullscreen.
// This is only ever set to TRUE in WinMain.
BOOL fullscreen;
// If TRUE then call Flip in DrawMain.
// This is only ever set to FALSE in dx_init
BOOL dd_use_backbuffer;
DWORD dword_61BF50; // UNUSED
DWORD dword_61BF54; // UNUSED
DWORD dword_61BF58; // UNUSED; set to 0, never read
DWORD dword_61BF5C; // UNUSED; set to 0, never read
DWORD dword_61BF60; // UNUSED; set to 640, never read
DWORD dword_61BF64; // UNUSED; set to 480, never read
// Whether the window is "active", i.e. the user wants to see and interact with it
BOOL gbActive;
// The "primary" DirectDraw surface; a.k.a. the front buffer. Given that dd_use_backbuf == FALSE, lpDDSBackBuf == lpDDSPrimary
LPDIRECTDRAWSURFACE lpDDSPrimary;
DWORD prevTime;
// Compiler-generated alignment to put array on 8-byte boundary
char fileLoadPrefix[64];
DWORD dword_61BFB8;
// Open diabdat.mpq handle, initialized by WndProc_BlizLogo after WM_DIABDONEFADEIN is sent by dx_init
HANDLE diabdat_mpq;
DWORD dword_61BFC0; // UNUSED
// DirectDraw handle, initialized in dx_init.
LPDIRECTDRAW lpDDInterface;
// Given that we don't know the DirectDraw locked buffer dimensions (could be
// weird due to hardware requirements), defer this calculation until DrawMain.
BOOL calculate_offsets_in_DrawMain;
// If TRUE then allow the paint routine to advance the player state such that we are ready to call game_logic.
// Basically:
// 1. The PaintEventTimer fires and posts an event for us to paint.
// 2. In response, the WndProc handles paint and gets us ready for another game tick. (calls Player_SerializeAction so that unknown_dplay_get_plractive returns TRUE)
// 3. In response, the main loop will run game logic to advance to the next tick.
// 4. Go to 1
BOOL allow_mp_action_send;
DWORD dword_61BFD0;
DWORD prev_timer_PostMessage_time;

//
// code (.text:00484A20)
//

// .text:00484A20
int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                      LPSTR lpCmdLine, int nShowCmd)
{
    UINT driveType;
    MSG message;
    BOOL error;
    int i;
    int mkdir_error;
    SYSTEM_INFO system_info;

    gbActivePlayers = 1;
    fullscreen = TRUE;
    if (gbActivePlayers == 1)
    {
        isSinglePlayer = TRUE;
        myplr = 0;
    }

    for (i = 0; i < MAX_PLRS; i++)
    {
        plr[i].to_send_message_id = 42; // TODO what is 42? (is it just a meme?)
    }

    allow_mp_action_send = TRUE;
    outstanding_paint_event = TRUE;
    shouldStopPaintTimer = FALSE;
    gMode = MODE_BLIZ_LOGO;
    calculate_offsets_in_DrawMain = TRUE;
    frames = 0;
    frames_drawn = 0;
    draw_framerate = 0;
    dword_61B758 = 0;
    dword_61BFC0 = 0;
    msgflag = 0;
    title_allow_loadgame = FALSE;
    fileLoadPrefix[0] = 0;
    savedir_abspath[0] = 0;

    // Try to find a suitable save directory
    driveType = GetDriveTypeA(NULL);
    if (driveType == DRIVE_CDROM)
    {
        // For CD: use a temporary directory
        error = FALSE;
        GetCurrentDirectoryA(sizeof(tempstr), tempstr);                  // BUG: Hope your current directory is < 64 chars ^_^
        if (GetTempPathA(sizeof(savedir_abspath), savedir_abspath) == 0) // BUG: Hope your temp directory is < 64 chars ^_^
        {
            error = TRUE;
        }
        else
        {
            if (_chdir(savedir_abspath) == -1)
            {
                error = TRUE;
            }
            if (*tempstr == *savedir_abspath)
            {
                error = TRUE;
            }
            _chdir(tempstr);
            sprintf(savedir_abspath, "%sSave", savedir_abspath);
        }

        if (error)
        {
            MessageBoxA(NULL, "No Windows Temp directory found. Cannot continue", "Diablo Demo", MB_ICONEXCLAMATION);
        }
    }
    else
    {
        // For HD: use the current directory
        GetCurrentDirectoryA(sizeof(savedir_abspath), savedir_abspath);
        sprintf(savedir_abspath, "%s\\Save", savedir_abspath);
    }

    mkdir_error = _mkdir(savedir_abspath);
    if (mkdir_error && errno == EEXIST)
    {
        mkdir_error = 0;
    }

    if (mkdir_error != 0 || driveType == DRIVE_UNKNOWN)
    {
        InitDiabloMsg(EMSG_NO_TMP_DIR);
    }

    // Require a Pentium processor. This is not present in Devilution. I guess they were concerned about the performance of the software renderer... maybe they just wanted the floating point hardware.
    GetSystemInfo(&system_info);
    if (system_info.wProcessorArchitecture & ~PROCESSOR_ARCHITECTURE_INTEL || system_info.dwProcessorType < PROCESSOR_INTEL_PENTIUM)
    {
        MessageBoxA(NULL, "The Diablo demo requires at least a Pentium.", "Diablo Demo", MB_DEFBUTTON2 | MB_ICONEXCLAMATION | MB_OKCANCEL);
        return 0;
    }

    // Perform a chunk of initialization mostly related to DirectDraw/DirectSound/Storm.
    // The rest waits until WndProc_BlizLogo with WM_DIABDONEFADEIN, such as opening diabdat.mpq.
    if (init_create_window(hInstance, nShowCmd) == FALSE)
    {
        return 0;
    }

    //
    // MAIN LOOP
    //

    while (TRUE)
    {
        // One of the first messages this processes is WM_DIABDONEFADEIN. WndProc_BlizLogo gets this and and opens diabdat.mpq, etc
        if (PeekMessageA(&message, NULL, 0, 0, 0))
        {
            if (!GetMessageA(&message, NULL, 0, 0))
            {
                return message.wParam;
            }
            TranslateMessage(&message);
            DispatchMessageA(&message);
            continue;
        }

        // Read at least one multiplayer message each loop
        if (gbActivePlayers > 1)
        {
            DPlayHandleMessage();
        }

        // The rest of this loop handles updating game state. Input processing
        // and painting for menus is handled by their respective WndProc
        if (gMode != MODE_GAME)
        {
            continue;
        }

        // Don't do game logic if the player hasn't caught up to all messages?
        if (unknown_dplay_get_plractive() == FALSE)
        {
            continue;
        }

        // Do normal unpaused game
        if (PauseMode != PAUSE_ON)
        {
            if (PauseMode == PAUSE_FRAMESTEP)
            {
                PauseMode = PAUSE_ON;
            }

            for (i = 0; gbActivePlayers > i; i++)
            {
                if (plr[i].plractive)
                {
                    plr[i].to_send_message_id += 1;
                }
            }

            // Allow GM_Game WM_DIABPAINT to prepare for next game tick.
            allow_mp_action_send = TRUE;

            // ~~~ Where all the magic happens ~~~
            game_logic();

            // Game logic thinks that we're behind on painting so immediately fire another paint event.
            if (need_to_repaint)
            {
                PostMessageA(ghMainWnd, WM_DIABPAINT, 0, 0);
                need_to_repaint = FALSE;
            }

            continue;
        }

        // Do view scrolling if paused. This scrolls INCREDIBLY quickly since it's not framerate limited.
        CheckCursMove();
        if (scrollflag)
        {
            ScrollView();
        }

        force_redraw = FORCE_REDRAW_ALL;
        if (need_to_repaint)
        {
            PostMessageA(ghMainWnd, WM_DIABPAINT, 0, 0);
            need_to_repaint = FALSE;
        }

        // This is a tight loop without any yields... This probably pegs the
        // CPU as a result!
    }

    return message.wParam; // no breaks; we should never get here!
}

// .text:00484E67
// Initialize DirectDraw and DirectSound for a given window `hWnd`. Returns TRUE on success, FALSE otherwise.
// This also creates gpBuffer where all drawing happens.
// `fullscreen` affects how DirectDraw is initialized.
// (windowed fullscreen doesn't work on modern windows... was probably only meant for Windows 95)
BOOL dx_init(HWND hWnd)
{
    HRESULT hDDVal;
    DDSURFACEDESC ddsd;
    DWORD dwStyle;
    RECT window_rect;
    RECT work_area_rect;

    if (fullscreen == FALSE)
    {
        // Windowed fullscreen
        SetFocus(hWnd);

        hDDVal = DirectDrawCreate(NULL, &lpDDInterface, NULL);
        if (hDDVal != DD_OK)
        {
            goto dd_create_err;
            goto cleanup;
        }

        hDDVal = lpDDInterface->SetCooperativeLevel(hWnd, DDSCL_NORMAL);
        if (hDDVal != DD_OK)
        {
            goto set_coop_err;
            goto cleanup;
        }

        dwStyle = GetWindowLongA(hWnd, GWL_STYLE);
        dwStyle &= ~WS_POPUP;
        dwStyle |= WS_MINIMIZEBOX | WS_SIZEBOX | WS_CAPTION;
        SetWindowLongA(hWnd, GWL_STYLE, dwStyle);

        // This looks like a bunch of malarkey to make sure the window is
        // 640x480 and visible to the player (actually on a desktop)
        SetRect(&window_rect, 0, 0, 640, 480);
        AdjustWindowRectEx(
            &window_rect,
            GetWindowLongA(hWnd, GWL_STYLE),
            !!GetMenu(hWnd), // TODO disassembly is
                             // call ds:GetMenu
                             // cmp eax, 1
                             // sbb eax, eax
                             // inc eax
                             // push eax
            GetWindowLongA(hWnd, GWL_EXSTYLE));
        SetWindowPos(
            hWnd,
            HWND_TOP,
            0,
            0,
            window_rect.right - window_rect.left,
            window_rect.bottom - window_rect.top,
            SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
        SetWindowPos(
            hWnd,
            HWND_NOTOPMOST,
            0,
            0,
            0,
            0,
            SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
        SystemParametersInfoA(SPI_GETWORKAREA, 0, &work_area_rect, 0);
        GetWindowRect(hWnd, &window_rect);
        if (window_rect.left < work_area_rect.left)
        {
            window_rect.left = work_area_rect.left;
        }
        if (window_rect.top < work_area_rect.top)
        {
            window_rect.top = work_area_rect.top;
        }
        SetWindowPos(
            hWnd,
            HWND_TOP,
            window_rect.left,
            window_rect.top,
            0,
            0,
            SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);

        memset(&ddsd, 0, sizeof(ddsd));
        ddsd.dwSize = sizeof(ddsd);

        // start setting parameters...
        ddsd.dwFlags = DDSD_BACKBUFFERCOUNT;
        ddsd.ddsCaps.dwCaps = DDSCAPS_COMPLEX | DDSCAPS_FLIP | DDSCAPS_PRIMARYSURFACE | DDSCAPS_VIDEOMEMORY;

        // psych! override those with what we actually want >:)
        // (someone probably just copy-pasted this here for testing...)
        dd_use_backbuffer = FALSE;
        ddsd.dwFlags = DDSD_CAPS;
        ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;

        hDDVal = lpDDInterface->CreateSurface(&ddsd, &lpDDSBackBuf, NULL);
        lpDDSPrimary = lpDDSBackBuf;
        // No error checking here :/
    }
    else
    {
        // fullscreen == TRUE: Exclusive fullscreen
        hDDVal = DirectDrawCreate(NULL, &lpDDInterface, NULL);
        if (hDDVal != DD_OK)
        {
            goto dd_create_fs_err;
            goto cleanup;
        }

        hDDVal = lpDDInterface->SetCooperativeLevel(hWnd, DDSCL_FULLSCREEN | DDSCL_EXCLUSIVE);
        if (hDDVal != DD_OK)
        {
            goto set_coop_fs_err;
            goto cleanup;
        }

        hDDVal = lpDDInterface->SetDisplayMode(640, 480, 8);
        if (hDDVal != DD_OK)
        {
            goto set_mode_err;
            goto cleanup;
        }

        memset(&ddsd, 0, sizeof(ddsd));
        ddsd.dwSize = sizeof(ddsd);

        dd_use_backbuffer = FALSE;
        ddsd.dwFlags = DDSD_CAPS;
        ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;

        hDDVal = lpDDInterface->CreateSurface(&ddsd, &lpDDSBackBuf, NULL);
        lpDDSPrimary = lpDDSBackBuf;
        if (hDDVal != DD_OK)
        {
            goto surface_err;
            goto cleanup;
        }
    }

    lpDDInterface->CreatePalette(DDPCAPS_8BIT, system_palette, &lpDDPalette, NULL);
    if (lpDDPalette == NULL)
    {
        goto palette_err;
        goto cleanup;
    }

    hDDVal = lpDDSBackBuf->SetPalette(lpDDPalette);
    if (hDDVal != DD_OK)
    {
        goto set_pal_err;
        goto cleanup;
    }

    SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE);

    snd_init(hWnd);
    SDrawManualInitialize(hWnd, lpDDInterface, lpDDSBackBuf, NULL, NULL, lpDDPalette, NULL);
    if (debugMusicOn)
    {
        // DDA = "direct disk access"? This is used for streaming chunks of audio from the MPQ.
        SFileDdaInitialize(sglpDS);
    }

    if (musicFromDisk)
    {
        // TODO
        //     Mopaq_479075(20, 10, 0x8000);
        //     if (SNDCPP_InitThread() == 0)
        //     {
        //         return FALSE;
        //     }
        //     music_start_NoStorm("Music\\Dintro.wav");
    }

    GdiSetBatchLimit(1);
    ShowCursor(FALSE);

    diablo_init_screen();
    gpBuffer = (BYTE *)DiabloAllocPtr(BUFFER_WIDTH * BUFFER_HEIGHT);

    // Create the timer that fires paint events at regular intervals. Notice that the PaintEventTimer fires every 5ms at most but the min delay between frames is 50ms.
    set_outstanding_paint_event(TRUE);
    timeGetDevCaps(&timecaps, sizeof(TIMECAPS));
    paint_event_timer_resolution = timecaps.wPeriodMin;
    paint_event_timer_delay = timecaps.wPeriodMin <= 5 ? 5 : timecaps.wPeriodMin;
    timer_expiry_per_second = 1000 / paint_event_timer_delay; // set but never read!
    timeBeginPeriod(paint_event_timer_resolution);
    prev_timer_PostMessage_time = timeGetTime();
    prevTime = prev_timer_PostMessage_time;
    paint_event_timer_id = timeSetEvent(paint_event_timer_delay,
                                        paint_event_timer_resolution,
                                        PaintEventTimer,
                                        (DWORD)hWnd,
                                        TIME_PERIODIC);

    // Trigger the rest of the initialization (assuming we start on the BlizLogo wndproc).
    // This will be deferred until we return and hit the event loop
    PostMessageA(hWnd, WM_DIABDONEFADEIN, 0, 0);

    return TRUE;

cleanup:
    dx_cleanup();
    MessageBoxA(hWnd, "DirectDraw Init FAILED", "Diablo Demo", MB_OK);
    DestroyWindow(hWnd);
    return FALSE;
set_pal_err:
    goto cleanup;
palette_err:
    goto cleanup;
surface_err:
    goto cleanup;
set_mode_err:
    goto cleanup;
set_coop_fs_err:
    goto cleanup;
dd_create_fs_err:
    goto cleanup;
set_coop_err:
    goto cleanup;
dd_create_err:
    goto cleanup;
}

// .text:004853DA
// The message processor for the blizzard logo/quotes.cel screen. Skip if
// show_intros == FALSE. Otherwise, next screen is the intro video.
//
// WM_DIABDONEFADEIN does an odd mix of things:
// - opens diabdat.mpq (important)
// - initializes some gameplay state?
// - plays logo.smk
// - draws quotes.cel
LRESULT WndProc_BlizLogo(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    BOOL bSuccess; // unused

    switch (Msg)
    {
    case WM_ACTIVATEAPP:
        if (gbActive)
        {
            ResetPal();
        }
        break;
    case WM_LBUTTONDOWN:
    case WM_KEYDOWN:
    case WM_CHAR:
    case WM_RBUTTONDOWN:
        if (hCurrentVideo)
        {
            SVidPlayEnd(hCurrentVideo);
            // Ending the video allows interfac_PlayLogo_DrawQuotes to finish
        }
        break;
    case WM_DIABDONEFADEIN:
        bSuccess = SFileOpenArchive("diabdat.mpq", 0, 0, &diabdat_mpq);
        priv_sound_init();
        MakeLightTable();
        InitCursor();
        InitLevelCursor(hWnd);
        if (!show_intros)
        {
            interfac_InitMainMenu();
            // Bug? WndProc_IntroVid loads Title.pal before transitioning to
            // main menu
            LoadPalette("Gendata\\Quotes.pal", orig_palette);
        }
        else
        {
            // This will block until the video is done playing. It runs the event loop in the interim.
            interfac_PlayLogo_DrawQuotes();
            LoadPalette("Gendata\\Quotes.pal", orig_palette);
        }

        set_outstanding_paint_event(FALSE);
        if (!show_intros)
        {
            gMode = MODE_MAINMENU;
        }
        else
        {
            gMode = MODE_INTRO_VID;
        }

        CopyPalette(logical_palette, orig_palette);
        PaletteFadeIn(32);
        force_redraw = FORCE_REDRAW_ALL;

        break;
    case WM_DIABPAINT:
        while (paint_mutex)
        {
            // do nothing, wait for mutex
        }

        // This doesn't call DoFadeIn/DoFadeOut so fade will never progress.
        // Probably fine since only a video is being played and when its over
        // we transition to a new screen
        paint_mutex = TRUE;
        if (fade_state == PALETTE_FADE_IN)
        {
            PostMessageA(hWnd, WM_DIABDONEFADEIN, 0, 0);
            fade_state = PALETTE_NO_FADE;
        }
        if (fade_state == PALETTE_FADE_OUT)
        {
            PostMessageA(hWnd, delayed_Msg, 0, 0);
            fade_state = PALETTE_NO_FADE;
        }
        lpDDPalette->SetEntries(0, 0, 256, system_palette);
        frames_drawn++;
        outstanding_paint_event = FALSE;
        paint_mutex = FALSE;

        return 0;
    case 0x3B9: // ???
        return 0;
    }

    return DefWindowProcA(hWnd, Msg, wParam, lParam);
}

// .text:0048565F TODO
static void LeftMouseCmd(BOOL)
{
    // TODO
}

// .text:004865C4
LRESULT GM_Game(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    DWORD now;
    int i;

    switch (uMsg)
    {
    case WM_ACTIVATEAPP:
        force_redraw = FORCE_REDRAW_ALL;
        gbActive = wParam;
        if (gbActive)
        {
            ResetPal();
            need_to_repaint = TRUE;
        }
        break;
    case WM_KEYDOWN:
        // If dead, only allow interactions with the menu
        if (plr[myplr]._pInvincible && deathflag)
        {
            if (wParam == VK_UP)
            {
                gmenu_up();
            }
            if (wParam == VK_DOWN)
            {
                gmenu_down();
            }
            if (wParam == VK_RETURN)
            {
                gmenu_select_option();
            }
            return 0;
        }

        // ESC: Close dialogs in order until none are open. Then toggle game menu
        if (wParam == VK_ESCAPE)
        {
            if (helpflag)
            {
                helpflag = FALSE;
            }
            else if (qtextflag)
            {
                qtextflag = FALSE;
                if (leveltype == DTYPE_TOWN)
                {
                    sfx_stop();
                }
            }
            else if (stextflag)
            {
                STextESC();
            }
            else if (questlog)
            {
                QuestlogOff();
            }
            else if (msgflag)
            {
                msgdelay = 0;
            }
            else if (!gmenuflag)
            {
                gmenuflag = 1;
                sgCurrentMenuIdx = 0;
            }
            else
            {
                gmenuflag = 0;
            }
        }

        // PAUSE: Toggle game pause (if not in town)
        if (wParam == VK_PAUSE)
        {
            if (currlevel == 0)
            {
                InitDiabloMsg(EMSG_NO_PAUSE_IN_TOWN);
            }
            else if (PauseMode)
            {
                PauseMode = PAUSE_OFF;
            }
            else
            {
                PauseMode = PAUSE_ON;
            }
        }

        // F1: Show in-game help document (if not in a store)
        if (wParam == VK_F1)
        {
            if (helpflag)
            {
                helpflag = FALSE;
            }
            else if (stextflag)
            {
                ClearPanel();
                AddPanelString("No help available", TRUE);
                AddPanelString("while in stores", TRUE);
                return 0;
            }
            else
            {
                invflag = FALSE;
                chrflag = FALSE;
                spselflag = FALSE;
                drawpotboxflag = FALSE;
                if (qtextflag && leveltype == DTYPE_TOWN)
                {
                    qtextflag = FALSE;
                    sfx_stop();
                }
                questlog = FALSE;
                automapflag = FALSE;
                msgdelay = 0;
                gmenuflag = 0;
                DisplayHelp();
                return 0;
            }
        }

        // F5 (cheat): teleport to town
        if (wParam == VK_F5 && cheat_mode)
        {
            NetSendCmd(myplr, ACTION_WARP2, 0, DTYPE_TOWN, 0);
        }

        // F6 (cheat): teleport to dlvl 1 (cathedral)
        if (wParam == VK_F6 && cheat_mode)
        {
            NetSendCmd(myplr, ACTION_WARP2, 0, DTYPE_CATHEDRAL, 1);
        }

        // F7 (cheat): teleport to dlvl 5 (catacombs)
        if (wParam == VK_F7 && cheat_mode)
        {
            NetSendCmd(myplr, ACTION_WARP2, 0, DTYPE_CATACOMBS, 5);
        }

        // F8 (cheat): teleport to dlvl 9 (caves)
        if (wParam == VK_F8 && cheat_mode)
        {
            NetSendCmd(myplr, ACTION_WARP2, 0, DTYPE_CAVES, 9);
        }

        // F9: Take screenshot
        if (wParam == VK_F9)
        {
            DrawAndBlit();
            CaptureScreen(screenshot_idx, orig_palette);
            ++screenshot_idx;
        }

        // F4 (cheat): toggle control panel FPS counter
        if (wParam == VK_F4)
        {
            if (debug_mode || cheat_mode)
            {
                if (!frameflag)
                {
                    frameflag = TRUE;
                }
                else
                {
                    frameflag = FALSE;
                }
            }
        }

        // ENTER: Use control panel item (if not on menu or in store)
        if (wParam == VK_RETURN)
        {
            if (plr[myplr].potbox_item != -1 && !gmenuflag && !stextflag)
            {
                // TRUE means use the control panel item
                // (as opposed to FALSE which uses the item under the cursor)
                NetSendCmd(myplr, ACTION_USEITEM, 0, TRUE, 0);
            }
        }

        // UP (not dead): Scroll the currently open panel up
        if (wParam == VK_UP)
        {
            if (gmenuflag)
            {
                gmenu_up();
            }
            else if (stextflag)
            {
                STextUp();
            }
            else if (questlog)
            {
                QuestlogUp();
            }
            else if (helpflag)
            {
                HelpScrollUp();
            }
            else if (automapflag)
            {
                AutomapUp();
            }
        }

        // DOWN (not dead): Scroll the currently open panel down
        if (wParam == VK_DOWN)
        {
            if (gmenuflag)
            {
                gmenu_down();
            }
            else if (stextflag)
            {
                STextDown();
            }
            else if (questlog)
            {
                QuestlogDown();
            }
            else if (helpflag)
            {
                HelpScrollDown();
            }
            else if (automapflag)
            {
                AutomapDown();
            }
        }

        // LEFT: Move automap left
        if (wParam == VK_LEFT)
        {
            if (automapflag)
            {
                AutomapLeft();
            }
        }

        // RIGHT: Move automap right
        if (wParam == VK_RIGHT)
        {
            if (automapflag)
            {
                AutomapRight();
            }
        }

        // ENTER (not dead): Select from open panel (e.g. shop)
        if (wParam == VK_RETURN)
        {
            if (gmenuflag)
            {
                gmenu_select_option();
            }
            else if (stextflag)
            {
                STextEnter();
            }
            else if (questlog)
            {
                QuestlogEnter();
            }
        }

        // TAB: Toggle automap
        if (wParam == VK_TAB)
        {
            if (currlevel != 0)
            {
                if (!automapflag)
                {
                    StartAutomap();
                }
                else
                {
                    automapflag = FALSE;
                }
            }
            else
            {
                InitDiabloMsg(EMSG_NO_AUTOMAP_IN_TOWN);
            }
        }

        // SPACE: Close any open panels, reset cursor state (if casting spells that modify the cursor)
        if (wParam == VK_SPACE)
        {
            // Keep the mouse in the same spot on the screen relative to the
            // level viewport.
            if (invflag && !chrflag && MouseX < SCREEN_WIDTH - POPOUT_HALF_WIDTH)
            {
                SetCursorPos(MouseX + POPOUT_HALF_WIDTH, MouseY);
            }
            if (chrflag && !invflag && MouseX > POPOUT_HALF_WIDTH)
            {
                SetCursorPos(MouseX - POPOUT_HALF_WIDTH, MouseY);
            }

            helpflag = FALSE;
            invflag = FALSE;
            chrflag = FALSE;
            spselflag = FALSE;
            drawpotboxflag = FALSE;

            if (qtextflag && leveltype == DTYPE_TOWN)
            {
                qtextflag = FALSE;
                sfx_stop();
            }

            questlog = FALSE;
            automapflag = FALSE;
            msgdelay = 0;
            gmenuflag = 0;

            if (pcurs != CURSOR_NONE && pcurs < CURSOR_FIRSTITEM)
            {
                SetCursor_(CURSOR_NONE);
            }
        }

        return 0;
    case WM_CHAR:
        if (plr[myplr]._pInvincible)
        {
            return 0;
        }

        switch (wParam)
        {
        case 'G':
        case 'g':
            // G: Change gamma
            if (gamma_correction >= 0.3)
            {
                gamma_correction -= 0.05;
                gamma_backup = gamma_correction;
            }
            CopyPalette(system_palette, orig_palette);
            ApplyGamma(system_palette, /*start=*/0, /*end=*/256);
            lpDDPalette->SetEntries(/*dwFlags=*/0, /*dwStartingEntry=*/0, /*dwCount=*/256, system_palette);
            return 0;
        case 'F':
        case 'f':
            // F: Change gamma
            if (gamma_correction <= 1.0)
            {
                gamma_correction += 0.05;
                gamma_backup = gamma_correction;
            }
            CopyPalette(system_palette, orig_palette);
            ApplyGamma(system_palette, /*start=*/0, /*end=*/256);
            lpDDPalette->SetEntries(/*dwFlags=*/0, /*dwStartingEntry=*/0, /*dwCount=*/256, system_palette);
            return 0;
        case 'I':
        case 'i':
            // I: Open/close inventory
            if (stextflag)
            {
                return 0;
            }

            if (!invflag)
            {
                invflag = TRUE;
            }
            else
            {
                invflag = FALSE;
            }

            if (invflag && !chrflag)
            {
                if (MouseX < SCREEN_WIDTH - POPOUT_HALF_WIDTH)
                {
                    SetCursorPos(MouseX + POPOUT_HALF_WIDTH, MouseY);
                }
            }
            else
            {
                if (MouseX > POPOUT_HALF_WIDTH)
                {
                    SetCursorPos(MouseX - POPOUT_HALF_WIDTH, MouseY);
                }
            }

            return 0;
        case 'C':
        case 'c':
            // C: Open/close character panel
            if (stextflag)
            {
                return 0;
            }

            if (!chrflag)
            {
                chrflag = TRUE;
            }
            else
            {
                chrflag = FALSE;
            }

            if (chrflag && !invflag)
            {
                if (MouseX > POPOUT_HALF_WIDTH)
                {
                    SetCursorPos(MouseX - POPOUT_HALF_WIDTH, MouseY);
                }
            }
            else
            {
                if (MouseX < SCREEN_WIDTH - POPOUT_HALF_WIDTH)
                {
                    SetCursorPos(MouseX + POPOUT_HALF_WIDTH, MouseY);
                }
            }

            return 0;
        case 'L':
        case 'l':
            // L (cheat): Toggle lighting
            if (cheat_mode)
            {
                ToggleLighting();
            }
            return 0;
        case 'T':
        case 't':
            // T (cheat): Print player location
            if (cheat_mode)
            {
                sprintf(tempstr, "X = %i  Y = %i", plr[myplr]._px, plr[myplr]._py);
                AddPanelString(tempstr, FALSE);
            }
            return 0;
        case 'd':
            // D (cheat): Show trans val (whatever that is)
            if (debug_mode)
            {
                sprintf(tempstr, "TransVal = %i", dTransVal[plr[myplr]._px][plr[myplr]._py]);
                AddPanelString(tempstr, FALSE);
            }
            return 0;
        case 'e':
            // E (cheat): Print E flag (whatever that is)
            if (debug_mode)
            {
                sprintf(tempstr, "EFlag = %i", plr[myplr]._peflag);
                AddPanelString(tempstr, FALSE);
            }
            return 0;
        case 'M':
        case 'm':
            // M (cheat): Print invincible flag (whatever that is)
            if (debug_mode)
            {
                sprintf(tempstr, "Invincible = %i", plr[myplr]._pInvincible);
                AddPanelString(tempstr, FALSE);
            }
            return 0;
        case 'P':
        case 'p':
            // P: Pause game. In debug_mode this is a frame advancing pause
            // This is slightly different from the PAUSE key which never activates the framestep behavior
            if (currlevel == 0)
            {
                InitDiabloMsg(EMSG_NO_PAUSE_IN_TOWN);
            }
            else if (debug_mode)
            {
                // As long as you are debug_mode, you will stay in PAUSE_FRAMESTEP
                // Need to use PAUSE key to break out!
                PauseMode = PAUSE_FRAMESTEP;
            }
            else if (PauseMode != PAUSE_OFF)
            {
                PauseMode = PAUSE_OFF;
            }
            else
            {
                PauseMode = PAUSE_ON;
            }
            return 0;
        case 'Q':
        case 'q':
            // Q: Open quest log
            if (gmenuflag)
            {
                return 0;
            }
            if (stextflag)
            {
                return 0;
            }
            if (questlog)
            {
                StartQuestlog();
            }
            else
            {
                questlog = FALSE;
            }
            return 0;
        case 'R':
        case 'r':
            // R: Print current dungeon seed
            sprintf(tempstr, "seed = %i", plr[myplr]._pSeedTbl[currlevel]);
            AddPanelString(tempstr, FALSE);
            return 0;
        case 'Z':
        case 'z':
            // Z: Toggle zoom
            if (!zoomflag)
            {
                zoomflag = TRUE;
            }
            else
            {
                zoomflag = FALSE;
            }
            return 0;
        case 'S':
        case 's':
            // S: Open spell selector
            if (gmenuflag)
            {
                return 0;
            }
            if (stextflag)
            {
                return 0;
            }
            if (!spselflag)
            {
                DoSpeedBook();
            }
            else
            {
                spselflag = FALSE;
            }
            return 0;
        case '*':
            // * (cheat): Gain enough experience to level up!
            if (cheat_mode)
            {
                AddPlrExperience(myplr, plr[myplr]._pLevel, plr[myplr]._pNextExper - plr[myplr]._pExperience);
            }
            return 0;
        case '+':
        case '=':
            // +: Zoom in on automap, enlarging the details
            if (automapflag)
            {
                AutomapZoomIn();
            }
            return 0;
        case '-':
        case '_':
            // -: Zoom out on automap, shrinking the details
            if (automapflag)
            {
                AutomapZoomOut();
            }
            return 0;
        case 'B':
        case 'b':
            // B (cheat): Current spell goes up a level
            if (cheat_mode)
            {
                ++plr[myplr]._pSplLvl[plr[myplr]._pRSpell];
            }
            return 0;
        case 'V':
        case 'v':
            // V: Print version to control panel
            if (demo_mode)
            {
                strcpy(tempstr, "Pre-release Demo");
            }
            else
            {
                strcpy(tempstr, "V1.0");
            }
            AddPanelString(tempstr, TRUE);
            return 0;
        case '1':
            // 1: Spell hotkey, either assign or switch to
            if (spselflag)
            {
                SetSpeedSpell(0);
            }
            else
            {
                ToggleSpell(0);
            }
            return 0;
        case '2':
            // 2: Spell hotkey, either assign or switch to
            if (spselflag)
            {
                SetSpeedSpell(1);
            }
            else
            {
                ToggleSpell(1);
            }
            return 0;
        case '3':
            // 3: Spell hotkey, either assign or switch to
            if (spselflag)
            {
                SetSpeedSpell(2);
            }
            else
            {
                ToggleSpell(2);
            }
            return 0;
        }

        break;
    case WM_MOUSEMOVE:
        MouseX = (lParam & 0xFFFF) - MouseOffX;
        MouseY = ((lParam >> 16) & 0xFFFF) - MouseOffY;
        return 0;
    case WM_LBUTTONDOWN:
        // If dead, only allow interactions with the menu
        if (plr[myplr]._pInvincible && deathflag)
        {
            gmenu_left_mouse();
            return 0;
        }

        // Change spell if book is open
        if (spselflag)
        {
            SetSpell();
            return 0;
        }

        // Change item if selection is open
        if (drawpotboxflag)
        {
            control_do_update_potbox();
            return 0;
        }

        // Interact with shop if open
        if (stextflag)
        {
            CheckStoreBtn();
            return 0;
        }

        if (MouseY < SCREEN_HEIGHT - CTRLPAN_HEIGHT) // Click on isometric game view
        {
            // Interact with menu if open
            if (gmenuflag)
            {
                gmenu_left_mouse();
                return 0;
            }

            // Identify item if identify was cast
            if (pcurs == CURSOR_IDENTIFY)
            {
                DoIdentify();
                return 0;
            }

            // Repair item if repair was cast
            if (pcurs == CURSOR_REPAIR)
            {
                DoRepair();
                return 0;
            }

            // Recharge staff if spell was cast
            if (pcurs == CURSOR_RECHARGE)
            {
                DoRecharge();
                return 0;
            }

            // Reset cursor if cast disarm but no object selected
            if (pcurs == CURSOR_DISARM && pcursobj == -1)
            {
                SetCursor_(CURSOR_NONE);
                return 0;
            }

            // Interact with quest log
            if (questlog && MouseY > 32 && MouseY < 320 && MouseX > 24 && MouseX < 296)
            {
                QuestLogESC();
                return 0;
            }

            if (chrflag && MouseX > 320) // Interact with character panel
            {
                CheckChrBtns();
            }
            else if (invflag && MouseX < 320) // Interact with inventory
            {
                CheckInvItem();
            }
            else if (pcurs >= CURSOR_FIRSTITEM) // Drop held item
            {
                NetSendCmd(myplr, ACTION_PUTITEM, 0, cursmx, cursmy);
            }
            else
            {
                if (plr[myplr]._pStatPts != 0 && !spselflag && !drawpotboxflag) // Check if clicked level-up button
                {
                    CheckLvlBtn();
                }
                if (!lvlbtndown) // No level up button press so interact with the isometric world
                {
                    if (wParam == MK_SHIFT | MK_LBUTTON)
                    {
                        LeftMouseCmd(TRUE);
                    }
                    else
                    {
                        LeftMouseCmd(FALSE);
                    }
                }
            }
        }
        else // Click on control panel
        {
            control_check_btn_press();
            if (pcurs > CURSOR_NONE && pcurs < CURSOR_FIRSTITEM)
            {
                savecrsr_hide();
                SetCursor_(CURSOR_NONE);
                savecrsr_show();
            }
        }

        return 0;
    case WM_LBUTTONUP:
        if (plr[myplr]._pInvincible)
        {
            return 0;
        }

        if (panbtndown)
        {
            CheckBtnUp();
        }

        if (chrbtnactive)
        {
            ReleaseChrBtns();
        }

        if (lvlbtndown)
        {
            ReleaseLvlBtn();
        }

        if (stextflag)
        {
            ReleaseStoreBtn();
        }

        return 0;
    case WM_RBUTTONDOWN:
        if (plr[myplr]._pInvincible)
        {
            return 0;
        }

        if (stextflag)
        {
            return 0;
        }

        if (plr[myplr].walkpath[0] == 22) // TODO magic number! ACTION_UNUSED?
        {
            return 0;
        }

        if (MouseY < SCREEN_HEIGHT - CTRLPAN_HEIGHT && pcurs == CURSOR_NONE)
        {
            if (invflag && pcursinvitem != -1)
            {
                // v2 == 0 means use item under cursor
                NetSendCmd(myplr, ACTION_USEITEM, 0, /*v2=*/0, 0);
            }
            else
            {
                CheckPlrSpell();
            }
        }
        else if (MouseX >= 55 && MouseX < 116 && MouseY >= 404 && MouseY < 460 && plr->potbox_item != -1)
        {
            // v2 == 1 means use potion box item
            NetSendCmd(myplr, ACTION_USEITEM, 0, /*v2=*/1, 0);
        }

        if (pcurs > CURSOR_NONE && pcurs < CURSOR_FIRSTITEM)
        {
            SetCursor_(CURSOR_NONE);
        }

        return 0;
    case WM_DIABDONEFADEOUT:
        if (sghMusic && debugMusicOn && gbMusicOn)
        {
            SFileDdaEnd(sghMusic);
            SFileCloseFile(sghMusic);
            sghMusic = NULL;
        }
        set_outstanding_paint_event(TRUE);
        gMode = MODE_BLIZ_LOGO;
        show_intros = FALSE;
        MemFreeDbg(pLightTbl);
        MemFreeDbg(pDungeonCels);
        MemFreeDbg(pMegaTiles);
        MemFreeDbg(pLevelPieces);
        MemFreeDbg(pSpecialCels);
        FreeControlPan();
        FreeInvGFX();
        FreeGMenu();
        FreeQuestText();
        FreeStoreMem();
        for (i = 0; i < gbActivePlayers; ++i)
        {
            FreePlayerGFX(i);
        }
        FreeItemGFX();
        if (leveltype != DTYPE_TOWN)
        {
            MemFreeDbg(pSpeedCels);
            FreeMonsters();
            FreeObjectGFX();
            FreeMissileGFX();
            FreeSpells();
            FreeMonsterSnd();
        }
        else
        {
            FreeTownerGFX();
            FreeMissileGFX(); // I find this curious... what missiles are in town?!?
            FreeTownersEffects();
        }
        FreeDebugGFX();
        diablo_init_screen();
        MakeLightTable();
        fade_state = PALETTE_FADE_IN;
        title_allow_loadgame = FALSE;
        set_outstanding_paint_event(FALSE);
        break;
    case WM_DIABPAINT:
        // Prevent additional paints from happening until we're done
        while (paint_mutex)
        {
            // This space intentionally left blank
        }
        paint_mutex = TRUE;

        if (frameflag && PauseMode != PAUSE_ON)
        {
            // First number is the frame rate last second, should be ~20FPS.
            // Second number is latency between PostMessage and WndProc, should be <2ms.
            // Third number is the how many times the PaintEventTimer fired. Typically around ~200 (since timer fires every 5ms).
            sprintf(tempstr, "Frame Rate : %i %d;%d", draw_framerate, timeGetTime() - prev_timer_PostMessage_time, framerate);
            AddPanelString(tempstr, FALSE);
        }

        // Let the main loop call game_logic to progress another tick.
        if (allow_mp_action_send)
        {
            Player_SerializeNextAction();
            if (gbActivePlayers > 1)
            {
                unknown_dplay_send();
            }
            allow_mp_action_send = FALSE;
        }

        if (fade_state)
        {
            if (fade_state == PALETTE_FADE_IN)
            {
                DoFadeIn();
            }
            if (fade_state == PALETTE_FADE_OUT)
            {
                DoFadeOut();
            }

            // DoFade* will reset fade_state when the fade ends
            if (fade_state == PALETTE_NO_FADE)
            {
                PostMessageA(hWnd, delayed_Msg, 0, 0);
            }

            lpDDPalette->SetEntries(/*dwFlags=*/0, /*dwStartingEntry=*/0, /*dwCount=*/256, system_palette);
        }
        else
        {
            // Do caves palette swapping to make it look like lava.
            // I'm guess that fullscreen is check stems from a limitation of DirectDraw.
            // Maybe it can't change the palette if we're in a window (i.e. not exclusive fullscreen) because the entire desktop would be affected.
            if (leveltype == DTYPE_CAVES && fullscreen == TRUE)
            {
                palette_update_caves();
                lpDDPalette->SetEntries(/*dwFlags=*/0, /*dwStartingEntry=*/0, /*dwCount=*/256, system_palette);
            }
        }

        DrawAndBlit();
        ++frames_drawn;
        // We've been holding the mutex which prevents PaintEventTimer from doing anything.
        // If we took more than 50ms to draw a frame then we're late and the FPS will drop to < 20 FPS. To attempt to get back on track, make sure we generate a paint in WinMain.
        // This means that if we find ourselves in a situation where we're constantly late then we just render as fast as possible.
        // The alternative is we "miss the train" and wait for the next 50ms window. Think of it like a 20 Hz VSYNC. My guess is that this looks janky (esp at < 20 FPS)
        now = timeGetTime();
        if (now - prev_timer_PostMessage_time >= TIME_BETWEEN_FRAMES_MS)
        {
            need_to_repaint = TRUE;
            prev_timer_PostMessage_time = now;
        }
        else
        {
            // Acknowledge the paint event. Lets PaintEventTimer know that we're on track and can handle more frames.
            // Can set directly since we're holding paint_mutex
            outstanding_paint_event = FALSE;
        }

        paint_mutex = FALSE;
        return 0;
    case WM_DIABNEXTLVL:
    case WM_DIABPREVLVL:
    case WM_DIABRTNLVL:
    case WM_DIABSETLVL:
    case WM_406:
        delayed_Msg = uMsg;
        InitCutscene();
        fade_state = PALETTE_FADE_OUT;
        gMode = MODE_PROGRESS;
        set_outstanding_paint_event(FALSE);
        return 0;
    case WM_40A:
        if (sghMusic && debugMusicOn && gbMusicOn)
        {
            SFileDdaEnd(sghMusic);
            SFileCloseFile(sghMusic);
            sghMusic = NULL;
        }
        if (!demo_mode || debug_mode)
        {
            DestroyWindow(hWnd);
        }
        else
        {
            interfac_InitDemoEnd();
            LoadPalette("Gendata\\Screen01.pal", orig_palette);
            fade_state = PALETTE_FADE_OUT;
            delayed_Msg = WM_DIABDONEFADEOUT;
            gMode = MODE_DEMO_END;
            set_outstanding_paint_event(FALSE);
        }
        return 0;
    case WM_DESTROY:
        shouldStopPaintTimer = TRUE;
        timeEndPeriod(paint_event_timer_resolution);
        MemFreeDbg(gpBuffer);
        MemFreeDbg(pLightTbl);
        MemFreeDbg(pDungeonCels);
        MemFreeDbg(pMegaTiles);
        MemFreeDbg(pLevelPieces);
        MemFreeDbg(pSpecialCels);
        FreeControlPan();
        FreeInvGFX();
        FreeGMenu();
        FreeQuestText();
        FreeStoreMem();
        for (i = 0; i < gbActivePlayers; ++i)
        {
            FreePlayerGFX(i);
        }
        FreeItemGFX();
        if (leveltype != DTYPE_TOWN)
        {
            MemFreeDbg(pSpeedCels);
            FreeMonsters();
            FreeObjectGFX();
            FreeMissileGFX();
            FreeSpells();
            FreeMonsterSnd();
        }
        else
        {
            FreeTownerGFX();
            FreeMissileGFX();
            FreeTownersEffects();
        }
        FreeDebugGFX();
        if (musicFromDisk)
        {
            SNDCPP_FreeThread();
            Mopaq_free();
        }
        sound_stop();
        SFileCloseArchive(diabdat_mpq);
        SVidDestroy();
        if (debugMusicOn)
        {
            SFileDdaDestroy();
        }
        sound_cleanup();
        dx_cleanup();
        PostQuitMessage(0);
        return 0;
    }

    return DefWindowProcA(hWnd, uMsg, wParam, lParam);
}

// .text:004881C9
// This is THE wndproc used by the game window. It delegates to subroutines
// depending on game mode (gMode) but the wndproc itself is never changes.
//
// This largely just handles event processing and other non-game state.
LRESULT __stdcall MainWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    switch (gMode)
    {
    case MODE_BLIZ_LOGO:
        return WndProc_BlizLogo(hWnd, Msg, wParam, lParam);
    case MODE_MAINMENU:
        return WndProc_MainMenu(hWnd, Msg, wParam, lParam);
    case MODE_NEWGAME:
        return WndProc_ChooseClassAndName(hWnd, Msg, wParam, lParam);
    case MODE_GAME:
        // GM_Game does input processing (and painting). game_logic uses that to progress game state.
        return GM_Game(hWnd, Msg, wParam, lParam);
    case MODE_INTRO_VID:
        return WndProc_IntroVid(hWnd, Msg, wParam, lParam);
    case MODE_DEMO_END:
        return WndProc_QuitSlideshow(hWnd, Msg, wParam, lParam);
    case MODE_PROGRESS:
        return ShowProgress(hWnd, Msg, wParam, lParam);
    }

    return DefWindowProcA(hWnd, Msg, wParam, lParam);
}

// .text:004882F9
// Block on `paint_mutex` then set `outstanding_paint_event` to `b`.
// If `b == TRUE` then PaintEventTimer is allowed to generate WM_DIABPAINT.
// If `b == FALSE` then PaintEventTimer won't do anything.
// If you already hold `paint_mutex` then just set `outstanding_paint_event` yourself.
void set_outstanding_paint_event(BOOL b)
{
    while (paint_mutex)
    {
        // loop until set to 0
    }

    paint_mutex = TRUE;
    outstanding_paint_event = b;
    paint_mutex = FALSE;
}

// .text:0048833D
// Posts WM_DIABPAINT every 50ms to maintain 20 FPS. The timer itself is fired
// more often--every 5ms--which gives us more chances to check that we're on schedule.
void CALLBACK PaintEventTimer(UINT uTimerID,
                              UINT uMsg,
                              DWORD dwUser,
                              DWORD dw1,
                              DWORD dw2)
{
    // Another thread must be painting; no need to continue.
    if (paint_mutex)
    {
        return;
    }

    // Block other code from doing anything painting related
    paint_mutex = TRUE;

    DWORD now = timeGetTime();
    frames++;

    // Record and reset FPS count every second; useful for diagnostics only.
    if (now - prevTime >= 1000)
    {
        framerate = frames;
        frames = 0;

        prevTime = now;

        draw_framerate = frames_drawn;
        frames_drawn = 0;
    }

    // If it's been more than 50ms then try to generate a WM_DIABPAINT event.
    // There's two things that can stop this:
    // 1. The last paint wasn't handled (outstanding_paint_event == TRUE). Don't want to overwhelm the system.
    // 2. We're on the loading screen which takes control of painting (pause_paint_timer == TRUE). Don't want to paint more than we have to.
    // Note that there are cases like IncProgress where outstanding_paint_event is treated as the paint event.
    if (now - prev_timer_PostMessage_time >= TIME_BETWEEN_FRAMES_MS)
    {
        if (outstanding_paint_event == FALSE)
        {
            outstanding_paint_event = TRUE;
            if (pause_paint_timer == FALSE)
            {
                PostMessage((HWND)dwUser, WM_DIABPAINT, 0, 0);
            }

            prev_timer_PostMessage_time = now;
        }
    }

    // Stop if someone has signalled that to us (e.g. app exit)
    if (shouldStopPaintTimer)
    {
        timeKillEvent(uTimerID);
        ExitThread(0);
    }

    // Allow other code to do paint stuff
    paint_mutex = FALSE;
}

// .text:00488442
int InitLevelType(int l)
{
    if (l == 0)
        return DTYPE_TOWN;
    if (l >= 1 && l <= 4)
        return DTYPE_CATHEDRAL;
    if (l >= 5 && l <= 8)
        return DTYPE_CATACOMBS;
    if (l >= 9 && l <= 12)
        return DTYPE_CAVES;

    return DTYPE_HELL;
}

// .text:004884C8
// Initialize a bunch of important global variables like:
// - _pSeedTable and _pLevelTypeTbl
// - MouseX/Y
// - ScrollInfo
// - ... and many more!
void diablo_init_screen()
{
    int i;
    time_t seed_time;
    int j;

    // Set, but never read again...
    dword_61B718 = 0;
    dword_61B71C = 0;
    dword_61B720 = BUFFER_WIDTH;
    dword_61B724 = BUFFER_HEIGHT;
    dword_61BF58 = 0;
    dword_61BF5C = 0;
    dword_61BF60 = SCREEN_WIDTH;
    dword_61BF64 = SCREEN_HEIGHT;

    if (debug_mode)
    {
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < NUMLEVELS; j++)
            {
                plr[i]._pSeedTbl[j] = j;
                plr[i]._pLevelTypeTbl[j] = InitLevelType(j);
            }
        }
    }
    else
    {
        srand(time(&seed_time));
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < NUMLEVELS; j++)
            {
                plr[i]._pSeedTbl[j] = random_(0x8000);
                plr[i]._pLevelTypeTbl[j] = InitLevelType(j);
            }
        }
    }

    MouseX = SCREEN_WIDTH / 2;
    MouseY = SCREEN_HEIGHT / 2;
    fade_state = 0;
    ScrollInfo._sdx = 0;
    ScrollInfo._sdy = 0;
    ScrollInfo._sxoff = 0;
    ScrollInfo._syoff = 0;
    ScrollInfo._sdir = 0;
    screenshot_idx = 0;

    for (j = 0; j < 1024; j++)
    {
        PitchTbl[i] = i * BUFFER_WIDTH;
    }

    // All of these are set but never used.
    dword_615F18 = 0;
    dword_605474 = 0;
    dword_615F14 = 0;
    dword_605390 = 0; // This might actually be used
    dword_615F1C = 0;
    dword_615F20 = 0;
    dword_615F24 = 0;
    dword_615F28 = 0;
    dword_615F2C = 0;
    dword_615F30 = 0;
    dword_615F34 = 0;
    dword_615F38 = 0;
}

// .text:00488769
// Create a window and initialize DirectDraw/DirectSound/Storm.
// Returns FALSE if it can't make the window, TRUE otherwise.
// `hInstance` and `nShowCmd` should come from WinMain.
// The rest of the initialized is deferred until the event loop is running thanks to dx_init() posting WM_DIABDONEFADEIN.
BOOL init_create_window(HINSTANCE hInstance, int nShowCmd)
{
    HWND hWnd;
    WNDCLASSA wc;

    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = &MainWndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
    wc.hCursor = LoadCursor(hInstance, IDI_APPLICATION);
    wc.hbrBackground = 0;
    wc.lpszMenuName = "Diablo";
    wc.lpszClassName = "Diablo";
    RegisterClassA(&wc);

    hWnd = CreateWindowExA(WS_EX_TOPMOST,
                           "Diablo",
                           "Diablo Game",
                           WS_POPUP,
                           0,
                           0,
                           GetSystemMetrics(SM_CXSCREEN),
                           GetSystemMetrics(SM_CYSCREEN),
                           NULL,
                           NULL,
                           hInstance,
                           NULL);
    if (hWnd == NULL)
    {
        return FALSE;
    }

    ghMainWnd = hWnd;
    ShowWindow(hWnd, nShowCmd);
    UpdateWindow(hWnd);

    // Perform the rest of the initialization (DirectDraw, DirectSound, etc)
    dx_init(hWnd); // why is the return ignored :/

    return TRUE;
}

// .text:0048885D
void dx_cleanup()
{
    if (lpDDInterface)
    {
        if (lpDDSBackBuf)
        {
            lpDDSBackBuf->Release();
            lpDDSBackBuf = NULL;
        }
        if (lpDDPalette)
        {
            lpDDPalette->Release();
            lpDDPalette = NULL;
        }
        lpDDInterface->Release();
        lpDDInterface = NULL;
    }
}

// .text:004888E2
void FreeGameMem()
{
    MemFreeDbg(pDungeonCels);
    MemFreeDbg(pMegaTiles);
    MemFreeDbg(pLevelPieces);
    MemFreeDbg(pSpecialCels);

    if (leveltype != DTYPE_TOWN)
    {
        MemFreeDbg(pSpeedCels);

        FreeMonsters();
        FreeObjectGFX();
        FreeMissileGFX();
        FreeSpells();
        FreeMonsterSnd();
    }
    else
    {
        FreeTownerGFX();
        FreeMissileGFX();
        FreeTownersEffects();
    }
}

// .text:004889EA
static void LoadLvlGFX()
{
    switch (leveltype)
    {
    case DTYPE_TOWN:
        pDungeonCels = LoadFileInMem("Levels\\TownData\\Town.CEL");
        pMegaTiles = LoadFileInMem("Levels\\TownData\\Town.TIL");
        pLevelPieces = LoadFileInMem("Levels\\TownData\\Town.MIN");
        pSpecialCels = LoadFileInMem("Levels\\TownData\\TownS.CEL");
        break;
    case DTYPE_OLD_CATHEDRAL:
        pDungeonCels = LoadFileInMem("Levels\\L1Data\\L1.CEL");
        pMegaTiles = LoadFileInMem("Levels\\L1Data\\L1.TIL");
        pLevelPieces = LoadFileInMem("Levels\\L1Data\\L1.MIN");
        pSpecialCels = LoadFileInMem("Levels\\L1Data\\L1S.CEL");
        break;
    case DTYPE_CATACOMBS:
        pDungeonCels = LoadFileInMem("Levels\\L2Data\\L2.CEL");
        pMegaTiles = LoadFileInMem("Levels\\L2Data\\L2.TIL");
        pLevelPieces = LoadFileInMem("Levels\\L2Data\\L2.MIN");
        pSpecialCels = LoadFileInMem("Levels\\L2Data\\L2S.CEL");
        break;
    case DTYPE_CAVES:
        pDungeonCels = LoadFileInMem("Levels\\L3Data\\L3.CEL");
        pMegaTiles = LoadFileInMem("Levels\\L3Data\\L3.TIL");
        pLevelPieces = LoadFileInMem("Levels\\L3Data\\L3.MIN");
        pSpecialCels = LoadFileInMem("Levels\\L1Data\\L1S.CEL"); // lol
        break;
    case DTYPE_HELL:
        pDungeonCels = LoadFileInMem("Levels\\L2Data\\L2.CEL");
        pMegaTiles = LoadFileInMem("Levels\\L2Data\\L2.TIL");
        pLevelPieces = LoadFileInMem("Levels\\L2Data\\L2.MIN");
        pSpecialCels = LoadFileInMem("Levels\\L2Data\\L2S.CEL");
        break;
    case DTYPE_CATHEDRAL:
        pDungeonCels = LoadFileInMem("Levels\\L1Data\\L1.CEL");
        pMegaTiles = LoadFileInMem("Levels\\L1Data\\L1.TIL");
        pLevelPieces = LoadFileInMem("Levels\\L1Data\\L1.MIN");
        pSpecialCels = LoadFileInMem("Levels\\L1Data\\L1S.CEL");
        break;
    }
}

// .text:00488BC1
static void LoadAllGFX(BOOL inc_progress)
{
    pSpeedCels = (BYTE *)DiabloAllocPtr(0x100000);
    InitMonsterGFX();
    if (inc_progress)
    {
        IncProgress();
    }
    InitMonsterSND();
    if (inc_progress)
    {
        IncProgress();
    }
    InitObjectGFX();
    if (inc_progress)
    {
        IncProgress();
    }
    InitMissileGFX();
    if (inc_progress)
    {
        IncProgress();
    }
    InitSpellGFX();
    if (inc_progress)
    {
        IncProgress();
    }
}

// .text:00488C54
void CreateLevel(int lvldir)
{
    switch (leveltype)
    {
    case DTYPE_TOWN:
        CreateTown(lvldir);
        InitTownTriggers();
        LoadRndLvlPal(DTYPE_TOWN);
        break;
    case DTYPE_OLD_CATHEDRAL:
        CreateL1Dungeon(plr[myplr]._pSeedTbl[currlevel], lvldir);
        InitL1Triggers();
        LoadRndLvlPal(DTYPE_OLD_CATHEDRAL);
        break;
    case DTYPE_CATACOMBS:
        CreateL2Dungeon(plr[myplr]._pSeedTbl[currlevel], lvldir);
        InitL2Triggers();
        LoadRndLvlPal(DTYPE_CATACOMBS);
        break;
    case DTYPE_CAVES:
        CreateL3Dungeon(plr[myplr]._pSeedTbl[currlevel], lvldir);
        InitL3Triggers();
        LoadRndLvlPal(DTYPE_CATACOMBS);
        palette_init_caves();
        break;
    case DTYPE_HELL:
        CreateL4Dungeon(plr[myplr]._pSeedTbl[currlevel], lvldir);
        InitL4Triggers();
        LoadRndLvlPal(DTYPE_CATACOMBS);
        break;
    case DTYPE_CATHEDRAL:
        CreateL5Dungeon(plr[myplr]._pSeedTbl[currlevel], lvldir);
        InitL1Triggers();
        LoadRndLvlPal(DTYPE_OLD_CATHEDRAL);
        break;
    }
}

// .text:00488E06
// If `firstflag` is TRUE then treat this call as the first time that the player has entered `currlevel`. That means extra level logic needs to occur to ensure that it's suitable.
// TODO is firstflag per player or per session?
// If `inc_progress` is TRUE then this was called from the loading screen so this will increment the loading progress bar.
// TODO what is `lvldir`
void LoadGameLevel(BOOL firstflag, int lvldir, BOOL inc_progress)
{
    int pnum;

    // Stop any playing music.
    if (sghMusic && debugMusicOn && gbMusicOn)
    {
        SFileDdaEnd(sghMusic);
        SFileCloseFile(sghMusic);
        sghMusic = NULL;
    }
    SetCursor_(CURSOR_NONE);
    srand(plr[myplr]._pSeedTbl[currlevel]);
    if (leveltype == DTYPE_TOWN)
    {
        SetupTownStores();
    }
    if (inc_progress)
    {
        IncProgress();
    }
    LoadDebugGFX();
    if (inc_progress)
    {
        IncProgress();
    }
    LoadLvlGFX();
    if (inc_progress)
    {
        IncProgress();
    }
    if (firstflag)
    {
        gmenu_init_menu();
        InitInv();
        InitItemGFX();
        InitQuestText();
        for (pnum = 0; pnum < gbActivePlayers; ++pnum)
        {
            InitPlrGFXMem(pnum);
        }
        InitStores();
        InitAutomapOnce();
        InitHelp();
    }
    srand(plr[myplr]._pSeedTbl[currlevel]);
    if (inc_progress)
    {
        IncProgress();
    }
    if (leveltype != DTYPE_TOWN)
    {
        InitAutomap();
    }
    if (leveltype != DTYPE_TOWN && lvldir != ENTRY_LOAD)
    {
        InitLighting();
    }
    if (inc_progress)
    {
        IncProgress();
    }
    if (!setlevel)
    {
        CreateLevel(lvldir);
        if (inc_progress)
        {
            IncProgress();
        }
        // TODO
    }
    else
    {
        // TODO
    }

    // TODO
    // calls LoadLevel
}

// .text:0048947E
void InitLevels()
{
    if (debug_mode)
    {
        // With cheats, start in dlvl 1 with all quests
        setlevel = FALSE;
        setlvlnum = 1;
        currlevel = 1;
        leveltype = DTYPE_CATHEDRAL;
        quests[Q_SKELKING]._qactive = QUEST_ACTIVE;
        quests[Q_BUTCHER]._qactive = QUEST_ACTIVE;
        quests[Q_ROCK]._qactive = QUEST_ACTIVE;
        quests[Q_SCHAMB]._qactive = QUEST_ACTIVE;
    }
    else
    {
        // Otherwise start in town
        setlevel = FALSE;
        currlevel = 0;
        leveltype = DTYPE_TOWN;
    }

    LoadGameLevel(TRUE, 0, TRUE); // TODO 0 is magic number
}

// .text:00489510
// TODO better name
void diablo_489510_plr_rel()
{
    BOOL done;
    int i;

    // only do a thing if anonymous_2 for myplr is not 0 (so 1/2/3)
    if (plr[myplr].anonymous_2 != 0)
    {
        done = TRUE;

        // if we're 3
        if (plr[myplr].anonymous_2 == 3) // TODO magic number
        {
            //
            for (i = 0; i < gbActivePlayers; i++)
            {
                if (plr[i].plractive && plr[i].anonymous_2 != 3) // TODO magic number
                {
                    done = FALSE;
                }
            }

            if (done)
            {
                unknown_dplay_case_plr();
            }
        }
        else
        {
            for (i = 0; i < gbActivePlayers; i++)
            {
                if (plr[i].plractive)
                {
                    if (plr[i].anonymous_2 == 0) // TODO magic number
                    {
                        done = FALSE;
                    }
                    if (plr[i].anonymous_2 == 3) // TODO magic number
                    {
                        done = FALSE;
                    }
                }
            }

            if (done && plr[myplr].anonymous_2 == 2) // TODO magic number
            {
                unknown_dplay_game_send(dword_5DE7B4);
            }

            if (done && plr[myplr].anonymous_2 == 1) // TODO magic number
            {
                unknown_dplay_get_plrs(dword_5DE7B4);
            }
        }
    }
}

// .text:00489715
void game_logic()
{
    if (leveltype != DTYPE_TOWN)
    {
        Player_DeserializeNextAction();
        ProcessPlayers();
        ProcessMonsters();
        ProcessObjects();
        ProcessMissiles();
        ProcessSpells();
        ProcessItems();
        CheckCursMove();
        if (helpmodeflag)
        {
            DrawHelpMode();
        }
        if (cheat_mode)
        {
            if (GetAsyncKeyState(VK_SHIFT) & 0xF000)
            {
                ScrollView();
            }
        }
        ProcessLightList();
        ProcessVisionList();
    }
    else
    {
        Player_DeserializeNextAction();
        ProcessPlayers();
        ProcessTowners();
        ProcessItems();
        ProcessMissiles();
        CheckCursMove();
        if (helpmodeflag)
        {
            DrawHelpMode();
        }
        if (cheat_mode)
        {
            if (GetAsyncKeyState(VK_SHIFT) & 0xF000)
            {
                ScrollView();
            }
        }
    }
    CheckTriggers();
    CheckQuests();
    force_redraw |= FORCE_REDRAW_VIEWPORT;
    diablo_489510_plr_rel();
}
