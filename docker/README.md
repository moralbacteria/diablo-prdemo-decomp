# Wine-Based Build Environment

We provide several docker containers for easily compiling our source with a
period-appropriate compiler:

- `vc40/` - Visual C++ 4.0
- `vc41/` - Visual C++ 4.1
- `vc42/` - Visual C++ 4.2 <-- This is what we currently use

Each directory has a README explaining how to build