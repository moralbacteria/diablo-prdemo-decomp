#include "drlg_l1.h"

#include "diablo.h"
#include "engine.h"
#include "gendung.h"
#include "lighting.h"
#include "quests.h"

//
// defines
//

#define QUAD_UNSPECIFIED -1
#define QUAD_TOPLEFT 0
#define QUAD_TOPRIGHT 1
#define QUAD_BOTTOMLEFT 2
#define QUAD_BOTTOMRIGHT 3

#define WEST_DOOR 8
#define EAST_DOOR 4
#define NORTH_DOOR 2
#define SOUTH_DOOR 1
#define NO_DOOR 0

//
// structs
//

// TODO docs and names
struct l1_door_flags
{
    int field_0;  // flag (specific to drlg_l1_48F2A4_get_dflags)
    int field_4;  // doors for top-left quadrant; see *_DOOR
    int field_8;  // doors for top-right quadrant; see *_DOOR
    int field_C;  // doors for bottom-left quadrant; see *_DOOR
    int field_10; // doors for bottom-right quadrant; see *_DOOR
};

//
// Initialized variables (.data:004BC7A8)
//

// wtf is this? nobody references it
static char registration_block[128] = "REGISTRATION_BLOCK";

// TODO docs and names
// When a room is big enough, it's subdivided into four.
// This table says which doors those quadrants should have.
l1_door_flags door_flag_lookup[] = {
    {8, 4, 1, 4, 0},
    {8, 1, 1, 4, 0},
    {4, 5, 0, 4, 0},
    {8, 1, 1, 4, 0},
    {2, 5, 1, 0, 0},
    {2, 4, 1, 4, 0},
    {1, 5, 1, 0, 0},
    {1, 1, 1, 4, 0},
    {15, 5, 1, 4, 0},
};

ShadowStruct SPATS[37] = {
    {7, 13, 0, 13, 144, 0, 142},
    {16, 13, 0, 13, 144, 0, 142},
    {15, 13, 0, 13, 145, 0, 142},
    {5, 13, 13, 13, 152, 140, 139},
    {5, 13, 1, 13, 143, 146, 139},
    {5, 13, 13, 2, 143, 140, 148},
    {5, 0, 1, 2, 0, 146, 148},
    {5, 13, 11, 13, 143, 147, 139},
    {5, 13, 13, 12, 143, 140, 149},
    {5, 13, 11, 12, 150, 147, 149},
    {5, 13, 1, 12, 143, 146, 149},
    {5, 13, 11, 2, 143, 147, 148},
    {9, 13, 13, 13, 144, 140, 142},
    {9, 13, 1, 13, 144, 146, 142},
    {9, 13, 11, 13, 151, 147, 142},
    {8, 13, 0, 13, 144, 0, 139},
    {8, 13, 0, 12, 143, 0, 149},
    {8, 0, 0, 2, 0, 0, 148},
    {11, 0, 0, 13, 0, 0, 139},
    {11, 13, 0, 13, 139, 0, 139},
    {11, 2, 0, 13, 148, 0, 139},
    {11, 12, 0, 13, 149, 0, 139},
    {11, 13, 11, 12, 139, 0, 149},
    {14, 0, 0, 13, 0, 0, 139},
    {14, 13, 0, 13, 139, 0, 139},
    {14, 2, 0, 13, 148, 0, 139},
    {14, 12, 0, 13, 149, 0, 139},
    {14, 13, 11, 12, 139, 0, 149},
    {10, 0, 13, 0, 0, 140, 0},
    {10, 13, 13, 0, 140, 140, 0},
    {10, 0, 1, 0, 0, 146, 0},
    {10, 13, 11, 0, 140, 147, 0},
    {12, 0, 13, 0, 0, 140, 0},
    {12, 13, 13, 0, 140, 140, 0},
    {12, 0, 1, 0, 0, 146, 0},
    {12, 13, 11, 0, 140, 147, 0},
    {3, 13, 11, 12, 150, 0, 0},
};

// Maps tile IDs to their corresponding undecorated tile ID. This is used for
// CreateL1Dungeon. Often one tile will have multiple variants that include
// visual fluorish. E.g. tile 1 and 100 are both vertical walls, but 1 is plain
// and 100 includes a blood splatter.
//
// NOTE: There are 206 megatiles defined in L1.TIL
// NOTE: index 0 is unused; this is 1-based.
// NOTE: an entry of 0 means there is no equivalent.
//
// BUG should this be 207 since element 0 is unused?
BYTE L1BTYPES[206] = {
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9,         // 0-9
    10, 11, 12, 13, 14, 15, 16, 17, 0, 0, // 10-19
    0, 0, 0, 0, 0, 25, 26, 4, 28, 4,      // 20-29
    30, 31, 6, 7, 41, 1, 2, 4, 10, 43,    // 30-39
    40, 41, 42, 43, 14, 0, 0, 0, 0, 0,    // 40-49
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         // 50-59
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         // 60-69
    0, 0, 0, 0, 0, 0, 0, 0, 0, 79,        // 70-79
    80, 0, 82, 0, 0, 0, 0, 0, 0, 79,      // 80-89
    0, 80, 0, 0, 79, 80, 0, 2, 2, 2,      // 90-99
    1, 1, 11, 25, 13, 13, 13, 1, 2, 1,    // 100-109
    2, 1, 2, 1, 2, 2, 2, 2, 12, 0,        // 110-119
    0, 11, 1, 11, 1, 13, 0, 0, 0, 0,      // 120-129
    0, 0, 0, 13, 13, 13, 13, 13, 13,      // 130-139
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         // 140-149
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         // 150-159
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         // 160-169
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         // 170-179
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         // 180-189
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         // 190-199
    0, 0, 0, 0, 0, 0, 0};                 // 200-206

/** Maps tile IDs to their corresponding base tile ID. */
// eg west door maps to west wall
// The DRLG only really cares about a the basic functionality of a tile, not the specifics of exactly what it looks like.
// Many tiles are variations on the same basic functional tile.
BYTE BSTYPES[206] = {
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9,         // 0-9
    10, 11, 12, 13, 14, 15, 16, 17, 0, 0, // 10-19
    0, 0, 0, 0, 0, 1, 2, 10, 4, 5,        // 20-29
    6, 7, 8, 9, 10, 11, 12, 14, 5, 14, 10, 4, 14, 4, 5, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 1,
    6, 7, 16, 17, 2, 1, 1, 2, 2, 1, 1, 2, 2, 2, 2, 2, 1,
    1, 11, 1, 13, 13, 13, 1, 2, 1, 2, 1, 2, 1, 2, 2, 2,
    2, 12, 0, 0, 11, 1, 11, 1, 13, 0, 0, 0, 0, 0, 0, 0,
    13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13,
    1, 11, 2, 12, 13, 13, 13, 12, 2, 1, 2, 2, 4, 14, 4,
    10, 13, 13, 4, 4, 1, 1, 4, 2, 2, 13, 13, 13, 13, 25,
    26, 28, 30, 31, 41, 43, 40, 41, 42, 43, 25, 41, 43,
    28, 28, 1, 2, 25, 26, 22, 22, 25, 26, 0, 0, 0, 0, 0,
    0, 0};

BYTE L5BTYPES[206] = {
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9,         // 0-9
    10, 11, 12, 13, 14, 15, 16, 17, 0, 0, // 10-19
    0, 0, 0, 0, 0, 25, 26, 0, 28, 0,      // etc
    30, 31, 0, 0, 0, 0, 0, 0, 0, 0,
    40, 41, 42, 43, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 79,
    80, 0, 82, 0, 0, 0, 0, 0, 0, 79,
    0, 80, 0, 0, 79, 80, 0, 2, 2, 2,
    1, 1, 11, 25, 13, 13, 13, 1, 2, 1,
    2, 1, 2, 1, 2, 2, 2, 2, 12, 0,
    0, 11, 1, 11, 1, 13, 0, 0, 0, 0,
    0, 0, 0, 13, 13, 13, 13, 13, 13, 0, // 130-139
    0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0};

/** Miniset: stairs up (empty space behind)*/
// This can be generated on a horizontal wall with floor space on either side
BYTE STAIRSUP[] = {
    4, 4, // width, height

    13, 13, 13, 13, // search
    2, 2, 2, 2,
    13, 13, 13, 13,
    13, 13, 13, 13,

    0, 66, 6, 0, // replace
    63, 64, 65, 0,
    0, 67, 68, 0,
    0, 0, 0, 0};

/** Miniset: stairs up. (wall behind) */
BYTE L5STAIRSUP[] = {
    4, 4, // width, height

    22, 22, 22, 22, // search
    2, 2, 2, 2,
    13, 13, 13, 13,
    13, 13, 13, 13,

    0, 66, 23, 0, // replace
    63, 64, 65, 0,
    0, 67, 68, 0,
    0, 0, 0, 0};

/** Miniset: stairs down. */
BYTE STAIRSDOWN[] = {
    4, 3, // width, height

    13, 13, 13, 13, // search
    13, 13, 13, 13,
    13, 13, 13, 13,

    62, 57, 58, 0, // replace
    61, 59, 60, 0,
    0, 0, 0, 0};

/** Miniset: candle stick. */
// 128 is the brazier; 129/130 are floor tiles with the brazier shadow
BYTE LAMPS[] = {
    2, 2, // width, height

    13, 0, // search
    13, 13,

    129, 0, // replace
    130, 128};

/** UNUSED Miniset: sarcophogus */
// 131 is sarc, 132 is floor with shadow
BYTE SARC[] = {
    2, 2, // width, height

    13, 13, // search
    13, 13,

    132, 131, // replace
    0, 0};

/** Miniset: fence wall (horizontal) */
BYTE FENCE1[] = {
    5, 1, // width, height

    2, 2, 2, 2, 2, // search

    36, 36, 36, 36, 36 // replace
};

/** Miniset: fence wall (vertical) */
BYTE FENCE2[] = {
    1, 5, // width, height

    1, 1, 1, 1, 1, // search

    35, 35, 35, 35, 35 // replace
};

/**
 * A lookup table for the 16 possible patterns of a 2x2 area,
 * where each cell either contains a SW wall or it doesn't.
 */
BYTE L5ConvTbl[16] = {
    22, 13,
    1, 13,

    2, 13,
    13, 13,

    4, 13,
    1, 13,

    2, 13,
    16, 13};

//
// Uninitialized variables (.data:0061BFE8)
//

// See enum dlrg_flag
char L5dflags[DMAXX][DMAXY];
BYTE *L5pSetPiece;
// ...
BOOL L5setloadflag;
// ...

//
// Code (.text:0048EAA0)
//

// .text:0048EAA0
// Draw the large central chamber with four pillars.
// sx/sy are in megatile coordinates. They are the topleft-ish coordinates of the chamber.
// The entire room extends from (sx-2, sy-2) to (sx-2+11, sy-2+11)
// The flags determine which sides are open for exits. They are all TRUE.
void DRLG_L1GChamber(int sx, int sy, BOOL topflag, BOOL bottomflag, BOOL leftflag, BOOL rightflag)
{
    int i, j;

    sx -= 2;
    sy -= 2;

    // Generate corners
    dungeon[sx][sy] = 4;           // corner with walls
    dungeon[sx + 11][sy] = 6;      // vertical wall with edge
    dungeon[sx][sy + 11] = 7;      // horizontal wall with edge
    dungeon[sx + 11][sy + 11] = 3; // corner edge

    // Generate sides
    if (topflag == TRUE)
    {
        // Add opening that integrates with hallways.
        dungeon[sx + 1][sy] = 2;  // horizontal wall
        dungeon[sx + 2][sy] = 12; // horizontal arch
        dungeon[sx + 3][sy] = 12;
        dungeon[sx + 4][sy] = 3;
        dungeon[sx + 7][sy] = 9; // horizontal arch edge
        dungeon[sx + 8][sy] = 12;
        dungeon[sx + 9][sy] = 2;
        dungeon[sx + 10][sy] = 2;
    }
    else
    {
        // Add solid wall.
        for (i = 1; i < 11; ++i)
        {
            dungeon[sx + i][sy] = 2;
        }
    }

    if (bottomflag == TRUE)
    {
        sy += 11;
        dungeon[sx + 1][sy] = 2;
        dungeon[sx + 2][sy] = 10;
        dungeon[sx + 3][sy] = 12;
        dungeon[sx + 4][sy] = 8;
        dungeon[sx + 7][sy] = 5;
        dungeon[sx + 8][sy] = 12;
        dungeon[sx + 9][sy] = 4;
        dungeon[sx + 10][sy] = 2;
        sy -= 11;
    }
    else
    {
        for (i = 1; i < 11; ++i)
        {
            dungeon[sx + i][sy + 11] = 2;
        }
    }

    if (leftflag == TRUE)
    {
        dungeon[sx][sy + 1] = 1;
        dungeon[sx][sy + 2] = 11;
        dungeon[sx][sy + 3] = 11;
        dungeon[sx][sy + 4] = 3;
        dungeon[sx][sy + 7] = 8;
        dungeon[sx][sy + 8] = 11;
        dungeon[sx][sy + 9] = 1;
        dungeon[sx][sy + 10] = 1;
    }
    else
    {
        for (j = 1; j < 11; ++j)
        {
            dungeon[sx][sy + j] = 1;
        }
    }

    if (rightflag == TRUE)
    {
        sx += 11;
        dungeon[sx][sy + 1] = 1;
        dungeon[sx][sy + 2] = 14;
        dungeon[sx][sy + 3] = 11;
        dungeon[sx][sy + 4] = 9;
        dungeon[sx][sy + 7] = 5;
        dungeon[sx][sy + 8] = 11;
        dungeon[sx][sy + 9] = 4;
        dungeon[sx][sy + 10] = 1;
        sx -= 11;
    }
    else
    {
        for (j = 1; j < 11; ++j)
        {
            dungeon[sx + 11][sy + j] = 1;
        }
    }

    // Fill in center
    for (j = 1; j < 11; j++)
    {
        for (i = 1; i < 11; i++)
        {
            dungeon[i + sx][j + sy] = 13;
            L5dflags[i + sx][j + sy] |= DLRG_CHAMBER;
        }
    }

    // Add pillars
    dungeon[sx + 4][sy + 4] = 15;
    dungeon[sx + 7][sy + 4] = 15;
    dungeon[sx + 4][sy + 7] = 15;
    dungeon[sx + 7][sy + 7] = 15;
}

// .text:0048EEE9
// Determine the best tile for (x, y) given the surrounding tiles.
// x/y are in megatile coordinates.
// TODO better name
BYTE drlg_l1_48EEE9_piece_type(int x, int y)
{
    BYTE east_tile;
    BYTE south_tile;
    BYTE return_tile;

    east_tile = dungeon[x + 1][y];
    south_tile = dungeon[x][y + 1];
    return_tile = 0;

    // Treat arches like walls
    if (east_tile && east_tile == 12)
    {
        east_tile = 2;
    }
    if (south_tile && south_tile == 11)
    {
        south_tile = 1;
    }

    if (east_tile == 2)
    {
        if (south_tile == 1)
        {
            return_tile = 4; // NW corner
        }
        if (south_tile == 11)
        {
            return_tile = 14; // NW corner with West Arch
        }
        if (!south_tile)
        {
            if (dungeon[x - 1][y])
            {
                return_tile = 2; // North wall
            }
            else
            {
                return_tile = 7; // North edge
            }
        }
    }

    // BUG? This is never hit since east_tile is overridedn earlier
    if (east_tile == 12)
    {
        if (south_tile == 11)
        {
            return_tile == 5;
        }
        if (!south_tile)
        {
            if (dungeon[x - 1][y])
            {
                return_tile = 12;
            }
        }
        else
        {
            return_tile = 9;
        }
    }

    if (!east_tile)
    {
        if (south_tile == 1)
        {
            if (dungeon[x][y - 1])
            {
                return_tile = 1;
            }
            else
            {
                return_tile = 6;
            }
        }
        if (south_tile == 11)
        {
            if (dungeon[x][y - 1])
            {
                return_tile = 11; // west arch
            }
            else
            {
                return_tile = 8; // West edge arch
            }
        }
        if (!south_tile)
        {
            east_tile = dungeon[x - 1][y];  // repurpose east_tile for west tile
            south_tile = dungeon[x][y - 1]; // repurpose south_tile for north tile
            return_tile = 0;

            if (!east_tile && (south_tile == 1 || south_tile == 11))
            {
                return_tile = 16;
            }
            if (east_tile == 2 || east_tile == 12)
            {
                if (!south_tile)
                {
                    return_tile = 17;
                }
            }
            if (east_tile && south_tile)
            {
                return_tile = 3;
            }
        }
    }

    return return_tile;
}

// .text:0048F163
// Places a door along the boundaries of a quad so that the player can get in.
// x1/y1/x2/y2 are the boundaries of the quadrant in megatiles
// `flag` describes which wall of the quadrant to include a door, one of *_DOOR
// If 0 then no doors are placed
void DRLG_L1PlaceQuadBoundaryDoors(int x1, int y1, int x2, int y2, int flag)
{
    int width, height;
    int xx, yy;

    width = x2 - x1;
    height = y2 - y1;

    if (flag & WEST_DOOR)
    {
        xx = x1;
        yy = y1 + random_(height);
        L5dflags[xx][yy] |= DLRG_VDOOR;
    }

    if (flag & EAST_DOOR)
    {
        xx = x2;
        yy = y1 + random_(height);
        L5dflags[xx][yy] |= DLRG_VDOOR;
    }

    if (flag & NORTH_DOOR)
    {
        xx = x1 + random_(width);
        yy = y1;
        L5dflags[xx][yy] |= DLRG_HDOOR;
    }

    if (flag & SOUTH_DOOR)
    {
        xx = x1 + random_(width);
        yy = y2;
        L5dflags[xx][yy] |= DLRG_HDOOR;
    }
}

// .text:0048F2A4
// Choose doors for a generated room based on the surrounding generated rooms.
// x1/y1/x2/y2 is the room
// x3/y3 is inside the room so x1 < x3 < x2 etc
// Returns an index into `door_flag_lookup`
int drlg_l1_48F2A4_get_dflags(int x1, int y1, int x2, int y2, int x3, int y3)
{
    int flags;
    int i;
    int rv;

    flags = 0;

    for (i = 0; i < x3; ++i)
    {
        if (L5dflags[i][y1] & ~DLRG_PROTECTED)
        {
            flags |= 8;
        }
        if (L5dflags[i][y2] & ~DLRG_PROTECTED)
        {
            flags |= 2; // TODO magic numebr
        }
    }

    for (i = x3; i < x2; ++i)
    {
        if (L5dflags[i][y1] & ~DLRG_PROTECTED)
        {
            flags |= 4; // TODO magic numebr
        }
        if (L5dflags[i][y2] & ~DLRG_PROTECTED)
        {
            flags |= 1; // TODO magic numebr
        }
    }

    for (i = x1; i < x3; ++i)
    {
        if (L5dflags[x1][i] & ~DLRG_PROTECTED)
        {
            flags |= 8; // TODO magic numebr
        }
        if (L5dflags[x2][i] & ~DLRG_PROTECTED)
        {
            flags |= 2; // TODO magic numebr
        }
    }

    for (i = y3; i < y2; ++i)
    {
        if (L5dflags[x1][i] & ~DLRG_PROTECTED)
        {
            flags |= 4; // TODO magic numebr
        }
        if (L5dflags[x2][i] & ~DLRG_PROTECTED)
        {
            flags |= 1; // TODO magic numebr
        }
    }

    rv = random_(9);
    i = -1;
    while (rv >= 0)
    {
        ++i;
        if (i == 9)
        {
            i = 0;
        }
        if (flags & door_flag_lookup[i].field_0)
        {
            --rv;
        }
    }

    return i;
}

// .text:0048F48D
// Subdivide a room with a wall, arch, or floor. Called recusively while there's still space.
// x1/y1/x2/y2 is the rectangle of space to generate the dungeon inside.
// x3/y3 is the location of the center room. It defines the boundaries of the quadrants
void drlg_l1_next_piece_1(int x1, int y1, int x2, int y2, int x3, int y3, int flags)
{
    int width, height;
    int rv;
    BYTE tile1, tile2, tile3;
    int i;
    int random_x, random_y;

    width = x2 - x1;
    height = y2 - y1;

    // Determine which tile form the east and south walls.
    if (width <= 4)
    {
        rv = random_(4);
        if (rv < 2)
        {
            tile1 = 2; // north wall
        }
        if (rv == 2)
        {
            tile1 = 12; // north arch
        }
        if (rv == 3)
        {
            tile1 = 0;
        }
    }
    else
    {
        tile1 = 2;
    }

    if (height <= 4)
    {
        rv = random_(4);
        if (rv < 2)
        {
            tile2 = 1;
        }
        if (rv == 2)
        {
            tile2 = 11;
        }
        if (rv == 3)
        {
            tile2 = 0;
        }
    }
    else
    {
        tile2 = 1;
    }

    // Place east and south walls.
    if (y2 != y3)
    {
        for (i = x1 + 1; i < x2; ++i)
        {
            if (!dungeon[i][y2])
            {
                dungeon[i][y2] = tile1;
            }
        }
    }
    if (x2 != x3)
    {
        for (i = y1 + 1; i < y2; ++i)
        {
            if (!dungeon[x2][i])
            {
                dungeon[x2][i] = tile2;
            }
        }
    }

    // Set the corner piece using surrounding tiles
    tile1 = dungeon[x1 + 1][y1];
    tile2 = dungeon[x1][y1 + 1];
    if (tile1 == 2)
    {
        if (tile2 == 1)
        {
            tile3 = 4;
        }
        if (tile2 == 11)
        {
            tile3 = 14;
        }
        if (!tile2)
        {
            tile3 = 2;
        }
    }
    if (tile1 == 12)
    {
        if (tile2 == 1)
        {
            tile3 = 10;
        }
        if (tile2 == 11)
        {
            tile3 = 5;
        }
        if (!tile2)
        {
            tile3 = 12;
        }
    }
    if (!tile1)
    {
        if (tile2 == 1)
        {
            tile3 = 1;
        }
        if (tile2 == 11)
        {
            tile3 = 11;
        }
        if (!tile2)
        {
            tile3 = 3;
        }
    }
    if (dungeon[x1][y1] == 13)
    {
        dungeon[x1][y1] = tile3;
    }

    // Place doors so the player can get in.
    DRLG_L1PlaceQuadBoundaryDoors(x1, y1, x2, y2, flags);

    // If room is large, always subdivide. Otherwise, 1/4 chance.
    rv = random_(4);
    if (width >= 12 || height >= 12)
    {
        rv = 1;
    }
    if (rv != 0)
    {
        // If the room is big enough, subdivide into quadrants.
        if (width > 4 && height > 4)
        {
            random_x = x1 + random_(width - 4) + 2;
            random_y = y1 + random_(height - 4) + 2;
            i = drlg_l1_48F2A4_get_dflags(x1, y1, x2, y2, random_x, random_y);
            drlg_l1_next_piece_1(x1, y1,
                                 random_x, random_y,
                                 x3, y3,
                                 door_flag_lookup[i].field_4);
            drlg_l1_next_piece_1(random_x, y1,
                                 x2, random_y,
                                 x3, y3,
                                 door_flag_lookup[i].field_8);
            drlg_l1_next_piece_1(x1, random_y,
                                 random_x, y2,
                                 x3, y3,
                                 door_flag_lookup[i].field_C);
            drlg_l1_next_piece_1(random_x, random_y,
                                 x2, y2,
                                 x3, y3,
                                 door_flag_lookup[i].field_10);
        }

        // If the room is too small then cut it in half.
        if (width >= 12 && height <= 4)
        {
            random_x = x1 + random_(width - 4) + 2;
            drlg_l1_next_piece_1(x1, y1,
                                 random_x, y2,
                                 x3, y3,
                                 EAST_DOOR);
            drlg_l1_next_piece_1(random_x, y1,
                                 y2, y2,
                                 x3, y3,
                                 NO_DOOR);
        }
        if (height >= 12 && width <= 4)
        {
            random_y = y1 + random_(height - 4) + 2;
            drlg_l1_next_piece_1(x1, y1,
                                 x2, random_y,
                                 x3, y3,
                                 SOUTH_DOOR);
            drlg_l1_next_piece_1(x1, random_y,
                                 y2, y2,
                                 x3, y3,
                                 NO_DOOR);
        }
    }

    // Small rooms: fix corners.
    if (width <= 4 || height <= 4)
    {
        if (x1 != x3 && y1 != y3)
        {
            dungeon[x1][y1] = drlg_l1_48EEE9_piece_type(x1, y1);
        }
        if (x2 != x3 && y1 != y3)
        {
            dungeon[x2][y1] = drlg_l1_48EEE9_piece_type(x2, y1);
        }
        if (x1 != x3 && y2 != y3)
        {
            dungeon[x1][y2] = drlg_l1_48EEE9_piece_type(x1, y2);
        }
        if (x2 != x3 && y2 != y3)
        {
            dungeon[x2][y2] = drlg_l1_48EEE9_piece_type(x2, y2);
        }
    }
}

// .text:0048FA10
// Finds a quad with enough space for a setpiece.
// rx1/ry1 is the location of the central chamber in mega tile coordinates. They form the boundaries of each quadrant.
// Returns -1 if the setpiece was not placed. This indicates that the dungeon needs to be regenerated until there is space.
// On success, returns one of QUAD_* indicating which quad the piece was placed inside.
int DRLG_L1DetermineSetPieceQuad(BYTE *pSetPiece, int rx1, int ry1)
{
    int rw, rh;              // Width and height of pSetPiece, loaded from header
    char quad_candidates[4]; // 1 if the quad i has enough space, 0 otherwise.
    int i;
    int x1, y1;                  // quad coords for topleft point
    int x2, y2;                  // quad coords for bottomleft point
    int quad_width, quad_height; // dimensions of the quad
    int rv;                      // random number
    int ret;                     // return value

    for (i = 0; i < 4; i++)
    {
        quad_candidates[i] = 0;
    }

    // Red setpiece header to get dimensions.
    rw = *pSetPiece;
    pSetPiece += 2;
    rh = *pSetPiece;

    // Check if top-left has enough space.
    x1 = 1;
    y1 = 1;
    x2 = rx1 - 2;
    y2 = ry1 - 2;
    quad_width = x2 - x1;
    quad_height = y2 - y1;
    if (rw < quad_height - 4 && rh < quad_height - 4)
    {
        quad_candidates[QUAD_TOPLEFT] = 1;
    }

    // Check top-right.
    x1 = rx1 + 9;
    y1 = 1;
    x2 = DMAXX - 1;
    y2 = ry1 - 2;
    quad_width = x2 - x1;
    quad_height = y2 - y1;
    if (rw < quad_height - 4 && rh < quad_height - 4)
    {
        quad_candidates[QUAD_TOPRIGHT] = 1;
    }

    // Check bottom-left.
    x1 = 1;
    y1 = ry1 + 9;
    x2 = rx1 - 2;
    y2 = DMAXY - 1; // return
    quad_width = x2 - x1;
    quad_height = y2 - y1;
    if (rw < quad_height - 4 && rh < quad_height - 4)
    {
        quad_candidates[QUAD_BOTTOMLEFT] = 1;
    }

    // Check bottom-right.
    x1 = rx1 + 9;
    y1 = ry1 + 9;
    x2 = DMAXX - 1;
    y2 = DMAXY - 1;
    quad_width = x2 - x1;
    quad_height = y2 - y1;
    if (rw < quad_height - 4 && rh < quad_height - 4)
    {
        quad_candidates[QUAD_BOTTOMRIGHT] = 1;
    }

    if (quad_candidates[QUAD_BOTTOMLEFT] + quad_candidates[QUAD_BOTTOMRIGHT] + quad_candidates[QUAD_TOPLEFT] + quad_candidates[QUAD_TOPRIGHT] != 0)
    {
        // Choose a "random" quad out of the candidates.
        i = 0;
        rv = random_(16) + 1;
        while (rv > 0)
        {
            if (quad_candidates[i])
            {
                --rv;
                // BUG? Since i is not modified in this case, this is not actually random.
                // This finds the first free quadrant then sits there...
                // To fix this we also need `i = (i+1) % 4;` here as well.
            }
            else
            {
                // Skip over candidates without enough space.
                i = (i + 1) % 4;
            }
        }
        ret = i;
    }
    else
    {
        // No quad has enough space
        ret = -1;
    }

    return ret;
}

// .text:0048FC51
// x1/y1/x2/y2 are the quad boudaries in mega tiles.
// x3/y3 are typically the same as x2/y2.
//
void DRLG_L1GenerateSetPieceQuad(BYTE *pSetPiece, int x1, int y1, int x2, int y2, int x3, int y3, int flag)
{
    BYTE *sp_iter;
    int quad_width, quad_height;
    int sp_width, sp_height;
    int sp_x1, sp_y1, sp_x2, sp_y2;
    int i, j;
    BYTE tile;

    DRLG_L1PlaceQuadBoundaryDoors(x1, y1, x2, y2, flag);

    quad_width = x2 - x1;
    quad_height = y2 - y1;

    sp_iter = pSetPiece;
    sp_width = *sp_iter;
    sp_iter += 2;
    sp_height = *sp_iter;
    sp_iter += 2;

    sp_x1 = x1 + random_(quad_width - 4 - sp_width) + 2;
    sp_y1 = y1 + random_(quad_height - 4 - sp_height) + 2;
    sp_x2 = sp_width + sp_x1 + 1;
    sp_y2 = sp_height + sp_y1 + 1;

    // Place the set piece into the dungeon. We do this several times. This first
    // time mostly marks the space as protected and gives tiles for generating the
    // surrounding area.
    for (j = 0; j < sp_height; ++j)
    {
        for (i = 0; i < sp_width; ++i)
        {
            if (*sp_iter)
            {
                // We convert the tile to it's "base tile" and place that to give
                // the next steps enough information to generate the surrounding chambers.
                // We will replace this with the actual tiles later in this function.
                tile = BSTYPES[*sp_iter];
                // Replace the floor with an ungenerated tile.
                // Later DLRG steps will turn ungenerated tiles into more visually interesting floors.
                if (tile == 13)
                {
                    tile = 0;
                }
                dungeon[i + sp_x1][j + sp_y1] = tile;
                L5dflags[i + sp_x1][j + sp_y1] |= DLRG_PROTECTED;
            }
            else
            {
                dungeon[i + sp_x1][j + sp_y1] = 0;
            }
            sp_iter += 2;
        }
    }

    // Mark the location of the set piece for later DRLG steps.
    setpc_x = sp_x1;
    setpc_y = sp_y1;
    setpc_w = sp_width;
    setpc_h = sp_height;

    // Correct the transparency. This assumes there's a chamber within a chamber
    // i.e. butcher chamber or hero2.dun
    DRLG_MRectTrans(sp_x1, sp_y1, sp_x2, sp_y2);
    DRLG_MRectTrans(sp_x1 + 2, sp_y1 + 2, sp_x2 - 2, sp_y2 - 2);

    // Subdivide the remaining space.
    drlg_l1_next_piece_1(x1, y2,
                         sp_x1, sp_y2,
                         x3, y3,
                         4); // TODO magic number
    drlg_l1_next_piece_1(sp_x1, y1,
                         x2, sp_y1,
                         x3, y3,
                         1); // TODO magic number
    drlg_l1_next_piece_1(x1, sp_y2,
                         sp_x2, y2,
                         x3, y3,
                         2); // TODO magic number
    drlg_l1_next_piece_1(sp_x2, sp_y1,
                         x2, y2,
                         x3, y3,
                         8); // TODO magic number

    // Place the set piece again. This places the actual tile (not the base tile).
    sp_iter = pSetPiece + 4;
    for (j = 0; j < sp_height - 1; ++j)
    {
        // Place a row up to the right edge
        for (i = 0; i < sp_width - 1; ++i)
        {
            if (*sp_iter)
            {
                dungeon[i + sp_x1][j + sp_y1] = *sp_iter;
            }
            sp_iter += 2;
        }

        // Place the right edge
        if (!dungeon[sp_x2 + 1][j + sp_y1])
        {
            dungeon[sp_x2][j + sp_y1] = *sp_iter;
        }
        sp_iter += 2;
    }

    // Place the bottom edge
    for (i = 0; i < sp_width - 1; ++i)
    {
        if (*sp_iter)
        {
            if (!dungeon[i + sp_x1][sp_y2 + 1])
            {
                dungeon[i + sp_x1][sp_y2] = *sp_iter;
            }
        }
        sp_iter += 2;
    }

    // Place corners
    dungeon[sp_x2][sp_y1] = drlg_l1_48EEE9_piece_type(sp_x2, sp_y1);
    dungeon[sp_x1][sp_y2] = drlg_l1_48EEE9_piece_type(sp_x1, sp_y2);
    dungeon[sp_x2][sp_y2] = drlg_l1_48EEE9_piece_type(sp_x2, sp_y2);
}

// drlg_l1_bool_two_piece	000000000049000F
// Adjust door flags based on surrounding tiles
// TODO I'm doing a very literal translation of thex86 (lots of `test`+`jz`), this remains to be verified and simplified.
// TODO "horizontal" and "vertical" don't make a lot of sense... does the level need to be transposed to match intuition?
void drlg_l1_bool_two_piece(BOOL horizontal, BOOL vertical, int center_x, int center_y)
{
    int i;
    BYTE tile;

    if (horizontal)
    {
        // Walk the horizontal hall.
        for (i = 0; i < DMAXX - 1; ++i)
        {
            // Propogate doors on the one side slightly inward.
            if (L5dflags[i][center_y - 2] & DLRG_HDOOR)
            {
                L5dflags[i][center_y - 2] |= DLRG_HDOOR; // redundant???
                if (dungeon[i][center_y - 1] == 0 || i == 1)
                {
                    L5dflags[i][center_y] |= DLRG_HDOOR;
                }
                else
                {
                    L5dflags[i][center_y - 2] |= DLRG_HDOOR; // redundant???
                }
            }

            // Propogate doors on the opposite side slightly inward.
            if ((L5dflags[i][center_y + 9] & DLRG_HDOOR) != 0)
            {
                L5dflags[i][center_y + 9] |= DLRG_HDOOR;// redundant???
                if (dungeon[i][center_y + 8] == 0 || i == 1)
                {
                    L5dflags[i][center_y + 7] |= DLRG_HDOOR;
                }
                else
                {
                    L5dflags[i][center_y + 8] |= DLRG_HDOOR;
                }
            }

            if (dungeon[i][center_y - 3] != 0)
            {
                tile = dungeon[i][center_y - 3];
                if (tile == 3 && dungeon[i][center_y - 2] != 0) // 3 is SE edge
                {
                    dungeon[i][center_y - 1] = 13;
                }
                if (tile == 6) // west wall edge
                {
                    tile = 1; // west wall
                }
                if (tile == 8) // west arch edge
                {
                    tile == 11; // west arch
                }
                if (dungeon[i - 1][center_y - 2] == 0 && dungeon[i + 1][center_y - 2] == 0)
                {
                    if (tile == 1 || tile == 11)
                    {
                        if (dungeon[i][center_y - 2] != 0)
                        {
                            dungeon[i][center_y - 2] = tile;
                        }
                        if (dungeon[i][center_y - 1] != 0)
                        {
                            dungeon[i][center_y - 1] = tile;
                        }
                    }
                }
                else
                {
                    if (dungeon[i][center_y - 1] == 0)
                    {
                        if (dungeon[i][center_y - 2] == 0)
                        {
                            dungeon[i - 1][center_y - 2] = 4;
                            dungeon[i][center_y - 2] = 3;
                        }
                        if (dungeon[i - 1][center_y - 2] == 0)
                        {
                            dungeon[i][center_y - 2] = 7;
                        }
                    }
                }
            }

            if (dungeon[i][center_y + 9] != 0) // TODO magic numbers, why +9?
            {
                tile = dungeon[i][center_y + 9];
                if (dungeon[i][center_y + 8] == 0)
                {
                    dungeon[i][center_y + 9] = 13; // empty floor
                }
                if (tile == 6) // west wall edge
                {
                    tile = 1; // west wall
                }
                if (tile == 8) // west arch edge
                {
                    tile = 11; // west arch
                }

                // TODO
            }
        } // for (i = 0; i < DMAXY - 1; ++i)
    }

    if (vertical)
    {
        for (i = 0; i < DMAXY - 1; ++i)
        {
            // TODO
        }
    }
}
// drlg_l1_place_door_maybe	0000000000490B37
void drlg_l1_place_door_maybe(int, int)
{
    // TODO
}

// .text:00490D33
// Replace a wall with a doors based on what was predetermined in L5dflags.
// Works on megatile coords. May not do anything, particularly if DLRG_PROTECTED
// or pattern for (x,y) doesn't match.
static void DRLG_PlaceDoor(int x, int y)
{
    BYTE c;  // the megatile generated at (x, y)
    BYTE df; // the

    // A tile may already have something generated on it.
    if ((L5dflags[x][y] & DLRG_PROTECTED) == 0)
    {
        // This seems redundant, we already know it's not protected!
        df = L5dflags[x][y] & ~DLRG_PROTECTED;
        c = dungeon[x][y];

        if (df == DLRG_HDOOR)
        {
            // Replace walls with the same variant which includes a door.
            // Can't place doors on y=1.
            if (y != 1 && c == 2)
                dungeon[x][y] = 26;
            if (y != 1 && c == 7)
                dungeon[x][y] = 31;
            if (y != 1 && c == 14)
                dungeon[x][y] = 42;
            if (y != 1 && c == 4)
                dungeon[x][y] = 43;
            if (x != 1 && c == 1)
                dungeon[x][y] = 25;
            if (x != 1 && c == 10)
                dungeon[x][y] = 40;
            if (x != 1 && c == 6)
                dungeon[x][y] = 30;
        }
        if (df == DLRG_VDOOR)
        {
            // Replace walls with the same variant which includes a door.
            // Can't place doors on x=1.
            if (x != 1 && c == 1)
                dungeon[x][y] = 25;
            if (x != 1 && c == 6)
                dungeon[x][y] = 30;
            if (x != 1 && c == 10)
                dungeon[x][y] = 40;
            if (x != 1 && c == 4)
                dungeon[x][y] = 41;
            if (y != 1 && c == 2)
                dungeon[x][y] = 26;
            if (y != 1 && c == 14)
                dungeon[x][y] = 42;
            if (y != 1 && c == 7)
                dungeon[x][y] = 31;
        }
        if (df == DLRG_HDOOR | DLRG_VDOOR)
        {
            if (x != 1 && y != 1 && c == 4)
                dungeon[x][y] = 28;
            if (x != 1 && c == 10)
                dungeon[x][y] = 40;
            if (y != 1 && c == 14)
                dungeon[x][y] = 42;
            if (y != 1 && c == 2)
                dungeon[x][y] = 26;
            if (x != 1 && c == 1)
                dungeon[x][y] = 25;
            if (y != 1 && c == 7)
                dungeon[x][y] = 31;
            if (x != 1 && c == 6)
                dungeon[x][y] = 30;
        }
    }

    // Mark the tile as protected to prevent being overridden.
    L5dflags[x][y] = DLRG_PROTECTED;
}

// .text:00491134
// Randomly choose or mix bitwise flags. Returns either: `a`, `b`, or `a|b`.
//
// Results could be biased if `a == 1` or `b == 2`!
static int DRLG_L1DetermineQuadBoundaryDoors(int a, int b)
{
    int v;
    v = random_(3);
    if (v == 0)
    {
        v = a;
    }
    // BUG? if random(3) == 0 and a == 1 then we hit this case
    if (v == 1)
    {
        v = b;
    }
    // BUG? if random(3) == 1 and b == 2 then we hit this case
    if (v == 2)
    {
        v = b | a;
    }
    return v;
}

// DRLG_L1Shadows	0000000000491190
static void DRLG_L1Shadows()
{
    // TODO
}

// .text:004917D6
// Places a structure (a rectangular collection of tiles) into the dungeon.
//
// - `miniset` is a structured collection of tiles describing what to place. This is essentially a find+replace. The first two bytes are width and height. The next width*height bytes define the "find" criteria. The next width*height bytes define the "replace". 0 means "don't care"/"keep what's there".
// - `tmin` and `tmax` are the low/upper bounds of how many to generate
// - `cx` and `cy` are the coordinates of the randomly chosen center of the alpha cathedral. They form the boundary of the quadrants (see noquad). If -1 then no boundaries.
// - If `setview == TRUE` then ViewX and ViewY are set to center on the location of the miniset when placed
// - `noquad` specifies which alpha cathedral quadrant to place the miniset in. -1 means don't care. 0 is top-left, 1 is top-right, 2 is bottom-left, 3 is bottom-right.
// - If `ldir == 0` then LvlViewX/LvlViewY are set to center on the location of the miniset when placed. I think this prevents monsters from spawning around the miniset.
//
// Returns -1 on failure, otherwise returns the quadrant in which the miniset was placed. NOTE: If tmin-tmax > 0 then only the quadrant of the last placed miniset is returned.
static int DRLG_PlaceMiniSet(const BYTE *miniset, int tmin, int tmax, int cx, int cy, BOOL setview, int noquad, int ldir)
{
    int xx, yy, i, ii, found, t;
    int numt;   // Number of minisets to place
    BOOL abort; // Keeping trying to place until this is TRUE
    int sw;     // width of miniset
    int sh;     // height of miniset
    int sx;     // X iterator in megatiles
    int sy;     // y iterator in megatiles

    // The header of the miniset is width and height
    sw = miniset[0];
    sh = miniset[1];

    // Determine number to place between tmax and tmin
    if (tmax - tmin == 0)
    {
        numt = 1;
    }
    else
    {
        numt = random_(tmax - tmin) + tmin;
    }

    for (i = 0; i < numt; i++)
    {
        sx = random_(DMAXX - sw);
        sy = random_(DMAXY - sh);
        abort = FALSE;
        found = 0;

        // Step 1. Find
        while (abort == FALSE)
        {
            abort = TRUE;

            // Check that (sx, sy) fall inside the boundary of the quadrant.
            // BUGFIX: This code has no purpose but causes the set piece to never appear in x 0-13 or y 0-13
            if (cx != -1 && sx >= cx - sw && sx <= cx + 12)
            {
                sx++;
                abort = FALSE;
            }
            if (cy != -1 && sy >= cy - sh && sy <= cy + 12)
            {
                sy++;
                abort = FALSE;
            }

            switch (noquad)
            {
            case 0:
                if (sx < cx && sy < cy)
                    abort = FALSE;
                break;
            case 1:
                if (sx > cx && sy < cy)
                    abort = FALSE;
                break;
            case 2:
                if (sx < cx && sy > cy)
                    abort = FALSE;
                break;
            case 3:
                if (sx > cx && sy > cy)
                    abort = FALSE;
                break;
            }

            // Check whether the miniset fits according to the substitution criteria.
            ii = 2; // miniset index; skip the header bytes
            for (yy = 0; yy < sh && abort == TRUE; yy++)
            {
                for (xx = 0; xx < sw && abort == TRUE; xx++)
                {
                    if (miniset[ii] && dungeon[xx + sx][sy + yy] != miniset[ii])
                        abort = FALSE;
                    if (L5dflags[xx + sx][sy + yy])
                        abort = FALSE;
                    ii++;
                }
            }

            // Not yet placed? Advance iterators and try again
            if (abort == FALSE)
            {
                if (++sx == DMAXX - sw)
                {
                    sx = 0;
                    if (++sy == DMAXY - sh)
                        sy = 0;
                }
                // Try up to 4000 times
                if (++found > 4000)
                    return -1;
            }
        }

        // Step 2. Replace
        ii = sw * sh + 2; // Skip header and "find" criteria
        for (yy = 0; yy < sh; yy++)
        {
            for (xx = 0; xx < sw; xx++)
            {
                if (miniset[ii])
                    dungeon[xx + sx][sy + yy] = miniset[ii];
                ii++;
            }
        }
    }

    if (setview == TRUE)
    {
        ViewX = 2 * sx + 19;
        ViewY = 2 * sy + 20;
    }

    if (ldir == 0)
    {
        // This only ever set here and only used by InitMonsters.
        // I think this prevents monsters from generating around this location.
        LvlViewX = 2 * sx + 19;
        LvlViewY = 2 * sy + 20;
    }

    if (sx < cx && sy < cy)
        return 0;
    if (sx > cx && sy < cy)
        return 1;
    if (sx < cx && sy > cy)
        return 2;
    else
        return 3;
}

// drlg_l1_place_some_set	0000000000491C19
void drlg_l1_place_some_set(BYTE *, int)
{
    // TODO
}

// .text:00491E01
// Randomly replaces tiles with equivalents that include more visual decorations.
static void DRLG_L1Subs()
{
    int x, y, rv, i;

    for (y = 0; y < DMAXY; y++)
    {
        for (x = 0; x < DMAXX; x++)
        {
            // 1 in 6 chance on each tile of attempting a substitution.
            rv = random_(6);
            if (rv == 0)
            {
                // c=0 means that there is no substitute.
                // Also check flags in case the tile is protected.
                BYTE c = L1BTYPES[dungeon[x][y]];
                if (c && !L5dflags[x][y])
                {
                    // Find a random tile that works as a substitute and store in `i`.
                    rv = random_(16);
                    i = -1;
                    while (rv >= 0)
                    {
                        if (++i == sizeof(L1BTYPES))
                        {
                            i = 0;
                        }
                        if (c == L1BTYPES[i])
                        {
                            rv--;
                        }
                    }

                    // Wall tapestries come in megatile pairs. Ensure the pair is generated.
                    // Only perform this subtitution on a specific kind of wall.
                    // For alpha cathedral, this is the exterior wall.
                    // Interestingly, this is present in the new cathedral despite there being 0 chance of these tiles being present.
                    // BUGFIX: Add `&& y > 0` to the if statement.
                    if (i == 89)
                    {
                        if (L1BTYPES[dungeon[x][y - 1]] != 79 || L5dflags[x][y - 1])
                            i = 79;
                        else
                            dungeon[x][y - 1] = 90;
                        // i=89 will be placed in the dungeon below
                    }
                    // BUGFIX: Add `&& x + 1 < DMAXX` to the if statement.
                    if (i == 91)
                    {
                        if (L1BTYPES[dungeon[x + 1][y]] != 80 || L5dflags[x + 1][y])
                            i = 80;
                        else
                            dungeon[x + 1][y] = 92;
                        // i=91 will be placed in the dungeon below
                    }

                    // Perform the substitution.
                    dungeon[x][y] = i;
                }
            }
        }
    }
}

// .text:00491FEE
// Replaces plain floor with slight variants. These are very subtle changes.
// Some floor tiles may already be replaced by DRLG_L1Subs
static void DRLG_L1Floor()
{
    int i, j;
    LONG rv;

    for (j = 0; j < DMAXY; j++)
    {
        for (i = 0; i < DMAXX; i++)
        {
            if (L5dflags[i][j] == 0 && dungeon[i][j] == 13)
            {
                rv = random_(3);

                if (rv == 1)
                    dungeon[i][j] = 162;
                if (rv == 2)
                    dungeon[i][j] = 163;
            }
        }
    }
}

// DRLG_L1FTVR	00000000004920B5
void DRLG_L1FTVR()
{
    // TODO
}

// DRLG_L1FloodTVal	0000000000492400
void DRLG_L1FloodTVal()
{
    // TODO
}

// .text:004924BB
// `entry` is one of `enum lvl_entry`. This encodes what direction the player came from (up stairs vs down stairs, etc)
void DRLG_L1(int entry)
{
    int i; // X iterator
    int j; // Y iterator
    // Sometimes the level generator can fail to find space for important things like the stairs.
    // The algo will keep trying to generate a new dungeon until it can place everything that is important.
    BOOL retry;
    // How many corridors to generate. Set to 2 and never changed.
    int corridor_count;
    // TRUE if the vertical corridor was generated
    BOOL done_vertical;
    // TRUE if the horizontal corridor was generated
    BOOL done_horizontal;
    // If TRUE then generate the horizontal corridor.
    // If FALSE then generate the vertical corridor.
    BOOL do_horizontal;
    // A random location (in megatile coordinates) for the "center" of the four quads.
    // This is where the large central chamber is generated. It also is used as limits for each quadrant when playing minisets, etc.
    int random_x; // TODO rename center_x/ center_y
    int random_y;
    int y;
    int x;
    // Quad for setpiece, see QUAD_*
    int setroom_quad;
    int flag; // TODO rename door_bits or something like that
    int xx;
    int yy;
    // Quad for up stairs, see setroom_quad
    int stair_quad;

    do
    {
        retry = FALSE;

        // Reset vars used during level gen
        for (j = 0; j < DMAXY; j++)
        {
            for (i = 0; i < DMAXX; i++)
            {
                dungeon[i][j] = 0;
                L5dflags[i][j] = 0;
            }
        }

        // Generate walls on the top and bottom
        for (i = 1; i < DMAXX - 1; i++)
        {
            dungeon[i][0] = 22;  // dirt "roof"
            dungeon[i][1] = 2;   // horizontal wall
            dungeon[i][39] = 19; // horizontal wall from backside
        }

        // Generate walls on the left and right sides
        for (j = 1; i < DMAXY - 1; j++)
        {
            dungeon[0][j] = 22;
            dungeon[1][j] = 1; // vertical wall
            dungeon[39][j] = 18;
        }

        // Generate corners
        dungeon[0][0] = 22;
        dungeon[1][1] = 4; // corner
        dungeon[39][0] = 22;
        // 20/23/24 are backsides of walls
        dungeon[39][1] = 23;
        dungeon[0][39] = 22;
        dungeon[1][39] = 24;
        dungeon[39][39] = 20;

        // Draw the two main corridors that divide the level into quadrants.
        corridor_count = 2; // TODO: What happens if this is 1 or 0?
        do_horizontal = TRUE;
        done_vertical = FALSE;
        done_horizontal = FALSE;
        random_x = random_(11) + 9;
        random_y = random_(11) + 9;
        for (i = 0; i < corridor_count; ++i)
        {
            if (!do_horizontal)
            {
                // Draw the vertical corridor
                dungeon[random_x][1] = 4;
                dungeon[random_x + 2][1] = 14; // horizontal wall, with arch on vertical
                dungeon[random_x + 5][1] = 14;
                dungeon[random_x + 7][1] = 4;

                for (y = 2; y < DMAXY - 1; y++)
                {
                    dungeon[random_x][y] = 1;
                    dungeon[random_x + 1][y] = 13; // floor
                    dungeon[random_x + 2][y] = 11; // arch
                    dungeon[random_x + 3][y] = 13;
                    dungeon[random_x + 4][y] = 13;
                    dungeon[random_x + 5][y] = 11;
                    dungeon[random_x + 6][y] = 13;
                    dungeon[random_x + 7][y] = 1;
                }

                DRLG_MRectTrans(random_x, 1, random_x + 2, 39);
                DRLG_MRectTrans(random_x + 2, 1, random_x + 5, 39);
                DRLG_MRectTrans(random_x + 5, 1, random_x + 7, 39);

                done_vertical = TRUE;
                // Generate the horizontal next.
                do_horizontal = TRUE;
            }
            else
            {
                // Draw the horizontal corridor.
                dungeon[1][random_y] = 4;
                dungeon[1][random_y + 2] = 10; // vertical wall, with arch on horizontal
                dungeon[1][random_y + 5] = 10;
                dungeon[1][random_y + 7] = 4;

                for (x = 2; x < DMAXX - 1; ++x)
                {
                    dungeon[x][random_y] = 2;
                    dungeon[x][random_y + 1] = 13;
                    dungeon[x][random_y + 2] = 12;
                    dungeon[x][random_y + 3] = 13;
                    dungeon[x][random_y + 4] = 13;
                    dungeon[x][random_y + 5] = 12;
                    dungeon[x][random_y + 6] = 13;
                    dungeon[x][random_y + 7] = 2;
                }

                DRLG_MRectTrans(1, random_y, DMAXX - 1, random_y + 2);
                DRLG_MRectTrans(1, random_y + 2, DMAXX - 1, random_y + 5);
                DRLG_MRectTrans(1, random_y + 5, DMAXX - 1, random_y + 7);

                done_horizontal = TRUE;
                // Generate the vertical next.
                do_horizontal = FALSE;
            }
        }

        // Draw a large room centered on the corridors with all four exits open.
        DRLG_L1GChamber(random_x, random_y, TRUE, TRUE, TRUE, TRUE);

        // Find a quad for the quest set piece.
        setroom_quad = QUAD_UNSPECIFIED;
        if (currlevel == quests[Q_BUTCHER]._qlevel)
        {
            setroom_quad = DRLG_L1DetermineSetPieceQuad(L5pSetPiece, random_x, random_y);
        }
        if (currlevel == quests[Q_MAZE]._qlevel)
        {
            setroom_quad = DRLG_L1DetermineSetPieceQuad(L5pSetPiece, random_x, random_y);
        }

        // Generate each of the quadrants in turn.
        flag = DRLG_L1DetermineQuadBoundaryDoors(EAST_DOOR, SOUTH_DOOR);
        if (setroom_quad == QUAD_TOPLEFT)
        {
            DRLG_L1GenerateSetPieceQuad(L5pSetPiece,
                                        1, 1,
                                        random_x - 2, random_y - 2,
                                        random_x - 2, random_y - 2,
                                        flag);
        }
        else
        {
            drlg_l1_next_piece_1(1, 1,
                                 random_x - 2, random_y - 2,
                                 random_x - 2, random_y - 2,
                                 flag);
        }

        flag = DRLG_L1DetermineQuadBoundaryDoors(WEST_DOOR, SOUTH_DOOR);
        if (setroom_quad == QUAD_TOPRIGHT)
        {
            DRLG_L1GenerateSetPieceQuad(L5pSetPiece,
                                        random_x + 9, 1,
                                        DMAXX - 1, random_y - 2,
                                        DMAXX - 1, random_y - 2,
                                        flag);
        }
        else
        {
            drlg_l1_next_piece_1(random_x + 9, 1,
                                 DMAXX - 1, random_y - 2,
                                 DMAXX - 1, random_y - 2,
                                 flag);
        }

        flag = DRLG_L1DetermineQuadBoundaryDoors(EAST_DOOR, NORTH_DOOR);
        if (setroom_quad == QUAD_BOTTOMLEFT)
        {
            DRLG_L1GenerateSetPieceQuad(L5pSetPiece,
                                        1, random_y + 9,
                                        random_x - 2, DMAXY - 1,
                                        random_x - 2, DMAXY - 1,
                                        flag);
        }
        else
        {
            drlg_l1_next_piece_1(1, random_y + 9,
                                 random_x - 2, DMAXY - 1,
                                 random_x - 2, DMAXY - 1,
                                 flag);
        }

        flag = DRLG_L1DetermineQuadBoundaryDoors(WEST_DOOR, NORTH_DOOR);
        if (setroom_quad == QUAD_BOTTOMRIGHT)
        {
            DRLG_L1GenerateSetPieceQuad(L5pSetPiece,
                                        random_x + 9, random_y + 9,
                                        DMAXX - 1, DMAXY - 1,
                                        DMAXX - 1, DMAXY - 1,
                                        flag);
        }
        else
        {
            drlg_l1_next_piece_1(random_x + 9, random_y + 9,
                                 DMAXX - 1, DMAXY - 1,
                                 DMAXX - 1, DMAXY - 1,
                                 flag);
        }

        drlg_l1_bool_two_piece(done_horizontal, done_vertical, random_x, random_y);
        DRLG_MRectTrans(random_x - 2, random_y - 2, random_x + 9, random_y + 9);

        for (j = 1; j < DMAXY - 1; ++j)
        {
            for (i = 1; i < DMAXX - 1; ++i)
            {
                if (!(L5dflags[i][j] & DLRG_PROTECTED))
                {
                    drlg_l1_place_door_maybe(i, j);
                }
            }
        }

        // Replace empty, ungenerated tiles with floor
        for (j = 0; j < DMAXY; ++j)
        {
            for (i = 0; i < DMAXX; ++i)
            {
                if (!dungeon[i][j])
                {
                    dungeon[i][j] = 13;
                }
            }
        }

        for (j = 0; j < DMAXY; ++j)
        {
            for (i = 0; i < DMAXX; ++i)
            {
                if (!(L5dflags[i][j] & DLRG_PROTECTED))
                {
                    DRLG_PlaceDoor(i, j);
                }
            }
        }

        // Replace outer walls with a more visually distinct variant.
        for (i = 1; i < DMAXX - 1; ++i)
        {
            if (dungeon[i][1] == 2)
            {
                dungeon[i][1] = 80;
            }
            if (dungeon[i][1] == 4)
            {
                dungeon[i][1] = 158;
            }
            if (dungeon[i][1] == 14)
            {
                dungeon[i][1] = 159;
            }
            if (dungeon[i][1] == 41)
            {
                dungeon[i][1] = 164;
            }
        }
        for (j = 1; j < DMAXY - 1; ++j)
        {
            if (dungeon[1][j] == 39)
            {
                dungeon[1][j] = 79;
            }
            if (dungeon[1][j] == 4)
            {
                dungeon[1][j] = 160;
            }
            if (dungeon[1][j] == 10)
            {
                dungeon[1][j] = 161;
            }
            if (dungeon[1][j] == 43)
            {
                dungeon[1][j] = 165;
            }
        }
        dungeon[1][1] = 82;

        DRLG_L1FloodTVal();

        // TODO what is this doing?
        for (i = 0; i < MAXDUNX; ++i)
        {
            DRLG_CopyTrans(i, 94, i, 95);
        }
        for (j = 0; j < MAXDUNY; ++j)
        {
            DRLG_CopyTrans(94, j, 95, j);
        }

        // Place up and down stairs. On dlvl4, only place the stairs up (since the stairs down is part of hero2.dun)
        if (currlevel == quests[Q_MAZE]._qlevel)
        {
            if (entry == ENTRY_MAIN)
            {
                if (DRLG_PlaceMiniSet(STAIRSUP, 1, 1, random_x, random_y, TRUE, setroom_quad, 0) < 0)
                {
                    retry = TRUE;
                }
            }
            else
            {
                if (DRLG_PlaceMiniSet(STAIRSUP, 1, 1, random_x, random_y, FALSE, setroom_quad, 0) < 0)
                {
                    retry = TRUE;
                    ViewY--;
                }
            }
        }
        else
        {
            if (entry == ENTRY_MAIN)
            {
                stair_quad = DRLG_PlaceMiniSet(STAIRSUP, 1, 1, random_x, random_y, TRUE, QUAD_UNSPECIFIED, 0);
                if (stair_quad < 0)
                {
                    retry = TRUE;
                }
                else
                {
                    if (DRLG_PlaceMiniSet(STAIRSDOWN, 1, 1, random_x, random_y, FALSE, stair_quad, 1) < 0)
                    {
                        retry = TRUE;
                    }
                }
            }
            else
            {
                stair_quad = DRLG_PlaceMiniSet(STAIRSUP, 1, 1, random_x, random_y, FALSE, QUAD_UNSPECIFIED, 0);
                if (stair_quad < 0)
                {
                    retry = TRUE;
                }
                else
                {
                    if (DRLG_PlaceMiniSet(STAIRSDOWN, 1, 1, random_x, random_y, TRUE, stair_quad, 1) < 0)
                    {
                        retry = TRUE;
                        ViewY--;
                    }
                }
            }
        }
    } while (retry);

    // Fix the transparency info for the stairs up
    for (j = 0; j < DMAXY; ++j)
    {
        for (i = 0; i < DMAXX; ++i)
        {
            if (dungeon[i][j] == 64)
            {
                xx = 2 * i + 16;
                yy = 2 * j + 16;
                DRLG_CopyTrans(xx, yy + 1, xx, yy);
                DRLG_CopyTrans(xx + 1, yy + 1, xx + 1, yy);
            }
        }
    }

    // Replace a bunch of walls with fences.
    drlg_l1_place_some_set(FENCE1, 20); // TODO magic numbers
    drlg_l1_place_some_set(FENCE2, 20);

    // Add more visual variety to dungeon tiles.
    DRLG_L1Subs();

    DRLG_L1Shadows();

    // Place a bunch of braziers
    DRLG_PlaceMiniSet(LAMPS, 5, 10, random_x, random_y, FALSE, QUAD_UNSPECIFIED, 4); // TODO magic numbers

    // Replace shadowed walls with the fancy exterior wall variant.
    // The rest of the wall was taken care of in the do-while ealier.
    // This fixes the result of calling DRLG_L1Shadows().
    for (i = 1; i < DMAXX - 1; ++i)
    {
        if (dungeon[i][1] == 148)
        {
            dungeon[i][1] = 156;
        }
        if (dungeon[i][1] == 154)
        {
            dungeon[i][1] = 157;
        }
    }
    for (j = 1; j < DMAXY - 1; ++j)
    {
        if (dungeon[1][j] == 146)
        {
            dungeon[1][j] = 155;
        }
    }

    // Add more variety to the floor tiles. Makes it more visually interesting.
    // Some of this could have been done by DRLG_L1Subs already?
    DRLG_L1Floor();

    // Backup dungeon into pdungeon. AFAICT pdungeon is only used by objects.
    for (j = 0; j < DMAXX; j++)
    {
        for (i = 0; i < DMAXY; i++)
        {
            pdungeon[i][j] = dungeon[i][j];
        }
    }

    DRLG_CheckQuests(setpc_x, setpc_y);
}

// .text:0049317C
// Initialize `dPiece` from `pMegaTiles` based on generated `dungeon`.
// `dungeon` is unused after this point since it's in "megatiles" and the rest
// of the game like dPiece is in tiles...
static void DRLG_L1Pass3()
{
    int lv;
    int i;
    int v1;
    int xx;
    int v2;
    int yy;
    int v3;
    int v4;
    int j;

    // Init dPiece (tile coords) based on megatile 22 (megatile coords)
    lv = 22 - 1;

    // A 1x1 megatile is a 2x2 tile, so 4 tiles total.
    // One pMegaTiles entry encodes 4 tiles, each tile is a WORD
    __asm {
        mov		esi, pMegaTiles
        mov		eax, lv
        shl		eax, 3
        add		esi, eax
        xor		eax, eax
        lodsw
        inc		eax
        mov		v1, eax
        lodsw
        inc		eax
        mov		v2, eax
        lodsw
        inc		eax
        mov		v3, eax
        lodsw
        inc		eax
        mov		v4, eax
    }

    for (j = 0; j < MAXDUNY; j += 2)
    {
        for (i = 0; i < MAXDUNX; i += 2)
        {
            dPiece[i][j] = v1;
            dPiece[i + 1][j] = v2;
            dPiece[i][j + 1] = v3;
            dPiece[i + 1][j + 1] = v4;
        }
    }

    // Override dPiece (tile coords) based on dungeon megatiles (megatile coords)
    // Start at 16 for the tile buffer
    j = 16; // TODO magic number
    for (yy = 0; yy < DMAXY; yy++)
    {
        i = 16;
        for (xx = 0; xx < DMAXX; xx++)
        {
            lv = dungeon[i][j] - 1;

            __asm {
                mov		esi, pMegaTiles
                mov		eax, lv
                shl		eax, 3
                add		esi, eax
                xor		eax, eax
                lodsw
                inc		eax
                mov		v1, eax
                lodsw
                inc		eax
                mov		v2, eax
                lodsw
                inc		eax
                mov		v3, eax
                lodsw
                inc		eax
                mov		v4, eax
            }

            dPiece[i][j] = v1;
            dPiece[i + 1][j] = v2;
            dPiece[i][j + 1] = v3;
            dPiece[i + 1][j + 1] = v4;

            // add 2 each time because a 1x1 megatile is a 2x2 tile
            j += 2;
        }
        j += 2;
    }
}

// .text:00493360
// Load a setpiece based on dlvl. A setpiece here is e.g. the butcher chamber
// or the entrance to the Skeleton King Lair. There can be only one setpiece
// on a dlvl.
//
// Side-effects: If something was loaded:
//
// - L5setloadflag is TRUE
// - L5pSetPiece contains the raw .DUN data
static void DRLG_LoadL1SP()
{
    L5setloadflag = FALSE;

    if (quests[Q_BUTCHER]._qlevel == currlevel)
    {
        if (leveltype == DTYPE_OLD_CATHEDRAL)
        {
            L5pSetPiece = LoadFileInMem("Levels\\L1Data\\rnd5.DUN");
        }
        else
        {
            L5pSetPiece = LoadFileInMem("Levels\\L1Data\\rnd6.DUN");
        }
        L5setloadflag = TRUE;
    }

    if (quests[Q_SKELKING]._qlevel == currlevel)
    {
        if (quests[Q_SKELKING]._qactive == QUEST_ACTIVE)
        {
            L5pSetPiece = LoadFileInMem("Levels\\L1Data\\SKngDO.DUN");
        }
        else
        {
            L5pSetPiece = LoadFileInMem("Levels\\L1Data\\SKngDC.DUN");
        }
        L5setloadflag = TRUE;
    }

    if (quests[Q_MAZE]._qlevel == currlevel)
    {
        L5pSetPiece = LoadFileInMem("Levels\\L1Data\\Hero2.DUN");
        L5setloadflag = TRUE;
    }
}

// .text:00493443
// If loaded, free the setpiece. See DRLG_LoadL1SP.
//
// Since L5setloadflag is not set to FALSE, do not call twice in a row!
static void DRLG_FreeL1SP()
{
    if (L5setloadflag)
    {
        MemFreeDbg(L5pSetPiece);
    }
}

// .text:00493486
// Assuming dPiece is initialized:
//  - Set d* to 0
//  - set dLight based on lightflag/light4flag
//  - set dSpecial based on tiles
static void DRLG_InitL1Vals()
{
    int i, j;

    for (j = 0; j < MAXDUNY; j++)
    {
        for (i = 0; i < MAXDUNX; i++)
        {
            if (lightflag)
            {
                // If fullbright (no lighting), set all tiles to max light level
                if (light4flag)
                {
                    dLight[i][j] = 3;
                }
                else
                {
                    dLight[i][j] = 15;
                }
            }
            else
            {
                dLight[i][j] = 0;
            }

            // Get d* ready for further level generatino steps
            dFlags[i][j] = 0;
            dPlayer[i][j] = 0;
            dMonster[i][j] = 0;
            dDead[i][j] = 0;
            dObject[i][j] = 0;
            dItem[i][j] = 0;
            dMissile[i][j] = 0;
            dSpecial[i][j] = 0;

            // Initialize the tiles that are drawn on top of everything else, i.e. arch ways
            if (dPiece[i][j] == 12)
            {
                dSpecial[i][j] = 1;
            }
            else if (dPiece[i][j] == 11)
            {
                dSpecial[i][j] = 2;
            }
            else if (dPiece[i][j] == 71)
            {
                dSpecial[i][j] = 1;
            }
            else if (dPiece[i][j] == 259)
            {
                dSpecial[i][j] = 5;
            }
            else if (dPiece[i][j] == 249)
            {
                dSpecial[i][j] = 2;
            }
            else if (dPiece[i][j] == 325)
            {
                dSpecial[i][j] = 2;
            }
            else if (dPiece[i][j] == 321)
            {
                dSpecial[i][j] = 1;
            }
            else if (dPiece[i][j] == 255)
            {
                dSpecial[i][j] = 4;
            }
            else if (dPiece[i][j] == 211)
            {
                dSpecial[i][j] = 1;
            }
            else if (dPiece[i][j] == 344)
            {
                dSpecial[i][j] = 2;
            }
            else if (dPiece[i][j] == 341)
            {
                dSpecial[i][j] = 1;
            }
            else if (dPiece[i][j] == 331)
            {
                dSpecial[i][j] = 2;
            }
            else if (dPiece[i][j] == 418)
            {
                dSpecial[i][j] = 1;
            }
            else if (dPiece[i][j] == 421)
            {
                dSpecial[i][j] = 2;
            }
        }
    }
}

// .text:00493919
// Generate a new random alpha cathedral.
//
// - `rseed` is the pseudo random number generator seed which determines how the level will generate.
// - `entry` is how the player is entering the level, see enum lvl_entry (i.e. stairs up vs stairs down, etc)
void CreateL1Dungeon(DWORD rseed, int entry)
{
    srand(rseed);

    // Describe the bounds of what the player can see. The level is 40x40 megatiles which turn into 80x80 mini tiles.
    // Then add 16 tile padding on each side for... reasons? Maybe they waste memory to make the drawing routine simpler.
    dminx = MINITILE_PADDING_X;
    dminy = MINITILE_PADDING_Y;
    dmaxx = MAXDUNX - MINITILE_PADDING_X;
    dmaxy = MAXDUNY - MINITILE_PADDING_Y;

    DRLG_InitTrans();
    DRLG_LoadL1SP();
    DRLG_L1(entry);
    DRLG_L1Pass3();
    DRLG_FreeL1SP();
    DRLG_InitL1Vals();
    DRLG_SetPC();
}

// LoadL1Dungeon	000000000049398C
void LoadL1Dungeon(const char *sFileName, int vx, int vy)
{
    // TODO
}

// LoadPreL1Dungeon	0000000000493B53
void LoadPreL1Dungeon(const char *sFileName, int vx, int vy)
{
    // TODO
}

// InitL5Dungeon	0000000000493D47
void InitL5Dungeon()
{
    // TODO
}

// L5ClearFlags	0000000000493DB8
static void L5ClearFlags()
{
    // TODO
}

// L5drawRoom	0000000000493E2C
// L5checkRoom	0000000000493E9E
// L5roomGen	0000000000493F62

// L5firstRoom	00000000004942B0
static void L5firstRoom()
{
    // TODO
}

// L5GetArea	0000000000494646
static int L5GetArea()
{
    // TODO
    return 0;
}

// L5makeDungeon	00000000004946BD
static void L5makeDungeon()
{
    // TODO
}

// L5makeDmt	00000000004947AA
static void L5makeDmt()
{
    // TODO
}

// L5HWallOk	00000000004948E5
// L5VWallOk	0000000000494A63
// L5HorizWall	0000000000494BE3
// L5VertWall	0000000000494D86

// L5AddWall	0000000000494F29
static void L5AddWall()
{
    // TODO
}

// DRLG_L5GChamber	000000000049519C
// DRLG_L5GHall	0000000000495496

// L5tileFix	0000000000495545
static void L5tileFix()
{
    // TODO
}

// DRLG_L5Subs	0000000000496274
static void DRLG_L5Subs()
{
    // TODO
}

// DRLG_L5SetRoom	0000000000496461

// L5FillChambers	0000000000496559
static void L5FillChambers()
{
    // TODO
}

// DRLG_L5FTVR	0000000000496BE5

// DRLG_L5FloodTVal	0000000000496F30
static void DRLG_L5FloodTVal()
{
    // TODO
}

// DRLG_L5TransFix	0000000000496FEB
static void DRLG_L5TransFix()
{
    // TODO
}

// DRLG_L5DirtFix	0000000000497310
static void DRLG_L5DirtFix()
{
    // TODO
}

// .text:0049750C
static void DRLG_L5(int entry)
{
    int area;
    int minarea;
    int x;
    int i;
    int j;
    int yy;
    BOOL doneflag;

    doneflag = FALSE;

    switch (currlevel)
    {
    case 1:
        minarea = 533;
        break;
    case 2:
        minarea = 693;
        break;
    case 3:
    case 4:
        minarea = 761;
        break;
    }

    while (!doneflag)
    {
        DRLG_InitTrans();

        do
        {
            InitL5Dungeon();
            L5firstRoom();
            area = L5GetArea();
        } while (area < minarea);

        L5makeDungeon();
        L5makeDmt();
        L5FillChambers();
        L5tileFix();
        L5AddWall();
        L5ClearFlags();
        DRLG_L5FloodTVal();

        doneflag = TRUE;

        // On dlvl4, don't place the stairs down (that is part of hero2.dun).
        // This will be handled by DRLG_CheckQuestions at the end of the
        // function.
        if (currlevel == quests[Q_MAZE]._qlevel)
        {
            if (entry == ENTRY_MAIN)
            {
                if (DRLG_PlaceMiniSet(STAIRSUP, 1, 1, 0, 0, TRUE, -1, 0) < 0)
                {
                    doneflag = FALSE;
                }
            }
            else
            {
                if (DRLG_PlaceMiniSet(STAIRSUP, 1, 1, 0, 0, FALSE, -1, 0) < 0)
                {
                    doneflag = FALSE;
                }
                ViewY--;
            }
        }
        else if (entry == ENTRY_MAIN)
        {
            if (DRLG_PlaceMiniSet(L5STAIRSUP, 1, 1, 0, 0, TRUE, -1, 0) < 0)
            {
                doneflag = FALSE;
            }
            else if (DRLG_PlaceMiniSet(STAIRSDOWN, 1, 1, 0, 0, FALSE, -1, 1) < 0)
            {
                doneflag = FALSE;
            }
        }
        else
        {
            if (DRLG_PlaceMiniSet(L5STAIRSUP, 1, 1, 0, 0, FALSE, -1, 0) < 0)
            {
                doneflag = FALSE;
            }
            else if (DRLG_PlaceMiniSet(STAIRSDOWN, 1, 1, 0, 0, TRUE, -1, 1) < 0)
            {
                doneflag = FALSE;
            }
            ViewY--;
        }
    }

    DRLG_L5TransFix();
    DRLG_L5DirtFix();

    // Replace walls with doors based on what was predetermined in L5dflags
    for (j = 0; j < DMAXY; j++)
    {
        for (i = 0; i < DMAXX; i++)
        {
            if (L5dflags[i][j] & ~DLRG_PROTECTED)
                DRLG_PlaceDoor(i, j);
        }
    }

    DRLG_L5Subs();

    DRLG_L1Shadows();
    DRLG_PlaceMiniSet(LAMPS, 5, 10, 0, 0, FALSE, -1, 4);
    DRLG_L1Floor();

    for (j = 0; j < DMAXY; j++)
    {
        for (i = 0; i < DMAXX; i++)
        {
            pdungeon[i][j] = dungeon[i][j];
        }
    }

    DRLG_CheckQuests(setpc_x, setpc_y);
}

// .text:004978C4
// UNUSED; This function is never called
BYTE get_l1_btype(int i)
{
    return L1BTYPES[i];
}

// .text:004978E3
void CreateL5Dungeon(DWORD rseed, int entry)
{
    srand(rseed);

    dminx = 16;
    dminy = 16;
    dmaxx = 96;
    dmaxy = 96;

    DRLG_InitTrans();
    DRLG_LoadL1SP();
    DRLG_L5(entry);
    DRLG_L1Pass3();
    DRLG_FreeL1SP();
    DRLG_InitL1Vals();
    DRLG_SetPC();
}
