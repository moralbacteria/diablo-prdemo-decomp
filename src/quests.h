#ifndef __QUESTS_H__
#define __QUESTS_H__

#include <windows.h>

//
// defines
//

// There's actually 10 quests allocated, but only 5 of the slots are used
#define MAXQUESTS 5

//
// enums
//

enum quest_id
{
    Q_SKELKING = 0, // The Skeleton King
    Q_BUTCHER = 1,  // The Butcher
    Q_ROCK = 2,     // The Magic Rock
    Q_SCHAMB = 3,   // The Bone Chamber
    Q_MAZE = 4,     // The Maze
    Q_INVALID = -1
};

enum quest_state
{
    QUEST_NOTAVAIL = 0, // Quest not available this playthough (e.g. demo)
    QUEST_INIT = 1,     // Quest is not started but can be found
    QUEST_ACTIVE = 2,   // Quest is in progress
    QUEST_DONE = 3      // Quest has been completed
};

//
// structs
//

// TODO: size assert 0xC
// Compile-time quest table entry. Notable differences from retail include a range of levels
// to find the quest (however no quests take advantage of this). Also _qdrnd is actually used
struct QuestData
{
    char _qdlvlmin;     // Earliest dlvl to find the quest
    char _qdlvlmax;     // Latest dlvl to find the quest
    char _qdtype;       // enum quest_id; i == questlist[i]._qdtype ???
    char _qdrnd;        // Chance of finding the quest
    char _qslvl;        // enum _setlevels; 0 means "no setlevel"
    const char *_qlstr; // Quest name that appears in the quest log
};

// #pragma pack(push, 1)
// TODO: size assert 0x10
// Runtime information about a quest.
struct QuestStruct
{
    char _qlevel;  // The quest dlvl
    char _qtype;   // enum quest_id
    char _qactive; // enum quest_state
    int _qtx;      // X location of entrance to _qslvl
    int _qty;      // Y location pf entrance to _qslvl
    char _qslvl;   // emum _setlevels
    char _qidx;    // index into questlist
    char _qmsg;    // minitext from quest log
};
// #pragma pack(pop)

//
// variables
//

// There's actually 10 quests allocated, but only 5 of the slots are used
extern QuestStruct quests[10];
extern int numquests;
extern BOOL questlog;

//
// functions
//

void SetReturnLvlPos();
void DRLG_CheckQuests(int x, int y);
void QuestlogEnter();
void QuestlogUp();
void QuestlogDown();
void QuestlogOff();
void StartQuestlog();
void QuestLogESC();
void CheckQuests();
void InitQuests();

#endif