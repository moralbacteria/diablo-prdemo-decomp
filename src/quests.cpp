#include "quests.h"

#include "diablo.h"
#include "effects.h"
#include "engine.h"
#include "gendung.h"
#include "monster.h"
#include "player.h"
#include "setmaps.h"

//
// Initialized variables (.data:004B9AC8)
//

// Compile-time table of all quest data.
//
// Notice that all quests have dlvl min = dlvl max with 100% chance of finding.
//
// Also notice that the last quest is called "The Maze"... I think this is a leftover.
// This entry is used for the stairs to the catacombs but you never actually get
// a quest entry (outside of cheats). Also _qdslvl is set to 3 but _qtx/_qty is left at 0 so there's no way to enter w/o cheats
QuestData questlist[MAXQUESTS] = {
    {3, 3, Q_SKELKING, 100, SL_SKELKING, "The Skeleton King"},
    {1, 1, Q_BUTCHER, 100, SL_NONE, "The Butcher"},
    {5, 5, Q_ROCK, 100, SL_NONE, "The Magic Rock"},
    {7, 7, Q_SCHAMB, 100, SL_BONECHAMB, "The Bone Chamber"},
    {4, 4, Q_MAZE, 100, SL_MAZE, "The Maze"},
};

//
// Uninitialized variables (.data:00615F40)
//

QuestStruct quests[10]; // Runtime information about quests.
int numqlines;
int numquests;
int ReturnLvlX;
int ReturnLvlY;
int qline;
int qlist[MAXQUESTS];
BOOL questlog;      // Is the quest log shown?
int questpentframe; // The frame of animation that the spinning pentagram uses

//
// Code
//

// .text:00471730
// Determine which quests are availble and get ready for gameplay.
void InitQuests()
{
    int i;

    // Initialize all quests
    numquests = 0;

    for (i = 0; i < MAXQUESTS; ++i)
    {
        if (questlist[i]._qdrnd >= random_(100))
        {
            quests[numquests]._qtype = questlist[i]._qdtype;
            // Given that max=min for all questlist[i] this should not be random
            quests[numquests]._qlevel = random_(questlist[i]._qdlvlmax - questlist[i]._qdlvlmin + 1) + questlist[i]._qdlvlmin;
            quests[numquests]._qactive = QUEST_INIT;
            quests[numquests]._qslvl = questlist[i]._qslvl;
            // x/y will be set as necessary when the quest is found (see DRLG_CheckQuests)
            quests[numquests]._qtx = 0;
            quests[numquests]._qty = 0;
            quests[numquests]._qidx = i;

            ++numquests;
        }
    }

    // Disable quests not available in the demo
    if (demo_mode)
    {
        for (i = 0; i < MAXQUESTS; i++)
        {
            if (i != Q_BUTCHER)
            {
                quests[i]._qactive = QUEST_NOTAVAIL;
            }
        }
    }

    // Initialize quest log state
    questlog = FALSE;
    questpentframe = 1;
}

// .text:004718AB
// Warp to the set level for the appropriate quest if standing on the correct square.
// The location is determined by _qtx and _qty. This is set by DRLG_CheckQuests
// during level generation.
void CheckQuests()
{
    int i;

    if (!setlevel)
    {
        for (i = 0; i < numquests; i++)
        {
            // Player is standing in a specific spot on the quest level with the quest active
            if (currlevel == quests[i]._qlevel && quests[i]._qactive == QUEST_ACTIVE && plr[myplr]._pmode == PM_STAND && plr[myplr]._px == quests[i]._qtx && plr[myplr]._py == quests[i]._qty)
            {
                NetSendCmd(myplr, ACTION_NEWLVL, 0, WM_DIABSETLVL, quests[i]._qslvl);
            }
        }
    }
}

// .text:004719CD
// If the given monster is a quest monster then complete the quest.
void CheckQuestKill(int m)
{
    if (monster[m].MType->mtype == MT_SKING)
    {
        quests[Q_SKELKING]._qactive = QUEST_DONE;
        sfxdelay = 30;
        sfxdnum = IS_WARRIOR3;
    }
    if (monster[m].MType->mtype == MT_CLEAVER)
    {
        quests[Q_BUTCHER]._qactive = QUEST_DONE;
        sfxdelay = 30;
        sfxdnum = IS_WARRIOR3;
    }
}

// .text:00471A5D
// Copy dungeon into pdungeon
static void Quest_BackupDungeon()
{
    int i, j;

    for (j = 0; j < DMAXY; j++)
    {
        for (i = 0; i < DMAXX; i++)
        {
            pdungeon[i][j] = dungeon[i][j];
        }
    }
}

// .text:00471ACC
// Correct the wall transparency for the Butcher's room.
static void DrawButcher()
{
    int x, y;

    // Translate megatile coords to tile coords and account for buffer
    x = 2 * setpc_x + 16;
    y = 2 * setpc_y + 16;
    DRLG_RectTrans(x + 3, y + 3, x + 10, y + 10);
}

// .text:00471B18
// Set the entrance tile to the Skeleton King's Lair.
static void DrawSkelKing(int q, int x, int y)
{
    quests[q]._qtx = 2 * x + 28;
    quests[q]._qty = 2 * y + 23;
}

// .text:00471B5B
static void DrawSChamb(int q, int qactive, int x, int y)
{
    int rh;
    int xx;
    BYTE *setp;
    int i;
    int j;
    BYTE *sp;
    int yy;
    int rw;

    // Load the dungeon entrance and copy it into the level. If the Mythical Book
    // was read then copy the variant without walls. Otherwise (the quest is
    // done by reading the Book of Novaness or the Mythical Book wasn't read),
    // copy the sealed variant.
    if (quests[q]._qactive == QUEST_ACTIVE)
    {
        setp = LoadFileInMem("Levels\\L2Data\\Bonestr2.DUN");
    }
    else
    {
        setp = LoadFileInMem("Levels\\L2Data\\Bonestr1.DUN");
    }

    // Read .DUN header. First WORD is width, second WORD is height.
    sp = setp;
    rw = *sp;
    sp += 2; // WORD is 2 bytes
    rh = *sp;

    for (j = y; j < y + rh; j++)
    {
        for (i = x; i < x + rw; i++)
        {
            // 0 represents the lack of a notable tile. The game interprets this
            // as empty space, i.e. a walkable floor tile (tile 3).
            if (*sp != 0)
            {
                dungeon[i][j] = *sp;
            }
            else
            {
                dungeon[i][j] = 3;
            }
            sp += 2; // each tile is a WORD (2 bytes)
        }
    }

    // Record the location of the stairs (relative to the bottom corner?) for
    // use with CheckQuests.
    xx = 2 * x + 22;
    yy = 2 * y + 23;
    quests[q]._qtx = xx;
    quests[q]._qty = yy;

    Quest_BackupDungeon();

    // BUG: Memory leak! setp never freed
}

// .text:00471C89
// Place the sealed stairs to the catacombs into the level.
void DrawMaze(int x, int y)
{
    int rh;
    int xx;
    BYTE *setp;
    int i;
    int j;
    BYTE *sp;
    int yy;
    int rw;

    // Load the set piece and copy it into the level. Hero1 is the sealed
    // stairs to the catacombs found on dlvl 4.
    setp = LoadFileInMem("Levels\\L1Data\\Hero1.DUN");

    // Read .DUN header. First WORD is width, second WORD is height.
    sp = setp;
    rw = *sp;
    sp += 2; // WORD is 2 bytes
    rh = *sp;

    for (j = 0; j < rh; j++)
    {
        for (i = 0; i < rw; i++)
        {
            if (*sp != 0)
            {
                pdungeon[x + i][y + j] = *sp;
            }
            sp += 2; // WORD is 2 bytes
        }
    }

    MemFreeDbg(setp);
}

// .text:00471D61
// Perform any level generation tasks related to quests in the level. For
// example, place the sealed stairs to the catacombs on level 4.
void DRLG_CheckQuests(int x, int y)
{
    int i;

    for (i = 0; i < numquests; i++)
    {
        if (quests[i]._qlevel == currlevel)
        {
            switch (quests[i]._qtype)
            {
            case Q_BUTCHER:
                DrawButcher();
                break;
            case Q_SKELKING:
                DrawSkelKing(i, x, y);
                break;
            case Q_SCHAMB:
                DrawSChamb(i, quests[i]._qactive, x, y);
                break;
            case Q_MAZE:
                DrawMaze(x, y);
                break;
            }
        }
    }
}

// .text:00471E4D
void SetReturnLvlPos()
{
    switch (setlvlnum)
    {
    case SL_SKELKING:
        ReturnLvlX = quests[Q_SKELKING]._qtx + 1;
        ReturnLvlY = quests[Q_SKELKING]._qty;
        break;
    case SL_BONECHAMB:
        ReturnLvlX = quests[Q_SCHAMB]._qtx + 1;
        // The stairs is sealed off once the quest is done. Push the return
        // coordinate to prevent the player from getting stuck.
        if (quests[Q_SCHAMB]._qactive == QUEST_DONE)
        {
            ReturnLvlX += 4;
        }
        ReturnLvlY = quests[Q_SCHAMB]._qty;
        break;
    }
}

// .text:00471ED6
void GetReturnLvlPos()
{
    ViewX = ReturnLvlX;
    ViewY = ReturnLvlY;
}

// ResyncQuests	0000000000471EFA

// DrawQBack	0000000000471FC1

// PrintQLString	0000000000472030

// DrawQLine	0000000000472218

// DrawQuestLog	0000000000472274

// StartQuestlog	0000000000472324
void StartQuestlog()
{
    // TODO
}

// .text:004723C1
// Scroll up in the quest log
void QuestlogUp()
{
        if (numqlines)
    {
        if (qline == 8)
        {
            // Jump from the first quest in the log to Close Quest Log
            qline = 22;
        }
        else if (qline == 22)
        {
            // Jump from Close Quest Log to the last quest in the log
            qline = 2 * numqlines - 2 + 8;
        }
        else
        {
            // Scroll up to next entry down
            qline -= 2;
        }
    }
}

// .text:00472427
// Scroll down in the quest log
void QuestlogDown()
{
    if (numqlines)
    {
        if (qline == 22)
        {
            // Jump from Close Quest Log to first quest in the log
            qline = 8;
        }
        else if (qline == 2 * numqlines - 2 + 8)
        {
            // Jump from the log down to Close Quest Log
            qline = 22;
        }
        else
        {
            // Scroll down to next entry down
            qline += 2;
        }
    }
}

// QuestlogEnter	0000000000472491
void QuestlogEnter()
{
    // TODO
}
// QuestlogESC	00000000004724F3
void QuestLogESC()
{
    // TODO
}

// .text:00472580
// Hide the quest log
void QuestlogOff()
{
    questlog = FALSE;
}
