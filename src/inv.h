#ifndef __INV_H__
#define __INV_H__

#include <windows.h>

//
// defines
//

//
// enums
//

//
// structs
//

//
// variables
//

extern BOOL invflag;

//
// functions
//

void InitInv();
void RemoveScroll();
void UseStaffCharge();
void RemoveInvItem(int pnum, int iv);
void AutoGetItem(int pnum, int ii);
int InvPutItem(int pnum, int v1, int v2, int v3);
void UseInvItem(BOOL use_potbox);
void CheckInvItem();
void FreeInvGFX();

#endif