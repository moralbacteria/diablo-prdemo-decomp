#include "trigs.h"

#include <windows.h>
#include <stdio.h>
#include <string.h>

#include "control.h"
#include "cursor.h"
#include "diablo.h"
#include "defines.h"
#include "effects.h"
#include "gendung.h"
#include "player.h"
#include "quests.h"

//
// Initialized variables (.data:004B9BC0)
//

/** Specifies the dungeon piece IDs which constitute stairways leading down to the cathedral from town. */
int TownDownList[] = {716, 715, 719, 720, 721, 723, 724, 725, 726, 727, -1};
/** Specifies the dungeon piece IDs which constitute stairways leading up from the cathedral. */
int L1UpList[] = {127, 129, 130, 131, 132, 133, 135, 137, 138, 139, 140, -1};
/** Specifies the dungeon piece IDs which constitute stairways leading down from the cathedral. */
int L1DownList[] = {106, 107, 108, 109, 110, 112, 114, 115, 118, -1};
/** Specifies the dungeon piece IDs which constitute stairways leading up from the catacombs. */
int L2UpList[] = {266, 267, -1};
/** Specifies the dungeon piece IDs which constitute stairways leading down from the catacombs. */
int L2DownList[] = {269, 270, 271, 272, -1};
/** Specifies the dungeon piece IDs which constitute stairways leading up from the caves. */
int L3UpList[] = {170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, -1};
/** Specifies the dungeon piece IDs which constitute stairways leading down from the caves. */
int L3DownList[] = {162, 163, 164, 165, 166, 167, 168, 169, -1};

//
// Uninitialized variables (.data:00616028)
//

int numtrigs;
TriggerStruct trigs[5];
BOOL trigflag;

//
// Code (.text:004725A0)
//

// .text:004725A0
// DEAD CODE; This function is never called!
void InitNoTriggers()
{
    numtrigs = 0;
    trigflag = FALSE;
}

// .text:004725C4
// Create the trigger to enter the cathedral from town.
void InitTownTriggers()
{
    numtrigs = 1;
    trigs[0]._tx = 25;
    trigs[0]._ty = 29;
    trigs[0]._tmsg = WM_DIABNEXTLVL;
    trigflag = FALSE;
}

// .text:00472606
// Create triggers in the cathedral for up/down stairs. Called by CreateLevel after level generation.
// Since this is called after level generation, this will handle the stairs to
// the catacombs on level 4.
// 
// The entrace to the Skeleton King's Lair is handled by CheckQuests.
void InitL1Triggers()
{
    int i, j;

    numtrigs = 0;
    for (j = 0; j < MAXDUNY; j++)
    {
        for (i = 0; i < MAXDUNX; i++)
        {
            if (dPiece[i][j] == 129)
            {
                trigs[numtrigs]._tx = i;
                trigs[numtrigs]._ty = j;
                trigs[numtrigs]._tmsg = WM_DIABPREVLVL;
                numtrigs++;
            }
            if (dPiece[i][j] == 115)
            {
                trigs[numtrigs]._tx = i;
                trigs[numtrigs]._ty = j;
                trigs[numtrigs]._tmsg = WM_DIABNEXTLVL;
                numtrigs++;
            }
        }
    }
    trigflag = FALSE;
}

// .text:00472726
// Create triggers in the the catacomb for up/down stairs.
//
// The entrance to the Bone Chamber is handled by CheckQuests
void InitL2Triggers()
{
    int i, j;

    numtrigs = 0;
    for (j = 0; j < MAXDUNY; j++)
    {
        for (i = 0; i < MAXDUNX; i++)
        {
            // Create a trigger fo the up stairs. On level 7, only create the
            // trigger to go back to level 6. Ignore the Bone Chamber stairs;
            // this is handled by CheckQuests.
            if (dPiece[i][j] == 267 && (i != quests[Q_SCHAMB]._qtx || j != quests[Q_SCHAMB]._qty))
            {
                trigs[numtrigs]._tx = i;
                trigs[numtrigs]._ty = j;
                trigs[numtrigs]._tmsg = WM_DIABPREVLVL;
                numtrigs++;
            }
            if (dPiece[i][j] == 271)
            {
                trigs[numtrigs]._tx = i;
                trigs[numtrigs]._ty = j;
                trigs[numtrigs]._tmsg = WM_DIABNEXTLVL;
                numtrigs++;
            }
        }
    }
    trigflag = FALSE;
}

// .text:00472867
// Create the caves stair triggers.
void InitL3Triggers()
{
    int i, j;

    numtrigs = 0;
    for (j = 0; j < MAXDUNY; j++)
    {
        for (i = 0; i < MAXDUNX; i++)
        {
            if (dPiece[i][j] == 171)
            {
                trigs[numtrigs]._tx = i;
                trigs[numtrigs]._ty = j;
                trigs[numtrigs]._tmsg = WM_DIABPREVLVL;
                numtrigs++;
            }
            if (dPiece[i][j] == 168)
            {
                trigs[numtrigs]._tx = i;
                trigs[numtrigs]._ty = j;
                trigs[numtrigs]._tmsg = WM_DIABNEXTLVL;
                numtrigs++;
            }
        }
    }
    trigflag = FALSE;
}

// .text:0047298A
// This is basically the same function as InitL2Triggers (minus the quest check)
// since the catacombs and hell use the same tilset.
void InitL4Triggers()
{
    int i, j;

    numtrigs = 0;
    for (j = 0; j < MAXDUNY; j++)
    {
        for (i = 0; i < MAXDUNX; i++)
        {
            if (dPiece[i][j] == 267)
            {
                trigs[numtrigs]._tx = i;
                trigs[numtrigs]._ty = j;
                trigs[numtrigs]._tmsg = WM_DIABPREVLVL;
                numtrigs++;
            }
            if (dPiece[i][j] == 271)
            {
                trigs[numtrigs]._tx = i;
                trigs[numtrigs]._ty = j;
                trigs[numtrigs]._tmsg = WM_DIABNEXTLVL;
                numtrigs++;
            }
        }
    }
    trigflag = FALSE;
}

// .text:00472AAD
// Create the trigger to leave the Skeleton King's Lair.
void InitSKingTriggers()
{
    numtrigs = 1;
    // Unlike retail, this is the room past Leoric (with the fallen)
    trigs[0]._tx = 18;
    trigs[0]._ty = 49;
    trigs[0]._tmsg = WM_DIABRTNLVL;
    trigflag = FALSE;
}

// .text:00472AEF
// Create the trigger to leave the Bone Chamber.
void InitSChambTriggers()
{
    numtrigs = 1;
    trigs[0]._tx = 70;
    trigs[0]._ty = 39;
    trigs[0]._tmsg = WM_DIABRTNLVL;
    trigflag = FALSE;
}

// .text:00472B31
static BOOL ForceTownTrig()
{
    int i;
    for (i = 0; TownDownList[i] != -1; i++)
    {
        if (dPiece[cursmx][cursmy] == TownDownList[i])
        {
            strcpy(infostr, "Down to dungeon");
            cursmx = 25;
            cursmy = 29;
            return TRUE;
        }
    }
    return FALSE;
}

// .text:00472BC7
static BOOL ForceL1Trig()
{
    int i, j;

    for (i = 0; L1UpList[i] != -1; i++)
    {
        if (dPiece[cursmx][cursmy] == L1UpList[i])
        {
            if (currlevel > 1)
                sprintf(infostr, "Up to level %i", currlevel - 1);
            else
                strcpy(infostr, "Up to town");
            for (j = 0; j < numtrigs; j++)
            {
                if (trigs[j]._tmsg == WM_DIABPREVLVL)
                {
                    cursmx = trigs[j]._tx;
                    cursmy = trigs[j]._ty;
                    return TRUE;
                }
            }
        }
    }

    for (i = 0; L1DownList[i] != -1; i++)
    {
        if (dPiece[cursmx][cursmy] == L1DownList[i])
        {
            // lol wtf why does this exist
            if (demo_mode || currlevel == 1)
            {
                sprintf(infostr, "Down to level 2");
            }
            else
            {
                sprintf(infostr, "Down to level %i", currlevel + 1);
            }

            for (j = 0; j < numtrigs; j++)
            {
                if (trigs[j]._tmsg == WM_DIABNEXTLVL)
                {
                    cursmx = trigs[j]._tx;
                    cursmy = trigs[j]._ty;
                    return TRUE;
                }
            }
        }
    }

    return FALSE;
}

// .text:00472DD5
static BOOL ForceL2Trig()
{
    int i, j;

    for (i = 0; L2UpList[i] != -1; i++)
    {
        // Note that this looks under the cursor for specific tiles and ignores
        // the actual location of the trigger. This is probably the cause of
        // https://gitlab.com/moralbacteria/diablo-prdemo-patches/-/issues/17
        // On level 7 there's two up stairs (to level 6, and to the bone chamber)
        // but only one trigger.
        // 
        // (This is fixed in retail.)
        if (dPiece[cursmx][cursmy] == L2UpList[i])
        {
            for (j = 0; j < numtrigs; j++)
            {
                if (trigs[j]._tmsg == WM_DIABPREVLVL)
                {
                    // This is the only function that puts the sprintf inside the inner most trigs loop
                    sprintf(infostr, "Up to level %i", currlevel - 1);
                    // Since the stairs graphic exists on multiple tiles, this
                    // override ensures that the user will walk into the actual
                    // location of the trigger when clicking on any part of the
                    // stairs.
                    cursmx = trigs[j]._tx;
                    cursmy = trigs[j]._ty;
                    return TRUE;
                }
            }
        }
    }

    for (i = 0; L2DownList[i] != -1; i++)
    {
        if (dPiece[cursmx][cursmy] == L2DownList[i])
        {
            sprintf(infostr, "Down to level %i", currlevel + 1);

            for (j = 0; j < numtrigs; j++)
            {
                if (trigs[j]._tmsg == WM_DIABNEXTLVL)
                {
                    cursmx = trigs[j]._tx;
                    cursmy = trigs[j]._ty;
                    return TRUE;
                }
            }
        }
    }

    return FALSE;
}

// .text:00472F8C
static BOOL ForceL3Trig()
{
    int i, j;

    for (i = 0; L3UpList[i] != -1; i++)
    {
        if (dPiece[cursmx][cursmy] == L3UpList[i])
        {
            sprintf(infostr, "Up to level %i", currlevel - 1);
            for (j = 0; j < numtrigs; j++)
            {
                if (trigs[j]._tmsg == WM_DIABPREVLVL)
                {
                    cursmx = trigs[j]._tx;
                    cursmy = trigs[j]._ty;
                    return TRUE;
                }
            }
        }
    }

    for (i = 0; L3DownList[i] != -1; i++)
    {
        if (dPiece[cursmx][cursmy] == L3DownList[i])
        {
            sprintf(infostr, "Down to level %i", currlevel + 1);
            for (j = 0; j < numtrigs; j++)
            {
                if (trigs[j]._tmsg == WM_DIABNEXTLVL)
                {
                    cursmx = trigs[j]._tx;
                    cursmy = trigs[j]._ty;
                    return TRUE;
                }
            }
        }
    }

    return FALSE;
}

// .text:00473143
void CheckTrigForce()
{
    // And I guess this is why there's no text for leaving Skeleton King's Lair or the Bone Chamber
    if (setlevel || MouseY > PANEL_TOP - 1)
    {
        trigflag = FALSE;
        return;
    }

    switch (leveltype)
    {
    case DTYPE_TOWN:
        trigflag = ForceTownTrig();
        break;
    case DTYPE_OLD_CATHEDRAL:
        trigflag = ForceL1Trig();
        break;
    case DTYPE_CATACOMBS:
        trigflag = ForceL2Trig();
        break;
    case DTYPE_CAVES:
        trigflag = ForceL3Trig();
        break;
    case DTYPE_HELL:
        trigflag = ForceL2Trig();
        break;
    case DTYPE_CATHEDRAL:
        trigflag = ForceL1Trig();
        break;
    }
}

// .text:0047321A
void CheckTriggers()
{
    int i;

    if (plr[myplr]._pmode != PM_STAND)
        return;

    for (i = 0; i < numtrigs; i++)
    {
        if (plr[myplr]._px == trigs[i]._tx && plr[myplr]._py == trigs[i]._ty)
        {
            if (trigs[i]._tmsg == WM_DIABNEXTLVL)
            {
                if (!demo_mode || currlevel != 1)
                {
                    NetSendCmd(myplr, ACTION_NEWLVL, 0, trigs[i]._tmsg, 0);
                }
                else
                {
                    PlayRndSFX(IS_WARRIOR4);
                    NetSendCmd(myplr, ACTION_WALK_SW, 0, 0, 0);
                }
            }
            if (trigs[i]._tmsg == WM_DIABPREVLVL)
            {
                NetSendCmd(myplr, ACTION_NEWLVL, 0, trigs[i]._tmsg, 0);
            }
            if (trigs[i]._tmsg == WM_DIABRTNLVL)
            {
                SetReturnLvlPos();
                NetSendCmd(myplr, ACTION_NEWLVL, 0, trigs[i]._tmsg, 0);
            }
        }
    }
}
