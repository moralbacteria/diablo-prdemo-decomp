#include "monst2.h"

#include "engine.h"
#include "gendung.h"
#include "monster.h"
#include "player.h"

//
// functions (.text:0040CE80)
//

// M_GetDir	000000000040CE80
// TODO signature
__declspec(naked) void M_GetDir()
{
    __asm {
                push    ebp
                mov     ebp, esp
                sub     esp, 8
                push    ebx
                push    esi
                push    edi
                mov     [ebp-8], edx
                mov     [ebp-4], ecx
                mov     eax, [ebp-8]
                mov     ecx, eax
                shl     eax, 5
                add     eax, ecx
                lea     eax, [ecx+eax*4]
                shl     eax, 3
                sub     eax, ecx
                shl     eax, 4
                mov     eax, plr._py[eax]
                push    eax
                mov     eax, [ebp-8]
                mov     ecx, eax
                shl     eax, 5
                add     eax, ecx
                lea     eax, [ecx+eax*4]
                shl     eax, 3
                sub     eax, ecx
                shl     eax, 4
                mov     eax, plr._px[eax]
                push    eax
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 2
                lea     eax, [eax+eax*2]
                lea     eax, [ecx+eax*4]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     edx, monster._my[eax*4]
                mov     ecx, monster._mx[ecx*4]
                call    GetDirection
                jmp     $+5
; ---------------------------------------------------------------------------

loc_40CEFB:                             ; CODE XREF: .text:0040CEF6↑j
                pop     edi
                pop     esi
                pop     ebx
                leave
                retn
    }
}

// .text:0040CF00
// Returns if tile at (`x`, `y`) is solid
// TODO what does solid mean?
BOOL SolidLoc(int x, int y)
{
    return nSolidTable[dPiece[x][y]];
}