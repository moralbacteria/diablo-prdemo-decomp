#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <windows.h>
#include <DDRAW.H>

//
// variables
//

extern BYTE *pSquareCels;

//
// functions
//

void LoadDebugGFX();
void CaptureScreen(int pic_idx, PALETTEENTRY *palette);
void FreeDebugGFX();

#endif