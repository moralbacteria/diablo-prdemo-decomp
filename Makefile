# nmake pseudotargets don't need .PHONY
# https://learn.microsoft.com/en-us/cpp/build/reference/description-blocks?view=msvc-170#pseudotargets
all: build_storm_lib build_diablo_exe

# phony
build_storm_lib:
	cd storm
	nmake /nologo
	cd ..

# phony
build_diablo_exe:
	cd src
	nmake /nologo
	cd ..

# phony
clean:
	cd storm
	nmake /nologo clean
	cd ../src
	nmake /nologo clean
