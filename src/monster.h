#ifndef __MONSTER_H__
#define __MONSTER_H__

#include <windows.h>

#include "monstdat.h"
#include "sound.h"

//
// defines
//

#define MAXMONSTERS 200
#define MAX_LVLMTYPES 16

//
// enums
//

// Behaviors as part of the monster state machine. See ProcessMonsters
enum MON_MODE
{
    MM_STAND = 0,
    MM_WALK = 1,
    MM_WALK2 = 2,
    MM_WALK3 = 3,
    MM_ATTACK = 4,
    MM_GOTHIT = 5,
    MM_DEATH = 6,
    MM_SATTACK = 7,
    MM_FADEIN = 8,
    MM_FADEOUT = 9,
    MM_RATTACK = 10,
    MM_SPSTAND = 11,
    MM_RSPATTACK = 12,
    MM_DELAY = 13,
    MM_CHARGE = 14,
    MM_STONE = 15, // Monster will act like it was turned to stone
    MM_HEAL = 16,
};

// TODO: Copied from Devilution, verify
enum monster_flag
{
    MFLAG_HIDDEN = 0x01,
    // If set then monster _mAnimFrame won't progress. This stops the animation
    // Used by gargoyles and hidden
    MFLAG_LOCK_ANIMATION = 0x02, // verified (see ProcessMonsters)
    MFLAG_ALLOW_SPECIAL = 0x04,  // TODO this is used by gargoyles
    MFLAG_NOHEAL = 0x08,
    MFLAG_TARGETS_MONSTER = 0x10,
    MFLAG_GOLEM = 0x20,
    MFLAG_QUEST_COMPLETE = 0x40,
    MFLAG_KNOCKBACK = 0x80,
    MFLAG_SEARCH = 0x100,
    MFLAG_CAN_OPEN_DOOR = 0x200,
    MFLAG_NO_ENEMY = 0x400,
    MFLAG_BERSERK = 0x800,
    MFLAG_NOLIFESTEAL = 0x1000
};

// TODO: Copied from Devilution, verify
enum monster_goal
{
    MGOAL_NORMAL = 1,
    MGOAL_RETREAT = 2,
    MGOAL_HEALING = 3,
    MGOAL_MOVE = 4,
    MGOAL_ATTACK2 = 5,
    MGOAL_INQUIRING = 6,
    MGOAL_TALKING = 7,
};

// Monster animations. Each monster has 5 required animations and an optional
// 6th monster-dependent animation. `animletter` can be indexed by MON_ANIM to
// load from disk.
enum MON_ANIM
{
    MA_STAND = 0,
    MA_WALK = 1,
    MA_ATTACK = 2,
    MA_GOTHIT = 3,
    MA_DEATH = 4,
    // Optional special animation. E.g. skeleton rising from the ground, sneak disappearing, etc
    MA_SPECIAL = 5,
};

//
// structs
//

struct AnimStruct
{
    BYTE *CMem;    // Pointer to beginning of CEL data
    BYTE *Data[8]; // CEL animation data for each direction
    int Frames;    // # of frames for each direction
    int Rate;      // Delay between each frame in game ticks. The game runs at 20 FPS so the delay is 50ms * Rate between each frame.
};

// TODO: TSnd struct and Snds var
// TODO: Remove padding
// Traits that apply to a group of monsters in the dungeon, e.g. all Fallen, or all Horrors, etc.
// A lot of this is copied from `monsterdata` during `InitMonsterGFX()`
struct CMonster
{
    // `enum _monster_id`. Index into `monsterdata`
    char mtype;
    // Loaded CEL animation data.
    // There are 6 distinct animations: idle, walk, attack, hit, death, and (optional) special.
    AnimStruct Anims[6];
    DWORD field_10C; // padding?
    DWORD field_110; // padding?
    DWORD field_114; // padding?
    // Monster sounds. There's 4 sound types with 2 variants for each sound.
    TSnd Snds[4][2];
    BYTE gap278[36]; // padding???
    // Width in pixels for all CELs in `Anims`.
    int width;
    // `(width - 64) / 2`. Used to center the monster inside a tile (see scrollrt.cpp)
    int width2;
    // Lower bound on randomly generated HP
    unsigned char mMinHP;
    // Upper bound on randomly generated HP
    unsigned char mMaxHP;
    // If true then need to load the special attack graphics
    BOOL has_special;
    // "Attack Frame Number". Damage is not done until this attack animation frame.
    char mAFNum;
    // Index into `dead`. Used for creating corpses.
    char mdeadval;
    // Pointer to an entry in `monsterdata`
    MonsterData *MData;
    // Pointer to TRN file loaded into memory. Must be 256 bytes.
    BYTE *trans_file;
    // Difference from Devilution: no mFlags
};

// Represents the state of a single monster in a dungeon. There's 3 kinds of "tiers" of data
// 1. `monsterdata`: This is the compile-time table of 
struct MonsterStruct
{
    // Index into `Monsters`
    int _mMTidx;
    // Current state machine state
    int _mmode;
    char _mgoal;
    char anonymous_1[3]; // padding?
    int _mgoalvar1;
    int _mgoalvar2;
    BYTE gap14[8]; // ???
    int _mx;
    int _my;
    int _moldx;
    int _moldy;
    int _mxoff;
    int _myoff;
    int _mxvel;
    int _myvel;
    int _mdir;
    int _menemy;
    // Pointer to the current CEL animation, derived from AnimStruct.Data for the current facing direction
    unsigned char* _mAnimData;
    int _mAnimDelay;
    int _mAnimCnt;
    int _mAnimLen;
    int _mAnimFrame;
    // TRUE if monster is occluded by another tile (e.g. behind a wall)?
    BOOL _meflag;
    int _mDelFlag;
    int _mVar1;
    int _mVar2;
    int _mVar3;
    int _mVar4;
    int _mVar5;
    int _mVar6;
    int _mVar7;
    int _mVar8;
    int _mmaxhp;
    int _mhitpoints;
    // Index into AiProc. Determines how the monster behaves.
    char _mAi;
    // How "smart" the enemy is. Each enemy AI has variations of difficulty based on how smart it is. Higher is smarter.
    char _mint;
    char anonymous_10[2]; // padding?
    int _mFlags;
    BYTE _msquelch;
    char anonymous_12[3]; // padding?
    int anonymous_13;
    int _lastx;
    int _lasty;
    BYTE gapA0[4];
    // If unique, index into UniqMonst.
    char _uniqtype;
    // If unique, which TRN to use.
    // TODO how is this used?
    char _uniqtrans;
    char _udeadval;
    char mLevel;
    __int16 mExp;
    char mHit;
    char mMinDamage;
    char mMaxDamage;
    char mHit2;
    char mMinDamage2;
    char mMaxDamage2;
    char mArmorClass;
    char anonymous_28;
    __int16 mMagicRes;
    // Index into `monsters`. The group leader, usually a unique.
    char leader;
    // Set to 1 if this monster is part of a group. In that case, `leader` should be respected.
    char leaderflag;
    // For a unique monster acting as a leader, how many lackies are in their group. This can be between 0 and 8 (inclusive) based on how the algorithm actually placed the monsters.
    // Only used if mUniqAttr & UA_GROUP.
    char packsize;
    // Monsters (i.e. unique monster) can have a light that follows them. This is the ID into the lights table.
    char mlid;
    const char *mName;
    // Pointer into `Monsters` which describes properties common among types of monsters in a dungeon.
    CMonster *MType;
    // Pointer into `monsterdata` for other compile-time monster properties
    MonsterData *MData;
};

//
// variables
//

extern int nummtypes;
extern int nummonsters;
extern int monstactive[MAXMONSTERS];
extern int monstkills[MAXMONSTERS];
extern MonsterStruct monster[MAXMONSTERS];
extern CMonster Monsters[MAX_LVLMTYPES];
extern int offset_x[8];
extern int offset_y[8];

//
// functions
//

void InitMonsterGFX();

void M_StartKill(int i, int pnum);

void MAI_Zombie(int i);
void MAI_Fat(int i);
void MAI_SkelSd(int i);
void MAI_SkelBow(int i);
void MAI_Scav(int i);
void MAI_Rhino(int i);
void MAI_GoatMc(int i);
void MAI_GoatBow(int i);
void MAI_Fallen(int i);
void MAI_Magma(int i);
void MAI_SkelKing(int i);
void MAI_Bat(int i);
void MAI_Garg(int i);
void MAI_Cleaver(int i);
void MAI_Succ(int i);
void MAI_Sneak(int i);
void MAI_Storm(int i);

void SpawnSkeleton(int x, int y);
void FreeMonsters();
void ProcessMonsters();
void PlaceGroup(int *mid, int mtype, int num, BOOL has_leader, int leader);
void SyncMonsterAnim(int i);

#endif