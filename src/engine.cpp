#include "engine.h"

#include <storm.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include "defines.h"
#include "diablo.h"
#include "scrollrt.h"

//
// uninitialized vars (.data:???)
//

char file_to_load_with_base[MAX_PATH];
DWORD dword_61B6F4;
DWORD dword_61B6F8;
char byte_61B6FC;

//
// code (.text:00482790)
//

// .text:00482790
// Load an entire file from disk and return it in a freshly allocated buffer.
// You must free this buffer when done with MemFreeDbg
BYTE *LoadFileInMem(const char *pszName)
{
    int fileLen;
    BYTE *buf;
    HANDLE file;
    char fullName[256];

    sprintf(fullName, "%s%s", fileLoadPrefix, pszName);
    while (!SFileOpenFile(fullName, &file))
    {
        // This space intentionally left blank
    }

    fileLen = SFileGetFileSize(file, NULL);
    buf = (BYTE *)DiabloAllocPtr(fileLen);

    while (!SFileReadFile(file, buf, fileLen, NULL, NULL))
    {
        // This space intentionally left blank
    }

    SFileCloseFile(file);
    return buf;
}

// .text:0048283B
// Like LoadFileInMem but you've already allocated the memory
void LoadFileWithMem(const char *pszName, BYTE *p)
{
    DWORD dwFileLen;
    HANDLE hsFile;
    char fullName[256];

    sprintf(fullName, "%s%s", fileLoadPrefix, pszName);
    while (!SFileOpenFile(fullName, &hsFile))
    {
        // This space intentionally left blank
    }

    dwFileLen = SFileGetFileSize(hsFile, NULL);

    while (!SFileReadFile(hsFile, p, dwFileLen, NULL, NULL))
    {
        // This space intentionally left blank
    }

    SFileCloseFile(hsFile);
}

// .text:004828D6
// Allocate and read the wave format. Also read the data size into pSnd->pcm_size
int LoadWaveFileHeaderInMem(const char *filename, TSnd *pSnd, LPWAVEFORMATEX *pFormat)
{
    DWORD dwSize; // unused
    DWORD unused;
    HANDLE hFile;

    SFileOpenFile(filename, &hFile);
    dwSize = SFileGetFileSize(hFile, NULL);

    // Surgically load parts of the RIFF https://docs.fileformat.com/audio/wav/
    // 0x14 to 0x24 is WAVEFORMATEX... kinda. cbSize is not stored in the file,
    // it goes straight to "data" chunk header. This means that cbSize is read
    // as "da" or 0x6164
    SFileSetFilePointer(hFile, 0x14, NULL, 0);
    *pFormat = (LPWAVEFORMATEX)GlobalAlloc(GMEM_FIXED, sizeof(WAVEFORMATEX));
    SFileReadFile(hFile, *pFormat, sizeof(WAVEFORMATEX), NULL, NULL);

    // The programmers seemed to know that they were reading part of the data
    // chunk header above and just skip over the rest ("ta")
    SFileReadFile(hFile, &unused, 2, NULL, NULL);

    // Read WAV file size (data) into the first element of pSnd (pSnd->pcm_size)
    SFileReadFile(hFile, pSnd, 4, NULL, NULL);

    SFileCloseFile(hFile);
    return 0;
}

// .text:0048297B
// See LoadWaveFileHeaderInMem
int LoadWaveFileWithMem(const char *filename, TSnd *pSnd, LPWAVEFORMATEX *pFormat, void **pcm_data)
{
    DWORD dwSize; // unused
    DWORD unused;
    HANDLE hFile;

    SFileOpenFile(filename, &hFile);
    dwSize = SFileGetFileSize(hFile, NULL);

    SFileSetFilePointer(hFile, 0x14, NULL, 0);
    SFileReadFile(hFile, *pFormat, sizeof(WAVEFORMATEX), NULL, NULL);
    SFileReadFile(hFile, &unused, 2, NULL, NULL);
    SFileReadFile(hFile, pSnd, 4, NULL, NULL);
    SFileReadFile(hFile, *pcm_data, pSnd->pcm_size, NULL, NULL);

    SFileCloseFile(hFile);
    return 0;
}

// .text:00482A2A
// Demo specific!
DWORD FileGetSize(const char *filename)
{
    DWORD dwSize;
    HANDLE hFile;
    char filenameWithPrefix[256];

    sprintf(filenameWithPrefix, "%s%s", fileLoadPrefix, filename);
    while (!SFileOpenFile(filenameWithPrefix, &hFile))
    {
        // This space intentionally left blank
    }

    dwSize = SFileGetFileSize(hFile, NULL);
    SFileCloseFile(hFile);
    return dwSize;
}

// .text:00482A9D
char *FileAddLoadPrefix(const char *filename)
{
    sprintf(file_to_load_with_base, "%s%s", fileLoadPrefix, filename);
    return file_to_load_with_base;
}

// .text:00482AD3
// Renders a CEL frame into a buffer.
//
// `pDecodeTo` is the destination buffer.
//
// `pRLEBytes` is the run-length encoded CEL data (just the data, no frame table
// or skip header) to decode and render. It contains `nDataSize` bytes total and
// each row is `nWidth` pixels.
//
// This is exactly the same as Devilution (minus the asserts).
void CelBlit(BYTE *pDecodeTo, BYTE *pRLEBytes, int nDataSize, int nWidth)
{
    // w = nWidth + BUFFER_WIDTH; this is the amount that needs to be removed
    // from the output iterator in order to push it up one row after rendering.
    int w;

    // `pDecodeTo` is subtracted from all the time since CELs are rendered
    // bottom-to-top, left-to-right. The input iterator advances since it's
    // stored upside down, while the output iterator decreases to "flip" the
    // image.
    __asm {
        mov        esi, pRLEBytes
        mov        edi, pDecodeTo
        mov        eax, BUFFER_WIDTH
        add        eax, nWidth
        mov        w, eax ; // w = nWidth + BUFFER_WIDTH;
        mov        ebx, nDataSize
        add        ebx, esi ; // ebx = pRLEBytes + nDataSize; (end of input)
    start_row:
        mov        edx, nWidth ; // edx is the # of px left to render this row
    start_run:
        ; // Load a byte from pRLEBytes representing the run length
        xor        eax, eax
        lodsb
        ; // Negative? render transparency
        or        al, al
        js        transparent_run
        ; // Otherwise, copy the next `al` pixels from pRLEBytes into pDecodeTo
        sub        edx, eax
        mov        ecx, eax
        ; // Use the most efficient movs* instructions to copy the run. Prefer
        ; // movsd (4 bytes) but use movsb (1 byte) or movsw (2 bytes) if the
        ; // run is not a multiple of 4.
        shr        ecx, 1
        jnb        copy_words
        movsb
        jecxz    finish_copy_run
    copy_words:
        shr        ecx, 1
        jnb        copy_dwords
        movsw
        jecxz    finish_copy_run
    copy_dwords:
        rep movsd
    finish_copy_run:
        ; // If we haven't rendered enough px this row, read another run
        or        edx, edx
        jz        finish_row
        jmp        start_run
    transparent_run:
        ; // Run length is negative; skip pixels to perform transparency.
        neg        al
        add        edi, eax
        ; // If we haven't rendered enough pixels this row, read another run.
        sub        edx, eax
        jnz        start_run
        ; // Otherwise, fall through
    finish_row:
        ; // "Advance" the output iterator up 1 row
        sub        edi, w // pDecodeTo -= w;
        ; // More data left to read? Keep reading
        cmp        ebx, esi
        jnz        start_row
    }
}

// .text:00482B53
// sx and sy are locations into the pixel buffer gpBuffer. Beware that there are
// invisible portions on top/left/bottom/right! See defines.h
//
// nCel is 1-based (don't give 0!)
void CelDraw(int sx, int sy, BYTE *pCelBuff, int nCel, int nWidth)
{
    BYTE *pRLEBytes;
    BYTE *pDest;
    int nDataSize;

    // This kinda looks like asm based on the optimizations of array access.
    // This just sets up the locals used in the next function call.
    __asm {
        mov     ebx, pCelBuff ; // Treat pCelBuff like frame table
        mov     eax, nCel
        shl     eax, 2
        add     ebx, eax ; // ebx now points to current frame offset
        mov     eax, [ebx+4] ; // ebx+4 is next frame offset
        sub     eax, [ebx] ; // length is next frame offset - current frame offset
        mov     nDataSize, eax

        mov     eax, pCelBuff
        add     eax, [ebx]
        mov     pRLEBytes, eax ; // Use frame offset to find start of frame data

        mov     eax, sy
        mov     eax, PitchTbl[eax*4] ; // idk why they use PitchTbl when it's basically sy * 768
        add     eax, sx
        add     eax, gpBuffer
        mov     pDest, eax
    }

    CelBlit(pDest, pRLEBytes, nDataSize, nWidth);
}

// engine_cel_482BB2    0000000000482BB2
// DrawSlabCel    0000000000482BFB
// CelDecodeHdrOnly    0000000000482CBC
// DecodeFullCelL    0000000000482D5F
// DrawFullCelL_xbytes    0000000000482DDC
// CelDecDatLightTrans    0000000000482E56
// engine_cel_482F54    0000000000482F54
// CelDrawLight    0000000000483046
// engine_cel_48312C    000000000048312C
// CelDecodeHdrLightTrans    00000000004831F4
// CelDrawLightTbl    00000000004832E1

// .text:00483477
/**
 * @brief Same as CelBlit but checks for drawing outside the buffer
 * @param pDecodeTo The output buffer
 * @param pRLEBytes CEL pixel stream (run-length encoded)
 * @param nDataSize Size of CEL in bytes
 * @param nWidth Width of sprite
 */
static void CelBlitSafe(BYTE *pDecodeTo, BYTE *pRLEBytes, int nDataSize, int nWidth)
{
    int w; // Number of bytes to skip in order to get from the end of one row to the beginning of the next
    __asm {
        mov     esi, pRLEBytes
        mov     edi, pDecodeTo
        mov     eax, 300h ; // TODO magic number
        add     eax, nWidth
        mov     w, eax
        mov     ebx, nDataSize
        add     ebx, esi

    row_start:
        ; // Reset number of pixels left this row to CEL width
        mov     edx, nWidth

    decode_run:
        ; // Read the first byte of a run to determine what to do.
        ; // There are 2 cases:
        ; // 1. Negative number: the run contains `abs(AL)` number of transparent pixels
        ; // 2. Positive number: the run contains `AL` number of pixels to copy
        xor     eax, eax
        lodsb

        ; // < 0? Treat the run as transparent
        or      al, al
        js      draw_transparency

        ; // Still within buffer bounds? Keep drawing
        sub     edx, eax
        cmp     edi, gpBufEnd
        jb      copy_byte

        ; // ???
        add     esi, eax
        add     edi, eax
        jmp     check_if_done_row

        ; // Copy EAX pixels from ESI to EDI. Split into three sections to handle
        ; // cases of EAX % 4 != 0
    copy_byte:
        mov     ecx, eax
        shr     ecx, 1
        jnb     copy_word
        movsb
        jecxz   short check_if_done_row

    copy_word:
        shr     ecx, 1
        jnb     copy_dwords
        movsw
        jecxz   short check_if_done_row

    copy_dwords:
        rep movsd
    
    check_if_done_row:
        ; // Reache the end of the row? Go to next row.
        or      edx, edx
        jz      finish_row
        ; // Otherwise, keep decoding runs 
        jmp     decode_run

    draw_transparency:
        ; // Draw transparency by advancing the iterator `AL * -1` bytes
        neg     al
        add     edi, eax
        sub     edx, eax
        jnz     decode_run

    finish_row:
        ; // Shift iterator to the next row (drawn bottom-up).
        ;  // Continue drawing as long as there are more rows.
        sub     edi, w
        cmp     ebx, esi
        jnz     row_start
    }
}

// .text:0048350C
/**
 * @brief Same as CelDraw but checks for drawing outside the buffer
 * @param sx Back buffer coordinate
 * @param sy Back buffer coordinate
 * @param pCelBuff CEL buffer
 * @param nCel CEL frame number
 * @param nWidth Width of sprite
 * @param CelSkip Skip lower parts of sprite, must be multiple of 2, max 8
 * @param CelCap Amount of sprite to render from lower to upper, must be multiple of 2, max 8
 */
void CelDrawSafe(int sx, int sy, BYTE *pCelBuff, int nCel, int nWidth, int CelSkip, int CelCap)
{

    // TODO
}

// CDrawSlabCelP    00000000004835CD
// Cel2DecDatLightOnly    0000000000483670
// Cel2DecDatLightEntry    0000000000483702
// Cel2DecDatLightTrans    000000000048377C

// .text:0048388F
/**
 * @brief Same as CelDrawLight but checks for drawing outside the buffer
 * @param sx Back buffer coordinate
 * @param sy Back buffer coordinate
 * @param pCelBuff CEL buffer
 * @param nCel CEL frame number
 * @param nWidth Width of sprite
 * @param CelSkip Skip lower parts of sprite, must be multiple of 2, max 8
 * @param CelCap Amount of sprite to render from lower to upper, must be multiple of 2, max 8
 */
void CelDrawLightSafe(int sx, int sy, BYTE *pCelBuff, int nCel, int nWidth, int CelSkip, int CelCap)
{
    // TODO
}

// engine_cel_483975    0000000000483975
// Cel2DecodeLightTrans    0000000000483A3D

// .text:00483B2A
/**
 * @brief Same as CelDrawLightTbl but checks for drawing outside the buffer
 * @param sx Back buffer coordinate
 * @param sy Back buffer coordinate
 * @param pCelBuff CEL buffer
 * @param nCel CEL frame number
 * @param nWidth Width of sprite
 * @param CelSkip Skip lower parts of sprite, must be multiple of 2, max 8
 * @param CelCap Amount of sprite to render from lower to upper, must be multiple of 2, max 8
 * @param light light shade to use
 */
void CelDrawLightTblSafe(int sx, int sy, BYTE *pCelBuff, int nCel, int nWidth, int CelSkip, int CelCap, char light)
{
    BYTE *pRLEBytes;

    pRLEBytes = &pCelBuff[((DWORD *)pCelBuff)[nCel]];
    // TODO
}

// CelBlitWidth    0000000000483CD5
void CelBlitWidth(BYTE *pBuff, int x, int y, int wdt, BYTE *pCelBuff, int nCel, int nWidth)
{
    // TODO
}
// OutlineSlabCel    0000000000483D7E

// .text:00483E99
void CelDrawOutlineSafe(char col, int sx, int sy, BYTE *pCelBuff, int nCel, int nWidth, int CelSkip, int CelCap)
{
    // TODO
}

// .text:00483FFB
// Performs a color swap on a CEL `p` for frames 1 to `nCel` (inclusive).
// For each color i, replace it with `ttbl[i]`.
void CelApplyTrans(BYTE *p, BYTE *ttbl, int nCel)
{
    int i;

    for (i = 1; i <= nCel; i++)
    {
        // The star of the show is the XLAT instruction.
        // The rest just sets up pointers and loop sizes.
        __asm {
            mov     ebx, p
            mov     eax, i
            shl     eax, 2
            add     ebx, eax ; // ebx is the offset within the group for the frame data

            mov     esi, p ; // Load group
            add     esi, [ebx] ; // Find frame i data
            add     esi, 10 ; // Ignore skip header

            mov     edx, [ebx+4]
            sub     edx, [ebx]
            sub     edx, 10 ; // Size is (next offset) - (this offset) - (skip header size)

            mov     edi, esi
            mov     ebx, ttbl ; // Used in XLAT instruction below

        top:
            xor     eax, eax
            lodsb ; // Load+store CEL "instruction"
            stosb
            dec     edx
            jz      done

            or      al, al
            js      top ; // "Transparency" instruction (eax < 0) has no data following

                ; // "Copy" instruction (eax > 0) is followed by data
                ; // the byte of instruction is the size of that data
            sub     edx, eax
            mov     ecx, eax

        apply_trans:
            lodsb
                    ; // Here's a weird instruction: https://www.felixcloutier.com/x86/xlat:xlatb
            xlat ; // AL = ttbl[AL] in our case, where AL is the pixel 256-color
            stosb
            loop apply_trans

            or      edx, edx
            jnz     top

        done:
            nop ; // Go to next for-loop iteration
        }
    }
}

// ENG_set_pixel    000000000048407B
void ENG_set_pixel(int sx, int sy, BYTE col)
{
    // TODO
}

// engine_draw_pixel    00000000004840F9

// DrawLine    00000000004841F1
void DrawLine(int x0, int y0, int x1, int y1, BYTE col)
{
    // TODO
}

// .text:004848A1
/**
 * @brief Calculate the best fit direction between two points
 * @param x1 Tile coordinate
 * @param y1 Tile coordinate
 * @param x2 Tile coordinate
 * @param y2 Tile coordinate
 * @return A value from the direction enum
 */
int GetDirection(int x1, int y1, int x2, int y2)
{
    int mx, my;
    int md;

    mx = x2 - x1;
    my = y2 - y1;

    if (mx >= 0)
    {
        if (my >= 0)
        {
            md = DIR_S;
            if (2 * mx < my)
            {
                md = DIR_SW;
            }
        }
        else
        {
            md = DIR_E;
            my = -my;
            if (2 * mx < my)
            {
                md = DIR_NE;
            }
        }
        if (2 * my < mx)
        {
            return DIR_SE;
        }
    }
    else
    {
        if (my >= 0)
        {
            md = DIR_W;
            my = -mx;
            if (2 * my < my)
            {
                md = DIR_SW;
            }
        }
        else
        {
            md = DIR_N;
            my = -mx;
            my = -my;
            if (2 * my < my)
            {
                md = DIR_NE;
            }
        }
        if (2 * my < my)
        {
            return DIR_NW;
        }
    }

    return md;
}

// .text:004849E2
// Generate a pseudo-random number from 0 (inclusive) to `v` (exclusive).
// NOTE: If v > 65535 is ignored; the max value is 65535.
// NOTE: If v < 0 then 0 is always returned.
// NOTE: Set seed with `srand()`.
int random_(int v)
{
    if (v <= 0)
    {
        return 0;
    }
    return rand() % v;
}
