#ifndef __STORES_H__
#define __STORES_H__

#include "items.h"

//
// variables
//

extern int SStringY[24];
extern char stextflag;
extern ItemStruct golditem;

//
// functions
//

void InitStores();
void SetupTownStores();
void ClearSText(int s, int e);
void SetGoldCurs(int i);
void TakePlrsMoney(int cost);
void StartStore(char s);
void STextESC();
void STextUp();
void STextDown();
void STextEnter();
void CheckStoreBtn();
void ReleaseStoreBtn();
void FreeStoreMem();
void DrawSTextHelp();
void PrintSString(int x, int y, BOOL cjustflag, const char *str, char col, int val);
void DrawSLine(int y);

#endif