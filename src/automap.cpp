#include "automap.h"

#include <windows.h>

#include "defines.h"
#include "diablo.h"
#include "engine.h"
#include "gendung.h"
#include "palette.h"

//
// defines
//

#define MAX_AUTOMAP_SCALE 200
#define MIN_AUTOMAP_SCALE 50
#define AUTOMAP_SCALE_DELTA 5

// automap tile definitions are a WORD (16 bits). The lower 4 bits are the
// "type". The upper 8 bits are flags. The rest is ignored.
#define MAPFLAG_TYPE 0x000F

#define MAPFLAG_VERTDOOR 0x01
#define MAPFLAG_HORZDOOR 0x02
#define MAPFLAG_VERTARCH 0x04
#define MAPFLAG_HORZARCH 0x08
#define MAPFLAG_VERTGRATE 0x10
#define MAPFLAG_HORZGRATE 0x20
#define MAPFLAG_DIRT 0x40
#define MAPFLAG_STAIRS 0x80

/** color for bright map lines (doors, stairs etc.) */
#define COLOR_BRIGHT PAL8_YELLOW
/** color for dim map lines/dots */
#define COLOR_DIM (PAL16_YELLOW + 8)
#define COLOR_AUTOMAP_ITEM (PAL8_BLUE + 3)

#define CALC_AMLINE64(scale) (scale * 64 / 100)
#define RECALCULATE_AMLINE(scale) \
    CALC_AMLINE64(scale);         \
    AmLine32 = AmLine64 / 2;      \
    AmLine16 = AmLine32 / 2;      \
    AmLine8 = AmLine16 / 2;       \
    AmLine4 = AmLine8 / 2;

//
// uninitialized variables (.data:005FD590)
//

// This is zoom %, e.g. 100=100%
// min is 50 (so 1/2), and max is 200 (so 2x)
// increments are 5%
// Used to scale `AmLine*` by AutoMapScale/100
int AutoMapScale;
char AmShiftTab[32];
BOOLEAN automapview[DMAXX][DMAXY];
BOOL automapflag;
WORD automaptype[512];
int AutoMapX;
int AutoMapY;
// Size of a "64px" line. Scaled based on AutoMapScale
// e.g. AmLine64 = 64 * (AutoMapScale / 100)
// Only 64px at AutoMapScale=100
int AmLine64;
// Size of a "32px" line. See AmLine64
int AmLine32;
// Size of a "16px" line. See AmLine64
int AmLine16;
// Size of a "8px" line. See AmLine64
int AmLine8;
// Size of a "4px" line. See AmLine64
int AmLine4;
// Offset from player location. Normally player is in middle of the screen
int AutoMapXOfs;
int AutoMapYOfs;

//
// code (.text:00451CD0)
//

// .text:00451CD0
void InitAutomapOnce()
{
    automapflag = FALSE;
    if (demo_mode)
    {
        // ... why does demo mode use a more zoomed out map?
        AutoMapScale = 65;
        RECALCULATE_AMLINE(AutoMapScale);
    }
    AutoMapScale = 100;
    RECALCULATE_AMLINE(AutoMapScale);
}

// .text:00451D87
void InitAutomap()
{
    DWORD dwTiles; // # of words in AMP file
    int j;         // auto map scale iterator, from 50 to 200 in 5 point increments
    BYTE *pTmp;    // AMP file contents iterator
    int d;         // AmLine64 len at various auto map scales
    BYTE b1;       // WORD lo 8 bits
    int i;         // general purpose iterator
    BYTE b2;       // WORD hi 8 bits
    int y;         // row iterator
    BYTE *pAFile;  // AMP file contents

    // Look at all automap zoom levels [50..200]
    // 31 = (MAX_AUTOMAP_SCALE - MIN_AUTOMAP_SCALE) / AUTOMAP_SCALE_DELTA + 1
    j = MIN_AUTOMAP_SCALE;
    for (i = 0; i < 31; i++)
    {
        // i represents zoom level 50 + 5*i
        d = CALC_AMLINE64(j);
        AmShiftTab[i] = 2 * (SCREEN_WIDTH / 2 / d) + 1;
        if ((SCREEN_WIDTH / 2) % d)
            AmShiftTab[i]++;
        if ((SCREEN_WIDTH / 2) % d >= (j << 5) / 100)
            AmShiftTab[i]++;
        j += AUTOMAP_SCALE_DELTA;
    }

    // Flush automaptype of previous AMP. Prevents using old AMP contents,
    // regardless of whether a new AMP is loaded.
    for (i = 0; i < 512; ++i)
    {
        automaptype[i] = 0;
    }

    // For some reason, they only wanted minimap to work in certain leveltypes.
    // In reality, it also works for DTYPE_OLD_CATHEDRAL and DTYPE_HELL and
    // actually doesn't work well for DTYPE_CAVES...
    if (leveltype == DTYPE_CATHEDRAL || leveltype == DTYPE_CATACOMBS || leveltype == DTYPE_CAVES)
    {
        // Load the correct AMP file
        switch (leveltype)
        {
        case DTYPE_CATHEDRAL:
            pAFile = LoadFileInMem("Levels\\L1Data\\L1.AMP");
            // We're interested in number of words. To convert from bytes to
            // words, divide by sizeof(WORD) == 2.
            dwTiles = FileGetSize("Levels\\L1Data\\L1.AMP") / 2;
            break;
        case DTYPE_CATACOMBS:
            pAFile = LoadFileInMem("Levels\\L2Data\\L2.AMP");
            dwTiles = FileGetSize("Levels\\L2Data\\L2.AMP") / 2;
            break;
        case DTYPE_CAVES:
            pAFile = LoadFileInMem("Levels\\L3Data\\L3.AMP");
            dwTiles = FileGetSize("Levels\\L3Data\\L3.AMP") / 2;
            break;
        }

        // .AMP is an array of little-endian words. Parse into automaptype
        pTmp = pAFile;
        for (i = 1; i <= dwTiles; i++)
        {
            b1 = *pTmp++; // lo
            b2 = *pTmp++; // hi
            automaptype[i] = b1 + (b2 << 8);
        }
        MemFreeDbg(pAFile);

        // Forget old exploration
        for (y = 0; y < DMAXY; ++y)
        {
            for (i = 0; i < DMAXX; ++i) // reuse i as column iterator
            {
                automapview[i][y] = 0;
            }
        }
        for (y = 0; y < MAXDUNY; y++)
        {
            for (i = 0; i < MAXDUNX; i++) // reuse i as column iterator
                dFlags[i][y] &= ~BFLAG_EXPLORED;
        }
    }
}

// .text:00452057
void StartAutomap()
{
    AutoMapXOfs = 0;
    AutoMapYOfs = 0;
    automapflag = TRUE;
}

// text:00452085
void AutomapUp()
{
    AutoMapXOfs--;
    AutoMapYOfs--;
}

// text:004520A1
void AutomapDown()
{
    AutoMapXOfs++;
    AutoMapYOfs++;
}

// text:000520BD
void AutomapLeft()
{
    AutoMapXOfs--;
    AutoMapYOfs++;
}

// text:000520D9
void AutomapRight()
{
    AutoMapXOfs++;
    AutoMapYOfs--;
}

// .text:004520F5
void AutomapZoomIn()
{
    if (AutoMapScale < MAX_AUTOMAP_SCALE)
    {
        AutoMapScale += AUTOMAP_SCALE_DELTA;
        RECALCULATE_AMLINE(AutoMapScale);
    }
}

// .text:00452161
void AutomapZoomOut()
{
    if (AutoMapScale > MIN_AUTOMAP_SCALE)
    {
        AutoMapScale -= AUTOMAP_SCALE_DELTA;
        RECALCULATE_AMLINE(AutoMapScale);
    }
}

// .text:004521CA
// DEAD CODE!!!!
// Note to self: this function cannot be static because if it is then the
// compiler will omit it as part of dead code removal
void DrawAutomapItem(int x, int y)
{
    int x1, y1, x2, y2;

    x1 = x - AmLine32;
    y1 = y - AmLine16;
    x2 = x1 + AmLine64;
    y2 = y1 + AmLine32;
    DrawLine(x, y1, x1, y, COLOR_AUTOMAP_ITEM);
    DrawLine(x, y1, x2, y, COLOR_AUTOMAP_ITEM);
    DrawLine(x, y2, x1, y, COLOR_AUTOMAP_ITEM);
    DrawLine(x, y2, x2, y, COLOR_AUTOMAP_ITEM);
}

// .text:00452273
// TODO I hope this is right, I didn't verify every function call... I just
// squinted at the ASM, squinted at Devilution, and decided that they were
// mostly the same.
static void DrawAutomapTile(int sx, int sy, WORD automap_type)
{
    int x1;
    int x2;
    int y1;
    BOOL do_horz;
    BOOL do_vert;
    BYTE flags;
    int y2;

    flags = automap_type >> 8;

    if (flags & MAPFLAG_DIRT)
    {
        ENG_set_pixel(sx, sy, COLOR_DIM);
        ENG_set_pixel(sx - AmLine8, sy - AmLine4, COLOR_DIM);
        ENG_set_pixel(sx - AmLine8, sy + AmLine4, COLOR_DIM);
        ENG_set_pixel(sx + AmLine8, sy - AmLine4, COLOR_DIM);
        ENG_set_pixel(sx + AmLine8, sy + AmLine4, COLOR_DIM);
        ENG_set_pixel(sx - AmLine16, sy, COLOR_DIM);
        ENG_set_pixel(sx + AmLine16, sy, COLOR_DIM);
        ENG_set_pixel(sx, sy - AmLine8, COLOR_DIM);
        ENG_set_pixel(sx, sy + AmLine8, COLOR_DIM);
        ENG_set_pixel(sx + AmLine8 - AmLine32, sy + AmLine4, COLOR_DIM);
        ENG_set_pixel(sx - AmLine8 + AmLine32, sy + AmLine4, COLOR_DIM);
        ENG_set_pixel(sx - AmLine16, sy + AmLine8, COLOR_DIM);
        ENG_set_pixel(sx + AmLine16, sy + AmLine8, COLOR_DIM);
        ENG_set_pixel(sx - AmLine8, sy + AmLine16 - AmLine4, COLOR_DIM);
        ENG_set_pixel(sx + AmLine8, sy + AmLine16 - AmLine4, COLOR_DIM);
        ENG_set_pixel(sx, sy + AmLine16, COLOR_DIM);
    }

    if (flags & MAPFLAG_STAIRS)
    {
        DrawLine(sx - AmLine8, sy - AmLine8 - AmLine4, sx + AmLine8 + AmLine16, sy + AmLine4, COLOR_BRIGHT);
        DrawLine(sx - AmLine16, sy - AmLine8, sx + AmLine16, sy + AmLine8, COLOR_BRIGHT);
        DrawLine(sx - AmLine16 - AmLine8, sy - AmLine4, sx + AmLine8, sy + AmLine8 + AmLine4, COLOR_BRIGHT);
        DrawLine(sx - AmLine32, sy, sx, sy + AmLine16, COLOR_BRIGHT);
    }

    do_vert = FALSE;
    do_horz = FALSE;

    switch (automap_type & MAPFLAG_TYPE)
    {
    case 1: // stand-alone column or other unpassable object
        x1 = sx - AmLine16;
        y1 = sy - AmLine16;
        x2 = x1 + AmLine32;
        y2 = sy - AmLine8;
        DrawLine(sx, y1, x1, y2, COLOR_DIM);
        DrawLine(sx, y1, x2, y2, COLOR_DIM);
        DrawLine(sx, sy, x1, y2, COLOR_DIM);
        DrawLine(sx, sy, x2, y2, COLOR_DIM);
        break;
    case 2:
    case 5:
        do_vert = TRUE;
        break;
    case 3:
    case 6:
        do_horz = TRUE;
        break;
    case 4:
        do_vert = TRUE;
        do_horz = TRUE;
        break;
        // no type 7...
    }

    if (do_vert) // right-facing obstacle
    {
        if (flags & MAPFLAG_VERTDOOR) // two wall segments with a door in the middle
        {
            x1 = sx - AmLine32;
            x2 = sx - AmLine16;
            y1 = sy - AmLine16;
            y2 = sy - AmLine8;

            DrawLine(sx, y1, sx - AmLine8, y1 + AmLine4, COLOR_DIM);
            DrawLine(x1, sy, x1 + AmLine8, sy - AmLine4, COLOR_DIM);
            DrawLine(x2, y1, x1, y2, COLOR_BRIGHT);
            DrawLine(x2, y1, sx, y2, COLOR_BRIGHT);
            DrawLine(x2, sy, x1, y2, COLOR_BRIGHT);
            DrawLine(x2, sy, sx, y2, COLOR_BRIGHT);
        }
        if (flags & MAPFLAG_VERTGRATE) // right-facing half-wall
        {
            DrawLine(sx - AmLine16, sy - AmLine8, sx - AmLine32, sy, COLOR_DIM);
            flags |= MAPFLAG_VERTARCH;
        }
        if (flags & MAPFLAG_VERTARCH) // window or passable column
        {
            x1 = sx - AmLine16;
            y1 = sy - AmLine16;
            x2 = x1 + AmLine32;
            y2 = sy - AmLine8;

            DrawLine(sx, y1, x1, y2, COLOR_DIM);
            DrawLine(sx, y1, x2, y2, COLOR_DIM);
            DrawLine(sx, sy, x1, y2, COLOR_DIM);
            DrawLine(sx, sy, x2, y2, COLOR_DIM);
        }
        if ((flags & (MAPFLAG_VERTDOOR | MAPFLAG_VERTGRATE | MAPFLAG_VERTARCH)) == 0)
            DrawLine(sx, sy - AmLine16, sx - AmLine32, sy, COLOR_DIM);
    }

    if (do_horz) // left-facing obstacle
    {
        if (flags & MAPFLAG_HORZDOOR)
        {
            x1 = sx + AmLine16;
            x2 = sx + AmLine32;
            y1 = sy - AmLine16;
            y2 = sy - AmLine8;

            DrawLine(sx, y1, sx + AmLine8, y1 + AmLine4, COLOR_DIM);
            DrawLine(x2, sy, x2 - AmLine8, sy - AmLine4, COLOR_DIM);
            DrawLine(x1, y1, sx, y2, COLOR_BRIGHT);
            DrawLine(x1, y1, x2, y2, COLOR_BRIGHT);
            DrawLine(x1, sy, sx, y2, COLOR_BRIGHT);
            DrawLine(x1, sy, x2, y2, COLOR_BRIGHT);
        }
        if (flags & MAPFLAG_HORZGRATE)
        {
            DrawLine(sx + AmLine16, sy - AmLine8, sx + AmLine32, sy, COLOR_DIM);
            flags |= MAPFLAG_HORZARCH;
        }
        if (flags & MAPFLAG_HORZARCH)
        {
            x1 = sx - AmLine16;
            y1 = sy - AmLine16;
            x2 = x1 + AmLine32;
            y2 = sy - AmLine8;

            DrawLine(sx, y1, x1, y2, COLOR_DIM);
            DrawLine(sx, y1, x2, y2, COLOR_DIM);
            DrawLine(sx, sy, x1, y2, COLOR_DIM);
            DrawLine(sx, sy, x2, y2, COLOR_DIM);
        }
        if ((flags & (MAPFLAG_HORZDOOR | MAPFLAG_HORZGRATE | MAPFLAG_HORZARCH)) == 0)
            DrawLine(sx, sy - AmLine16, sx + AmLine32, sy, COLOR_DIM);
    }

    // No caves tiles ><
}

// DrawAutomapPlr	0000000000452A62
// GetAutomapType	00000000004530FC
// DrawAutomap	00000000004532C9
// SetAutomapView	000000000045362C

// .text:00453A64
void AutomapZoomReset()
{
    RECALCULATE_AMLINE(AutoMapScale);
    AutoMapXOfs = 0;
    AutoMapYOfs = 0;
}
