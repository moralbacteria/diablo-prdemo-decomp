#ifndef __TOWN_H__
#define __TOWN_H__

#include <windows.h>

//
// enums
//

// Index into towner for all townsfolk
enum _talker_id
{
    TOWN_SMITH = 0x0,
    TOWN_TAVERN = 0x1,
    TOWN_DEADGUY = 0x2,
    TOWN_WITCH = 0x3,
    TOWN_PEGBOY = 0x4,
};

//
// structs
//

#pragma pack(push, 8)
struct TownerStruct
{
    int _tmode; // This is a guess since it's never used; maybe a switch between NData and WData?
    int _tx;    // Minitile location
    int _ty;    // Minitile location
    int _txoff;
    int _tyoff;
    int _txvel;
    int _tyvel;
    int _tdir;
    BYTE *_tAnimData; // Current animation direction, derived from _tNAnim
    int _tAnimDelay;  // Number of ticks to replay the same frame of animation
    int _tAnimCnt;
    int _tAnimLen;
    int _tAnimFrame;
    int _tAnimWidth;  // Width of graphics in pixels
    int _tAnimWidth2; // _tAnimWidth - 64 / 2; the offset from the current minitile to make the graphic look centerd?
    int field_3C;     // dead var never set???
    int _tbtcnt;      // If this is > 0 then the townsfolk is talking.
    BOOL _tSelFlag;   // TRUE if the towner can be talked to
    int _tVar1;       // When talking to a player, the index of the player
    int _tVar2;       // TOWN_DEADGUY: Set to 3, never read...
    int _tVar3;
    int _tVar4; // TOWN_TAVERN: controls which dialogue to say (demo only?)
    char _tName[32];
    BOOL freeGfx;     // If TRUE then free _tNData when leaving town
    BYTE *_tNAnim[8]; // Neutral/idle animations for each direction (indexed by `enum direction`); derived from _tNData
    int _tNFrames;    // number of frames in the idle animation
    BYTE *_tWAnim[8]; // dead var; this is a guess based on the graphics we have (smithw.cel from beta)
    int _tWFrames;    // dead var; this is a guess
    BYTE *_tNData;    // Neutral animation .CEL loaded into memory
    BYTE *_tWData;    // dead var; this is a guess
};
#pragma pack(pop)
// TODO assert sizeof(TownerStruct) == 0xCC

//
// functions
//

void FreeTownerGFX();
void TalkToTowner(int p, int t);
void CreateTown(int entry);
void ProcessTowners();
void T_DrawView(int StartX, int StartY);

#endif