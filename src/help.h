#ifndef __HELP_H__
#define __HELP_H__

#include <windows.h>

//
// variables
//

extern BOOL helpflag;
extern BOOL helpmodeflag;

//
// functions
//

void InitHelp();
void DisplayHelp();
void HelpScrollUp();
void HelpScrollDown();
void DrawHelpMode();

#endif
