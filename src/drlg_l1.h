// DRLG stands for Dynamic Random Level Generation
// See the Game Design section of the pitch doc
// https://www.graybeardgames.com/download/diablo_pitch.pdf

// "The heart of Diablo is the randomly created dungeon. A new dungeon level is
// generated each time a level is entered, using our Dynamic Random Level
// Generation (DRLG) System. Rooms, corridors, traps, treasures, monsters and
// stairways will be randomly placed, providing a new gaming experience every
// time Diablo is played. In addition to random halls and rooms, larger 'set
// piece' areas, like a maze or a cryptop complex, will be pre-designed and
// appear intact in the levels."

#ifndef __DRLG_L1_H__
#define __DRLG_L1_H__

#include <windows.h>

//
// functions
//

void CreateL1Dungeon(DWORD rseed, int entry);
void LoadL1Dungeon(const char *sFileName, int vx, int vy);
void LoadPreL1Dungeon(const char *sFileName, int vx, int vy);
void CreateL5Dungeon(DWORD rseed, int entry);

#endif
