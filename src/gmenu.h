#ifndef __GMENU_H__
#define __GMENU_H__

#include <windows.h>

//
// variables
//

extern BOOL title_allow_loadgame;
extern char gmenuflag;
extern char sgCurrentMenuIdx;

//
// functions
//

void gmenu_print_text(int x, int y, const char *pszStr);
void LoadGame(BOOL firstflag);
void gmenu_up();
void gmenu_down();
void gmenu_select_option();
void gmenu_left_mouse();
void FreeGMenu();
void SaveLevel();
void gmenu_init_menu();

#endif