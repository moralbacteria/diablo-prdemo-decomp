#include "monster.h"

#include <stdio.h>
#include <limits.h>

#include "debug.h"
#include "diablo.h"
#include "effects.h"
#include "engine.h"
#include "gendung.h"
#include "lighting.h"
#include "missiles.h"
#include "monstdat.h"
#include "monst2.h"
#include "objects.h"
#include "player.h"
#include "quests.h"

//
// typedef
//

// Performs monster tick logic. `i` is an index into `monster`
typedef void (*MonsterAiPrototype)(int i);

//
// Initialized variables (.data:004A2040)
//

int MWVel[24][3] = {
    {256, 512, 1024},
    {128, 256, 512},
    {85, 170, 341},
    {64, 128, 256},
    {51, 102, 204},
    {42, 85, 170},
    {36, 73, 146},
    {32, 64, 128},
    {28, 56, 113},
    {26, 51, 102},
    {23, 46, 93},
    {21, 42, 85},
    {19, 39, 78},
    {18, 36, 73},
    {17, 34, 68},
    {16, 32, 64},
    {15, 30, 60},
    {14, 28, 57},
    {13, 26, 54},
    {12, 25, 51},
    {12, 24, 48},
    {11, 23, 46},
    {11, 22, 44},
    {10, 21, 42}};
char animletter[7] = "nwahds"; // TODO is this a C-str (7 chars), or an array (6 chars)?
int left[8] = {7, 0, 1, 2, 3, 4, 5, 6};
int right[8] = {1, 2, 3, 4, 5, 6, 7, 0};
int opposite[8] = {4, 5, 6, 7, 0, 1, 2, 3};
// The X offset of an adjacent tile given a direction
int offset_x[8] = {1, 0, -1, -1, -1, 0, 1, 1};
// The Y offset of an adjacent tile given a direction
int offset_y[8] = {1, 1, 1, 0, -1, -1, -1, 0};
int rnd5[4] = {5, 10, 15, 20};   // unused
int rnd10[4] = {10, 15, 20, 30}; // unused
int rnd20[4] = {20, 30, 40, 50}; // unused
int rnd60[4] = {60, 70, 80, 90}; // unused
// List of function pointers for monster tick logic. Indexed by `MonsterStruct._mAi`.
// Each function is a unique monster behavior; this is what separates Skeletons from Zombies, etc.
// You can do fun things like make a Skeleton act like a Sneak. Several of the unique monsters take advantage of this fact to offer interesting challenges.
MonsterAiPrototype AiProc[] = {
    &MAI_Zombie,
    &MAI_Fat,
    &MAI_SkelSd,
    &MAI_SkelBow,
    &MAI_Scav,
    &MAI_Rhino,
    &MAI_GoatMc,
    &MAI_GoatBow,
    &MAI_Fallen,
    &MAI_Magma,
    &MAI_SkelKing,
    &MAI_Bat,
    &MAI_Garg,
    &MAI_Cleaver,
    &MAI_Succ,
    &MAI_Sneak,
    &MAI_Storm};

//
// Uninitialized variables (.data:004BE4E0)
//

// Number of elements in `Monsters`
int nummtypes;
// Number of elements in `monster`
int nummonsters;
// All the monsters in a given dungeon level.
MonsterStruct monster[MAXMONSTERS];
int monstactive[MAXMONSTERS];
// Monster "prototypes", one for each "class" of monster in the dungeon level.
// For example, if you have a skeleton, a fallen, and a carver then you have three entires here.
// Typically this describes things that are common to groups of monsters, e.g. the graphics.
CMonster Monsters[MAX_LVLMTYPES];
int monstkills[MAXMONSTERS];
char byte_4CAFC8[272]; // Only used by sub_00401117 (dead code)

//
// Code (.text:00401000)
//

// .text:00401000
// Applies the monster's TRN to it's graphics. `monst` is the monster type (index into `Monsters`)
void InitMonsterTRN(int monst, BOOL special)
{
    BYTE *f; // TRN buffer pointer
    int i;   // iterator; direction, from 0 to 8 (8 is the number of directions per animation)
    int j;   // iterator; animation type, from 0 to n (see below)
    int n;   // number of animations; either 5 (no S anim) or 6 (with S anim); corresponds to animletter

    f = Monsters[monst].trans_file;

    // Validate and entries in the TRN. Only really converts values of 255 to 0.
    // TODO why is this necessary?
    // I think this is hand-written ASM since the compiler doesn't touch ESI.
    __asm {
        mov     esi, f ; // array data to manipulate
        mov     ecx, 256 ; // loop counter (size of TRN file)
        xor     eax, eax ; // initialize AL and AH to 0
    top:
        mov     al, [esi]
        cmp     al, 255
        jnz     next
        mov     [esi], ah ; // If (*f == 255) { *f = 0; }
    next:
        inc     esi ; // ++f
        dec     ecx
        jnz     top // loop if ecx != 0
    }

    // Apply TRN to all graphics. A monster may or may not have a special attack so account for that here.
    if (special)
    {
        n = 6; // TODO magic number; 6 total animations: NWAHDS
    }
    else
    {
        n = 5; // TODO magic number; no special so 5 total aniomations: NWAHD
    }

    for (j = 0; j < n; j++)
    {
        for (i = 0; i < 8; i++) // TODO magic number, there are 8 directions per animation
        {
            CelApplyTrans(Monsters[monst].Anims[j].Data[i], Monsters[monst].trans_file, Monsters[monst].Anims[j].Frames);
        }
    }
}

// .text:00401117
// Dead code
__declspec(naked) int sub_401117(int mtype)
{
    // Since this is unremarkable I've just copy-pasted the disassembly from IDA
    __asm {
                push    ebp
                mov     ebp, esp
                sub     esp, 10h
                push    ebx
                push    esi
                push    edi
                mov     [ebp-10h], ecx
                mov     dword ptr [ebp-4], 0
                mov     dword ptr [ebp-0Ch], 0
                jmp     loc_401139
; ---------------------------------------------------------------------------

loc_401136:                             ; CODE XREF: .text:loc_401205\u2193j
                inc     dword ptr [ebp-0Ch]

loc_401139:                             ; CODE XREF: .text:00401131\u2191j
                mov     eax, currlevel
                cmp     [ebp-0Ch], eax
                jg      loc_40120A
                cmp     dword ptr [ebp-4], FALSE
                jnz     loc_40120A
                mov     dword ptr [ebp-8], 0
                jmp     loc_401160      ; x
; ---------------------------------------------------------------------------

loc_40115D:                             ; CODE XREF: .text:loc_401200\u2193j
                inc     dword ptr [ebp-8]

loc_401160:                             ; CODE XREF: .text:00401158\u2191j
                cmp     dword ptr [ebp-8], 10h
                jge     loc_401205
                mov     eax, [ebp-8]
                shl     eax, 4
                add     eax, offset byte_4CAFC8
                jz      loc_401205
                cmp     dword ptr [ebp-4], 0
                jnz     loc_401205
                mov     eax, [ebp-8]
                mov     ecx, [ebp-0Ch]
                shl     ecx, 4
                xor     edx, edx        ; $!
                mov     dl, byte_4CAFC8[eax+ecx]
                dec     edx
                cmp     edx, [ebp-10h]
                jle     loc_4011F9
                mov     eax, [ebp-8]
                mov     ecx, [ebp-0Ch]
                shl     ecx, 4
                xor     edx, edx
                mov     dl, byte_4CAFC8[eax+ecx]
                dec     edx
                mov     eax, edx
                ; // IDA wants to use
                ; //   lea     ecx, ds:0[edx*8]
                ; // However it will not compile and disassemble correctly.
                ; //
                ; // This is basically equivalent to
                ; //   shl edx,3 
                ; //   mov ecx,edx
                lea     ecx, [edx*8+0]
                sub     ecx, eax
                shl     ecx, 4
                mov     eax, monsterdata.GraphicType[ecx]
                push    eax
                mov     eax, [ebp-10h]
                mov     ecx, eax
                shl     eax, 3
                sub     eax, ecx
                shl     eax, 4
                mov     eax, monsterdata.GraphicType[eax]
                push    eax
                call    strcmp
                add     esp, 8
                test    eax, eax
                jnz     loc_4011F9
                mov     dword ptr [ebp-4], 1
                jmp     loc_401200
; ---------------------------------------------------------------------------

loc_4011F9:                             ; CODE XREF: .text:0040119B\u2191j
                                        ; .text:004011E7\u2191j
                mov     dword ptr [ebp-4], 0

loc_401200:                             ; CODE XREF: .text:004011F4\u2191j
                jmp     loc_40115D
; ---------------------------------------------------------------------------

loc_401205:                             ; CODE XREF: .text:00401164\u2191j
                                        ; .text:00401175\u2191j ...
                jmp     loc_401136
; ---------------------------------------------------------------------------

loc_40120A:                             ; CODE XREF: .text:00401141\u2191j
                                        ; .text:0040114B\u2191j
                mov     eax, [ebp-4]
                jmp     $+5
; ---------------------------------------------------------------------------

loc_401212:                             ; CODE XREF: .text:0040120D\u2191j
                pop     edi
                pop     esi
                pop     ebx
                leave
                retn
    }
}

// .text:00401217
// Determines which monsters to spawn based on the current dlvl.
// Side-effects:
// - `Monsters[i].mtype` will be overwritten with new data
// - `nummtypes` will be set to number of elements in `Monsters`
// NOTE: This function is split into 2 in Devilution.
void InitLevelMonsters_and_GetLevelMTypes()
{
    // TODO proper var order
    BYTE mamask;
    // "Monster image total"; the memory budget for monster graphics.
    int monstimgtot;
    int minl;
    int mtype;
    int i;
    int randType;
    int typelist[MAXMONSTERS]; // enum _monster_id; candidates for spawning in the dungeon
    int nt;                    // number of elements in `typelist`
    int maxl;

    nummtypes = 0;
    monstimgtot = 0;
    nt = 0;

    // Filter all monster types as candidates for dungeon generation.
    // Criteria:
    // 1. "Monster availability": Ignore unimplemented (always) and non-demo monsters (if demo_mode set)
    // 2. dungeon level is within a defined range
    if (demo_mode != FALSE)
    {
        mamask = MAT_ALWAYS;
    }
    else
    {
        mamask = MAT_ALWAYS | MAT_RETAIL;
    }

    for (i = 0; i < NUM_MTYPES; i++)
    {
        minl = 15 * monsterdata[i].mMinDLvl / 30 + 1;
        maxl = 15 * monsterdata[i].mMaxDLvl / 30 + 1;

        if (minl <= currlevel && currlevel <= maxl)
        {
            if (MonstAvailTbl[i] & mamask)
            {
                typelist[nt] = i;
                nt++;
            }
        }
    }

    // Hack to always have a `Monsters` entry for the Butcher if you're on dlvl 1.
    if (quests[Q_BUTCHER]._qlevel == currlevel)
    {
        Monsters[nummtypes].mtype = MT_CLEAVER;
        nummtypes++;
        monstimgtot += monsterdata[MT_CLEAVER].mImgSize;
    }

    // Choose random entries from the candidates in `typelist` to be placed into `Monsters` until the memory budget has been exhausted.
    // `Monsters` will then be used to generate monsters in the level.
    // Later monsters tend to be physically larger (think Thunder Demon or Mega Demon) and this is reflected in the file size of their animation.
    // As a result, the monster diversity in the level tends to decrease as you go deeper.
    // mImgSize doesn't reflect the actual CEL file size. That's recorded independently in `monsterdata` and is always wrong.
    // NOTE: devilution uses monstimgtot < 4000!
    while (nt > 0 && nummtypes < MAX_LVLMTYPES && monstimgtot < 3000)
    {
        randType = random_(nt);
        mtype = typelist[randType];
        if (monsterdata[mtype].mImgSize + monstimgtot <= 3000)
        {
            Monsters[nummtypes].mtype = mtype;
            nummtypes++;
            monstimgtot += monsterdata[mtype].mImgSize;
        }
        nt--;
        typelist[randType] = typelist[nt];
    }
}

// .text:00401414
// Loads the graphics for the monsters that will appear in the current dungeon level. This includes animations and missiles.
// Also, copies over other elements from `monsterdata` into `Monsters`.
// Pre-condition: Monsters[i].mtype is set, for i = 0..nummtypes
void InitMonsterGFX()
{
    BYTE *celBuf;
    char strBuff[256];
    int mtype;
    int i;
    int monst;
    int anim;

    // HACK: Debug mode, normal dungeon: only monsters are butcher and axe skeleton
    // Seems like a weird place for this to go (why not in InitLevelMonsters?)
    if (debug_mode && !setlevel)
    {
        nummtypes = 1;
        Monsters[0].mtype = MT_TSKELAX;
        if (currlevel == 1)
        {
            Monsters[nummtypes].mtype = Monsters[0].mtype;
            Monsters[0].mtype = MT_CLEAVER;
            nummtypes++;
        }
    }

    for (monst = 0; nummtypes > monst; monst++)
    {
        mtype = Monsters[monst].mtype;
        for (anim = 0; anim < 6; anim++)
        {
            // Read NWAHD animations and S conditional on has_special
            if (animletter[anim] != 's' || monsterdata[mtype].has_special != FALSE)
            {
                sprintf(strBuff, monsterdata[mtype].GraphicType, animletter[anim]);
                Monsters[monst].Anims[anim].CMem = LoadFileInMem(strBuff);

                // Read group header and store animation for each direction
                celBuf = Monsters[monst].Anims[anim].CMem;
                for (i = 0; i < 8; i++)
                {
                    // ((DWORD*)celBuf)[i] is the group offset
                    Monsters[monst].Anims[anim].Data[i] = celBuf + ((DWORD *)celBuf)[i];
                }
                Monsters[monst].Anims[anim].Frames = monsterdata[mtype].Frames[anim];
                Monsters[monst].Anims[anim].Rate = monsterdata[mtype].Rate[anim];
            }
        }

        Monsters[monst].width = monsterdata[mtype].width;
        Monsters[monst].width2 = (monsterdata[mtype].width - 64) >> 1;
        Monsters[monst].mMinHP = monsterdata[mtype].mMinHP;
        Monsters[monst].mMaxHP = monsterdata[mtype].mMaxHP;
        Monsters[monst].has_special = monsterdata[mtype].has_special;
        Monsters[monst].mAFNum = monsterdata[mtype].mAFNum;
        Monsters[monst].MData = &monsterdata[mtype];

        if (monsterdata[mtype].has_trans)
        {
            Monsters[monst].trans_file = LoadFileInMem(monsterdata[mtype].TransFile);
            InitMonsterTRN(monst, monsterdata[mtype].has_special);
            MemFreeDbg(Monsters[monst].trans_file);
        }

        if (mtype >= MT_NMAGMA && mtype <= MT_WMAGMA)
        {
            for (i = 0; i < 8; i++)
            {
                sprintf(strBuff, "Monsters\\Magma\\Magball%i.CEL", i + 1);
                pMagballCels[i] = LoadFileInMem(strBuff);
            }
        }

        if (mtype >= MT_STORM && mtype <= MT_MAEL)
        {
            pThinLghningCel = LoadFileInMem("Monsters\\Thin\\Lghning.CEL");
        }

        if (mtype >= MT_SUCCUBUS && mtype <= MT_SOLBRNR)
        {
            pFlareCel = LoadFileInMem("Monsters\\Succ\\Flare.CEL");
            pFlarexpCel = LoadFileInMem("Monsters\\Succ\\Flarexp.CEL");
        }
    }
}

// .text:004019B0
// Set all mVar* to 0
// TODO why
void ClearMVars(int i)
{
    monster[i]._mVar1 = 0;
    monster[i]._mVar2 = 0;
    monster[i]._mVar3 = 0;
    monster[i]._mVar4 = 0;
    monster[i]._mVar5 = 0;
    monster[i]._mVar6 = 0;
    monster[i]._mVar7 = 0;
    monster[i]._mVar8 = 0;
}

// .text:00401A8E
// Sets all relevant `monster[i]` fields based on `Monsters[mtype]`. `rd` is the initial facing direction (used for animations).
void InitMonster(int i, int rd, int mtype)
{
    CMonster *monst = &Monsters[mtype];

    monster[i]._mMTidx = mtype;
    monster[i].mName = monst->MData->mName;
    monster[i].MType = monst;
    monster[i].MData = monst->MData;
    monster[i]._mAnimData = monst->Anims[MA_STAND].Data[rd];
    monster[i]._mAnimDelay = monst->Anims[MA_STAND].Rate;
    monster[i]._mAnimCnt = random_(monster[i]._mAnimDelay - 1);
    monster[i]._mAnimLen = monst->Anims[MA_STAND].Frames;
    monster[i]._mAnimFrame = random_(monster[i]._mAnimLen - 1) + 1;
    monster[i]._mmaxhp = (monst->mMinHP + random_(monst->mMaxHP - monst->mMinHP + 1)) << 6;
    monster[i]._mhitpoints = monster[i]._mmaxhp;
    monster[i]._mAi = monst->MData->mAi;
    monster[i]._mint = monst->MData->mInt;
    monster[i]._mgoal = MGOAL_NORMAL;
    monster[i]._mgoalvar1 = 0;
    monster[i]._mgoalvar2 = 0;
    monster[i]._mDelFlag = FALSE;
    monster[i]._uniqtype = 0;
    monster[i]._msquelch = 0;
    monster[i].mLevel = monst->MData->mLevel;
    monster[i].mExp = monst->MData->mExp;
    monster[i].mHit = monst->MData->mHit;
    monster[i].mMinDamage = monst->MData->mMinDamage;
    monster[i].mMaxDamage = monst->MData->mMaxDamage;
    monster[i].mHit2 = monst->MData->mHit2;
    monster[i].mMinDamage2 = monst->MData->mMinDamage2;
    monster[i].mMaxDamage2 = monst->MData->mMaxDamage2;
    monster[i].mArmorClass = monst->MData->mArmorClass;
    monster[i].mMagicRes = monst->MData->mMagicRes;
    monster[i].leader = 0;
    monster[i].leaderflag = 0;
    monster[i]._mFlags = 1; // TODO magic number; also wtf

    if (monst->mtype >= MT_SNEAK && monst->mtype <= MT_ILLWEAV)
    {
        monster[i]._mFlags &= ~1; // TODO wtf
    }

    if (monst->mtype >= MT_WINGED && monst->mtype <= MT_DEATHW)
    {
        // Gargoyles start in their "special" state (stationary, stone-like)
        monster[i]._mAnimData = monst->Anims[MA_SPECIAL].Data[rd];
        monster[i]._mAnimFrame = 1;
        monster[i]._mFlags |= MFLAG_ALLOW_SPECIAL;
        monster[i]._mmode = MM_SATTACK;
    }
}

// .text:00401F77
// Zero out (almost) all `monster[i]` state.
void ClrAllMonsters()
{
    int i;

    for (i = 0; i < MAXMONSTERS; i++)
    {
        ClearMVars(i);
        monster[i]._mgoal = 0;
        monster[i]._mmode = MM_STAND;
        monster[i]._mVar1 = 0;
        monster[i]._mVar2 = 0;
        monster[i]._mx = 0;
        monster[i]._my = 0;
        monster[i]._moldx = 0;
        monster[i]._moldy = 0;
        monster[i]._mdir = random_(8);
        monster[i]._mxvel = 0;
        monster[i]._myvel = 0;
        monster[i]._mAnimData = NULL;
        monster[i]._mAnimDelay = 0;
        monster[i]._mAnimCnt = 0;
        monster[i]._mAnimLen = 0;
        monster[i]._mAnimFrame = 0;
        monster[i]._mFlags = 1; // TODO wtf
        monster[i]._mDelFlag = FALSE;
        monster[i]._menemy = random_(gbActivePlayers);
    }
}

// .text:00402198
// Query whether (xp, yp) is valid for monster placement. Criteria:
// - no monster AND
// - no player AND
// - not lit AND
// - not populated AND
// - not solid
BOOL MonstPlace(int xp, int yp)
{
    BOOL valid;
    valid = TRUE;

    if (dMonster[xp][yp] != 0)
    {
        valid = FALSE;
    }

    if (dPlayer[xp][yp] != 0)
    {
        valid = FALSE;
    }

    // NOTE: In devilution, this is BFLAG_VISIBLE
    if (dFlags[xp][yp] & BFLAG_LIT)
    {
        valid = FALSE;
    }

    if (dFlags[xp][yp] & BFLAG_POPULATED)
    {
        valid = FALSE;
    }

    if (SolidLoc(xp, yp))
    {
        valid = FALSE;
    }

    return valid;
}

// .text:0040226F
// Places the Butcher in the dungeon if the quest is active.
// NOTE: In Devilution, this is folded into PlaceUniqueMonst. Here, the Butcher isn't a unique monster.
void PlaceButcher(int i)
{
    int x, y;

    if (quests[Q_BUTCHER]._qlevel == currlevel && quests[Q_BUTCHER]._qactive == QUEST_ACTIVE)
    {
        for (y = 0; y < MAXDUNY; y++)
        {
            for (x = 0; x < MAXDUNX; x++)
            {
                // 367 is used by Devilution as well. TODO what does it look like?
                if (dPiece[x][y] == 367)
                {
                    // TODO are these indices correct?
                    dMonster[x + 1][y + 1] = i + 1;
                    // Butcher is always at Monsters[0]; see InitLevelMonsters()
                    // TODO: rd=2 is a magic number
                    InitMonster(i, /*rd=*/2, /*mtype=*/0);
                    monster[i]._mdir = 2;
                    monster[i]._mx = x + 1;
                    monster[i]._my = y + 1;
                    monster[i]._moldx = x + 1;
                    monster[i]._moldy = y + 1;
                    ++nummonsters;
                }
            }
        }
    }
}

// .text:004023BC
// Initialize a new monster of type `mtype` in slot `i` and place it in the dungeon at (`x`, `y`).
void PlaceMonster(int i, int mtype, int x, int y)
{
    int rd;

    dMonster[x][y] = i + 1;

    rd = random_(8);
    InitMonster(i, rd, mtype);
    monster[i]._mdir = rd;
    monster[i]._mx = x;
    monster[i]._my = y;
    monster[i]._moldx = x;
    monster[i]._moldy = y;
}

// .text:00402486
// Fills a room 75% full of monsters. Only works half of the time.
// `mid` is the next available index into `monsters`. It will be mutated as monsters are placed to refer to the next open slot.
// NOTE: Does nothing on dlvl 1.
// NOTE: This does not check nummonsters so could technically overshoot!
void PlaceMonsterCloset(int *mid)
{
    BOOL room_size[256]; // The same size as TransList
    int room_index;
    int x;
    int y;
    BOOL can_place;
    int i;
    int random_room;
    int mtype;

    for (i = 0; i < 256; i++)
    {
        room_size[i] = 0;
    }

    // Calculate the size of each room.
    for (x = 0; x < 112; x++) // TODO: Magic number
    {
        for (y = 0; y < 112; y++)
        {
            if (MonstPlace(x, y))
            {
                room_size[dTransVal[x][y]]++;
            }
        }
    }

    // If there is at least one room with enough space (15..65 tiles) then continue with placement.
    can_place = FALSE;
    for (i = 0; i < 256; i++) // TODO: Magic number
    {
        if (room_size[i] > 15 && room_size[i] < 65)
        {
            can_place = TRUE;
        }
    }

    // Override: don't place monster closets on dlvl 1. Also, dlvl 2-4 is only 50% chance.
    if (currlevel == 1 || random_(2))
    {
        can_place = FALSE;
    }

    if (can_place)
    {
        // Choose a random, appropriately sized room. This will be stored in `room_index`.
        room_index = 0;
        random_room = random_(10) + 1;
        while (random_room > 0)
        {
            if (room_size[room_index] > 15 && room_size[room_index] < 65)
            {
                --random_room;
            }
            if (random_room > 0)
            {
                ++room_index;
                if (room_index == 256)
                {
                    room_index = 0;
                }
            }
        }

        // The Butcher is always in `Monsters[0]`
        if (currlevel != quests[Q_BUTCHER]._qlevel)
        {
            mtype = random_(nummtypes);
        }
        else
        {
            mtype = random_(nummtypes - 1) + 1;
        }

        // Fill the chosen room 75% full with the random monster type.
        for (y = 0; y < 112; y++) // TODO magic number
        {
            for (x = 0; x < 112; x++)
            {
                if (room_index == dTransVal[x][y] && MonstPlace(x, y) && random_(100) < 75)
                {
                    PlaceMonster(*mid, mtype, x, y);
                    (*mid)++;
                }
            }
        }
    }
}

// .text:00402748
// DEAD CODE! A stub
void sub_402748(int)
{
    // This space intentionally left blank
}

// .text:0040275E
// Places all unique monsters given the current dlvl and which monster types were determined to spawn.
// Pre-conditions: currlevel is the level to generate; Monsters and nummtypes are set for the level to generate.
// `mid` is the next available index into `monsters`. It will be mutated as monsters are placed to refer to the next open slot.
void PlaceUniqueMonst(int *mid)
{
    int uniqindex;   // Index into UniqType
    int uniqtype;    // Index into Monsters
    int uniquetrans; // Light table entry where TRN is loaded
    BOOL found;      // True if a unique monster was found and should be generated
    int count;       // Random placement retry count; max is 1000
    int count2;      // Number of free tiles in a small box around a desired placement location
    int xp;
    int yp;
    int x;
    int y;
    char filestr[64];

    uniquetrans = 0;

    // Find a unique monster that (a) was determined to be a type in the level and (b) appears on the current dlvl.
    // The dlvl requirement is removed for debug mode.
    for (uniqindex = 0; UniqMonst[uniqindex].mType != MT_NULL; uniqindex++)
    {
        if (currlevel == UniqMonst[uniqindex].mlevel || debug_mode)
        {
            found = FALSE;
            for (uniqtype = 0; uniqtype < nummtypes && !found; uniqtype++)
            {
                if (Monsters[uniqtype].mtype == UniqMonst[uniqindex].mType)
                {
                    found = TRUE;
                }
                else
                {
                    found = FALSE;
                }
            }

            if (found)
            {
                // Monsters[uniqtype] from previous loop is the type of the unique monster to place
                --uniqtype;
                found = FALSE; // Not sure why this is necessary

                // Find a random location that has enough space for the unique and it's lackies.
                // There's a hard cap on number of retries to prevent this from getting stuck in an infinite loop.
                do
                {
                    // Try to place a group centered around a random coordinate
                    xp = random_(80) + 16; // TODO magic numbers
                    yp = random_(80) + 16;

                    // Look for enough space in a 8x8 square centered on the random tile
                    count2 = 0;
                    for (x = xp - 3; x < xp + 3; x++)
                    {
                        for (y = yp - 3; y < yp + 3; y++)
                        {
                            // TODO magic numbers
                            if (y >= 0 && y < 112 && x >= 0 && y < 112 && MonstPlace(x, y))
                            {
                                ++count2;
                            }
                        }
                    }
                } while (count2 < 9 || ++count < 1000 || !MonstPlace(xp, yp));

                // Put the unique monster into the level and set its attributes.
                PlaceMonster(*mid, uniqtype, xp, yp);
                monster[*mid]._uniqtype = uniqindex + 1;
                monster[*mid].mLevel = 2 * UniqMonst[uniqindex].mlevel;
                monster[*mid].mExp *= 2;
                monster[*mid].mName = UniqMonst[uniqindex].mName;
                monster[*mid]._mmaxhp = UniqMonst[uniqindex].maxhp << 6; // TODO magic numbers
                monster[*mid]._mAi = UniqMonst[uniqindex].mAi;
                monster[*mid]._mint = UniqMonst[uniqindex].mint;
                monster[*mid]._mhitpoints = monster[*mid]._mmaxhp;
                monster[*mid].mMinDamage = UniqMonst[uniqindex].mMinDamage;
                monster[*mid].mMaxDamage = UniqMonst[uniqindex].mMaxDamage;
                monster[*mid].mMinDamage2 = UniqMonst[uniqindex].mMinDamage;
                monster[*mid].mMaxDamage2 = UniqMonst[uniqindex].mMaxDamage;
                monster[*mid].mMagicRes = UniqMonst[uniqindex].mMagicRes;
                monster[*mid].mlid = AddLight(monster[*mid]._mx, monster[*mid]._my, 3);
                sprintf(filestr, "Monsters\\Monsters\\%s.TRN", UniqMonst[uniqindex].mTrnName);
                LoadFileWithMem(filestr, &pLightTbl[256 * (uniquetrans + 19)]); // TODO magic numbers
                monster[*mid]._uniqtrans = uniquetrans;
                ++uniquetrans;

                if (UniqMonst[uniqindex].mUnqAttr & UA_HIT)
                {
                    // This looks like a bug (why isn't mUnqVar2 used?) but probably isn't.
                    // mUnqVar2 is always 0 so it is probably unused.
                    monster[*mid].mHit = UniqMonst[uniqindex].mUnqVar1;
                    monster[*mid].mHit2 = UniqMonst[uniqindex].mUnqVar1;
                }

                if (UniqMonst[uniqindex].mUnqAttr & UA_AC)
                {
                    monster[*mid].mArmorClass = UniqMonst[uniqindex].mUnqVar1;
                }

                (*mid)++;

                // Optionally, generate a monster pack that accompanies the unique.
                if (UniqMonst[uniqindex].mUnqAttr & UA_GROUP)
                {
                    // The unique we just created is now (*mid - 1) after the increment a few lines previously.
                    // packsize is overridden by PlaceGroup so this initial value is never read.
                    monster[*mid - 1].packsize = random_(3) + 6;
                    PlaceGroup(mid, uniqtype, /*num=*/8, /*has_leader=*/TRUE, *mid - 1);
                }
            }
        }
    }
}

// .text:0000000000402D34
// Generate a group of monsters.
// `mid` is the next available slot in `monsters`. It is mutated as monsters are placed so should always point to the next slot.
// `mtype` is the type of monster to spawn (index into `Monsters`).
// `num` is how many to place. In reality, anwhere from 0 to num can be placed based on whether the function finds enough free space.
// `has_leader`: if `FALSE` then the location of the group is random. Otherwise the monsters are placed around the leader.
// `leader` is the index into `monsters` of the monster acting as the group leader. Ignored if `has_leader == FALSE`.
void PlaceGroup(int *mid, int mtype, int num, BOOL has_leader, int leader)
{
    int placed; // Number of monsters placed so far
    int try1;   // How many tries to place the entire group. Only 10 tries.
    int try2;   // How many tries to place the current monster within a group. Only 10 tries.
    int j;      // Counts number of mosnters placed within a group.
    int xp, yp; // Initial "seed" location. Either adjacent to the leader or random.
    int x1, y1; // The location of the next monster to place. Wanders with in a small box centered around (xp, yp)
    int offset; // Random initial direction to place a monster around a leader.

    placed = 0;
    try1 = 0;
    do
    {
        // It could be the case that only half the group could be placed (e.g. chosen space ended up being too small).
        // If so, undo any work that was done before trying again.
        while (placed)
        {
            (*mid)--;
            placed--;
            dMonster[monster[*mid]._mx][monster[*mid]._my] = 0;
        }

        // Determine a suitable location for the group.
        // If it has a leader then place the monsters randomly around it.
        // Otherwise, find a random dungeon location.
        if (has_leader)
        {
            offset = random_(8); // TODO magic numbers
            x1 = xp = monster[leader]._mx + offset_x[offset];
            y1 = yp = monster[leader]._my + offset_y[offset];
        }
        else
        {
            do
            {
                x1 = xp = random_(80) + 16; // TODO magic numbers
                y1 = yp = random_(80) + 16;
            } while (!MonstPlace(xp, yp));
        }

        // Ensure we don't exceed nummonsters during placement.
        if (*mid + num > nummonsters)
        {
            num = nummonsters - *mid;
        }

        j = 0;
        try2 = 0;
        // Devilution difference: try2 < 10 (not try2 < 100)!
        while (j < num && try2 < 10)
        {
            // The tile must be able to hold a monster, in the same room, and not have wandered too far from the initial location.
            if (MonstPlace(xp, yp) && dTransVal[xp][yp] == dTransVal[x1][y1] || has_leader && abs(xp - x1) < 4 && abs(yp - y1) < 4)
            {
                // Monster successfully placed!
                PlaceMonster(*mid, mtype, xp, yp);
                // Lackies inherit the behavior of the leader. They also have 2x the HP.
                if (has_leader)
                {
                    monster[*mid].leader = leader;
                    monster[*mid].leaderflag = 1;
                    monster[*mid]._mmaxhp *= 2;
                    monster[*mid]._mhitpoints = monster[*mid]._mmaxhp;
                    monster[*mid]._mAi = monster[leader]._mAi;
                    monster[*mid]._mint = monster[leader]._mint;
                }
                ++(*mid);
                ++placed;
                ++j;
            }
            else
            {
                // Space not suitable, try again.
                ++try2;
            }

            // Wander to an adjacent tile in order to try to place the next monster.
            xp += offset_x[random_(8)];
            yp += offset_x[random_(8)];
        }
    } while (placed < num || ++try1 < 10);

    // Correct leader packsize based on the number actually placed. This could be anywhere from 0 to num.
    if (has_leader)
    {
        monster[leader].packsize = placed;
    }
}

// .text:00403095
// Populate the dungeon with groups of standard and unique monsters.
// Pre: `nummonsters` is set. Quests are initialized.
// NOTE: Devilution doesn't have this function. This is rewritten and folded into InitMonsters
void StartPlacingMonsters()
{
    int mid;   // Monster ID. Index into `monster`. Between 0..nummonsters.
    int na;    // Random group size
    int mtype; // Random monster type (e.g. Butcher, Fallen, etc). index into `Monsters`. Between 0..nummtypes

    // Leave room for the Butcher to be added later in the functon.
    if (currlevel == quests[Q_BUTCHER]._qlevel)
    {
        --nummonsters;
    }

    mid = 0;

    // Old alpha DRLG only: fill at most one room with monsters.
    if (leveltype == DTYPE_OLD_CATHEDRAL)
    {
        PlaceMonsterCloset(&mid);
    }

    // Place all unique monsters.
    PlaceUniqueMonst(&mid);

    // Fill the remaining slots with groups of enemies.
    while (mid < nummonsters)
    {
        // Butcher is the first element in `Monsters` so skip it.
        if (currlevel != quests[Q_BUTCHER]._qlevel)
        {
            mtype = random_(nummtypes);
        }
        else
        {
            mtype = random_(nummtypes - 1) + 1;
        }

        // 50% chance to spawn a group of 3-6, 50% chance to spawn a solo monster
        if (random_(2))
        {
            na = random_(3) + 3;
        }
        else
        {
            na = 1;
        }

        PlaceGroup(&mid, mtype, na, FALSE, /*leader=*/0);
    }

    // Finally, add the Butcher in the remaining slot. This has a dlvl check hidden in it.
    PlaceButcher(na);
}

// .text:00403177
// Places monsters in the dungeon.
void InitMonsters()
{
    int na; // number of non-solid tiles
    int s, t;

    ClrAllMonsters();

    // Calculate number of non-solid tiles
    // TODO magic numbers
    na = 0;
    for (s = 16; s < 96; s++)
    {
        for (t = 16; t < 96; t++)
        {
            if (!SolidLoc(s, t))
            {
                na++;
            }
        }
    }

    // Monster density is a fraction of non-solid tiles (capped at engine monster limit)
    nummonsters = na / 30;
    if (nummonsters > MAXMONSTERS)
    {
        nummonsters = MAXMONSTERS;
    }

    // wtf...
    for (s = 0; s < nummonsters; ++s)
    {
        monstactive[s] = s;
    }
    for (s = nummonsters; s < MAXMONSTERS; ++s)
    {
        monstactive[s] = s;
    }

    // LevelViewX and LevelViewY are set by the DRLG to the location of a set of stairs.
    // Since MonstPlace() returns FALSE for BFLAG_LIT, this prevents monsters from spawning near the stair case.
    // NOTE: Devilution uses (trigs[i]._tx, trigs[i]._ty) instead of (LvlViewX, LvlViewY)
    DoVision(LvlViewX, LvlViewY, 15);
    StartPlacingMonsters();
    DoUnVision(LvlViewX, LvlViewY, 15);
}

// SetMapMonsters    000000000040329B
// TODO signature
__declspec(naked) void SetMapMonsters()
{
    // TODO function body
    __asm {
        ret 0
    }
}

// .text:004035FB
void DeleteMonster(int i)
{
    int temp;

    nummonsters--;
    temp = monstactive[nummonsters];
    monstactive[nummonsters] = monstactive[i];
    monstactive[i] = temp;
}

// AddMonster    000000000040364A
// TODO signature
__declspec(naked) void AddMonster()
{
    __asm {
                push    ebp
                mov     ebp, esp
                sub     esp, 0Ch
                push    ebx
                push    esi
                push    edi
                mov     [ebp-0Ch], edx
                mov     [ebp-8], ecx
                cmp     nummonsters, MAXMONSTERS
                jge     loc_40372D
                mov     eax, nummonsters
                mov     eax, monstactive[eax*4]
                mov     [ebp-4], eax
                inc     nummonsters
                mov     eax, [ebp-4]
                inc     eax
                mov     ecx, [ebp-0Ch]
                mov     edx, [ebp-8]
                mov     ebx, edx
                shl     edx, 3
                sub     edx, ebx
                shl     edx, 6
                mov     dMonster[edx+ecx*4], eax
                mov     eax, [ebp+0Ch]
                push    eax
                mov     edx, [ebp+8]
                mov     ecx, [ebp-4]
                call    InitMonster
                mov     eax, [ebp+8]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._mdir[ecx*4], eax
                mov     eax, [ebp-8]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._mx[ecx*4], eax
                mov     eax, [ebp-0Ch]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._my[ecx*4], eax
                mov     eax, [ebp-8]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._moldx[ecx*4], eax
                mov     eax, [ebp-0Ch]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._moldy[ecx*4], eax
                mov     eax, [ebp-4]
                jmp     loc_403737
; ---------------------------------------------------------------------------
                jmp     loc_403737
; ---------------------------------------------------------------------------

loc_40372D:                             ; CODE XREF: .text:00403663↑j
                mov     eax, 0FFFFFFFFh
                jmp     $+5
; ---------------------------------------------------------------------------

loc_403737:                             ; CODE XREF: .text:00403723↑j
                                        ; .text:00403728↑j ...
                pop     edi
                pop     esi
                pop     ebx
                leave
                retn    8
    }
}

// NewMonsterAnim    000000000040373E
__declspec(naked) void NewMonsterAnim()
{
    __asm {
                push    ebp
                mov     ebp, esp
                sub     esp, 8
                push    ebx
                push    esi
                push    edi
                mov     [ebp-8], edx
                mov     [ebp-4], ecx
                mov     eax, [ebp+8]
                mov     ecx, [ebp-8]
                mov     eax, [ecx+eax*4+4]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._mAnimData[ecx*4], eax
                mov     eax, [ebp-8]
                mov     eax, [eax+24h]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._mAnimLen[ecx*4], eax
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 2
                lea     eax, [eax+eax*2]
                lea     eax, [ecx+eax*4]
                mov     monster._mAnimFrame[eax*4], 1
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 2
                lea     eax, [eax+eax*2]
                lea     eax, [ecx+eax*4]
                mov     monster._mAnimCnt[eax*4], 0
                mov     eax, [ebp-8]
                mov     eax, [eax+28h]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._mAnimDelay[ecx*4], eax
                mov     eax, [ebp+8]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._mdir[ecx*4], eax
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 2
                lea     eax, [eax+eax*2]
                lea     eax, [ecx+eax*4]
                and     monster._mFlags[eax*4], 0FFFFFFF9h
                jmp     $+5
; ---------------------------------------------------------------------------

loc_403807:                             ; CODE XREF: .text:00403802↑j
                pop     edi
                pop     esi
                pop     ebx
                leave
                retn    4
    }
}

// M_Enemy    000000000040380E

// M_CheckEFlag    0000000000403973
void M_CheckEFlag(int i)
{
    int j;
    int f;
    WORD *m;
    int x;
    int y;

    x = monster[i]._mx - 1;
    y = monster[i]._my + 1;
    f = 0;
    m = dpiece_defs_map_2[x][y].mt;
    // j < 10 beacuse there are 10 microtiles per tile in dungeon?
    // j = 2 to ignore the first two floor microtiles?
    for (j = 2; j < 10; j++)
    {
        f |= m[j];
    }

    if (f | dSpecial[x][y])
        monster[i]._meflag = TRUE;
    else
    {
        monster[i]._meflag = FALSE;
    }
}

// M_StartStand    0000000000403A64
// M_StartDelay    0000000000403BAB

// M_StartSpStand    0000000000403BFF
// TODO signature
__declspec(naked) void M_StartSpStand()
{
    __asm {
                push    ebp
                mov     ebp, esp
                sub     esp, 8
                push    ebx
                push    esi
                push    edi
                mov     [ebp-8], edx
                mov     [ebp-4], ecx
                mov     eax, [ebp-8]
                push    eax             ; md
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 2
                lea     eax, [eax+eax*2]
                lea     eax, [ecx+eax*4]
                mov     edx, monster.MType[eax*4]
                add     edx, 0E0h ; 'à' ; anim
                mov     ecx, [ebp-4]    ; i
                call    NewMonsterAnim
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 2
                lea     eax, [eax+eax*2]
                lea     eax, [ecx+eax*4]
                mov     monster._mmode[eax*4], 0Bh
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 2
                lea     eax, [eax+eax*2]
                lea     eax, [ecx+eax*4]
                mov     monster._mxoff[eax*4], 0
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 2
                lea     eax, [eax+eax*2]
                lea     eax, [ecx+eax*4]
                mov     monster._myoff[eax*4], 0
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 2
                lea     eax, [eax+eax*2]
                lea     eax, [ecx+eax*4]
                mov     eax, monster._mx[eax*4]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._moldx[ecx*4], eax
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 2
                lea     eax, [eax+eax*2]
                lea     eax, [ecx+eax*4]
                mov     eax, monster._my[eax*4]
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._moldy[ecx*4], eax
                mov     eax, [ebp-8]    ; $!
                mov     ecx, [ebp-4]
                mov     edx, ecx
                shl     ecx, 2
                lea     ecx, [ecx+ecx*2]
                lea     ecx, [edx+ecx*4]
                mov     monster._mdir[ecx*4], eax
                mov     ecx, [ebp-4]
                call    M_CheckEFlag
                jmp     $+5
; ---------------------------------------------------------------------------

loc_403CF9:                             ; CODE XREF: .text:00403CF4↑j
                pop     edi
                pop     esi
                pop     ebx
                leave
                retn
    }
}

// M_StartWalk    0000000000403CFE
// M_StartWalk2    0000000000403ED9
// M_StartWalk3    00000000004041E3
// M_StartAttack    0000000000404529
// M_StartRAttack    000000000040465F
// M_StartRSpAttack    00000000004047B0
// M_StartSpAttack    0000000000404904
// M_StartEat    0000000000404A3D
// M_ClearSquares    0000000000404B33
// M_StartHit    0000000000404DCC
// M_StartKill    0000000000404F8D
void M_StartKill(int i, int pnum)
{
    // TODO
}

// M_StartFadein    0000000000405179
// M_StartFadeout    000000000040528E
// M_StartHeal    00000000004053D3
// M_ChangeLightOffset    0000000000405468
// M_DoWalk    0000000000405565
BOOL M_DoWalk(int i)
{
    // TODO
    return FALSE;
}
// M_DoWalk2    0000000000405821
BOOL M_DoWalk2(int i)
{
    // TODO
    return FALSE;
}
// M_DoWalk3    0000000000405A4A
BOOL M_DoWalk3(int i)
{
    // TODO
    return FALSE;
}
// M_TryH2HHit    0000000000405D7F
// M_DoAttack    000000000040641F
BOOL M_DoAttack(int i)
{
    // TODO
    return FALSE;
}
// M_DoRAttack    00000000004066ED
BOOL M_DoRAttack(int i)
{
    // TODO
    return FALSE;
}
// M_DoRSpAttack    0000000000406859
BOOL M_DoRSpAttack(int i)
{
    // TODO
    return FALSE;
}
// M_DoSAttack    00000000004069CB
BOOL M_DoSAttack(int i)
{
    // TODO
    return FALSE;
}
// M_DoFadein    0000000000406ADF
BOOL M_DoFadein(int i)
{
    // TODO
    return FALSE;
}
// M_DoFadeout    0000000000406B53
BOOL M_DoFadeout(int i)
{
    // TODO
    return FALSE;
}
// M_DoHeal    0000000000406BC9
BOOL M_DoHeal(int i)
{
    // TODO
    return FALSE;
}
// M_Teleport    0000000000406C72
// M_DoGotHit    0000000000406E63
BOOL M_DoGotHit(int i)
{
    // TODO
    return FALSE;
}
// M_DoDeath    0000000000406EDD
BOOL M_DoDeath(int i)
{
    // TODO
    return FALSE;
}
// M_DoSpStand    0000000000407181
BOOL M_DoSpStand(int i)
{
    // TODO
    return FALSE;
}
// M_DoDelay    0000000000407237
BOOL M_DoDelay(int i)
{
    // TODO
    return FALSE;
}

// .text:0040736F
BOOL M_DoStone(int i)
{
    // Don't update the monster, which makes them act like they're frozen.
    //
    // Remove the enemy if they run out of health. This is in contrast to other
    // ways of death which create a dead body.
    //
    // If this was caused by casting stone curse then MIS_STONE will handle
    // creating the shatter effect.
    if (monster[i]._mhitpoints == 0)
    {
        dMonster[monster[i]._mx][monster[i]._my] = 0;
        monster[i]._mDelFlag = TRUE;
    }
    return FALSE;
}

// M_WalkDir    00000000004073FB
// GroupUnity    00000000004075E4
void GroupUnity(int i)
{
    // TODO
}
// M_CallWalk    0000000000407A10
// M_DumbWalk    0000000000407C1A
// M_RoundWalk    0000000000407C59
// M_Face    0000000000407D84    dead code

// MAI_Zombie    0000000000407E52
void MAI_Zombie(int i)
{
    // TODO
}

// MAI_SkelSd    000000000040805B
void MAI_SkelSd(int i)
{
    // TODO
}
// MAI_Bat    0000000000408257
void MAI_Bat(int i)
{
    // TODO
}
// MAI_SkelBow    000000000040869A
void MAI_SkelBow(int i)
{
    // TODO
}
// MAI_Fat    00000000004088CA
void MAI_Fat(int i)
{
    // TODO
}
// MAI_Sneak    0000000000408B9C
void MAI_Sneak(int i)
{
    // TODO
}
// MAI_Fallen    0000000000408F04
void MAI_Fallen(int i)
{
    // TODO
}
// MAI_Cleaver    000000000040928B
void MAI_Cleaver(int i)
{
    // TODO
}

// MAI_Round    00000000004093C0
void MAI_Round(int i, BOOL special)
{
    // TODO
}

// MAI_GoatMc    0000000000409809
void MAI_GoatMc(int i)
{
    MAI_Round(i, TRUE);
}

// MAI_Ranged    000000000040982C
static void MAI_Ranged(int i, int missile_type)
{
    // tODO
}

// .text:00409A55
void MAI_GoatBow(int i)
{
    MAI_Ranged(i, MIS_ARROW);
}

// .text:00409A75
void MAI_Succ(int i)
{
    MAI_Ranged(i, MIS_FLARE);
}

// MAI_Scav    0000000000409A98
void MAI_Scav(int i)
{
    // TODO
}
// MAI_Garg    0000000000409EA9
void MAI_Garg(int i)
{
    // TODO
}

// MAI_RoundRanged    000000000040A04A
void MAI_RoundRanged(int i, int missile_type)
{
    // TODO
}

// .text:0040A5B6
void MAI_Magma(int i)
{
    MAI_RoundRanged(i, MIS_MAGMABALL);
}

// .text:0040A5D9
void MAI_Storm(int i)
{
    MAI_RoundRanged(i, MIS_LIGHTCTRL2);
}

// MAI_SkelKing    000000000040A5FC
void MAI_SkelKing(int i)
{
    // TODO
}

// MAI_Rhino    000000000040AB24
void MAI_Rhino(int i)
{
    // TODO
}

// .text:0040B08A
void ProcessMonsters()
{
    BOOL raflag;
    int i;
    int mx;
    int my;
    int mi;

    for (i = 0; i < nummonsters; i++)
    {
        mi = monstactive[i];
        raflag = 0;

        // Regenerate monster health based on its level
        // As much as this decomp looks weird I think it's true to the assembly:
        //   mov     eax, monster._mhitpoints[eax*4]
        //   and     eax, 0FFFFFFC0h
        //   xor     ecx, ecx
        //   and     ecx, 0FFFFFFC0h
        //   cmp     eax, ecx
        //   jle     loc_40B144
        if (monster[mi]._mmaxhp > monster[mi]._mhitpoints && (monster[mi]._mhitpoints & 0xFFFFFFC0) > (0 & 0xFFFFFFC0))
        {
            monster[mi]._mhitpoints += monster[mi].mLevel;
        }

        mx = monster[mi]._mx;
        my = monster[mi]._my;

        // Devilution checks BFLAG_VISIBLE instead...
        if (dFlags[mx][my] & BFLAG_LIT && monster[mi]._msquelch == 0 && monster[mi].MType->mtype == MT_CLEAVER)
        {
            PlaySFX();
        }

        if (dFlags[mx][my] & BFLAG_LIT)
        {
            monster[mi]._msquelch = UCHAR_MAX;
            monster[mi]._lastx = plr[monster[mi]._menemy]._pfutx;
            monster[mi]._lasty = plr[monster[mi]._menemy]._pfuty;
        }
        else if (monster[mi]._msquelch != 0)
        {
            monster[mi]._msquelch--;
        }

        do
        {
            AiProc[monster[mi]._mAi](mi);
            switch (monster[mi]._mmode)
            {
            case MM_STAND:
                monster[mi]._mVar2++;
                raflag = FALSE;
                break;
            case MM_WALK:
                raflag = M_DoWalk(mi);
                break;
            case MM_WALK2:
                raflag = M_DoWalk2(mi);
                break;
            case MM_WALK3:
                raflag = M_DoWalk3(mi);
                break;
            case MM_ATTACK:
                raflag = M_DoAttack(mi);
                break;
            case MM_RATTACK:
                raflag = M_DoRAttack(mi);
                break;
            case MM_GOTHIT:
                raflag = M_DoGotHit(mi);
                break;
            case MM_DEATH:
                raflag = M_DoDeath(mi);
                break;
            case MM_SATTACK:
                raflag = M_DoSAttack(mi);
                break;
            case MM_FADEIN:
                raflag = M_DoFadein(mi);
                break;
            case MM_FADEOUT:
                raflag = M_DoFadeout(mi);
                break;
            case MM_SPSTAND:
                raflag = M_DoSpStand(mi);
                break;
            case MM_RSPATTACK:
                raflag = M_DoRSpAttack(mi);
                break;
            case MM_DELAY:
                raflag = M_DoDelay(mi);
                break;
            case MM_CHARGE:
                raflag = FALSE;
                break;
            case MM_STONE:
                raflag = M_DoStone(mi);
                break;
            case MM_HEAL:
                raflag = M_DoHeal(mi);
                break;
            }

            if (raflag)
            {
                GroupUnity(mi);
            }
        } while (raflag);

        if (monster[mi]._mmode != MM_STONE)
        {
            monster[mi]._mAnimCnt++;
            if (monster[mi]._mAnimDelay <= monster[mi]._mAnimCnt)
            {
                monster[mi]._mAnimCnt = 0;
                if (monster[mi]._mFlags & MFLAG_LOCK_ANIMATION)
                {
                    monster[mi]._mAnimFrame--;
                    if (monster[mi]._mAnimFrame == 0)
                    {
                        monster[mi]._mAnimFrame = monster[mi]._mAnimLen;
                    }
                }
                else if (!(monster[mi]._mFlags & MFLAG_ALLOW_SPECIAL))
                {
                    monster[mi]._mAnimFrame++;
                    if (monster[mi]._mAnimLen < monster[mi]._mAnimFrame)
                    {
                        monster[mi]._mAnimFrame = 1;
                    }
                }
            }
        }
    }

    i = 0;
    while (i < nummonsters)
    {
        mi = monstactive[i];
        if (monster->_mDelFlag)
        {
            DeleteMonster(i);
            i = 0;
        }
        else
        {
            i++;
        }
    }
}

// FreeMonsters    000000000040B67A
void FreeMonsters()
{
    // TODO
}

// DirOK    000000000040B876
// LineClear    000000000040BDB5
// PosOkMissile    000000000040BDE9
// CheckNoSolid    000000000040BE53
// CheckNoSolid_2    000000000040BE9D
// LineClearF    000000000040BEE7

// .text:0040BFA3
// Finish initializing `monster[i]` given data that was stored in the save file.
// Since the struct is memcpy'd into the save file, this is mostly fixing up
// pointers.
void SyncMonsterAnim(int i)
{
    monster[i].MType = &Monsters[monster[i]._mMTidx];
    monster[i].MData = Monsters[monster[i]._mMTidx].MData;
    if (monster[i]._uniqtype != 0)
    {
        monster[i].mName = UniqMonst[monster[i]._uniqtype - 1].mName;
    }
    else
    {
        monster[i].mName = monster[i].MData->mName;
    }
    int _mdir = monster[i]._mdir;

    switch (monster[i]._mmode)
    {
    case MM_STAND:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_STAND].Data[_mdir];
        break;
    case MM_WALK:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_WALK].Data[_mdir];
        break;
    case MM_WALK2:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_WALK].Data[_mdir];
        break;
    case MM_WALK3:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_WALK].Data[_mdir];
        break;
    case MM_ATTACK:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_ATTACK].Data[_mdir];
        break;
    case MM_RATTACK:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_ATTACK].Data[_mdir];
        break;
    case MM_GOTHIT:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_GOTHIT].Data[_mdir];
        break;
    case MM_DEATH:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_DEATH].Data[_mdir];
        break;
    case MM_SATTACK:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_SPECIAL].Data[_mdir];
        break;
    case MM_FADEIN:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_SPECIAL].Data[_mdir];
        break;
    case MM_FADEOUT:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_SPECIAL].Data[_mdir];
        break;
    case MM_SPSTAND:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_SPECIAL].Data[_mdir];
        break;
    case MM_RSPATTACK:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_SPECIAL].Data[_mdir];
        break;
    case MM_DELAY:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_STAND].Data[_mdir];
        break;
    case MM_HEAL:
        monster[i]._mAnimData = monster[i].MType->Anims[MA_SPECIAL].Data[_mdir];
        break;
    case MM_CHARGE:
    case MM_STONE:
    default:
        // BUG: _mAnimData not set for stone cursed enemy (or charging enemy?)
        break;
    }
}

// M_FallenFear    000000000040C495
// PrintMonstHistory    000000000040C67F
// MissToMonst    000000000040C7B0

// PosOkMonst    000000000040CA2D
__declspec(naked) void PosOkMonst()
{
    // TODO what is signature?
    // Currently only called from asm in SpawnSkeleton
    __asm {
                push    ebp
                mov     ebp, esp
                sub     esp, 10h
                push    ebx
                push    esi
                push    edi
                mov     [ebp-10h], edx
                mov     [ebp-0Ch], ecx
                mov     dword ptr [ebp-8], 1
                mov     edx, [ebp-10h]
                mov     ecx, [ebp-0Ch]
                call    SolidLoc
                test    eax, eax
                jnz     loc_40CAA0
                mov     eax, [ebp-0Ch]
                mov     ecx, eax
                shl     eax, 3
                sub     eax, ecx
                shl     eax, 4
                mov     ecx, [ebp-10h]
                movsx   eax, dPlayer[eax+ecx]
                test    eax, eax
                jnz     loc_40CAA0
                mov     eax, [ebp-0Ch]
                mov     ecx, eax
                shl     eax, 3
                sub     eax, ecx
                shl     eax, 6
                mov     ecx, [ebp-10h]
                cmp     dMonster[eax+ecx*4], 0
                jnz     loc_40CAA0
                mov     dword ptr [ebp-8], 1
                jmp     loc_40CAA7
; ---------------------------------------------------------------------------

loc_40CAA0:                             ; CODE XREF: .text:0040CA50↑j
                                        ; .text:0040CA70↑j ...
                mov     dword ptr [ebp-8], 0

loc_40CAA7:                             ; CODE XREF: .text:0040CA9B↑j
                cmp     dword ptr [ebp-8], 0
                jz      loc_40CB52
                mov     eax, [ebp-0Ch]
                mov     ecx, eax
                shl     eax, 3
                sub     eax, ecx
                shl     eax, 4
                mov     ecx, [ebp-10h]
                movsx   eax, dObject[eax+ecx]
                test    eax, eax
                jz      loc_40CB52
                mov     eax, [ebp-0Ch]
                mov     ecx, eax
                shl     eax, 3
                sub     eax, ecx
                shl     eax, 4
                mov     ecx, [ebp-10h]
                movsx   eax, dObject[eax+ecx]
                test    eax, eax
                jle     loc_40CB12
                mov     eax, [ebp-0Ch]
                mov     ecx, eax
                shl     eax, 3
                sub     eax, ecx
                shl     eax, 4
                mov     ecx, [ebp-10h]
                movsx   eax, dObject[eax+ecx]
                dec     eax
                mov     [ebp-4], eax
                jmp     loc_40CB30
; ---------------------------------------------------------------------------

loc_40CB12:                             ; CODE XREF: .text:0040CAEB↑j
                mov     eax, [ebp-0Ch]
                mov     ecx, eax
                shl     eax, 3
                sub     eax, ecx
                shl     eax, 4
                mov     ecx, [ebp-10h]
                movsx   eax, dObject[eax+ecx]
                inc     eax
                neg     eax
                mov     [ebp-4], eax

loc_40CB30:                             ; CODE XREF: .text:0040CB0D↑j
                mov     eax, [ebp-4]
                mov     ecx, eax
                shl     eax, 3
                sub     eax, ecx
                lea     eax, [ecx+eax*4]
                cmp     object._oSolidFlag[eax*4], 0
                jz      loc_40CB52
                mov     dword ptr [ebp-8], 0

loc_40CB52:                             ; CODE XREF: .text:0040CAAB↑j
                                        ; .text:0040CACB↑j ...
                mov     eax, [ebp-8]
                jmp     $+5
; ---------------------------------------------------------------------------

loc_40CB5A:                             ; CODE XREF: .text:0040CB55↑j
                pop     edi
                pop     esi
                pop     ebx
                leave
                retn
    }
}

// IsSkel    000000000040CB5F
// TODO signature
__declspec(naked) void IsSkel()
{
    __asm {
                push    ebp
                mov     ebp, esp
                sub     esp, 4
                push    ebx
                push    esi
                push    edi
                mov     [ebp-4], ecx
                cmp     dword ptr [ebp-4], 8
                jl      loc_40CB7F
                cmp     dword ptr [ebp-4], 0Bh
                jle     loc_40CBA7

loc_40CB7F:                             ; CODE XREF: .text:0040CB6F↑j
                cmp     dword ptr [ebp-4], 14h
                jl      loc_40CB93
                cmp     dword ptr [ebp-4], 17h
                jle     loc_40CBA7

loc_40CB93:                             ; CODE XREF: .text:0040CB83↑j
                cmp     dword ptr [ebp-4], 18h
                jl      loc_40CBB1
                cmp     dword ptr [ebp-4], 1Bh
                jg      loc_40CBB1

loc_40CBA7:                             ; CODE XREF: .text:0040CB79↑j
                                        ; .text:0040CB8D↑j
                mov     eax, 1
                jmp     loc_40CBB3
; ---------------------------------------------------------------------------

loc_40CBB1:                             ; CODE XREF: .text:0040CB97↑j
                                        ; .text:0040CBA1↑j
                xor     eax, eax

loc_40CBB3:                             ; CODE XREF: .text:0040CBAC↑j
                jmp     $+5
; ---------------------------------------------------------------------------

loc_40CBB8:                             ; CODE XREF: .text:loc_40CBB3↑j
                pop     edi
                pop     esi
                pop     ebx
                leave
                retn
    }
}

// M_SpawnSkel    000000000040CBBD
// TODO what is signature?
__declspec(naked) void M_SpawnSkel()
{
    __asm {
                push    ebp
                mov     ebp, esp
                sub     esp, 18h
                push    ebx
                push    esi
                push    edi
                mov     [ebp-18h], edx
                mov     [ebp-14h], ecx
                mov     dword ptr [ebp-10h], 0
                mov     dword ptr [ebp-8], 0
                jmp     loc_40CBE2
; ---------------------------------------------------------------------------

loc_40CBDF:                             ; CODE XREF: .text:loc_40CC19↓j
                inc     dword ptr [ebp-8]

loc_40CBE2:                             ; CODE XREF: .text:0040CBDA↑j
                mov     eax, nummtypes
                cmp     [ebp-8], eax
                jge     loc_40CC1E
                mov     eax, [ebp-8]
                mov     ecx, eax
                shl     eax, 3
                sub     eax, ecx
                lea     eax, [ecx+eax*4]
                lea     eax, [eax+eax*2]
                xor     ecx, ecx
                mov     cl, byte ptr Monsters.mtype[eax*8]
                call    IsSkel
                test    eax, eax
                jz      loc_40CC19
                inc     dword ptr [ebp-10h]

loc_40CC19:                             ; CODE XREF: .text:0040CC10↑j
                jmp     loc_40CBDF
; ---------------------------------------------------------------------------

loc_40CC1E:                             ; CODE XREF: .text:0040CBEA↑j
                cmp     dword ptr [ebp-10h], 0
                jz      loc_40CCCC
                mov     ecx, [ebp-10h]  ; v
                call    random_
                mov     [ebp-0Ch], eax
                mov     dword ptr [ebp-10h], 0
                mov     dword ptr [ebp-8], 0
                jmp     loc_40CC49
; ---------------------------------------------------------------------------

loc_40CC46:                             ; CODE XREF: .text:loc_40CC8C↓j
                inc     dword ptr [ebp-8]

loc_40CC49:                             ; CODE XREF: .text:0040CC41↑j
                mov     eax, nummtypes
                cmp     [ebp-8], eax
                jge     loc_40CC91
                mov     eax, [ebp-0Ch]
                cmp     [ebp-10h], eax
                jg      loc_40CC91
                mov     eax, [ebp-8]
                mov     ecx, eax        ; $!
                shl     eax, 3
                sub     eax, ecx
                lea     eax, [ecx+eax*4]
                lea     eax, [eax+eax*2]
                xor     ecx, ecx
                mov     cl, byte ptr Monsters.mtype[eax*8]
                call    IsSkel
                test    eax, eax
                jz      loc_40CC8C
                inc     dword ptr [ebp-10h]

loc_40CC8C:                             ; CODE XREF: .text:0040CC83↑j
                jmp     loc_40CC46
; ---------------------------------------------------------------------------

loc_40CC91:                             ; CODE XREF: .text:0040CC51↑j
                                        ; .text:0040CC5D↑j
                dec     dword ptr [ebp-8]
                mov     eax, [ebp-8]
                push    eax
                mov     eax, [ebp+8]
                push    eax
                mov     edx, [ebp-18h]
                mov     ecx, [ebp-14h]
                call    AddMonster
                mov     [ebp-4], eax
                cmp     dword ptr [ebp-4], 0FFFFFFFFh
                jz      loc_40CCBF
                mov     edx, [ebp+8]
                mov     ecx, [ebp-4]
                call    M_StartSpStand

loc_40CCBF:                             ; CODE XREF: .text:0040CCAE↑j
                mov     eax, [ebp-4]
                jmp     loc_40CCD6
; ---------------------------------------------------------------------------
                jmp     loc_40CCD6
; ---------------------------------------------------------------------------

loc_40CCCC:                             ; CODE XREF: .text:0040CC22↑j
                mov     eax, 0FFFFFFFFh
                jmp     $+5
; ---------------------------------------------------------------------------

loc_40CCD6:                             ; CODE XREF: .text:0040CCC2↑j
                                        ; .text:0040CCC7↑j ...
                pop     edi
                pop     esi
                pop     ebx
                leave
                retn    4
    }
}

// .text:0040CCDD
__declspec(naked) void SpawnSkeleton(int x, int y)
{
    // TODO how does declspec naked deal with params?
    // Or is the signature more for the caller?
    __asm {
                push    ebp
                mov     ebp, esp
                sub     esp, 44h
                push    ebx
                push    esi
                push    edi
                mov     [ebp-44h], edx
                mov     [ebp-40h], ecx
                mov     edx, [ebp-44h]
                mov     ecx, [ebp-40h]
                call    PosOkMonst
                test    eax, eax
                jz      loc_40CD23
                mov     eax, [ebp-44h]
                push    eax
                mov     eax, [ebp-40h]
                push    eax
                mov     edx, [ebp-44h]
                mov     ecx, [ebp-40h]
                call    GetDirection
                push    eax
                mov     edx, [ebp-44h]
                mov     ecx, [ebp-40h]
                call    M_SpawnSkel
                jmp     loc_40CE6C
; ---------------------------------------------------------------------------

loc_40CD23:                             ; CODE XREF: .text:0040CCF9↑j
                mov     dword ptr [ebp-4], 0
                mov     dword ptr [ebp-3Ch], 0
                mov     eax, [ebp-44h]
                dec     eax
                mov     [ebp-34h], eax
                jmp     loc_40CD40
; ---------------------------------------------------------------------------

loc_40CD3D:                             ; CODE XREF: .text:0040CDAF↓j
                inc     dword ptr [ebp-34h]

loc_40CD40:                             ; CODE XREF: .text:0040CD38↑j
                mov     eax, [ebp-44h]
                inc     eax
                cmp     eax, [ebp-34h]
                jl      loc_40CDB4
                mov     dword ptr [ebp-2Ch], 0
                mov     eax, [ebp-40h]
                dec     eax
                mov     [ebp-30h], eax
                jmp     loc_40CD63
; ---------------------------------------------------------------------------

loc_40CD60:                             ; CODE XREF: .text:0040CDA7↓j
                inc     dword ptr [ebp-30h]

loc_40CD63:                             ; CODE XREF: .text:0040CD5B↑j
                mov     eax, [ebp-40h]
                inc     eax
                cmp     eax, [ebp-30h]
                jl      loc_40CDAC
                mov     edx, [ebp-34h]
                mov     ecx, [ebp-30h]
                call    PosOkMonst
                mov     ecx, [ebp-3Ch]
                mov     edx, [ebp-2Ch]
                lea     edx, [edx+edx*2]
                shl     edx, 2
                lea     ecx, [edx+ecx*4]
                mov     [ebp+ecx-28h], eax
                mov     eax, [ebp-3Ch]
                mov     ecx, [ebp-2Ch]
                lea     ecx, [ecx+ecx*2]
                shl     ecx, 2
                lea     eax, [ecx+eax*4]
                mov     eax, [ebp+eax-28h]
                or      [ebp-4], eax
                inc     dword ptr [ebp-2Ch]
                jmp     loc_40CD60
; ---------------------------------------------------------------------------

loc_40CDAC:                             ; CODE XREF: .text:0040CD6A↑j
                inc     dword ptr [ebp-3Ch]
                jmp     loc_40CD3D
; ---------------------------------------------------------------------------

loc_40CDB4:                             ; CODE XREF: .text:0040CD47↑j
                cmp     dword ptr [ebp-4], 0
                jnz     loc_40CDC5
                xor     eax, eax
                jmp     loc_40CE76
; ---------------------------------------------------------------------------

loc_40CDC5:                             ; CODE XREF: .text:0040CDB8↑j
                mov     ecx, 0Fh        ; v
                call    random_
                inc     eax
                mov     [ebp-38h], eax
                mov     dword ptr [ebp-2Ch], 0
                mov     dword ptr [ebp-3Ch], 0

loc_40CDE1:                             ; CODE XREF: .text:loc_40CE3A↓j
                cmp     dword ptr [ebp-38h], 0
                jle     loc_40CE3F
                mov     eax, [ebp-3Ch]
                mov     ecx, [ebp-2Ch]
                lea     ecx, [ecx+ecx*2]
                shl     ecx, 2
                lea     eax, [ecx+eax*4]
                cmp     dword ptr [ebp+eax-28h], 0
                jz      loc_40CE08
                dec     dword ptr [ebp-38h]

loc_40CE08:                             ; CODE XREF: .text:0040CDFF↑j
                cmp     dword ptr [ebp-38h], 0
                jle     loc_40CE3A
                inc     dword ptr [ebp-2Ch]
                cmp     dword ptr [ebp-2Ch], 3
                jnz     loc_40CE3A
                mov     dword ptr [ebp-2Ch], 0
                inc     dword ptr [ebp-3Ch]
                cmp     dword ptr [ebp-3Ch], 3
                jnz     loc_40CE3A
                mov     dword ptr [ebp-3Ch], 0

loc_40CE3A:                             ; CODE XREF: .text:0040CE0C↑j
                                        ; .text:0040CE19↑j ...
                jmp     loc_40CDE1
; ---------------------------------------------------------------------------

loc_40CE3F:                             ; CODE XREF: .text:0040CDE5↑j
                mov     eax, [ebp-40h]
                dec     eax
                add     [ebp-2Ch], eax
                mov     eax, [ebp-44h]
                dec     eax
                add     [ebp-3Ch], eax
                mov     eax, [ebp-44h]
                push    eax
                mov     eax, [ebp-40h]
                push    eax
                mov     edx, [ebp-3Ch]
                mov     ecx, [ebp-2Ch]
                call    GetDirection
                push    eax
                mov     edx, [ebp-3Ch]
                mov     ecx, [ebp-2Ch]
                call    M_SpawnSkel

loc_40CE6C:                             ; CODE XREF: .text:0040CD1E↑j
                mov     eax, 1
                jmp     $+5
; ---------------------------------------------------------------------------

loc_40CE76:                             ; CODE XREF: .text:0040CDC0↑j
                                        ; .text:0040CE71↑j
                pop     edi
                pop     esi
                pop     ebx
                leave
                retn
    }
}
