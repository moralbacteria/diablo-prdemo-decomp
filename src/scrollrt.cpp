#include "scrollrt.h"

#include "control.h"
#include "cursor.h"
#include "defines.h"
#include "diablo.h"
#include "engine.h"
#include "gendung.h"
#include "inv.h"
#include "missiles.h"
#include "monster.h"
#include "player.h"
#include "town.h"

#define TILE_WIDTH 64
#define TILE_HEIGHT 32

//
// Uninitialized variables (.data:????????)
//

int cel_transparency_active;
int light_table_index;
char arch_draw_type;
// I have no idea where any of these vars go.
int draw_monster_num;
int level_piece_id;
BYTE *gpBufEnd;
int nBtnLeftDstOff;
int nBtnRightDstOff;
int nDescDstOff;
int nSpellDestOff;
int nManaDestOff;
int nHpDstOff;
int nSpellBarDstOff;
// PitchTbl[row] is the the gpBuffer offset for that row.
// Initialized in diablo_init_screen
// Seems weird that they precompute basic multiplcation...
// (Each element is i * BUFFER_WIDTH)
int PitchTbl[1024];

//
// Code (.text: 0047BE30)
//

// DrawMissile	000000000047BE30

// .text:0047C1B3
/**
 * @brief Render a missile sprite, check for overdraw on lower screen
 * @param x dPiece coordinate
 * @param y dPiece coordinate
 * @param sx Back buffer coordinate
 * @param sy Back buffer coordinate
 * @param CelSkip Skip part of sprite, see Cl2Draw
 * @param CelCap  Skip part of sprite, see Cl2Draw
 * @param pre Is the sprite in the background
 */
void DrawClippedMissile(int x, int y, int sx, int sy, int CelSkip, int CelCap, BOOL pre)
{
    int m, mx, my;

    if (dMissile[x][y] != -1)
    {
        // Only one missile on tile (x, y); dMissile is an index into missile
        m = dMissile[x][y] - 1;
        if (missile[m]._miPreFlag == pre)
        {
            // NOTE: _miDrawFlag doesn't exist yet so this doesn't check for it
            mx = sx + missile[m]._mixoff - missile[m]._miAnimWidth2;
            my = sy + missile[m]._miyoff;
            if (missile[m]._miUniqTrans)
            {
                CelDrawLightTblSafe(mx, my, missile[m]._miAnimData, missile[m]._miAnimFrame, missile[m]._miAnimWidth, CelSkip, CelCap, missile[m]._miUniqTrans + 3);
            }
            else if (missile[m]._miLightFlag)
            {
                CelDrawLightSafe(mx, my, missile[m]._miAnimData, missile[m]._miAnimFrame, missile[m]._miAnimWidth, CelSkip, CelCap);
            }
            else
            {
                CelDrawSafe(mx, my, missile[m]._miAnimData, missile[m]._miAnimFrame, missile[m]._miAnimWidth, CelSkip, CelCap);
            }
        }
        // TODO
    }
    else
    {
        // Multiple missiles on the same tile. Need to iterate through all missiles to find which ones to draw
        // TODO
    }
    // TODO
}

// DrawMonster	000000000047C535

// .text:0047C732
/**
 * @brief Render a monster sprite, check for overdraw on lower screen
 * @param x dPiece coordinate
 * @param y dPiece coordinate
 * @param mx Back buffer coordinate
 * @param my Back buffer coordinate
 * @param CelSkip Skip part of sprite, see Cl2Draw
 * @param CelCap  Skip part of sprite, see Cl2Draw
 */
static void DrawClippedMonster(int x, int y, int mx, int my, int m, int CelSkip, int CelCap)
{
    if (!(dFlags[x][y] & BFLAG_LIT))
    {
        CelDrawLightTblSafe(mx, my, monster[m]._mAnimData, monster[m]._mAnimFrame, monster[m].MType->width, CelSkip, CelCap, 1);
    }
    else
    {
        // TODO
    }

    // TODO
}

// DrawObjCel	000000000047C92F
// CDrawObjCel	000000000047CBCC
// DrawEFlag1	000000000047CE69
// DrawHTLXsub	000000000047D02E

// .text:0047DC7A
/**
 * @brief Render a row of tiles
 * @param x dPiece coordinate
 * @param y dPiece coordinate
 * @param sx Back buffer coordinate
 * @param sy Back buffer coordinate
 * @param chunks tile width of row
 * @param eflag is it an even (0) or odd (1) row
 */
static void scrollrt_draw_lower(int x, int y, int sx, int sy, int chunks, BOOL eflag)
{
    // TODO
}

// .text:0047E2BC
/**
 * This variant checks for of screen element on the lower screen
 * This function it self causes rendering issues since it will render on top of objects on the other side of walls
 * @brief Re render tile to workaround sorting issues with players walking east/west
 * @param pBuff Pointer to output buffer at location sx,sy
 * @param y dPiece coordinate
 * @param x dPiece coordinate
 * @param row The current row being rendered
 * @param CelSkip chunks of cell to skip
 * @param sx Back buffer coordinate
 * @param sy Back buffer coordinate
 */
static void scrollrt_draw_clipped_e_flag_2(BYTE *pBuff, int x, int y, int row, int CelSkip, int sx, int sy)
{
    // TODO
}

// .text:0047E547
/**
 * This variant checks for of screen element on the lower screen
 * @brief Render object sprites, skip offscreen parts for lower screen
 * @param pBuff where to render to with sx,sy already applied
 * @param sx dPiece coordinate
 * @param sy dPiece coordinate
 * @param row The current row being rendered
 * @param CelSkip chunks of cell to skip
 * @param dx Back buffer coordinate
 * @param dy Back buffer coordinate
 * @param eflag Should the sorting workaround be applied
 */
static void scrollrt_draw_clipped_dungeon_2(BYTE *pBuff, int sx, int sy, int row, int CelSkip, int dx, int dy, BOOL eflag)
{
    int px, py;

    // TODO

    if (dMonster[sx][sy] > 0 && (dFlags[sx][sy] & BFLAG_LIT || plr[myplr]._pInfraFlag))
    {
        draw_monster_num = dMonster[sx][sy] - 1;
        if (!(monster[draw_monster_num]._mFlags & MFLAG_HIDDEN))
        {
            px = dx + monster[draw_monster_num]._mxoff - monster[draw_monster_num].MType->width2;
            py = dy + monster[draw_monster_num]._myoff;
            if (draw_monster_num == pcursmonst)
            {
                CelDrawOutlineSafe(233, px, py, monster[draw_monster_num]._mAnimData, monster[draw_monster_num]._mAnimFrame, monster[draw_monster_num].MType->width, CelSkip, 8);
            }
            DrawClippedMonster(sx, sy, px, py, draw_monster_num, CelSkip, 8);
            if (eflag && !monster[draw_monster_num]._meflag)
            {
                scrollrt_draw_clipped_e_flag_2(pBuff - TILE_WIDTH, sx - 1, sy + 1, row, CelSkip, dx - TILE_WIDTH, dy);
            }
            // TODO
        }
        // TODO
    }
    if (dFlags[sx][sy] & BFLAG_MISSILE)
    {
        DrawClippedMissile(sx, sy, dx, dy, 0, 8, FALSE);
    }

    // TODO
}

// .text:0047F1E7
/**
 * @brief Render a row of tile, checking for overdrawing on lower part of screen
 * @param x dPiece coordinate
 * @param y dPiece coordinate
 * @param sx Back buffer coordinate
 * @param sy Back buffer coordinate
 * @param chunks tile width of row
 * @param row current row being rendered
 * @param eflag is it an even (0) or odd (1) row
 */
static void scrollrt_draw_lower_2(int x, int y, int sx, int sy, int chunks, int row, BOOL eflag)
{
    int CelSkip;
    BYTE *dst;
    MICROS *pMap;

    pMap = &dpiece_defs_map_1[IsometricCoord(x, y)];
    CelSkip = 2 * row + 2;

    if (eflag)
    {
        if (y >= 0 && y < MAXDUNY && x >= 0 && x < MAXDUNX)
        {
            level_piece_id = dPiece[x][y];
            light_table_index = dLight[x][y];
            if (level_piece_id != 0)
            {
                dst = &gpBuffer[sx - (BUFFER_WIDTH * TILE_HEIGHT - 32) + PitchTbl[sy]];
                cel_transparency_active = (BYTE)(nTransTable[level_piece_id] & TransList[dTransVal[x][y]]);
                // TODO
            }
            // TODO
        }
        // TODO
    }
    // TODO
}

// DrawEFlag3	000000000047F6D7
// DrawHTLXsub3	000000000047F8AF

// .text:0048054F
/**
 * @brief Render a row of tile, checking for overdrawing on upper part of screen
 * @param x dPiece coordinate
 * @param y dPiece coordinate
 * @param sx Back buffer coordinate
 * @param sy Back buffer coordinate
 * @param chunks tile width of row
 * @param row current row being rendered
 * @param eflag is it an even (0) or odd (1) row
 */
static void scrollrt_draw_upper(int x, int y, int sx, int sy, int chunks, int row, BOOL eflag)
{
    // TODO
}

// .text:00480BD6
void DrawGame(int x, int y)
{
    int i, sx, sy, chunks, blocks;

    ViewDX = SCREEN_WIDTH;
    ViewDY = VIEWPORT_HEIGHT;
    ViewBX = SCREEN_WIDTH / TILE_WIDTH;
    ViewBY = VIEWPORT_HEIGHT / TILE_HEIGHT;

    sx = ScrollInfo._sxoff + SCREEN_X;
    sy = ScrollInfo._syoff + SCREEN_Y + (TILE_HEIGHT / 2 - 1);
    x -= SCREEN_WIDTH / TILE_WIDTH;
    y--;
    chunks = SCREEN_WIDTH / TILE_WIDTH;
    blocks = 8;

    if (invflag)
    {
        x += 2;
        y -= 2;
        sx += (SCREEN_WIDTH / 2) - TILE_WIDTH / 2;
        chunks = (SCREEN_WIDTH / TILE_WIDTH) - 4;
    }
    if (chrflag)
    {
        x += 2;
        y -= 2;
        sx -= TILE_WIDTH / 2;
        chunks = (SCREEN_WIDTH / TILE_WIDTH) - 4;
    }

    switch (ScrollInfo._sdir)
    {
    case SDIR_NONE:
        break;
    case SDIR_N:
        sy -= TILE_HEIGHT;
        x--;
        y--;
        blocks++;
        break;
    case SDIR_NE:
        sy -= TILE_HEIGHT;
        x--;
        y--;
        chunks++;
        blocks++;
        break;
    case SDIR_E:
        chunks++;
        break;
    case SDIR_SE:
        chunks++;
        blocks++;
        break;
    case SDIR_S:
        blocks++;
        break;
    case SDIR_SW:
        sx -= TILE_WIDTH;
        x--;
        y++;
        chunks++;
        blocks++;
        break;
    case SDIR_W:
        sx -= TILE_WIDTH;
        x--;
        y++;
        chunks++;
        break;
    case SDIR_NW:
        sx -= TILE_WIDTH;
        sy -= TILE_HEIGHT;
        x -= 2;
        chunks++;
        blocks++;
        break;
    }

    gpBufEnd = &gpBuffer[PitchTbl[SCREEN_Y]];
    for (i = 0; i < 4; i++)
    {
        scrollrt_draw_upper(x, y, sx, sy, chunks, i, FALSE);
        y++;
        sx -= TILE_WIDTH / 2;
        sy += TILE_HEIGHT / 2;
        scrollrt_draw_upper(x, y, sx, sy, chunks, i, TRUE);
        x++;
        sx += TILE_WIDTH / 2;
        sy += TILE_HEIGHT / 2;
    }
    gpBufEnd = &gpBuffer[PitchTbl[VIEWPORT_HEIGHT - TILE_HEIGHT + SCREEN_Y]];
    for (i = 0; i < blocks; i++)
    {
        scrollrt_draw_lower(x, y, sx, sy, chunks, FALSE);
        y++;
        sx -= TILE_WIDTH / 2;
        sy += TILE_HEIGHT / 2;
        scrollrt_draw_lower(x, y, sx, sy, chunks, TRUE);
        x++;
        sx += TILE_WIDTH / 2;
        sy += TILE_HEIGHT / 2;
    }
    arch_draw_type = 0;
    for (i = 0; i < 4; i++)
    {
        scrollrt_draw_lower_2(x, y, sx, sy, chunks, i, FALSE);
        y++;
        sx -= TILE_WIDTH / 2;
        sy += TILE_HEIGHT / 2;
        scrollrt_draw_lower_2(x, y, sx, sy, chunks, i, TRUE);
        x++;
        sx += TILE_WIDTH / 2;
        sy += TILE_HEIGHT / 2;
    }
}

// .text:00480EC1
void DrawZoom(int StartX, int StartY)
{
    // TODO
}

// DrawView	000000000048120E
void DrawView(int StartX, int StartY)
{
    if (zoomflag)
    {
        DrawGame(StartX, StartY);
    }
    else
    {
        DrawZoom(StartX, StartY);
    }
    // TODO
}

// .text:004813C2
// Fill the visible area with 0s
void ClearScreenBuffer()
{
    __asm {
        mov edi, gpBuffer
        add edi, 122944 //; (BUFFER_WIDTH) * BORDER_TOP + BORDER_LEFT
        mov edx, 480 //; SCREEN_HEIGHT
        xor eax, eax

    blank_row:
        mov ecx, 160 //; SCREEN_WIDTH / sizeof(DWORD)
        rep stosd
        add edi, 128 //; BUFFER_LEFT + BUFFER_RIGHT
        dec edx
        jnz blank_row
    }
}

// ScrollView	00000000004813F9
void ScrollView()
{
    // TODO
}

// .text:00481653
// Blits from our software buffer (`gpBuffer`) into the DirectDraw hardware
// buffer (`lpDDSPrimary`).
//
// `bufferpos` is an offset into `gpBuffer` of where to start blitting from.
// Ignored if `dwHgt` is 0. This is ignored by `draw_*`
//
// `dwHgt` is how many rows to unconditionally blit, starting at `bufferpos`.
// Common values are:
// - 0, nothing happens unless `draw_*` params are set; typically draw_cursor
// - 128, blitting the control panel.
// - 352, blitting the game view.
// - 480, blitting the entire screen.
//
// If `draw_cursor` is `TRUE` then blit the cursor.
//
// If `draw_btn` is `TRUE` then blit the control panel buttons on the left and
// right sides.
//
// NOTE: While you can still specify draw_* when `dwHgt` is greater than 0,
// this will lead to overdraw and worse performance! The only exception is
// `draw_cursor` which is never drawn unless `TRUE`.
static void DrawMain(int bufferpos, int dwHgt, BOOL draw_cursor, BOOL draw_desc,
                     BOOL draw_hp, BOOL draw_mana, BOOL draw_sbar,
                     BOOL draw_btn)
{
    HRESULT hDDVal; // written but never read
    DDSURFACEDESC ddsd;
    void *lpSurface;
    int pitchPastWidth;
    int i, row;

    // Don't bother if the window isn't visible or interactable
    if (!gbActive)
    {
        return;
    }

    // Recover from minimization, etc
    if (lpDDSBackBuf->IsLost() == DDERR_SURFACELOST)
    {
        lpDDSBackBuf->Restore();
    }

    // Get a surface to copy `gpBuffer` into
    hDDVal = lpDDSPrimary->Lock(NULL, &ddsd, DDLOCK_WAIT, NULL);
    lpSurface = ddsd.lpSurface;
    // Could be that lPitch != SCREEN_WIDTH if the buffer has hardare alignment
    // requirements. Need to account for that while we stride.
    pitchPastWidth = ddsd.lPitch - SCREEN_WIDTH;

    // Now we can use `ddsd` to calculate our destination blit offsets. Assuming
    // that the buffer dimensions never change, we only need to do this once.
    if (calculate_offsets_in_DrawMain)
    {
        // Create a mapping of row -> offset for `lpSurface`
        i = 0;
        for (row = 0; row < SCREEN_HEIGHT; row++)
        {
            DDPitchTbl[row] = i;
            i += ddsd.lPitch;
        }

        // Calculate offsets for specific elements
        // TODO: Finish me
        nDescDstOff = ddsd.lPitch * 414 + 177;
        // nHpDstOff = ddsd.lPitch * 480 + 96;
        // nManaDstOffBulb = ddsd.lPitch * 480 + 461
        nSpellDestOff = ddsd.lPitch * 406 + 526;
        // nSpellBarDstOff = ddsd.lPitch *
        nBtnLeftDstOff = ddsd.lPitch * 367 + 15;
        nBtnRightDstOff = ddsd.lPitch * 367 + 573;

        // Only do this once
        calculate_offsets_in_DrawMain = FALSE;
    }

    // Present the requested number of rows from the requested start position
    if (dwHgt > 0)
    {
        __asm {
            mov esi, gpBuffer
            add esi, bufferpos
            mov edi, lpSurface
            mov ebx, pitchPastWidth
            mov edx, dwHgt

        blit_row:
            mov ecx, SCREEN_WIDTH / 4
            rep movsd
            add esi, BUFFER_WIDTH - SCREEN_WIDTH
            add edi, ebx
            dec edx
            jnz blit_row
        }
    }

    if (draw_sbar)
    {
        // TODO
    }

    if (draw_cursor)
    {
        // TODO
    }

    if (draw_desc)
    {
        // TODO
    }

    if (draw_mana)
    {
        // TODO
    }

    if (draw_hp)
    {
        // TODO
    }

    if (draw_btn)
    {
        __asm {
            ; // Blit left. Copy a (x=15, y=367, w=52, h=98) rectangle.
            mov esi, gpBuffer
            add esi, SCREENXY(15, 367)
            mov edi, lpSurface
            add edi, nBtnLeftDstOff
            ; // Blit 98 rows, not sure why 98
            mov edx, 98

        blit_row_l:
            ; // panbtns.cel frames are at most 52 px wide. Since we copy DWORDs
            ; // (4 bytes) 13 times, 13 * 4 = 52
            mov ecx, 52 / 4
            rep movsd
            add esi, BUFFER_WIDTH - 52
            add edi, pitchPastWidth
            add edi, SCREEN_HEIGHT - 52
            dec edx
            jnz blit_row_l

            ; // TODO there might be the end of one __asm block here and the start of another
            ; // but autoformat is shitting the bed if I don't do this

            ; // Blit right. Copy a (x=573, y=367, w=52, h=98) rectangle.
            mov esi, gpBuffer
            add esi, SCREENXY(573, 367)
            mov edi, lpSurface
            add edi, nBtnRightDstOff
            mov edx, 98

        blit_row_r:
            mov ecx, 52 / 4
            rep movsd
            add esi, BUFFER_WIDTH - 52
            add edi, pitchPastWidth
            add edi, SCREEN_HEIGHT - 52
            dec edx
            jnz blit_row_r
        }
    }

    lpDDSPrimary->Unlock(NULL); // BUG: Give `lpSurface`, not NULL
    if (dd_use_backbuffer)
    {
        lpDDSBackBuf->Flip(NULL, 1); // TODO 1 is magic number
    }
}

// .text:00481AFC
// Presents the menu screem to the user.
void scrollrt_draw_game_screen(BOOL draw_cursor)
{
    if (force_redraw & FORCE_REDRAW_ALL)
    {
        DrawMain(SCREENXY(0, 0), SCREEN_HEIGHT, /*draw_cursor=*/TRUE, FALSE,
                 FALSE, FALSE, FALSE, FALSE);
    }
    else
    {
        DrawMain(0, 0, draw_cursor, FALSE, FALSE, FALSE, FALSE, FALSE);
    }
    force_redraw = 0;
}

// .text:00481B60
// Draws and presents all game screen elements. Relies on:
//
// - `force_redraw` to know whether to draw the game, control panel, or both
// - `drawinfoflag`, `drawhpflag`, `drawmanaflag, `drawbtnflag` to know which
//   subparts to draw in addition
void DrawAndBlit()
{
    savecrsr_hide();
    if (leveltype != DTYPE_TOWN)
    {
        if (force_redraw & FORCE_REDRAW_ALL)
        {
            DrawView(ViewX, ViewY);
            DrawCtrlPan();
            PrintInfo();
            UpdateLifeFlask();
            UpdateManaFlask();
            DrawCtrlBtns();
            savecrsr_show();
            DrawMain(SCREENXY(0, 0), SCREEN_HEIGHT, /*draw_cursor=*/TRUE, FALSE,
                     FALSE, FALSE, FALSE, FALSE);
        }
        else
        {
            if (force_redraw == FORCE_REDRAW_VIEWPORT)
            {
                DrawView(ViewX, ViewY);

                if (drawinfoflag)
                {
                    PrintInfo();
                }
                if (drawhpflag)
                {
                    UpdateLifeFlask();
                }
                if (drawmanaflag)
                {
                    UpdateManaFlask();
                }
                if (drawbtnflag)
                {
                    DrawCtrlBtns();
                }
                savecrsr_show();
                DrawMain(SCREENXY(0, 0), VIEWPORT_HEIGHT, /*draw_cusor=*/TRUE,
                         drawinfoflag, drawhpflag, drawmanaflag,
                         /*draw_sbar=*/TRUE, drawbtnflag);
            }

            if (force_redraw == FORCE_REDRAW_PANEL)
            {
                // NOTE: 0/1/4 are the only values used so this is dead code
                DrawCtrlPan();

                if (drawinfoflag)
                {
                    PrintInfo();
                }
                if (drawhpflag)
                {
                    UpdateLifeFlask();
                }
                if (drawmanaflag)
                {
                    UpdateManaFlask();
                }
                if (drawbtnflag)
                {
                    DrawCtrlBtns();
                }
                savecrsr_show();
                // Are they blitting from "offscreen"? bufferpos < SCREENXY(0, 0)
                DrawMain(BUFFER_WIDTH * (BORDER_TOP - PANEL_HEIGHT),
                         PANEL_HEIGHT, /*draw_cursor=*/TRUE, FALSE, FALSE,
                         FALSE, FALSE, FALSE);
            }

            if (force_redraw == FORCE_REDRAW_VIEWPORT | FORCE_REDRAW_PANEL)
            {
                // NOTE: 0/1/4 are the only values used so this is dead code
                DrawView(ViewX, ViewY);
                DrawCtrlPan();

                if (drawinfoflag)
                {
                    PrintInfo();
                }
                if (drawhpflag)
                {
                    UpdateLifeFlask();
                }
                if (drawmanaflag)
                {
                    UpdateManaFlask();
                }
                if (drawbtnflag)
                {
                    DrawCtrlBtns();
                }
                savecrsr_show();
                DrawMain(SCREENXY(0, 0), SCREEN_HEIGHT, /*draw_cursor=*/TRUE,
                         FALSE, FALSE, FALSE, FALSE, FALSE);
            }
        }
    }
    else
    {
        if (force_redraw & FORCE_REDRAW_ALL)
        {
            T_DrawView(ViewX, ViewY);
            DrawCtrlPan();
            PrintInfo();
            UpdateLifeFlask();
            UpdateManaFlask();
            DrawCtrlBtns();
            savecrsr_show();
            DrawMain(SCREENXY(0, 0), SCREEN_HEIGHT, /*draw_cursor=*/TRUE, FALSE,
                     FALSE, FALSE, FALSE, FALSE);
        }
        else
        {
            if (force_redraw == FORCE_REDRAW_VIEWPORT)
            {
                T_DrawView(ViewX, ViewY);

                if (drawinfoflag)
                {
                    PrintInfo();
                }
                if (drawhpflag)
                {
                    UpdateLifeFlask();
                }
                if (drawmanaflag)
                {
                    UpdateManaFlask();
                }
                if (drawbtnflag)
                {
                    DrawCtrlBtns();
                }
                savecrsr_show();
                DrawMain(SCREENXY(0, 0), VIEWPORT_HEIGHT, /*draw_cusor=*/TRUE,
                         drawinfoflag, drawhpflag, drawmanaflag,
                         /*draw_sbar=*/TRUE, drawbtnflag);
            }

            if (force_redraw == FORCE_REDRAW_PANEL)
            {
                // NOTE: 0/1/4 are the only values used so this is dead code
                DrawCtrlPan();

                if (drawinfoflag)
                {
                    PrintInfo();
                }
                if (drawhpflag)
                {
                    UpdateLifeFlask();
                }
                if (drawmanaflag)
                {
                    UpdateManaFlask();
                }
                if (drawbtnflag)
                {
                    DrawCtrlBtns();
                }
                savecrsr_show();
                // Are they blitting from "offscreen"? bufferpos < SCREENXY(0, 0)
                // Maybe this a bug that is not hit because this code path is
                // never taken
                DrawMain(BUFFER_WIDTH * (BORDER_TOP - PANEL_HEIGHT),
                         PANEL_HEIGHT, /*draw_cursor=*/TRUE, FALSE, FALSE,
                         FALSE, FALSE, FALSE);
            }

            if (force_redraw == FORCE_REDRAW_VIEWPORT | FORCE_REDRAW_PANEL)
            {
                // NOTE: 0/1/4 are the only values used so this is dead code
                T_DrawView(ViewX, ViewY);
                DrawCtrlPan();

                if (drawinfoflag)
                {
                    PrintInfo();
                }
                if (drawhpflag)
                {
                    UpdateLifeFlask();
                }
                if (drawmanaflag)
                {
                    UpdateManaFlask();
                }
                if (drawbtnflag)
                {
                    DrawCtrlBtns();
                }
                savecrsr_show();
                DrawMain(SCREENXY(0, 0), SCREEN_HEIGHT, /*draw_cursor=*/TRUE,
                         FALSE, FALSE, FALSE, FALSE, FALSE);
            }
        }
    }

    force_redraw = 0;
    drawinfoflag = FALSE;
    drawhpflag = FALSE;
    drawmanaflag = FALSE;
    drawbtnflag = FALSE;
}
