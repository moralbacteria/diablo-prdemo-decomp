#include "interfac.h"

#include <stdio.h>
#include <windows.h>
#include <storm.h>

#include "cursor.h"
#include "defines.h"
#include "diablo.h"
#include "effects.h"
#include "engine.h"
#include "gendung.h"
#include "gmenu.h"
#include "lighting.h"
#include "palette.h"
#include "player.h"
#include "quests.h"
#include "scrollrt.h"

#define MAINMENU_NEW_LOAD_QUIT 1
#define MAINMENU_NEWGAME_CLASS 2
#define MAINMENU_DEMO_END 3
#define MAINMENU_CUTSCENE 4

static void interfac_InitNewGame();

//
// initialized variables (.data:004AA608)
//

char interfac_mfontframe[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37, 49,
    38, 0, 39, 40, 47, 42, 43, 41, 45, 52, 44, 53, 55, 36,
    27, 28, 29, 30, 31, 32, 33, 34, 35, 51, 50, 0, 46, 0,
    54, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 42,
    0, 43, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
    13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    26, 20, 0, 21, 0, 0};

char interfac_mfontkern[] = {
    18, 33, 21, 26, 28, 19, 19, 26, 25, 11, 12, 25, 19,
    34, 28, 32, 20, 32, 28, 20, 28, 36, 35, 46, 33, 33,
    24, 11, 23, 22, 22, 21, 22, 21, 21, 21, 32, 10, 20,
    36, 31, 17, 13, 12, 13, 18, 16, 11, 20, 21, 11, 10,
    12, 11, 21, 23};

//
// uninitiailzed variables (.data:005DE8D0)
//

// Number of frames left until we transition away from Quotes.CEL. Set to 100 and decreases by 1 each frame (5s given 20 FPS)
int quote_timer;
// Handle to the video being played, either logo.smk or diablo.smk
HANDLE hCurrentVideo;
// ...
// Either 1, 2, 3, or 4. See draw_switch_title_option()
char menu_to_draw;
// If the highest bit is set (& 0x80) then change menu behavior
// TODO better name?
char menu_allow_key_input;
// TODO better name?
char fade_param; // TODO is this an int?
// An int reused amongst various menu screens.
// For cutscene, this is the progress bar length in px
// For character class select/naming,
int menu_selection_index;
// Which frame of animation the spining pentagram uses
// TODO this is reused in multiple ocntexts
// On cutscene this is color?
int pentSpinFrame;
// Which frame of animation the Diablo logo uses
int titleLogoFrame;
// Reused depending on screen
BYTE *pMenuGendata1;
// Reused depending on screen
BYTE *pMenuGendata2;
// Reused depending on screen
BYTE *pMenuGendata3;
// Reused depending on screen
BYTE *pMenuGendata4;
// Reused depending on screen
BYTE *pMenuGendata5;
// Read by paint_demo_end_screen() but never written; probably meant for Gendata/Screen06.CEL
BYTE *pMenuGendata6;
int player_name_length;
// ...
// UNUSED; set to 0 in draw_switch_title_option(), never read
char byte_5DE90C;
// ...
char game00_abspath[256];
// Either -128 if Game00.sav exists, or 0
char did_find_game00;

//
// code (.text:00418F10)
//

// TODO These functions really need to be renamed

// print_title_str_large	0000000000418F10
// print_title_str_small	0000000000418FB2

// .text:00419044
// Draw a 22px tall vertical line at (screen_x, screen_y).
// `progress_id` is used as the bar color.
void DrawProgress(int screen_x, int screen_y, int progress_id)
{
    BYTE *dst;
    int i;

    // TODO magic numbers
    // Index into palette
    BYTE BarColor[2] = {138, 43};

    dst = &gpBuffer[screen_x + PitchTbl[screen_y]];
    for (i = 0; i < 22; i++)
    {
        *dst = BarColor[progress_id];
        dst += BUFFER_WIDTH;
    }
}

// .text:004190AE
// Modifies `did_find_game00`:
// - 0x80 if Game00.sav exists
// - 0 otherwise
void Title_FindSaveGame()
{
    char buffer[52]; // BUG: This buffer is TINY and constantly overrun

    did_find_game00 = 0;
    sprintf(game00_abspath, "%s%s", savedir_abspath, "\\Game00.sav");
    _searchenv(game00_abspath, NULL, buffer);
    if (*buffer)
    {
        did_find_game00 |= 0x80; // wtf
    }
}

// .text:00419111
void mainmenu_draw()
{
    // szOptionTexts
    // TODO
    // int i;

    // for (i = 0; i < 3; i++) {
    //     drawx = 640 - iOptionDrawX
    //     drawy = i * 45 + 450;
    //     CelDraw(563, drawy + 1, pPenTitleCel, pentSpinFrame, 48);
    // }
}

// paint_select_class	00000000004192DE
void paint_select_class()
{
    // TODO
}

// .text:0041962C
// Draw Screen01 to Screen05 based on `menu_selection_index`
// This will crash if menu_selection_index is 6 (that cel is not loaded)
void paint_demo_end_screen()
{
    // TODO magic numbers
    switch (menu_selection_index)
    {
    case 1:
        CelDraw(64, 639, pMenuGendata1, 1, 640);
        break;
    case 2:
        CelDraw(64, 639, pMenuGendata2, 1, 640);
        break;
    case 3:
        CelDraw(64, 639, pMenuGendata3, 1, 640);
        break;
    case 4:
        CelDraw(64, 639, pMenuGendata4, 1, 640);
        break;
    case 5:
        CelDraw(64, 639, pMenuGendata5, 1, 640);
        break;
    case 6:
        // mystery background cel :eyes: probably Gendata/Screen06.CEL
        CelDraw(64, 639, pMenuGendata6, 1, 640);
        break;
    }
}

// .text:00419746
// Renders the cutscene screen: background and progress bar
void DrawCutscene()
{
    int i;
    // The screen position of the top left corner of the progress bar.
    POINT BarPos[2] = {
        {/*x=*/53, /*y=*/37},
        {/*x=*/53, /*y=*/421},
    };

    // pMenuGendata1 is "sgpBackCel", should be either Cuttt or Cut1d
    CelDraw(BORDER_LEFT,
            BORDER_TOP + SCREEN_HEIGHT - 1,
            pMenuGendata1,
            /*nCel=*/1,
            /*nCelW=*/640);

    // menu_selection_index is "sgdwProgress"; progress bar width in px capped at 534 px
    if (menu_selection_index > 534)
    {
        menu_selection_index = 534;
    }

    // Draw the progress bar in vertical lines
    for (i = 0; i < menu_selection_index; i++)
    {
        // pentSpinFrame is "progress_id"; whether progress bar is top or bottom screen
        DrawProgress(BORDER_LEFT + BarPos[pentSpinFrame].x + i,
                     BORDER_TOP + BarPos[pentSpinFrame].y,
                     pentSpinFrame);
    }
}

// .text:004197F7
// Draw the menu screen corresponding to `menu_to_draw_index`
void draw_switch_title_option(int menu_to_draw_index)
{
    byte_5DE90C = 0;
    switch (menu_to_draw_index)
    {
    case MAINMENU_NEW_LOAD_QUIT:
        mainmenu_draw();
        break;
    case MAINMENU_NEWGAME_CLASS:
        paint_select_class();
        break;
    case MAINMENU_DEMO_END:
        ClearScreenBuffer();
        paint_demo_end_screen();
        break;
    case MAINMENU_CUTSCENE:
        DrawCutscene();
        break;
    }
    force_redraw = FORCE_REDRAW_ALL;
}

// .text:00419882
// Progresses fade and presents the screen to the user until the `quote_timer`
// finishes.
//
// `hWnd` is the window to post messages to.
//
// This does not do any drawing! It only presents what is already drawn.
// In order to have something to fade in on, the graphic must already have been drawn to gpBuffer
// (typically via draw_switch_title_option)
static void interfac_PaintQuotes(HWND hWnd)
{
    while (paint_mutex)
    {
        // do nothing, wait for mutex
    }

    paint_mutex = TRUE;
    if (fade_state)
    {
        if (fade_state == PALETTE_FADE_IN)
        {
            DoFadeIn();
            if (fade_state == PALETTE_NO_FADE)
            {
                PostMessage(hWnd, WM_DIABDONEFADEIN, 0, 0);
            }
        }
        if (fade_state == PALETTE_FADE_OUT)
        {
            DoFadeOut();
            if (fade_state == PALETTE_NO_FADE)
            {
                PostMessage(hWnd, delayed_Msg, 0, 0);
            }
        }

        lpDDPalette->SetEntries(0, 0, 256, system_palette);
    }

    if (quote_timer > 0)
    {
        // Post a message to progress the quote_timer. Kind of annoyed that they
        // didn't just inline it here.
        PostMessageA(hWnd, WM_DIABMENUTICK, 0, 0);
    }

    scrollrt_draw_game_screen(TRUE);
    outstanding_paint_event = FALSE;
    paint_mutex = FALSE;
}

// .text:00419982
static void interfac_PaintMainMenu(HWND hWnd)
{
    while (paint_mutex)
    {
        // do nothing, wait for mutex
    }

    paint_mutex = TRUE;
    if (fade_state)
    {
        if (fade_state == PALETTE_FADE_IN)
        {
            DoFadeIn();
            if (fade_state == PALETTE_NO_FADE)
            {
                PostMessage(hWnd, WM_DIABDONEFADEIN, 0, 0);
            }
        }
        if (fade_state == PALETTE_FADE_OUT)
        {
            if (fade_param & 0x40)
            {
                DoFadeOut();
            }
            else
            {
                fade_state = PALETTE_NO_FADE;
            }

            if (fade_state == PALETTE_NO_FADE)
            {
                PostMessage(hWnd, delayed_Msg, 0, 0);
            }
        }

        lpDDPalette->SetEntries(0, 0, 256, system_palette);
    }

    savecrsr_hide();
    draw_switch_title_option(menu_to_draw);
    if (menu_allow_key_input & 0x80)
    {
        savecrsr_show();
    }
    scrollrt_draw_game_screen(TRUE);
    pentSpinFrame++;
    if (pentSpinFrame == 9)
    {
        pentSpinFrame = 1;
    }
    titleLogoFrame++;
    if (titleLogoFrame == 29)
    {
        titleLogoFrame = 1;
    }
    outstanding_paint_event = FALSE;
    paint_mutex = FALSE;
}

// .text:00419ADD
static void interfac_PaintProgress(HWND hWnd)
{
    while (paint_mutex)
    {
        // do nothing, wait for mutex
    }

    paint_mutex = TRUE;
    if (fade_state)
    {
        if (fade_state == PALETTE_FADE_IN)
        {
            DoFadeIn();
        }
        if (fade_state == PALETTE_FADE_OUT)
        {
            DoFadeOut();

            if (fade_state == PALETTE_NO_FADE)
            {
                PostMessage(hWnd, delayed_Msg, 0, 0);
            }
        }

        lpDDPalette->SetEntries(0, 0, 256, system_palette);
    }

    outstanding_paint_event = FALSE;
    paint_mutex = FALSE;
}

// .text:00419B93
// Do the fade. After the fade, each call increases the bar by 15px
void IncProgress()
{
    // Block until we've painted a frame with the progress bar incremented.
    while (TRUE)
    {
        if (fade_state)
        {
            // Handle fade to completion.
            if (outstanding_paint_event)
            {
                if (fade_state == PALETTE_FADE_IN)
                {
                    DoFadeIn();
                }
                if (fade_state == PALETTE_FADE_OUT)
                {
                    DoFadeOut();
                }

                lpDDPalette->SetEntries(0, 0, 256, system_palette);
            }

            draw_switch_title_option(menu_to_draw);
            scrollrt_draw_game_screen(TRUE);
            set_outstanding_paint_event(FALSE);

            continue;
        }
        else
        {
            // Wait for PaintEvent to generate a "paint event".
            if (outstanding_paint_event)
            {
                draw_switch_title_option(menu_to_draw);
                scrollrt_draw_game_screen(TRUE);
                set_outstanding_paint_event(FALSE);
                return;
            }
        }
    }
}

// .text:00419C60
LRESULT WndProc_IntroVid(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    BYTE *pCelBuff;
    MSG Msg;

    switch (uMsg)
    {
    case WM_ACTIVATEAPP:
        if (gbActive)
        {
            ResetPal();
        }
        return 0;
    case WM_DIABDONEFADEIN:
        force_redraw = FORCE_REDRAW_ALL;
        set_outstanding_paint_event(FALSE);
        return 0;
    case WM_DIABMENUTICK:
        // Progress the quote timer and transition when it's done.
        quote_timer--;
        if (quote_timer <= 0)
        {
            PaletteFadeOut(32);
            delayed_Msg = WM_DIABDONEFADEOUT;
        }
        return 0;
    case WM_LBUTTONDOWN:
    case WM_CHAR:
    case WM_RBUTTONDOWN:
        if (hCurrentVideo)
        {
            SVidPlayEnd(hCurrentVideo);
            set_outstanding_paint_event(FALSE);
        }
        break;
    case WM_KEYDOWN:
        // Any key ends the current video
        if (hCurrentVideo)
        {
            SVidPlayEnd(hCurrentVideo);
            set_outstanding_paint_event(FALSE);
        }

        // Do nothing in the fade
        if (fade_state)
        {
            break;
        }

        // Skip the quotes by pressing escape
        if (wParam == VK_ESCAPE)
        {
            quote_timer = 1;
        }
        return 0;
    case WM_DIABDONEFADEOUT:
        if (menu_selection_index < 2)
        {
            // We get here when Quotes.CEL has faded out
            menu_selection_index++;
            switch (menu_selection_index)
            {
            case 1:
                // Theoretically this branch is necessary but in practice it's
                // not used. menu_selection_index is set to 1 by
                // interfac_PlayLogo_DrawQuotes, then this function increments
                // it to 2 and the `case 2` is used instead.
                LoadPalette("Gendata\\Quotes.PAL", orig_palette);
                pCelBuff = pMenuGendata1;
                quote_timer = 100;
                break;
            case 2:
                // Play diablo.smk and block (handling events) until done.
                SVidPlayBegin("gendata\\diablo.smk", 0, 0, 0, 0x10080200, &hCurrentVideo);
                while (SVidPlayContinue())
                {
                    while (PeekMessageA(&Msg, NULL, 0, 0, PM_REMOVE))
                    {
                        if (Msg.message == WM_QUIT)
                        {
                            ExitProcess(0);
                        }
                        else
                        {
                            TranslateMessage(&Msg);
                            DispatchMessageA(&Msg);
                        }
                    }
                }
                SVidPlayEnd(hCurrentVideo);

                // Normally 0x409 is posted after fade out. Here it's posted
                // right after the video is done. There's also no fade in
                // BlizLogo which makes me think that palette swapping is
                // something unsupported by smacker
                PostMessageA(hWnd, WM_DIABDONEFADEOUT, 0, 0);
                set_outstanding_paint_event(FALSE);
                return 0;
            }

            // Again theoretically this is necessary but in practice it's not.
            // If BlizLogo was ignored then case 1 would drop down here and
            // draw the quotes. Makes me think that the bliz logo was kinda
            // a last second addition or even optional.
            CopyPalette(logical_palette, orig_palette);
            PaletteFadeIn(32);
            force_redraw = FORCE_REDRAW_ALL;
            ClearScreenBuffer();
            CelDraw(64, 639, pCelBuff, 1, 640);
        }
        else
        {
            // We get here when diablo.smk is done
            MemFreeDbg(pMenuGendata1);
            interfac_InitMainMenu();
            LoadPalette("Gendata\\Title.pal", orig_palette);
            gMode = MODE_MAINMENU;
            PaletteFadeOut(32);
            CopyPalette(logical_palette, orig_palette);
            PaletteFadeIn(32);
            force_redraw = FORCE_REDRAW_ALL;
        }

        return 0;
    case WM_DESTROY:
        shouldStopPaintTimer = TRUE;
        timeEndPeriod(paint_event_timer_resolution);
        MemFreeDbg(gpBuffer);
        // This seems like a weird thing to free here of all places
        MemFreeDbg(pLightTbl);
        ShowCursor(TRUE);
        PostQuitMessage(0);
        return 0;
    case WM_DIABPAINT:
        interfac_PaintQuotes(hWnd);
        return 0;
    }

    return DefWindowProcA(hWnd, uMsg, wParam, lParam);
}

// .text:0041A06E
LRESULT WndProc_MainMenu(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    int iOldMenuSel;

    switch (Msg)
    {
    case WM_ACTIVATEAPP:
        if (gbActive)
        {
            ResetPal();
        }
        return 0;
    case WM_DIABDONEFADEIN:
        force_redraw = FORCE_REDRAW_ALL;
        set_outstanding_paint_event(FALSE);
        return 0;
    case WM_MOUSEMOVE:
        MouseX = lParam & 0xFFFF;
        MouseY = (lParam >> 16) & 0xFFFF;
        return 0;
        break;
    case WM_LBUTTONDOWN:
        if (fade_state)
        {
            break;
        }

        iOldMenuSel = menu_selection_index;
        menu_selection_index = 0; // new game
        if (MouseY >= 290)
        {
            if (title_allow_loadgame == TRUE)
            {
                menu_selection_index = 1; // load game
            }
            else
            {
                menu_selection_index = iOldMenuSel;
            }
        }
        if (MouseY >= 335)
        {
            menu_selection_index = 2; // quit
        }

        PlayRndSFX(IS_TITLSLCT);
        if (MouseY >= 290 && menu_selection_index == 0)
        {
            return 0;
        }

        if (menu_selection_index == 0 || menu_selection_index == 1)
        {
            fade_param |= 64; // sentinel used by WM_DIABDONEFADEOUT
        }
        else
        {
            PaletteFadeOut(32);
        }
        fade_state = PALETTE_FADE_OUT;
        delayed_Msg = WM_DIABDONEFADEOUT;
        return 0;
    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_UP:
            PlayRndSFX(IS_TITLEMOV);
            if (menu_selection_index == 0)
            {
                menu_selection_index = 2;
            }
            else
            {
                menu_selection_index--;
            }
            if (menu_selection_index == 1 && !title_allow_loadgame)
            {
                menu_selection_index = 0;
            }
            return 0;
        case VK_DOWN:
            PlayRndSFX(IS_TITLEMOV);
            menu_selection_index++;
            if (menu_selection_index == 1 && !title_allow_loadgame)
            {
                menu_selection_index++;
            }
            if (menu_selection_index > 2)
            {
                menu_selection_index = 0;
            }
            return 0;
        case VK_RETURN:
            PlayRndSFX(IS_TITLSLCT);
            if (menu_selection_index == 0 || menu_selection_index == 1)
            {
                fade_param |= 64; // sentinel used by WM_DIABDONEFADEOUT
            }
            else
            {
                PaletteFadeOut(32);
            }
            fade_state = PALETTE_FADE_OUT;
            delayed_Msg = WM_DIABDONEFADEOUT;
            return 0;
            break;
        }

        break;
    case WM_DIABDONEFADEOUT:
        if (fade_param & 0x40)
        {
            MemFreeDbg(pMenuGendata1);
            MemFreeDbg(pMenuGendata2);
            MemFreeDbg(pMenuGendata3);
            MemFreeDbg(pMenuGendata4);
            MemFreeDbg(pMenuGendata5);
        }

        switch (menu_selection_index)
        {
        case 0: // new game
            interfac_InitNewGame();
            gMode = MODE_NEWGAME;
            force_redraw = FORCE_REDRAW_ALL;
            set_outstanding_paint_event(FALSE);
            break;
        case 1: // load game
            delayed_Msg = WM_DIABLOADGAME;
            InitCutscene();
            fade_state = PALETTE_FADE_OUT;
            gMode = MODE_PROGRESS;
            break;
        case 2: // quit
            interfac_InitDemoEnd();
            LoadPalette("Gendata\\Screen01.pal", orig_palette);
            fade_state = PALETTE_FADE_OUT;
            delayed_Msg = WM_DIABDONEFADEOUT;
            gMode = MODE_DEMO_END;
            break;
            break;
        }

        force_redraw = FORCE_REDRAW_ALL;
        return 0;
    case WM_DESTROY:
        shouldStopPaintTimer = TRUE;
        timeEndPeriod(paint_event_timer_resolution);
        MemFreeDbg(gpBuffer);
        // This seems like a weird thing to free here of all places
        MemFreeDbg(pLightTbl);
        ShowCursor(TRUE);
        PostQuitMessage(0);
        return 0;
    case WM_DIABPAINT:
        interfac_PaintMainMenu(hWnd);
        return 0;
    }

    return DefWindowProcA(hWnd, Msg, wParam, lParam);
}

// .text:0041A5DD
LRESULT WndProc_ChooseClassAndName(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    int class_;
    switch (Msg)
    {
    case WM_ACTIVATEAPP:
        if (gbActive)
        {
            ResetPal();
        }
        return 0;
    case WM_DIABDONEFADEIN:
        return 0;
    case WM_MOUSEMOVE:
        if (!(menu_allow_key_input & 0x80))
        {
            MouseX = lParam & 0xFFFF;
            MouseY = (lParam >> 16) & 0xFFFF;
        }
        return 0;
    case WM_LBUTTONDOWN:
        if (fade_state)
        {
            break;
        }

        // This one screen has two logic states. First, you choose the class from a list of possibilities. The choice is made here based on mouse position.
        // Second, you enter your name. Provided the character name is valid, any mouse click will accept the name and start the new game.
        if (!(menu_allow_key_input & 0x80))
        {
            // Make a selection based on mouse position. This will translate to a class later.
            // These seem arbitrary so I don't know why they didn't just set `class_` here directly.
            // What were 0 and 1 supposed to be (if anything)?
            menu_selection_index = 2; // warrior
            if (MouseY >= 335)
            {
                menu_selection_index = 3; // rogue
            }
            if (MouseY >= 380)
            {
                menu_selection_index = 4; // sorc
            }
            PlayRndSFX(IS_TITLSLCT);
            // Can only choose item 2 (warrior) in the demo. Do nothing if selection is 3 (rogue) or sorc (4)
            if (demo_mode)
            {
                if (menu_selection_index >= 3)
                {
                    return 0;
                }
            }
            // Translate arbitrary menu index into real class.
            if (menu_selection_index == 2)
            {
                class_ = PC_WARRIOR;
            }
            if (menu_selection_index == 3)
            {
                class_ = PC_ROGUE;
            }
            if (menu_selection_index == 4)
            {
                class_ = PC_SORCERER;
            }
            // Initialize most of the player, except the name.
            CreatePlayer(myplr, class_);
            // Proceed to entering the name.
            menu_allow_key_input |= 0x80;
        }
        else
        {
            // `-` is used as default player name meaning "no name was entered".
            // The game does not let you use a hyphen as the first letter in a name.
            if (plr[myplr]._pName[0] != '-')
            {
                PlayRndSFX(IS_TITLSLCT);
                // Finish naming the player and continue new game creation.
                plr[myplr]._pName[player_name_length] = '\0';
                InitQuests();
                PaletteFadeOut(32);
                fade_param |= ~(0x40);
                menu_allow_key_input |= 0x40;
                delayed_Msg = WM_DIABDONEFADEOUT;
            }
        }
        return 0;
    case WM_KEYDOWN:
        // During class selection, up/down changes selection
        if (!(menu_allow_key_input & 0x80))
        {
            switch (wParam)
            {
            case VK_UP:
                PlayRndSFX(IS_TITLEMOV);
                // Cycle through selection 4 -> 3 -> 2 -> 4 ... The choice will translate into class later.
                if (menu_selection_index == 2)
                {
                    menu_selection_index = 4;
                }
                else
                {
                    menu_selection_index--;
                }
                return 0;
            case VK_DOWN:
                PlayRndSFX(IS_TITLEMOV);
                // Cycle through selection 2 -> 3 -> 4 -> 2 ... The choice will translate into class later
                menu_selection_index++;
                if (menu_selection_index > 4)
                {
                    menu_selection_index = 2;
                }
                return 0;
            case VK_RETURN:
                PlayRndSFX(IS_TITLSLCT);
                // Can only choose item 2 (warrior) in the demo. Do nothing if selection is 3 (rogue) or sorc (4)
                if (demo_mode)
                {
                    if (menu_selection_index >= 3)
                    {
                        return 0;
                    }
                }
                // Translate arbitrary menu index into real class.
                if (menu_selection_index == 2)
                {
                    class_ = PC_WARRIOR;
                }
                if (menu_selection_index == 3)
                {
                    class_ = PC_ROGUE;
                }
                if (menu_selection_index == 4)
                {
                    class_ = PC_SORCERER;
                }
                // Initiailize most of the player.
                CreatePlayer(myplr, class_);
                // Proceed to entering the name.
                menu_allow_key_input |= 0x80;
                return 0;
            }
        }

        // Go back to main menu by pressing escape
        if (wParam == VK_ESCAPE)
        {
            PaletteFadeOut(32);
            fade_param &= ~(0x40);
            delayed_Msg = WM_DIABDONEFADEOUT;
        }

        return 0;
    case WM_CHAR:
        // Ignore input during fade
        if (fade_state)
        {
            break;
        }
        // TODO: player name entering logic
        return 0;
    case WM_DIABNEWGAME:
        return 0;
    case WM_DIABDONEFADEOUT:
        // TODO: Check for double free?
        MemFreeDbg(pMenuGendata1);
        MemFreeDbg(pMenuGendata2);
        MemFreeDbg(pMenuGendata3);
        MemFreeDbg(pMenuGendata4);
        MemFreeDbg(pMenuGendata5);

        if (!(menu_allow_key_input & 0x40))
        {
            // Go back to the main menu if the character hasn't entered a name (assumes Escape was pressed.)
            interfac_InitMainMenu();
            LoadPalette("GenData\\Title.pal", orig_palette);
            gMode = MODE_MAINMENU;
            menu_allow_key_input = 0;
            CopyPalette(logical_palette, orig_palette);
            PaletteFadeIn(32);
            force_redraw = FORCE_REDRAW_ALL;
        }
        else
        {
            // If the character has entered a name then start loading the game.
            delayed_Msg = WM_DIABNEWGAME;
            InitCutscene();
            fade_state = PALETTE_FADE_OUT;
            gMode = MODE_PROGRESS;
        }

        return 0;
    case WM_DIABPAINT:
        interfac_PaintMainMenu(hWnd);
        return 0;
    }

    return DefWindowProcA(hWnd, Msg, wParam, lParam);
}

// .text:0041AD3E
// Event handler for promotional slideshow shown on quit.
LRESULT WndProc_QuitSlideshow(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    switch (Msg)
    {
    case WM_ACTIVATEAPP:
        gbActive = wParam;
        if (gbActive)
        {
            ResetPal();
        }
        return 0;
    case WM_DIABDONEFADEIN:
        force_redraw = FORCE_REDRAW_ALL;
        set_outstanding_paint_event(FALSE);
        return 0;
    case WM_DIABMENUTICK:
        // Progress the quote timer and transition when it's done.
        quote_timer--;
        if (quote_timer <= 0)
        {
            PaletteFadeOut(128);
            delayed_Msg = WM_DIABDONEFADEOUT;
        }
        set_outstanding_paint_event(FALSE);
        return 0;
    case WM_LBUTTONDOWN:
    case WM_KEYDOWN:
        // If we're showing the slide (without fade transitions)
        if (fade_state == PALETTE_NO_FADE && quote_timer > 1)
        {
            // Immediately quit on pressing Tab
            if (wParam == VK_TAB)
            {
                menu_selection_index = 6;
            }
            // Otherwise, treat any interaction as the "next slide please" button
            quote_timer = 1;
            set_outstanding_paint_event(FALSE);
            return 0;
        }
        break;
    case WM_DIABDONEFADEOUT:
        // If there are stills screens to show
        if (menu_selection_index < 4)
        {
            // Progress to next screen
            ++menu_selection_index;
            // Load palette
            switch (menu_selection_index)
            {
            case 1:
                LoadPalette("Gendata\\Screen01.pal", orig_palette);
                break;
            case 2:
                LoadPalette("Gendata\\Screen02.pal", orig_palette);
                break;
            case 3:
                LoadPalette("Gendata\\Screen03.pal", orig_palette);
                break;
            case 4:
                LoadPalette("Gendata\\Screen04.pal", orig_palette);
                break;
            case 5:
                LoadPalette("Gendata\\Screen05.pal", orig_palette);
                break;
            case 6:
                // Unused!
                LoadPalette("Gendata\\Screen06.pal", orig_palette);
                break;
            }
            CopyPalette(logical_palette, orig_palette);
            PaletteFadeIn(128);
            force_redraw = FORCE_REDRAW_ALL;
            // Reset timer
            if (debug_mode)
            {
                // 0.5s at 20FPS
                quote_timer = 10;
            }
            else
            {
                // 12.5s at 20FPS
                quote_timer = 250;
            }
            draw_switch_title_option(menu_to_draw);
            set_outstanding_paint_event(FALSE);
            return 0;
        }
        else
        {
            // End of slideshow, shut down game. First, stop music
            if (sghMusic && debugMusicOn)
            {
                SFileDdaEnd(sghMusic);
                SFileCloseFile(sghMusic);
                sghMusic = 0;
            }
            // Free slides
            MemFreeDbg(pMenuGendata1);
            MemFreeDbg(pMenuGendata2);
            MemFreeDbg(pMenuGendata3);
            MemFreeDbg(pMenuGendata4);
            MemFreeDbg(pMenuGendata5);
            // Goodbye!
            DestroyWindow(hWnd);
            return 0;
        }
    case WM_DESTROY:
        shouldStopPaintTimer = TRUE;
        timeEndPeriod(paint_event_timer_resolution);
        MemFreeDbg(gpBuffer);
        // This seems like a weird thing to free here of all places
        MemFreeDbg(pLightTbl);
        ShowCursor(TRUE);
        PostQuitMessage(0);
        return 0;
    case WM_DIABPAINT:
        // Draw slide and post message to progress slide show
        interfac_PaintQuotes(hWnd);
        return 0;
    }

    return DefWindowProcA(hWnd, Msg, wParam, lParam);
}

// .text:0041B180
// Event handler for the game loading screen.
LRESULT ShowProgress(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    int i;

    switch (Msg)
    {
    case WM_ACTIVATEAPP:
        gbActive = wParam;
        if (gbActive)
        {
            ResetPal();
        }
        return 0;
    case WM_DESTROY:
        shouldStopPaintTimer = TRUE;
        timeEndPeriod(paint_event_timer_resolution);
        MemFreeDbg(gpBuffer);
        MemFreeDbg(pLightTbl);
        ShowCursor(TRUE);
        PostQuitMessage(0);
        return 0;
    case WM_DIABLOADGAME:
        set_outstanding_paint_event(FALSE);
        IncProgress();

        // Prevent PaintEventTimer from generating paint events
        pause_paint_timer = TRUE;
        CopyPalette(logical_palette, orig_palette);
        PaletteFadeIn(32); // TODO magic number
        IncProgress();

        CopyPalette(progress_palette, orig_palette);
        LoadGame(TRUE);
        IncProgress();

        CopyPalette(logical_palette, progress_palette);
        fade_state = PALETTE_FADE_OUT;
        IncProgress();

        CopyPalette(logical_palette, orig_palette);
        ApplyGamma(logical_palette, /*start=*/0, /*end=*/256);
        PaletteFadeIn(32);
        force_redraw = FORCE_REDRAW_ALL;
        MemFreeDbg(pMenuGendata1);

        // Allow paint events
        pause_paint_timer = FALSE;

        // Go to game
        gMode = MODE_GAME;
        return 0;
    case WM_DIABNEWGAME:
        set_outstanding_paint_event(FALSE);
        IncProgress();

        // Prevent PaintEventTimer from generating paint events
        pause_paint_timer = TRUE;
        CopyPalette(logical_palette, orig_palette);
        PaletteFadeIn(32); // TODO magic number
        IncProgress();

        CopyPalette(progress_palette, orig_palette);
        InitLevels();
        IncProgress();

        CopyPalette(logical_palette, progress_palette);
        fade_state = PALETTE_FADE_OUT;
        IncProgress();

        CopyPalette(logical_palette, orig_palette);
        ApplyGamma(logical_palette, /*start=*/0, /*end=*/256);
        PaletteFadeIn(32);
        force_redraw = FORCE_REDRAW_ALL;
        MemFreeDbg(pMenuGendata1);

        // Allow paint events
        pause_paint_timer = FALSE;

        // Go to game
        gMode = MODE_GAME;
        break;
    case 0x402:
        set_outstanding_paint_event(FALSE);
        IncProgress();

        // Prevent PaintEventTimer from generating paint events
        pause_paint_timer = TRUE;
        CopyPalette(logical_palette, orig_palette);
        PaletteFadeIn(32); // TODO magic number
        IncProgress();

        for (i = 0; i < gbActivePlayers; i++)
        {
            // TODO
        }

        IncProgress();
        SaveLevel();
        FreeGameMem();
        currlevel++;

        leveltype = plr[myplr]._pLevelTypeTbl[currlevel];
        IncProgress();

        // TODO

        break;
    }

    // TODO other cases

    return DefWindowProc(hWnd, Msg, wParam, lParam);
}

// .text:0041BC8B
// Plays the Blizzard Logo.
//
// This technically blocks until the end of the video, but not completely
// since it runs the event loop. At the end of the video, the quotes are drawn
// and a timer is started.
//
// (Only called if `show_intros == TRUE`)
void interfac_PlayLogo_DrawQuotes()
{
    MSG Msg;

    SVidPlayBegin("gendata\\logo.smk", 0, 0, 0, 0x10080200, &hCurrentVideo); // TODO what is 0x10080200
    while (SVidPlayContinue())
    {
        while (PeekMessageA(&Msg, NULL, 0, 0, PM_REMOVE))
        {
            if (Msg.message == WM_QUIT)
            {
                ExitProcess(0);
            }
            else
            {
                TranslateMessage(&Msg);
                DispatchMessageA(&Msg);
            }
        }
    }
    SVidPlayEnd(hCurrentVideo);

    pMenuGendata1 = LoadFileInMem("Gendata\\Quotes.CEL");
    // This skips parts of WndProc_IntroVid (since this function takes care of
    // that stuff anyway)
    menu_selection_index = 1;
    // The results of this one draw persists for 5s!
    CelDraw(64, 639, pMenuGendata1, 1, 640);
    quote_timer = 100; // 5s at 20FPS
}

// .text:0041BD5E
// Get ready to do main menu stuff:
// - Play music
// - Load menu graphics
// - Find save game file
void interfac_InitMainMenu()
{
    fade_param = 0;
    menu_allow_key_input = 0;

    if (!sghMusic && debugMusicOn)
    {
        SFileOpenFile("Music\\Dintro.WAV", &sghMusic);
        SFileDdaBegin(sghMusic, 0x40000, 0x40000); // TODO magic numbers
    }

    pMenuGendata1 = LoadFileInMem("Gendata\\Titlgray.CEL");
    pMenuGendata2 = LoadFileInMem("Gendata\\Titltext.CEL");
    pMenuGendata3 = LoadFileInMem("Gendata\\Pentitle.CEL");
    pMenuGendata4 = LoadFileInMem("Gendata\\Titlqtxt.CEL");
    pMenuGendata5 = LoadFileInMem("Gendata\\Diabfr.CEL");

    menu_selection_index = 0;
    pentSpinFrame = 1;
    titleLogoFrame = 1;

    Title_FindSaveGame();
    if (did_find_game00)
    {
        title_allow_loadgame = TRUE;
    }

    menu_to_draw = MAINMENU_NEW_LOAD_QUIT;
    draw_switch_title_option(menu_to_draw);
    SetCursor_(CURSOR_HAND);
}

// .text:0041BE60
// Initialize vars for the class/name entry screen
static void interfac_InitNewGame()
{
    // `-` is the "player has not entered a name yet" sentinel
    // This also means that the player cannot be named "-"
    plr[myplr]._pName[0] = '-';
    plr[myplr]._pName[1] = '\0';
    menu_to_draw = MAINMENU_NEWGAME_CLASS;
    menu_allow_key_input = 0;
    menu_selection_index = 2;
}

// .text:0041BEC4
// Get ready to show Screen01 to Screen05 promotional images at the end of the demo
void interfac_InitDemoEnd()
{
    if (!sghMusic && debugMusicOn)
    {
        SFileOpenFile("Music\\Dintro.WAV", &sghMusic);
        SFileDdaBegin(sghMusic, 0x40000, 0x40000);
    }
    pMenuGendata1 = LoadFileInMem("Gendata\\Screen01.CEL");
    pMenuGendata2 = LoadFileInMem("Gendata\\Screen02.CEL");
    pMenuGendata3 = LoadFileInMem("Gendata\\Screen03.CEL");
    pMenuGendata4 = LoadFileInMem("Gendata\\Screen04.CEL");
    pMenuGendata5 = LoadFileInMem("Gendata\\Screen05.CEL");
    menu_selection_index = 0;
    quote_timer = 0;
    menu_to_draw = MAINMENU_DEMO_END;
}

// .text:0041BF78
// Get ready to draw the loading screen (either Cuttt or Cutl1d) based on `delayed_Msg`
void InitCutscene()
{
    // TODO magic numbers galore
    switch (delayed_Msg)
    {
    case 0x402:
        if (plr[myplr]._pLevelTypeTbl[currlevel] == DTYPE_TOWN)
        {
            pMenuGendata1 = LoadFileInMem("Gendata\\Cuttt.CEL");
            LoadPalette("Gendata\\Cuttt.pal", orig_palette);
            pentSpinFrame = 1;
        }
        else
        {
            pMenuGendata1 = LoadFileInMem("Gendata\\Cutl1d.CEL");
            LoadPalette("Gendata\\Cutl1d.pal", orig_palette);
            pentSpinFrame = 0;
        }
        break;
    case 0x403:
        if (plr[myplr]._pLevelTypeTbl[currlevel - 1] == DTYPE_TOWN)
        {
            pMenuGendata1 = LoadFileInMem("Gendata\\Cuttt.CEL");
            LoadPalette("Gendata\\Cuttt.pal", orig_palette);
            pentSpinFrame = 1;
        }
        else
        {
            pMenuGendata1 = LoadFileInMem("Gendata\\Cutl1d.CEL");
            LoadPalette("Gendata\\Cutl1d.pal", orig_palette);
            pentSpinFrame = 0;
        }
        break;
    case 0x405:
        pMenuGendata1 = LoadFileInMem("Gendata\\Cutl1d.CEL");
        LoadPalette("Gendata\\Cutl1d.pal", orig_palette);
        pentSpinFrame = 0;
        break;
    case 0x404:
        pMenuGendata1 = LoadFileInMem("Gendata\\Cutl1d.CEL");
        LoadPalette("Gendata\\Cutl1d.pal", orig_palette);
        pentSpinFrame = 0;
        break;
    case 0x406:
        if (portal_leveltype == DTYPE_TOWN)
        {
            pMenuGendata1 = LoadFileInMem("Gendata\\Cuttt.CEL");
            LoadPalette("Gendata\\Cuttt.pal", orig_palette);
            pentSpinFrame = 1;
        }
        else
        {
            pMenuGendata1 = LoadFileInMem("Gendata\\Cutl1d.CEL");
            LoadPalette("Gendata\\Cutl1d.pal", orig_palette);
            pentSpinFrame = 0;
        }
        break;
    case WM_DIABLOADGAME:
        pMenuGendata1 = LoadFileInMem("Gendata\\Cutl1d.CEL");
        LoadPalette("Gendata\\Cutl1d.pal", orig_palette);
        pentSpinFrame = 0;
        break;
    case WM_DIABNEWGAME:
        pMenuGendata1 = LoadFileInMem("Gendata\\Cuttt.CEL");
        LoadPalette("Gendata\\Cuttt.pal", orig_palette);
        pentSpinFrame = 1;
        break;
    case 0x407:
    case 0x408:
    case 0x409:
    case 0x40A:
    case 0x40B:
        break;
    }

    menu_selection_index = 0;
    quote_timer = 0;
    menu_to_draw = MAINMENU_CUTSCENE;
}
