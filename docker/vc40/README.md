# Visual C++ 4.0

> NOTE: This docker container is not used and only kept for historical purposes

This creates a docker container with:

- Visual C++ 4.0 available in `/MSDEV`
- DirectX 1.0 available in `/GAMESDK`
- Wine, to run Visual C++ 4.0

## Setup

Copy [Visual C++ 4.0] and [DirectX 1.0] into this directory.

[Visual C++ 4.0]: https://winworldpc.com/download/48045f5e-18c3-9a11-c3a4-e284a2c3a570
[DirectX 1.0]: https://archive.org/details/gamesdk

## Build

	docker build -t registry.gitlab.com/moralbacteria/diablo-prdemo-decomp/build .

## Push

	docker login registry.gitlab.com
	docker push registry.gitlab.com/moralbacteria/diablo-prdemo-decomp/build

This image is hosted on this repo: https://gitlab.com/moralbacteria/diablo-prdemo-decomp/container_registry

For help logging into the GitLab registry: https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry