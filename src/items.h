#ifndef __ITEMS_H__
#define __ITEMS_H__

#include "itemdat.h"

#include <windows.h>

//
// defines
//

#define MAXITEMS 127
#define ITEMTYPES 28

//
// enums
//

enum item_quality
{
    ITEM_QUALITY_NORMAL = 0,
    ITEM_QUALITY_MAGIC = 1,
    ITEM_QUALITY_UNIQUE = 2,
};

enum spell_type
{
    RSPLTYPE_SKILL = 0x0,
    RSPLTYPE_SPELL = 0x1,
    RSPLTYPE_SCROLL = 0x2,
    RSPLTYPE_CHARGES = 0x3,
    RSPLTYPE_INVALID = 0x4,
};

//
// structs
//

#pragma pack(push, 8)
struct ItemStruct
{
    int _itype;
    int _ix;
    int _iy;
    BOOL _iAnimFlag;
    BYTE *_iAnimData;
    int _iAnimLen;
    int _iAnimFrame;
    int _iAnimWidth;
    int _iAnimWidth2;
    BOOL _iDelFlag;
    char _iSelFlag;
    BOOL _iPostDraw;
    BOOL _iIdentified;
    char _iMagical;
    char _iName[64];
    char _iIName[64];
    char _iLoc;
    char _iClass;
    char _iCurs;
    int _ivalue; // item value (buy cost)
    int _iIvalue; // identified value (buy cost)
    int _iMinDam;
    int _iMaxDam;
    int _iAC;
    int _iFlags;
    int _iMiscId;
    int _iSpell;
    int _iCharges;
    int _iMaxCharges;
    int _iDurability;
    int _iMaxDur;
    int _iPLDam;
    int _iPLToHit;
    int _iPLAC;
    int _iPLStr;
    int _iPLMag;
    int _iPLDex;
    int _iPLVit;
    int _iPLFR;
    int _iPLLR;
    int _iPLMR;
    int _iPLMana;
    int _iPLHP;
    int _iPLDamMod;
    int _iPLGetHit;
    int _iPLLight;
    char _iSplLvlAdd;
    char _iPLSplCost; // Demo specific
    char _iPLSplDur;  // Demo specific
    char _iPrePower;  // IPL_*
    char _iSufPower;  // IPL_*
    char _iMinStr;
    char _iMinMag;
    char _iMinDex;
    BOOL _iStatFlag;
    int IDidx;     // Index into AllItemsList
    int field_134; // unused?
};
#pragma pack(pop)

//
// variables
//

extern BYTE ItemCAnimTbl[];
extern int ItemInvSnds[];
extern ItemStruct item[];
extern int numitems;

//
// functions
//

void InitItemGFX();
void CalcPlrInv(int p);
void CreatePlrItems(int pnum);
void CalcPlrItemVals(int pnum);
BOOL CalcPlrItemMin(int pnum);
void CalcPlrItemDurs(int pnum);
void UseItem(int p, int Mid, int spl);
void CreateTypeItem(int x, int y, BOOL onlygood, int itype, int imisc);
void CreateRndItem(int x, int y);
void CreateRndUseful(int x, int y);
void DoIdentify();
void DoRepair();
void DoRecharge();
void FreeItemGFX();
void ProcessItems();
void PrintItemPower(char plidx, ItemStruct x);
void CreateItem(int uid, int x, int y);

#endif