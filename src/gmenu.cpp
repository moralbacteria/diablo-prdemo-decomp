// This might be called loadsave? Hard to tell since this has both loadsave and gmenu functions

#include "gmenu.h"

#include "automap.h"
#include "control.h"
#include "cursor.h"
#include "diablo.h"
#include "engine.h"
#include "gendung.h"
#include "inv.h"
#include "lighting.h"
#include "missiles.h"
#include "monster.h"
#include "objects.h"
#include "player.h"
#include "quests.h"

#include <stdio.h>
#include <windows.h>

//
// initialized data (.data:004B8C98)
//

// Maps from font index to bigtgold.cel frame number.
BYTE lfontframe[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 37, 49, 38, 0, 39, 40, 47, 42, 43, 41, 45, 52, 44, 53, 55, 36,
    27, 28, 29, 30, 31, 32, 33, 34, 35, 51, 50, 0, 46, 0,
    54, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 42,
    0, 43, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
    13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    26, 20, 0, 21, 0, 0};
// Maps from bigtgold.cel frame number to character width.
BYTE lfontkern[] = {
    18, 33, 21, 26, 28, 19, 19, 26, 25, 11, 12, 25, 19,
    34, 28, 32, 20, 32, 28, 20, 28, 36, 35, 46, 33, 33,
    24, 11, 23, 22, 22, 21, 22, 21, 21, 21, 32, 10, 20,
    36, 31, 17, 13, 12, 13, 18, 16, 11, 20, 21, 11, 10,
    12, 11, 21, 23};
// TODO change to references based on BORDER_LEFT?
int draw_x_cheats[6] = {226, 258, 285, 253, 113, 275};
char options_with_cheats[6][16] = {
    "New Game",
    "Load Game",
    "Cheats : Off",
    "Save Game",
    "Quit",
    "Cheats : On"};
// TODO change to references based on BORDER_LEFT?
int draw_x[6] = {226, 258, 249, 253, 113, 259};
char options[6][16] = {
    "New Game",
    "Load Game",
    "Music : Off",
    "Save Game",
    "Quit",
    "Music : On"};

//
// uninitialized data (.data:00603EF0)
//

BYTE *sgpLogo;
BYTE *PentSpin_cel;
int qactive_bkup[5];
char sgCurrentMenuIdx;
BOOL title_allow_loadgame;
char gmenuflag;
DWORD memspells_bkup;
BYTE *BigTGold_cel;
BYTE *tbuff;
char PentSpin_frame;
DWORD dword_603F28; // always 5?
// potential for another DWORD?

//
// code (.text:004605A0)
//

// .text:004605A0
void gmenu_init_menu()
{
    gmenuflag = 0;
    sgCurrentMenuIdx = 0;
    PentSpin_frame = 1;
    BigTGold_cel = LoadFileInMem("Data\\BigTGold.CEL");
    PentSpin_cel = LoadFileInMem("Data\\PentSpin.CEL");
    sgpLogo = LoadFileInMem("Data\\DiabSmal.CEL");
}

// .text:004605F2
// Draw `pszStr` at (`x`, `y`) using BigTGold.cel
void gmenu_print_text(int x, int y, const char *pszStr)
{
    int i;
    int length;
    char c;

    length = strlen(pszStr);
    for (i = 0; i < length; i++)
    {
        c = lfontframe[pszStr[i]];
        if (c != 0)
        {
            CelDraw(x, y, BigTGold_cel, c, /*nCelW=*/46);
        }
        x += lfontkern[c] + 2;
    }
}

// gmenu_draw
// gmenu_draw_0 (bad name...)
// gmenu_up
void gmenu_up()
{
    // TODO
}
// gmenu_down
void gmenu_down()
{
    // TODO
}

// .text:00460951
// Parses a BYTE from `tbuff` and advances the iterator.
static char BLoad()
{
    int rv;
    rv = *tbuff;
    tbuff++;
    return rv;
}

// .text:00460977
// Parses a WORD from `tbuff`. The return is sign extended to fit an int.
int WLoad()
{
    int rv;
    if (*tbuff & 0x80)
    {
        rv = 0xFFFF0000;
    }
    else
    {
        rv = 0;
    }
    rv |= *tbuff++ << 8;
    rv |= *tbuff++;
    return rv;
}

// .text:004609D9
// Returns an int from `tbuff`
int ILoad()
{
    int rv;
    rv |= *tbuff++ << 24;
    rv |= *tbuff++ << 16;
    rv |= *tbuff++ << 8;
    rv |= *tbuff++;
    return rv;
}

// .text:00460A40
// Parses a BOOL from `tbuff`.
BOOL OLoad()
{
    char *v = (char *)tbuff++;
    if (*v == TRUE)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

// .text:00460A82
static void LoadPlayer(int i)
{
    // Demo difference! Omit the final 9 (not 10) pointers
    int count = sizeof(*plr) - (9 * sizeof(void *));
    memcpy(&plr[i], tbuff, count);
    tbuff += count;
}

// .text:00460AD5
static void LoadMonster(int i)
{
    int count = sizeof(*monster) - (3 * sizeof(void *));
    memcpy(&monster[i], tbuff, count);
    tbuff += count;
    SyncMonsterAnim(i);
}

// .text:00460B2B
static void LoadMissile(int i)
{
    int count = 152; // TODO magic number
    memcpy(&missile[i], tbuff, count);
    tbuff += count;
    SetMissileGFX(i);
}

// .text:00460B7A
static void LoadSpell(int i)
{
    int count = 108; // TODO magic number
    memcpy(&spells[i], tbuff, count);
    tbuff += count;
    // TODO
}

// .text:00460BCB
static void LoadObject(int i)
{
    int count = 116; // TODO magic number
    memcpy(&object[i], tbuff, count);
    tbuff += count;
}

// LoadItem

static void LoadQuest(int i)
{
    memcpy(&quests[i], tbuff, sizeof(*quests));
    tbuff += sizeof(*quests);
}

// .text: 00460CB1
static void LoadLighting(int i)
{
    int count = 48; // TODO magic number sizeof(LightListStruct)
    memcpy(&LightList[i], tbuff, count);
    tbuff += count;
}

// .text:00460CF8
static void LoadVision(int i)
{
    // TODO
}

// .text:00460D3F
void LoadGame(BOOL firstflag)
{
    int i;

    if (firstflag == FALSE)
    {
        FreeGameMem();
    }

    char path_name[128];

    sprintf(path_name, "%s\\Game00.sav", savedir_abspath);

    HFILE hFile = _lopen(path_name, OF_SHARE_DENY_WRITE | OF_READ);
    LONG size = _llseek(hFile, 0, 2);
    LPVOID lpBuffer = DiabloAllocPtr(size);
    tbuff = (BYTE *)lpBuffer;
    _llseek(hFile, 0, 0);
    _lread(hFile, lpBuffer, size);
    _lclose(hFile);

    // Yes this is literally the decomp.
    if (demo_mode != FALSE)
    {
        tbuff += 3;
    }
    else
    {
        tbuff += 3;
    }

    setlevel = OLoad();
    setlvlnum = WLoad();
    currlevel = WLoad();
    leveltype = WLoad();
    int _ViewX = WLoad();
    int _ViewY = WLoad();
    invflag = OLoad();
    chrflag = OLoad();
    gbActivePlayers = WLoad();
    int _nummonsters = WLoad();
    int _numitems = WLoad();
    int _nummissiles = WLoad();
    int _numspells = WLoad();
    int _nobjects = WLoad();
    plr[0].plractive = OLoad();
    plr[1].plractive = OLoad();
    plr[2].plractive = OLoad();
    plr[3].plractive = OLoad();

    for (i = 0; i < gbActivePlayers; ++i)
    {
        if (plr[i].plractive)
        {
            LoadPlayer(i);
        }
    }

    numquests = WLoad();
    for (i = 0; i < MAXQUESTS; i++)
    {
        LoadQuest(i);
    }

    LoadGameLevel(firstflag, ENTRY_LOAD, firstflag);

    for (i = 0; i < gbActivePlayers; ++i)
    {
        if (plr[i].plractive)
        {
            SyncInitPlr(i);
        }
    }

    ViewX = _ViewX;
    ViewY = _ViewY;
    nummonsters = _nummonsters;
    numitems = _numitems;
    nummissiles = _nummissiles;
    numspells = _numspells;
    nobjects = _nobjects;

    for (i = 0; i < MAXMONSTERS; i++)
    {
        monstkills[i] = ILoad();
    }

    if (leveltype != DTYPE_TOWN)
    {
        // Monsters
        for (i = 0; i < MAXMONSTERS; i++)
        {
            monstactive[i] = WLoad();
        }
        for (i = 0; i < nummonsters; i++)
        {
            LoadMonster(monstactive[i]);
        }

        // Missiles
        for (i = 0; i < MAXMISSILES; ++i)
        {
            missileactive[i] = BLoad();
        }
        for (i = 0; i < MAXMISSILES; ++i)
        {
            missileavail[i] = BLoad();
        }
        for (i = 0; i < nummissiles; ++i)
        {
            LoadMissile(missileactive[i]);
        }

        // Spells
        for (i = 0; i < MAX_SPELLS; ++i)
        {
            spellsactive[i] = BLoad();
        }
        for (i = 0; i < MAX_SPELLS; ++i)
        {
            availspells[i] = BLoad();
        }
        for (i = 0; i < numspells; ++i)
        {
            LoadSpell(spellsactive[i]);
        }

        // Objects
        for (i = 0; i < MAXOBJECTS; ++i)
        {
            objectactive[i] = BLoad();
        }
        for (i = 0; i < MAXOBJECTS; ++i)
        {
            objectavail[i] = BLoad();
        }
        for (i = 0; i < nobjects; ++i)
        {
            LoadObject(objectactive[i]);
        }
        for (i = 0; i < nobjects; ++i)
        {
            SyncObjectAnim(objectactive[i]);
        }

        // Lights
        nextLightId = WLoad();
        numlights = WLoad();
        for (i = 0; i < numlights; ++i)
        {
            LoadLighting(i);
        }

        // Vision
        visionid = WLoad();
        numvision = WLoad();
        for (i = 0; i < numvision; ++i)
        {
            LoadVision(i);
        }

        // Items


        // TODO
    }

    // TODO

    MemFreeDbg(lpBuffer);
    CalcPlrItemDurs(myplr);
    AutomapZoomReset();
    savecrsr_hide();
    SetCursor_(CURSOR_NONE);
    savecrsr_show();
}

// BSave
// WSave
// ISave
// OSave
// SavePlayer
// SaveMonster
// SaveMissile
// SaveSpell
// SaveObject
// SaveItem
// SaveQuest
// SaveLighting
// SaveVision
// save_game
// gmenu_send_quit_cmd
// gmenu_select_option
void gmenu_select_option()
{
    // TODO
}
// gmenu_left_mouse
void gmenu_left_mouse()
{
    // TODO
}
// FreeGMenu
void FreeGMenu()
{
    // TODO
}

// SaveLevel
void SaveLevel()
{
    // TODO
}

// LoadLevel

// END FUNCTIONS (no functions after this point)