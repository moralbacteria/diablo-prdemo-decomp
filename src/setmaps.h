// From http://www.graybeardgames.com/download/diablo_pitch.pdf:
//
// "The heart of Diablo is the randomly created dungeon. A new dungeon level is
// generated each time a level is entered, using our Dynamic Random Level
// Generation (DRLG) System. Rooms, corridors, traps, treasures, monsters and
// stairways will be randomly placed, providing a new gaming experience every
// time Diablo is played. In addition to random halls and rooms, larger 'set
// piece' areas, like a maze or a crypt complex, will be pre-designed and
// appear intact in the levels."
//
// In code, this is interchangably called a "set piece" or "set map" or
// "set level"

#ifndef __SETMAPS_H__
#define __SETMAPS_H__

// IDs for all set levels
enum _setlevels
{
    SL_NONE = 0x0,      // No set level (e.g. the Butcher's room is part of dlvl 1, the magic rock is found in dlvl 5)
    SL_SKELKING = 0x1,  // The Skeleton King's Lair
    SL_BONECHAMB = 0x2, // The Bone Chamber
    SL_MAZE = 0x3,      // The Maze
};

#endif