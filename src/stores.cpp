#include "stores.h"

#include <cstdio>

#include "control.h"
#include "diablo.h"
#include "engine.h"
#include "gendung.h"
#include "inv.h"
#include "items.h"
#include "player.h"
#include "quests.h"

#define STORE_LINES 24

enum text_color
{
    COL_WHITE = 0x0,
    COL_BLUE = 0x1,
    COL_RED = 0x2,
    COL_GOLD = 0x3,
};

struct STextStruct
{
    int _sx;
    int _syoff;
    char _sstr[128];
    BOOL _sjust;
    char _sclr;
    BOOL _sline;
    BOOL _ssel;
    int _sval;
};

enum talk_id
{
    STORE_NONE = 0x0,
    STORE_SMITH = 0x1,
    STORE_SBUY = 0x2,
    STORE_SSELL = 0x3,
    STORE_SREPAIR = 0x4,
    STORE_WITCH = 0x5,
    STORE_WBUY = 0x6,
    STORE_WSELL = 0x7,
    STORE_WRECHARGE = 0x8,
    STORE_NOMONEY = 0x9,
    STORE_NOROOM = 0xA,
    STORE_CONFIRM = 0xB,
    STORE_BOY = 0xC,
    STORE_BBOY = 0xD,
};

//
// initialized variables (.data:00428750)
//

// Divide the screen into 24 lines of text, each 12px tall.
//
// SStringY[i] is the y location in to draw line i of text. This matches up with
// the rest of the store graphics.
//
// SStringY[i] = i*12
int SStringY[STORE_LINES] = {
    0,
    12,
    24,
    36,
    48,
    60,
    72,
    84,
    96,
    108,
    120,
    132,
    144,
    156,
    168,
    180,
    192,
    204,
    216,
    228,
    240,
    252,
    264,
    276,
};

//
// uninitialized variables (.data:???)
//

// ...
BYTE *pSTextBoxCels; // .data:005EC000
ItemStruct boyitem;  // .data:005EC008
// ...
BYTE *pSTextSlidCels; // .data:005EC168
char stextscrldbtn;   // .data:005EC16C
// ...
int stextsel;       // .data:005EF230
char stextscrlubtn; // .data:005EF234
BOOL stextscrl;     // (.data:005EF238) what do?
// ...
char stextsize;                 // (.data:005EF240) either 0 or 1; if 0 means take up the right half of the screen, 1 means take up the whole screen
STextStruct stext[STORE_LINES]; // .data:005EF248
// ...
char stextflag; // .data:005F00F4
// ...
ItemStruct golditem;  // .data:005F0100
int storenumh;        // .data:005F0238
int stextshold;       // (.data:005F023C) The previous STORE_* value
BYTE *pSPentSpn2Cels; // .data:005F0240
//...

//
// code (.text:00428750)
//

// .text:00428750
void InitStores()
{
    pSTextBoxCels = LoadFileInMem("Data\\TextBox2.CEL");
    pSPentSpn2Cels = LoadFileInMem("Data\\PentSpn2.CEL");
    pSTextSlidCels = LoadFileInMem("Data\\TextSlid.CEL");
    ClearSText(0, STORE_LINES);
    stextflag = 0;
    if (currlevel == 0 && debug_mode)
    {
        StartStore(STORE_SMITH);
    }
    // TODO
}

// SetupTownStores	00000000004287DF
void SetupTownStores()
{
    // TODO
}

// FreeStoreMem	0000000000428875
void FreeStoreMem()
{
    // TODO
}
// DrawSTextBack	00000000004288F7

// .text:00428966
void PrintSString(int x, int y, BOOL cjustflag, const char *str, char col, int val)
{
    // TODO
}

// .text:00428C5B
void DrawSLine(int y)
{
    // TODO
}

// DrawSArrows	0000000000428D07
// DrawSTextUniq	0000000000428ECB

// .text:00428EEC
void DrawSTextHelp()
{
    stextsize = 1;
    stextsel = -1;
}

// .text:00428F0D
void ClearSText(int s, int e)
{
    int i;

    for (i = s; i < e; i++)
    {
        stext[i]._sx = 0;
        stext[i]._syoff = 0;
        stext[i]._sstr[0] = 0;
        stext[i]._sjust = FALSE;
        stext[i]._sclr = 0; // TODO magic number
        stext[i]._sline = FALSE;
        stext[i]._ssel = FALSE;
        stext[i]._sval = -1;
    }
}

// .text:00428FFF
static void AddSLine(int y)
{
    stext[y]._sx = 0;
    stext[y]._syoff = 0;
    stext[y]._sstr[0] = '\0';
    stext[y]._sline = TRUE;
}

// .text:00429072
static void AddSTextVal(int y, int val)
{
    stext[y]._sval = val;
}

// .text:004290A2
static void OffsetSTextY(int y, int yo)
{
    stext[y]._syoff = yo;
}

// .text:004290D2
// @param x The x offset from the box top-left or line center (if justified)
// @param y The y location in store lines; there are 24 lines, each 12px tall
// @param j Center-justify?
// @param str The string to add
// @param slr The color (a palette entry, typically [0..3])
// @param sel Whether this item is selectable by the player
static void AddSText(int x, int y, BOOL j, const char *str, char clr, BOOL sel)
{
    stext[y]._sx = x;
    stext[y]._syoff = 0;
    strcpy(stext[y]._sstr, str);
    stext[y]._sjust = j;
    stext[y]._sclr = clr;
    stext[y]._sline = 0;
    stext[y]._ssel = sel;
}

// .text:0042919D
static void PrintStoreItem(int l, char iclr, ItemStruct x)
{
    char sstr[128];

    sstr[0] = '\0';
    if (x._iMagical != ITEM_QUALITY_UNIQUE && x._iPrePower != -1 && x._iIdentified)
    {
        PrintItemPower(x._iPrePower, x);
        strcat(sstr, tempstr);
    }

    // BUG: uniques with sufpower?
    if (x._iSufPower != -1 && x._iIdentified)
    {
        PrintItemPower(x._iSufPower, x);
        if (sstr[0])
        {
            strcat(sstr, ",  ");
        }
        strcat(sstr, tempstr);
    }

    if (x._iMiscId == IMISC_STAFF && x._iMaxCharges)
    {
        sprintf(tempstr, "Charges : %i/%i", x._iCharges, x._iMaxCharges);
        if (sstr[0])
        {
            strcat(sstr, ",  ");
        }
        strcat(sstr, tempstr);
    }

    if (sstr[0])
    {
        AddSText(40, l, FALSE, sstr, iclr, FALSE);
        l++;
    }

    if (x._iMaxDur == 1000 || x._iMaxDur == 0) // TODO magic numbers
    {
        strcat(sstr, "Indestructible,  ");
    }
    else
    {
        sprintf(sstr, "Durability : %i/%i,  ", x._iDurability, x._iMaxDur);
    }

    if (x._itype == ITYPE_MISC)
    {
        sstr[0] = '\0';
    }

    if ((x._iMinStr + x._iMinMag + x._iMinDex) == 0)
    {
        strcat(sstr, "No required attributes");
    }
    else
    {
        strcpy(tempstr, "Required:");
        if (x._iMinStr)
        {
            sprintf(tempstr, "%s %i Str", tempstr, x._iMinStr);
        }
        if (x._iMinMag)
        {
            sprintf(tempstr, "%s %i Mag", tempstr, x._iMinMag);
        }
        if (x._iMinDex)
        {
            sprintf(tempstr, "%s %i Dex", tempstr, x._iMinDex);
        }
        strcat(sstr, tempstr);
    }

    AddSText(40, l, FALSE, sstr, iclr, FALSE);
}

// S_StartSmith	0000000000429479
static void S_StartSmith()
{
    // TODO
}

// S_ScrollSBuy	000000000042954F
// S_StartSBuy	0000000000429708
static void S_StartSBuy()
{
    // TODO
}

// SmithSellOk	0000000000429825
// S_ScrollSSell	0000000000429A09
// S_StartSSell	0000000000429BFE
static void S_StartSSell()
{
    // TODO
}

// SmithRepairOk	0000000000429F17
// AddStoreHoldRepair	000000000042A084
// S_StartSRepair	000000000042A23B
static void S_StartSRepair()
{
    // TODO
}

// S_StartWitch	000000000042A714
static void S_StartWitch()
{
    // TODO
}

// S_ScrollWBuy	000000000042A7D3
// S_StartWBuy	000000000042A992
static void S_StartWBuy()
{
    // TODO
}
// WitchSellOk	000000000042AAB9
// S_StartWSell	000000000042ABEF
static void S_StartWSell()
{
    // TODO
}
// WitchRechargeOk	000000000042AF08
// AddStoreHoldRecharge	000000000042AFC0
// S_StartWRecharge	000000000042B12D
static void S_StartWRecharge()
{
    // TODO
}

// S_StartNoMoney	000000000042B420
static void S_StartNoMoney()
{
    // TODO
}

// S_StartNoRoom	000000000042B472
static void S_StartNoRoom()
{
    // TODO
}

// .text:0042B4BD
static void S_StartConfirm()
{
    char iclr;

    StartStore(stextshold);
    stextscrl = FALSE;
    ClearSText(5, 23);

    // Determine color to use. Start out as white (normal)
    iclr = COL_WHITE;
    // Magical? blue
    if (plr[myplr].HoldItem._iMagical != ITEM_QUALITY_NORMAL)
    {
        iclr = COL_BLUE;
    }
    // can't use? red
    if (!plr[myplr].HoldItem._iStatFlag)
    {
        iclr = COL_RED;
    }

    if (plr[myplr].HoldItem._iMagical != ITEM_QUALITY_NORMAL)
    {
        AddSText(20, 8, FALSE, plr[myplr].HoldItem._iIName, iclr, FALSE);
    }
    else
    {
        AddSText(20, 8, FALSE, plr[myplr].HoldItem._iName, iclr, FALSE);
    }

    AddSTextVal(8, plr[myplr].HoldItem._iIvalue);
    PrintStoreItem(9, iclr, plr[myplr].HoldItem);
    switch (stextshold)
    {
    case STORE_SBUY:
    case STORE_WBUY:
        strcpy(tempstr, "Are you sure you want to buy this item?");
        break;
    case STORE_SSELL:
    case STORE_WSELL:
        strcpy(tempstr, "Are you sure you want to sell this item?");
        break;
    case STORE_SREPAIR:
        strcpy(tempstr, "Are you sure you want to repair this item?");
        break;
    case STORE_WRECHARGE:
        strcpy(tempstr, "Are you sure you want to recharge this item?");
        break;
    case STORE_BBOY:
        strcpy(tempstr, "Do we have a deal?");
        break;
    }
    AddSText(0, 15, TRUE, tempstr, COL_WHITE, FALSE);

    AddSText(0, 18, TRUE, "Yes", COL_WHITE, TRUE);
    AddSText(0, 20, TRUE, "No", COL_WHITE, TRUE);
}

// .text:0042B75D
static void S_StartBoy()
{
    stextsize = 0;
    stextscrl = FALSE;

    AddSText(0, 2, TRUE, "Pegged leg boy", COL_GOLD, FALSE);
    AddSLine(5);
    if (boyitem._itype != ITYPE_NONE)
    {
        AddSText(0, 8, TRUE, "Pssst. Over here.", COL_GOLD, FALSE);
        AddSText(0, 10, TRUE, "I have something for sale.", COL_GOLD, FALSE);
        AddSText(0, 12, TRUE, "It will cost 50 gold", COL_GOLD, FALSE);
        AddSText(0, 14, TRUE, "just to start looking. ", COL_GOLD, FALSE);
        AddSText(0, 18, TRUE, "Yes, I would like to look", COL_WHITE, TRUE);
        AddSText(0, 20, TRUE, "No, I'll pass", COL_WHITE, TRUE);
    }
    else
    {
        AddSText(0, 12, TRUE, "I have nothing for you.", COL_GOLD, FALSE);
        AddSText(0, 16, TRUE, "Leave", COL_WHITE, TRUE);
    }
}

// .text:0042B869
static void S_StartBBoy()
{
    int iclr;

    stextsize = 1;
    stextscrl = FALSE;

    sprintf(tempstr, "I have this item for sale :           Your gold : %i", plr[myplr]._pGold);
    AddSText(0, 1, TRUE, tempstr, COL_GOLD, FALSE);
    AddSLine(3);
    AddSLine(21);

    // Determine color to use. Start out as white (normal)
    iclr = COL_WHITE;
    // Magical? blue
    if (boyitem._iMagical != ITEM_QUALITY_NORMAL)
    {
        iclr = COL_BLUE;
    }
    // can't use? red
    if (!boyitem._iStatFlag)
    {
        iclr = COL_RED;
    }

    // Magical? Use the identified name. Otherwise use normal name
    if (boyitem._iMagical != ITEM_QUALITY_NORMAL)
    {
        AddSText(20, 10, FALSE, boyitem._iIName, iclr, TRUE);
    }
    else
    {
        AddSText(20, 10, FALSE, boyitem._iName, iclr, TRUE);
    }
    // Item costs 1.5x more!
    AddSTextVal(10, boyitem._iIvalue + (boyitem._iIvalue / 2));
    PrintStoreItem(11, iclr, boyitem);

    AddSText(0, 22, TRUE, "Leave", COL_WHITE, TRUE);
    OffsetSTextY(22, 6);
}

// .text:0042B9CD
void StartStore(char s)
{
    int i;

    if (invflag)
    {
        invflag = FALSE;
    }
    if (chrflag)
    {
        chrflag = FALSE;
    }
    if (questlog)
    {
        questlog = FALSE;
    }
    ClearSText(0, STORE_LINES);
    ReleaseStoreBtn();
    switch (s)
    {
    case STORE_SMITH:
        S_StartSmith();
        break;

    case STORE_SBUY:
        if (storenumh > 0)
        {
            S_StartSBuy();
        }
        break;
    case STORE_SSELL:
        S_StartSSell();
        break;
    case STORE_SREPAIR:
        S_StartSRepair();
        break;
    case STORE_WITCH:
        S_StartWitch();
        break;
    case STORE_WBUY:
        if (storenumh > 0)
        {
            S_StartWBuy();
        }
        break;
    case STORE_WSELL:
        S_StartWSell();
        break;
    case STORE_WRECHARGE:
        S_StartWRecharge();
        break;
    case STORE_NOMONEY:
        S_StartNoMoney();
        break;
    case STORE_NOROOM:
        S_StartNoRoom();
        break;
    case STORE_CONFIRM:
        S_StartConfirm();
        break;
    case STORE_BOY:
        S_StartBoy();
        break;
    case STORE_BBOY:
        S_StartBBoy();
        break;
    }

    for (i = 0; i < STORE_LINES; i++)
    {
        if (stext[i]._ssel)
            break;
    }

    if (i == STORE_LINES)
    {
        stextsel = -1;
    }
    else
    {
        stextsel = i;
    }

    stextflag = s;
}

// DrawSText	000000000042BB93
// STextESC	000000000042BD3F
void STextESC()
{
    // TODO
}
// STextUp	000000000042BE77
void STextUp()
{
    // TODO
}

// .text:0042BF89
void STextDown()
{
    // TODO
}

// S_SmithEnter	000000000042C09F

// SetGoldCurs	000000000042C131
void SetGoldCurs(int i)
{
    // TODO
}

// TakePlrsMoney	000000000042C244
void TakePlrsMoney(int i)
{
    // TODO
}

// SmithBuyItem	000000000042C535
// S_SBuyEnter	000000000042C6E4
// StoreGoldFit	000000000042C87E
// PlaceStoreGold	000000000042CADE
// StoreSellItem	000000000042CC67
// S_SSellEnter	000000000042CF1B
// SmithRepairItem	000000000042CFEB
// S_SRepairEnter	000000000042D1E7
// S_WitchEnter	000000000042D2DF
// WitchBuyItem	000000000042D371
// S_WBuyEnter	000000000042D4E8
// S_WSellEnter	000000000042D682
// WitchRechargeItem	000000000042D752
// S_WRechargeEnter	000000000042D875
// S_BoyEnter	000000000042D96D
// BoyBuyItem	000000000042D9FB
// S_BBuyEnter	000000000042DACD
// S_ConfirmEnter	000000000042DC61

// STextEnter	000000000042DD4A
void STextEnter()
{
    // TODO
}

// CheckStoreBtn	000000000042DE58
void CheckStoreBtn()
{
    // TODO
}

// .text:0042E057
void ReleaseStoreBtn()
{
    stextscrlubtn = -1;
    stextscrldbtn = -1;
}