#include "gendung.h"

#include "engine.h"

//
// uninitialized variables (.data:004CB0D8)
//

BYTE *pLevelPieces; // .MIN
// Current dungeon level. 0 is town, 17 doesn't exist
int currlevel;
// NOTE: In Devilution, this is set but never used...
// This is set during stairs generation then checked before/after InitMonsters...
int LvlViewX;
// NOTE: In Devilution, this is set but never used...
int LvlViewY;
BOOL nTransTable[MAXTILES + 1];
char dItem[MAXDUNX][MAXDUNY];
BOOL nSolidTable[MAXTILES + 1];
int dmaxx;
int dmaxy;
// There's enough room for 3 ints here so it's probably not alignment...
int dword_4D21FC; // unused 1
int dword_4D2200; // unused 2
int dword_4D2204; // unused 3
WORD level_frame_types[MAXTILES];
int level_frame_count[MAXTILES];
// The next transparency index into TransList available for use.
char TransVal;
char dPlayer[MAXDUNX][MAXDUNY];
int dminx;
BYTE *pSpeedCels;
int dminy;
ScrollStruct ScrollInfo;
char dFlags[MAXDUNX][MAXDUNY]; // NOTE: dflags (note case!) exists in retail, I can't find it in demo...
int SpeedFrameTbl[128][16];
char dPlrPath[MAXDUNX][MAXDUNY]; // not in Devilution? something path related
// Precalculated dLight values, based on level gen?
char dPreLight[MAXDUNX][MAXDUNY];
BYTE *pDungeonCels;
int portal_leveltype;
int leveltype;
// Specifies the transparency at each coordinate of the map.
// This stores an index into TransList.
// Multiple (x,y) locations can belong to the same "transparency group" (the same TransList index)
char dTransVal[MAXDUNX][MAXDUNY];
int ViewX;
int ViewY;
int portal_ViewX; // demo specific
int portal_ViewY; // demo specific
// This is used by the level generator to build levels. It is then copied to pdungeon for... some reason...
// It is described with megatiles (from the .TIL file). These will be translated into tiles and stored into dPiece.
// After level generation, this is unused?
BYTE dungeon[DMAXX][DMAXY];
char block_lvid[MAXTILES + 1];
int tile_defs[MAXTILES];
BYTE *pSpecialCels;
int setlvlnum;
char dDead[MAXDUNX][MAXDUNY];
BOOL portal_setlevel; // demo specific
// Specifies the active transparency indices.
// Each index is a unique "group" of transparency that can be turned on or off.
// These groups are determined during level generation and static during gameplay.
// A single index can be represented multiple times in dTransVal.
BOOL TransList[256];
int dPiece[MAXDUNX][MAXDUNY];
char dSpecial[MAXDUNX][MAXDUNY];
int portal_currlevel_or_setlvlnum; // demo specific
char dLight[MAXDUNX][MAXDUNY];
BOOL nMissileTable[MAXTILES + 1];
BOOL nTrapTable[MAXTILES + 1];
int MouseOffX; // demo specific
int MouseOffY; // demo specific
// Width of game view in tiles
int ViewBX;
// Height of game view in tiles
int ViewBY;
// Width of game view in px
int ViewDX;
// Height of a setpiece in mega tile coordinates
int setpc_h;
// Height of game view in px
int ViewDY;
// Width of a setpiece in mega tile coordinates
int setpc_w;
// X location of a setpiece in mega tile coordinates
int setpc_x;
// Y location of a setpiece in mega tile coordinates
int setpc_y;
BOOL portal_save_load_level; // demo specific
MICROS dpiece_defs_map_1[MAXDUNX * MAXDUNY];
int level_frame_sizes[MAXTILES];
int dMonster[MAXDUNX][MAXDUNY];
char dObject[MAXDUNX][MAXDUNY];
BOOL setlevel;
BOOL nBlockTable[MAXTILES + 1];
char dMissile[MAXDUNX][MAXDUNY];
MICROS dpiece_defs_map_2[MAXDUNX][MAXDUNY];
// This looks like a copy of dungeon made after level generation?
BYTE pdungeon[DMAXX][DMAXY];
int nlevel_frames;
// .TIL file loaded into heap. Each record defines a collection of 4 tiles that form a recognizable structure, e.g. a corner of a wall, a door, etc.
// Each record is four WORDs; each WORD is a tile.
BYTE *pMegaTiles;

//
// code (.text:0040CF40)
//

// .text:0040CF40
// Initialize n*Table from the appropriate .SOL file (based on leveltype).
// The .SOL file contains information about
void FillSolidBlockTbls()
{
    DWORD dwTiles; // Number of bytes in pSBFile
    BYTE *pTmp;    // pSBFile iterator
    BYTE *pSBFile; // COntents of the .SOL file on heap
    int i;
    BYTE bv; // A byte read from the .SOL file

    // Initilaze with a clean slate
    for (i = 0; i < MAXTILES; i++)
    {
        nBlockTable[i] = FALSE;
        nSolidTable[i] = FALSE;
        nTransTable[i] = FALSE;
        nMissileTable[i] = FALSE;
        nTrapTable[i] = FALSE;
    }

    // Not sure why index 0 is treated specially but i=1 in the for loop below...
    nBlockTable[0] = FALSE;
    nSolidTable[0] = FALSE;
    nMissileTable[0] = FALSE;

    // Load the correct .SOL for the level into the heap.
    // The .SOL file contains flags for each tile that describe how it interacts with gameplay. E.g. is it solid? Does it stop missiles?
    switch (leveltype)
    {
    case DTYPE_TOWN:
        pSBFile = LoadFileInMem("Levels\\TownData\\Town.SOL");
        dwTiles = FileGetSize("Levels\\TownData\\Town.SOL");
        break;
    case DTYPE_OLD_CATHEDRAL:
        pSBFile = LoadFileInMem("Levels\\L1Data\\L1.SOL");
        dwTiles = FileGetSize("Levels\\L1Data\\L1.SOL");
        break;
    case DTYPE_CATACOMBS:
        pSBFile = LoadFileInMem("Levels\\L2Data\\L2.SOL");
        dwTiles = FileGetSize("Levels\\L2Data\\L2.SOL");
        break;
    case DTYPE_CAVES:
        pSBFile = LoadFileInMem("Levels\\L3Data\\L3.SOL");
        dwTiles = FileGetSize("Levels\\L3Data\\L3.SOL");
        break;
    case DTYPE_HELL:
        pSBFile = LoadFileInMem("Levels\\L2Data\\L2.SOL");
        dwTiles = FileGetSize("Levels\\L2Data\\L2.SOL");
        break;
    case DTYPE_CATHEDRAL:
        pSBFile = LoadFileInMem("Levels\\L1Data\\L1.SOL");
        dwTiles = FileGetSize("Levels\\L1Data\\L1.SOL");
        break;
    }

    // Each entry in the file is a byte. Each bit of that byte is a flag.
    pTmp = pSBFile;
    for (i = 1; dwTiles >= i; i++)
    {
        bv = *pTmp++;
        // TODO magic numbers
        if (bv & 1)
        {
            nSolidTable[i] = TRUE;
        }
        if (bv & 2)
        {
            nBlockTable[i] = TRUE;
        }
        if (bv & 4)
        {
            nMissileTable[i] = TRUE;
        }
        if (bv & 8)
        {
            nTransTable[i] = TRUE;
        }
        if (bv & 0x80)
        {
            nTrapTable[i] = TRUE;
        }
        block_lvid[i] = (bv & 0x70) >> 4;
    }

    MemFreeDbg(pSBFile);
}

// .text:0040D1BE
// Same as Devilution
static void SwapTile(int f1, int f2)
{
    int swap;

    swap = level_frame_count[f1];
    level_frame_count[f1] = level_frame_count[f2];
    level_frame_count[f2] = swap;
    swap = tile_defs[f1];
    tile_defs[f1] = tile_defs[f2];
    tile_defs[f2] = swap;
    swap = level_frame_types[f1];
    level_frame_types[f1] = level_frame_types[f2];
    level_frame_types[f2] = swap;
    swap = level_frame_sizes[f1];
    level_frame_sizes[f1] = level_frame_sizes[f2];
    level_frame_sizes[f2] = swap;
}

// .text:0040D295
// Basically the same as Devilution
static void SortTiles(int in_frames)
{
    BOOL doneflag;
    int i;
    int frames;

    frames = in_frames;

    doneflag = FALSE;
    while (frames > 0 && !doneflag)
    {
        doneflag = TRUE;
        for (i = 0; i < frames; i++)
        {
            if (level_frame_count[i] < level_frame_count[i + 1])
            {
                SwapTile(i, i + 1);
                doneflag = FALSE;
            }
        }
        frames--;
    }
}

// .text:0040D328
void MakeSpeedCels()
{
    int i;
    for (i = 0; i < MAXTILES; i++)
    {
        tile_defs[i] = i;
        level_frame_count[i] = 0;
    }

    // TODO
}

// .text:0040D804
int IsometricCoord(int x, int y)
{
    // TODO
    return 0;
}

// RotateMicros	000000000040D89C
// SetDungeonMicros	000000000040D926

// .text:0040DAAA
// Initiailize dTransVal, TransList, and TransVal.
void DRLG_InitTrans()
{
    int x;
    int y;

    for (y = 0; y < MAXDUNY; y++)
    {
        for (x = 0; x < MAXDUNX; x++)
        {
            dTransVal[x][y] = 0;
        }
    }

    // BUG? TransList has 256 elements
    for (x = 0; x < 255; x++)
    {
        TransList[x] = FALSE;
    }

    TransVal = 1;
}

// .text:0040DB47
// Defines a rectangle bounded by x1, y1, x2, y2 as a new transparency group.
// x1/y1/x2/y2 are in megatile coordinates [0..40]
//
// Side effects:
// - dTransVal set
// - TransVal incremented
void DRLG_MRectTrans(int x1, int y1, int x2, int y2)
{
    int i, j;

    // Translate megatile coordinates into minitile coordinates and account for padding
    // TODO why the +1 offset for x1/y1?
    x1 = 2 * x1 + MINITILE_PADDING_X + 1;
    y1 = 2 * y1 + MINITILE_PADDING_Y + 1;
    x2 = 2 * x2 + MINITILE_PADDING_X;
    y2 = 2 * y2 + MINITILE_PADDING_Y;

    // Record the transparency group
    for (j = y1; j <= y2; j++)
    {
        for (i = x1; i <= x2; i++)
        {
            dTransVal[i][j] = TransVal;
        }
    }

    // Next time this is called, use a new transparency group
    TransVal++;
}

// .text:0040DBEE
// Copy a transparency group from (x1, y1) to (x2, y2).
// x1/y1/x2/y2 are in megatile coordinates [0..40]
// Since this translates megatile coordinates into minitiles, the BOOLs determine which mini tiles should get the transparency group.
// NOTE: This is dead code.
void __dc_gendung_40DBEE(int x1, int y1, int x2, int y2, BOOL topleft, BOOL topright, BOOL bottomleft, BOOL bottomright)
{
    BYTE transval;

    // Get the reference value from the top left mini tile of the mega tile.
    x1 = x1 * 2 + MINITILE_PADDING_X;
    y1 = y1 * 2 + MINITILE_PADDING_Y;
    transval = dTransVal[x1][y1];

    // Copy the value into the decomposed mini tile coordinates (based on BOOLs)
    x2 = x2 * 2 + MINITILE_PADDING_X;
    y2 = y2 * 2 + MINITILE_PADDING_Y;
    if (topleft)
    {
        dTransVal[x2][y2] = transval;
    }
    if (topright)
    {
        dTransVal[x2 + 1][y2] = transval;
    }
    if (bottomleft)
    {
        dTransVal[x2][y2 + 1] = transval;
    }
    if (bottomright)
    {
        dTransVal[x2 + 1][y2 + 1] = transval;
    }
}

// .text:0040DCE1
// TODO document the Trans functions
// x1/y1/x2/y2 are in mini tile coordinates [0..112]
void DRLG_RectTrans(int x1, int y1, int x2, int y2)
{
    int i, j;

    for (j = y1; j <= y2; j++)
    {
        for (i = x1; i <= x2; i++)
        {
            dTransVal[i][j] = TransVal;
        }
    }
    TransVal++;
}

// .text:0040DD5C
// Copy the trans value from (sx, sy) to (dx, dy)
// x1/y1/x2/y2 are in mini tile coordinates [0..112]
void DRLG_CopyTrans(int sx, int sy, int dx, int dy)
{
    dTransVal[dx][dy] = dTransVal[sx][sy];
}

// .text:0040DDA5
void DRLG_ListTrans(int num, BYTE *List)
{
    BYTE x1, x2, y1, y2;
    int i;

    for (i = 0; i < num; i++)
    {
        x1 = *List++;
        y1 = *List++;
        x2 = *List++;
        y2 = *List++;
        DRLG_RectTrans(x1, y1, x2, y2);
    }
}

// .text:0040DE27
void DRLG_AreaTrans(int num, BYTE *List)
{
    BYTE x1, x2, y1, y2;
    int i;

    for (i = 0; i < num; i++)
    {
        x1 = *List++;
        y1 = *List++;
        x2 = *List++;
        y2 = *List++;
        DRLG_RectTrans(x1, y1, x2, y2);
        TransVal--;
    }
    TransVal++;
}

// .text:0040DEB5
// Mark a rectangle bounded by {`setpc_x`, `setpc_y`, `setpc_w`, `setpc_h`} as populated.
void DRLG_SetPC()
{
    int i, j, x, y, w, h;

    // Translate from megatile coords to mini tile coords then account for buffer
    w = 2 * setpc_w;
    h = 2 * setpc_h;
    x = 2 * setpc_x + MINITILE_PADDING_X;
    y = 2 * setpc_y + MINITILE_PADDING_Y;

    for (j = 0; j < h; j++)
    {
        for (i = 0; i < w; i++)
        {
            dFlags[i + x][j + y] |= BFLAG_POPULATED;
        }
    }
}

// gendung_GetPortalLevel	000000000040DF73
// gendung_GetPortalLvlPos	000000000040DFD1
