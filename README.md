# Diablo Pre-Release Demo Decompilation

Reconstructing the Diablo Pre-Release Demo source code from the assembly.

## Status

It runs! And plays `logo.smk`! Then crashes!

## Building with Docker

We provide a Docker image with the necessary tools and libraries already installed.

    docker pull registry.gitlab.com/moralbacteria/diablo-prdemo-decomp/build_vc42

> See [docker/vc42/README.md](docker/vc42/README.md) for how this image is made.

> If running Docker Desktop on Windows then consider turning off [autocrlf] to use Unix line endings.

[autocrlf]: https://docs.github.com/en/get-started/getting-started-with-git/configuring-git-to-handle-line-endings

Run the container with source mounted inside (e.g. `/repo`):

    docker run -v ${PWD}:/repo -it registry.gitlab.com/moralbacteria/diablo-prdemo-decomp/build_vc42 bash

Inside the Docker container:

    cd /repo
    wine nmake

This will create:

- `storm/storm.lib`
- `src/diablo.exe`
- `src/diablo.debug.exe`
- `src/diablo.debug.pdb`

> To clean, use `wine nmake clean`

## Running

Copy `diablo.exe` into an existing Pre-Release Demo installation.

> The Pre-Release Demo can be downloaded from [Diablo Evolution].

[Diablo Evolution]: https://diablo-evolution.net/index.php?pageid=files

## Debugging

1. Either: copy `diablo.debug.exe` and `diablo.debug.pdb` into an existing
   Pre-Release Demo installation, OR copy the Demo files into `src/`.
1. Run `diablo.debug.exe` without the debugger to allow it through Windows
   SmartScreen. Otherwise the debugger won't work.
1. Launch IDA. Use the `-Opdb:msdia` commandline option so it can load the PDB.
1. Open `diablo.debug.exe` with `diablo.debug.pdb` in IDA.
1. Run the debugger.

NOTE: The path to the EXE must be short otherwise the game will crash trying to
find your save game file. E.g. my checkout is `C:\demo-decomp`.

NOTE: [IDA 7 Freeware] seems to be the best version of IDA since it can load the
PDB and debug out-of-the-box without issue.

[IDA 7 Freeware]: https://out7.hex-rays.com/files/idafree70_windows.exe

## Contributing

Open with VSCode and Format Document With... > C/C++
