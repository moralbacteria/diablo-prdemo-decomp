#include "items.h"

#include "control.h"
#include "diablo.h"
#include "effects.h"
#include "engine.h"
#include "lighting.h"
#include "player.h"

#include <stdio.h>

#define MAXRESIST 100
#define DUR_INDESTRUCTIBLE 100

//
// initialized variables (.data:004AAB20)
//

// (objcurs.cel frame - 8) => ItemDropNames index
BYTE ItemCAnimTbl[] = {
    20, 16, 0, 12, 12, 4, 4, 4, 2, 21, 12, 12, 12, 12, 12,
    12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 19, 22, 23,
    24, 25, 26, 27, 8, 8, 8, 8, 8, 6, 8, 8, 8, 6, 8, 8,
    6, 8, 8, 5, 9, 13, 13, 13, 0, 0, 5, 15, 5, 5, 18, 18,
    18, 0, 5, 5, 7, 1, 3, 17, 1, 15, 10, 14, 3, 11, 8, 0,
    1, 7, 0, 7, 6, 7, 3, 3, 3, 6, 6, 11, 11, 11, 0, 14,
    14, 14, 6, 6, 7, 3, 8, 14, 14, 14, 14, 0, 0, 1, 1, 1,
    1, 1, 7, 7, 7, 14, 14,
    // These two are past the end of objcurs.cel...
    4, 0};
const char *ItemDropNames[] = {
    "Armor2",
    "Axe",
    "FBttle",
    "Bow",
    "GoldFlip",
    "Helmut",
    "Mace",
    "Shield",
    "SwrdFlip",
    "Rock",
    "Cleaver",
    "Staff",
    "Ring",
    "CrownF",
    "LArmor",
    "WShield",
    "Scroll",
    "FPlateAr",
    "FBook",
    "Food",
    "FBttleBB",
    "FBttleDY",
    "FBttleOR",
    "FBttleBR",
    "FBttleBL",
    "FBttleBY",
    "FBttleWH",
    "FBttleDB"};
BYTE ItemAnimLs[] = {
    15,
    13,
    16,
    13,
    10,
    13,
    13,
    13,
    13,
    10,
    13,
    13,
    13,
    13,
    13,
    13,
    13,
    13,
    13,
    1,
    16,
    16,
    16,
    16,
    16,
    16,
    16,
    16};
// TODO: Sound enums
int ItemDropSnds[] = {
    IS_FHARM,
    IS_FAXE,
    IS_FPOT,
    IS_FBOW,
    IS_GOLD,
    IS_FCAP,
    IS_FSWOR,
    IS_FSHLD,
    IS_FSWOR,
    IS_MAGIC,
    IS_FAXE,
    IS_FSTAF,
    IS_FRING,
    IS_FCAP,
    IS_FLARM,
    IS_FSHLD,
    IS_FSCRL,
    IS_FHARM,
    IS_FLARM,
    IS_FLARM,
    IS_FPOT,
    IS_FPOT,
    IS_FPOT,
    IS_FPOT,
    IS_FPOT,
    IS_FPOT,
    IS_FPOT,
    IS_FPOT};
int ItemInvSnds[] = {
    IS_IHARM,
    IS_IAXE,
    IS_IPOT,
    IS_IBOW,
    IS_GOLD,
    IS_ICAP,
    IS_ISWORD,
    IS_ISHIEL,
    IS_ISWORD,
    IS_IGRAB,
    IS_IAXE,
    IS_ISTAF,
    IS_IRING,
    IS_ICAP,
    IS_ILARM,
    IS_ISHIEL,
    IS_ISCROL,
    IS_IHARM,
    IS_IHARM,
    IS_IHARM,
    IS_IPOT,
    IS_IPOT,
    IS_IPOT,
    IS_IPOT,
    IS_IPOT,
    IS_IPOT,
    IS_IPOT,
    IS_IPOT};

//
// uninitialized variables
//

// ???
ItemStruct curruitem;
BOOL uitemflag;
// ???
int numitems;
BYTE *itemanims[ITEMTYPES];
ItemStruct item[MAXITEMS];
BOOL UniqueItemFlag[128];
// ???

//
// code (.data:0041C230)
//

// .text:0041C230
void InitItemGFX()
{
    int i;
    char arglist[64];

    for (i = 0; i < ITEMTYPES; ++i)
    {
        sprintf(arglist, "Items\\%s.CEL", ItemDropNames[i]);
        itemanims[i] = LoadFileInMem(arglist);
    }
    for (i = 0; i < sizeof(UniqueItemFlag); ++i)
    {
        UniqueItemFlag[i] = 0;
    }
}

// ItemPlace	000000000041C2BE
// AddInitItems	000000000041C3CE
// InitItems	000000000041C581

// .text:0041C736
void CalcPlrItemVals(int p)
{
    int d;
    int g;
    int lrad;
    int i;
    ItemStruct *itm;

    CalcPlrItemMin(p); // return value is ignored :O
    CalcPlrItemDurs(p);

    int mind = 0; // min damage
    int maxd = 0; // max damage
    int tac = 0;  // accuracy

    int bdam = 0;   // bonus damage
    int btohit = 0; // bonus chance to hit
    int bac = 0;    // bonus accuracy

    int iflgs = ISPL_NONE; // item_special_effect flags

    int sadd = 0; // added strength
    int madd = 0; // added magic
    int dadd = 0; // added dexterity
    int vadd = 0; // added vitality

    DWORD spl = 0; // bitarray for all enabled/active spells

    int fr = 0; // fire resistance
    int lr = 0; // lightning resistance
    int mr = 0; // magic resistance

    int dmod = 0; // bonus damage mod?
    int ghit = 0; // increased damage from enemies

    lrad = 10; // light radius

    int ihp = 0;   // increased HP
    int imana = 0; // increased mana

    int spllvladd = 0; // increased spell level
    int spllvlcost = 0;
    int spllvldur = 0;

    for (i = 0; i < NUM_INVLOC; ++i)
    {
        switch (i)
        {
        case INVLOC_HEAD:
            itm = &plr[p].InvBody[INVLOC_HEAD];
            break;
        case INVLOC_CHEST:
            itm = &plr[p].InvBody[INVLOC_CHEST];
            break;
        case INVLOC_RING_LEFT:
            itm = &plr[p].InvBody[INVLOC_RING_LEFT];
            break;
        case INVLOC_RING_RIGHT:
            itm = &plr[p].InvBody[INVLOC_RING_RIGHT];
            break;
        case INVLOC_HAND_LEFT:
            itm = &plr[p].InvBody[INVLOC_HAND_LEFT];
            break;
        case INVLOC_HAND_RIGHT:
            itm = &plr[p].InvBody[INVLOC_HAND_RIGHT];
            break;
        }

        if (itm->_itype != ITYPE_NONE && itm->_iStatFlag)
        {
            // All normal items have some basic stats: damage (weapons), AC (armor), spell charges (staves)
            mind += itm->_iMinDam;
            maxd += itm->_iMaxDam;
            tac += itm->_iAC;
            spl |= SPELLBIT(itm->_iSpell);

            // Magic items can have affixes that modify more. But only apply if the item is identified!
            if (itm->_iIdentified)
            {
                bdam += itm->_iPLDam;
                btohit += itm->_iPLToHit;
                bac += itm->_iAC;
                iflgs |= itm->_iFlags;
                sadd += itm->_iPLStr;
                madd += itm->_iPLMag;
                dadd += itm->_iPLDex;
                vadd += itm->_iPLVit;
                fr += itm->_iPLFR;
                lr += itm->_iPLLR;
                mr += itm->_iPLMR;
                dmod += itm->_iPLDamMod;
                ghit += itm->_iPLGetHit;
                lrad += itm->_iPLLight;
                ihp += itm->_iPLHP;
                imana += itm->_iPLMana;
                spllvladd += itm->_iSplLvlAdd;
                spllvlcost += itm->_iPLSplCost;
                spllvldur += itm->_iPLSplDur;
            }
        }
    }

    if (mind == 0 && maxd == 0)
    {
        mind = 1;
        maxd = 1;

        // Fist + shield is more damage than fist? :O
        if (plr[p].InvBody[INVLOC_HAND_LEFT]._itype == ITYPE_SHIELD && plr[p].InvBody[INVLOC_HAND_LEFT]._iStatFlag)
        {
            maxd = 3;
        }
        if (plr[p].InvBody[INVLOC_HAND_RIGHT]._itype == ITYPE_SHIELD && plr[p].InvBody[INVLOC_HAND_RIGHT]._iStatFlag)
        {
            maxd = 3;
        }
    }

    plr[p]._pIMinDam = mind;
    plr[p]._pIMaxDam = maxd;
    plr[p]._pIAC = tac;
    plr[p]._pIBonusDam = bdam;
    plr[p]._pIBonusToHit = btohit;
    plr[p]._pIBonusAC = bac;
    plr[p]._pIFlags = iflgs;
    plr[p]._pIBonusDamMod = dmod;
    plr[p]._pIGetHit = ghit;

    // Clamp light radius
    if (lrad < 2)
    {
        lrad = 2;
    }
    if (lrad > 16)
    {
        lrad = 16;
    }
    // If light radius changed then update both the lighting and the vision systems
    if (plr[p]._pLightRad != lrad && p == myplr)
    {
        ChangeLightRadius(plr[p]._plid, lrad);
        if (lrad < 10)
        {
            ChangeVisionRadius(plr[p]._pvid, 10);
        }
        else
        {
            ChangeVisionRadius(plr[p]._pvid, lrad);
        }
        plr[p]._pLightRad = lrad;
    }

    plr[p]._pStrength = sadd + plr[p]._pBaseStr;
    plr[p]._pMagic = madd + plr[p]._pBaseMag;
    plr[p]._pDexterity = dadd + plr[p]._pBaseDex;
    plr[p]._pVitality = vadd + plr[p]._pBaseVit;
    plr[p]._pDamageMod = plr[p]._pLevel * plr[p]._pStrength / 100;
    plr[p]._pISpells = spl;

    // check if the current RSplType is a valid/allowed spell
    if (plr[p]._pRSplType == RSPLTYPE_CHARGES && !(plr[p]._pISpells & SPELLBIT(plr[p]._pRSpell)))
    {
        plr[p]._pRSpell = SPL_INVALID;
        plr[p]._pRSplType = RSPLTYPE_INVALID;
        force_redraw = FORCE_REDRAW_ALL;
    }

    plr[p]._pISplLvlAdd = spllvladd;
    plr[p]._pISplCost = spllvlcost;
    plr[p]._pISplDur = spllvldur;

    if (mr > MAXRESIST)
    {
        mr = MAXRESIST;
    }
    plr[p]._pMagResist = mr;

    if (fr > MAXRESIST)
    {
        fr = MAXRESIST;
    }
    plr[p]._pFireResist = fr;

    if (lr > MAXRESIST)
    {
        lr = MAXRESIST;
    }
    plr[p]._pLghtResist = lr;

    ihp += INT2FP(vadd);   // Each point of vit is 1 HP
    imana += INT2FP(madd); // Each point of magic is 1 MP

    plr[p]._pHitPoints = ihp + plr[p]._pHPBase;
    if (FP_FLOOR(plr[p]._pHitPoints) <= FP_FLOOR(0))
    {
        plr[p]._pHitPoints = 0;
    }
    plr[p]._pMaxHP = ihp + plr[p]._pMaxHPBase;
    plr[p]._pMana = imana + plr[p]._pManaBase;
    plr[p]._pMaxMana = imana + plr[p]._pMaxManaBase;

    if (iflgs & ISPL_INFRAVISION)
    {
        plr[p]._pInfraFlag = TRUE;
    }
    else
    {
        plr[p]._pInfraFlag = FALSE;
    }

    plr[p]._pBlockFlag = FALSE;
    plr[p]._pwtype = WT_MELEE;

    // Figure out which player graphics to use. Start with weapon the player is using
    g = 0;
    if (plr[p].InvBody[INVLOC_HAND_LEFT]._itype != ITYPE_NONE && plr[p].InvBody[INVLOC_HAND_LEFT]._iClass == ICLASS_WEAPON && plr[p].InvBody[INVLOC_HAND_LEFT]._iStatFlag)
    {
        g = plr[p].InvBody[INVLOC_HAND_LEFT]._itype;
    }
    if (plr[p].InvBody[INVLOC_HAND_RIGHT]._itype != ITYPE_NONE && plr[p].InvBody[INVLOC_HAND_RIGHT]._iClass == ICLASS_WEAPON && plr[p].InvBody[INVLOC_HAND_RIGHT]._iStatFlag)
    {
        g = plr[p].InvBody[INVLOC_HAND_RIGHT]._itype;
    }
    // Then convert to animation.
    switch (g)
    {
    case ITYPE_SWORD:
        g = ANIM_ID_SWORD;
        break;
    case ITYPE_MACE:
        g = ANIM_ID_MACE;
        break;
    case ITYPE_BOW:
        plr[p]._pwtype = WT_RANGED;
        g = ANIM_ID_BOW;
        break;
    case ITYPE_AXE:
        g = ANIM_ID_AXE;
        break;
    case ITYPE_STAFF:
        g = ANIM_ID_STAFF;
        break;
    }
    // Account for shield.
    if (plr[p].InvBody[INVLOC_HAND_LEFT]._itype == ITYPE_SHIELD && plr[p].InvBody[INVLOC_HAND_LEFT]._iStatFlag)
    {
        plr[p]._pBlockFlag = TRUE;
        g++;
    }
    if (plr[p].InvBody[INVLOC_HAND_RIGHT]._itype == ITYPE_SHIELD && plr[p].InvBody[INVLOC_HAND_RIGHT]._iStatFlag)
    {
        plr[p]._pBlockFlag = TRUE;
        g++;
    }
    // LMH heavy graphics for warrior in full game only
    if (!demo_mode && !plr[p]._pClass)
    {
        if (plr[p].InvBody[INVLOC_CHEST]._itype == ITYPE_MARMOR && plr[p].InvBody[INVLOC_CHEST]._iStatFlag)
        {
            g += ANIM_ID_MEDIUM_ARMOR;
        }
        if (plr[p].InvBody[INVLOC_CHEST]._itype == ITYPE_HARMOR && plr[p].InvBody[INVLOC_CHEST]._iStatFlag)
        {
            g += ANIM_ID_HEAVY_ARMOR;
        }
    }
    // Load the player graphics based on what they are wearing.
    if (plr[p]._pgfxnum != g)
    {
        plr[p]._pGFXLoad = 0;
        LoadPlrGFX(p, PFILE_STAND);
        SetPlrAnims(p);

        d = plr[p]._pdir;
        plr[p]._pAnimData = plr[p]._pNAnim[d];
        plr[p]._pAnimLen = plr[p]._pNFrames;
        plr[p]._pAnimFrame = 1;
        plr[p]._pAnimCnt = 0;
        plr[p]._pAnimDelay = 3;
        plr[p]._pAnimWidth = plr[p]._pNWidth;
        plr[p]._pAnimWidth2 = (plr[p]._pNWidth - 64) / 2;
    }

    drawmanaflag = TRUE;
    drawhpflag = TRUE;
}

// .text:0041D71F
void CalcPlrScrolls(int p)
{
    int i;

    plr[p]._pScrlSpells = 0;

    for (i = 0; i < plr[p]._pNumInv; i++)
    {
        if (plr[p].InvList[i]._itype != ITYPE_NONE && plr[p].InvList[i]._iMiscId == IMISC_SCROLL)
        {
            if (plr[p].InvList[i]._iStatFlag)
            {
                plr[p]._pScrlSpells |= SPELLBIT(plr[p].InvList[i]._iSpell);
            }
        }
    }
}

// .text:0041D868
void CalcPlrStaff(int p)
{
    // BUG: Missing iStatFlag check
    if (plr[p].InvBody[INVLOC_HAND_LEFT]._iCharges > 0)
    {
        plr[p]._pISpells |= SPELLBIT(plr[p].InvBody[INVLOC_HAND_LEFT]._iSpell);
    }
}

// .text:0041D8FD
static BOOL ItemMinStats(int pnum, ItemStruct x)
{
    BOOL retval = TRUE;

    if (plr[pnum]._pStrength < x._iMinStr)
    {
        retval = FALSE;
    }
    if (plr[pnum]._pMagic < x._iMinMag)
    {
        retval = FALSE;
    }
    if (plr[pnum]._pDexterity < x._iMinDex)
    {
        retval = FALSE;
    }

    return retval;
}

// .text:0041D9AC
// This might not be the right function name but there's no 1:1 analogue with
// Devilution
// Checks all worn items for stat requirements. Returns TRUE if there is a
// change in a worn item (e.g. could wear but now can no longer wear)
BOOL CalcPlrItemMin(int pnum)
{
    BOOL retval;
    BOOL old_stat_flag;
    int i;

    retval = FALSE;

    if (plr[pnum].InvBody[INVLOC_HEAD]._itype != -1)
    {
        old_stat_flag = plr[pnum].InvBody[INVLOC_HEAD]._iStatFlag;
        plr[pnum].InvBody[INVLOC_HEAD]._iStatFlag = ItemMinStats(pnum, plr[pnum].InvBody[INVLOC_HEAD]);
        if (plr[pnum].InvBody[INVLOC_HEAD]._iStatFlag != old_stat_flag)
        {
            retval = TRUE;
        }
    }
    if (plr[pnum].InvBody[INVLOC_CHEST]._itype != -1)
    {
        old_stat_flag = plr[pnum].InvBody[INVLOC_CHEST]._iStatFlag;
        plr[pnum].InvBody[INVLOC_CHEST]._iStatFlag = ItemMinStats(pnum, plr[pnum].InvBody[INVLOC_CHEST]);
        if (plr[pnum].InvBody[INVLOC_CHEST]._iStatFlag != old_stat_flag)
        {
            retval = TRUE;
        }
    }
    if (plr[pnum].InvBody[INVLOC_RING_LEFT]._itype != -1)
    {
        old_stat_flag = plr[pnum].InvBody[INVLOC_RING_LEFT]._iStatFlag;
        plr[pnum].InvBody[INVLOC_RING_LEFT]._iStatFlag = ItemMinStats(pnum, plr[pnum].InvBody[INVLOC_RING_LEFT]);
        if (plr[pnum].InvBody[INVLOC_RING_LEFT]._iStatFlag != old_stat_flag)
        {
            retval = TRUE;
        }
    }
    if (plr[pnum].InvBody[INVLOC_RING_RIGHT]._itype != -1)
    {
        old_stat_flag = plr[pnum].InvBody[INVLOC_RING_RIGHT]._iStatFlag;
        plr[pnum].InvBody[INVLOC_RING_RIGHT]._iStatFlag = ItemMinStats(pnum, plr[pnum].InvBody[INVLOC_RING_RIGHT]);
        if (plr[pnum].InvBody[INVLOC_RING_RIGHT]._iStatFlag != old_stat_flag)
        {
            retval = TRUE;
        }
    }
    if (plr[pnum].InvBody[INVLOC_HAND_LEFT]._itype != -1)
    {
        old_stat_flag = plr[pnum].InvBody[INVLOC_HAND_LEFT]._iStatFlag;
        plr[pnum].InvBody[INVLOC_HAND_LEFT]._iStatFlag = ItemMinStats(pnum, plr[pnum].InvBody[INVLOC_HAND_LEFT]);
        if (plr[pnum].InvBody[INVLOC_HAND_LEFT]._iStatFlag != old_stat_flag)
        {
            retval = TRUE;
        }
    }
    if (plr[pnum].InvBody[INVLOC_HAND_RIGHT]._itype != -1)
    {
        old_stat_flag = plr[pnum].InvBody[INVLOC_HAND_RIGHT]._iStatFlag;
        plr[pnum].InvBody[INVLOC_HAND_RIGHT]._iStatFlag = ItemMinStats(pnum, plr[pnum].InvBody[INVLOC_HAND_RIGHT]);
        if (plr[pnum].InvBody[INVLOC_HAND_RIGHT]._iStatFlag != old_stat_flag)
        {
            retval = TRUE;
        }
    }

    for (i = 0; i < plr[pnum]._pNumInv; ++i)
    {
        plr[pnum].InvList[i]._iStatFlag = ItemMinStats(pnum, plr[pnum].InvList[i]);
    }

    return retval;
}

// .text:0041DEBD
void CalcPlrInv(int p)
{
    CalcPlrItemVals(p);
    while (CalcPlrItemMin(p)) // TODO: wtf
    {
        CalcPlrItemVals(p);
    }
    CalcPlrScrolls(p);
    CalcPlrStaff(p);
}

// .text:0041DF08
// Sets durflag = TRUE if any work item is below 5 durability. DrawView uses
// this to call DrawDurIcon()... which is not really necessary though :/
void CalcPlrItemDurs(int pnum)
{
    durflag = FALSE;

    if (plr[myplr].InvBody[INVLOC_HAND_LEFT]._itype != ITYPE_NONE && plr[myplr].InvBody[INVLOC_HAND_LEFT]._iDurability <= 5)
    {
        durflag = TRUE;
    }
    if (plr[myplr].InvBody[INVLOC_HAND_RIGHT]._itype != ITYPE_NONE && plr[myplr].InvBody[INVLOC_HAND_RIGHT]._iDurability <= 5)
    {
        durflag = TRUE;
    }
    if (plr[myplr].InvBody[INVLOC_HEAD]._itype != ITYPE_NONE && plr[myplr].InvBody[INVLOC_HEAD]._iDurability <= 5)
    {
        durflag = TRUE;
    }
    if (plr[myplr].InvBody[INVLOC_CHEST]._itype != ITYPE_NONE && plr[myplr].InvBody[INVLOC_CHEST]._iDurability <= 5)
    {
        durflag = TRUE;
    }
}

// .text:0041E060
static void SetPlrHandItem(ItemStruct *h, int idata)
{
    h->_itype = AllItemsList[idata].itype;
    h->_iCurs = AllItemsList[idata].iCurs;
    strcpy(h->_iName, AllItemsList[idata].iName);
    strcpy(h->_iIName, AllItemsList[idata].iName);
    if (AllItemsList[idata].iMiscId == IMISC_STAFF)
    {
        h->_iCharges = 6;
    }
    else
    {
        h->_iCharges = 0;
    }
    h->_iMaxCharges = h->_iCharges;
    h->_iDurability = AllItemsList[idata].iDurability;
    h->_iMaxDur = AllItemsList[idata].iDurability;
    h->_iMinStr = AllItemsList[idata].iMinStr;
    h->_iMinMag = AllItemsList[idata].iMinMag;
    h->_iMinDex = AllItemsList[idata].iMinDex;
    h->_ivalue = AllItemsList[idata].iValue;
    h->_iIvalue = AllItemsList[idata].iValue;
    h->_iPLHP = 0;
    h->_iPLHP = 0;
    h->_iPLDam = 0;
    h->_iPLToHit = 0;
    h->_iPLAC = 0;
    h->_iPLStr = 0;
    h->_iPLMag = 0;
    h->_iPLDex = 0;
    h->_iPLVit = 0;
    h->_iPLFR = 0;
    h->_iPLLR = 0;
    h->_iPLMR = 0;
    h->_iPLMana = 0;
    h->_iPLHP = 0;
    h->_iPLDamMod = 0;
    h->_iPLGetHit = 0;
    h->_iPLLight = 0;
    h->_iSplLvlAdd = 0;
    h->_iPLSplCost = 0;
    h->_iPLSplDur = 0;
    h->_iPrePower = -1;
    h->_iSufPower = -1;
    h->_iMagical = ITEM_QUALITY_NORMAL;
    h->IDidx = idata;
}

// .text:0041E35D
void CreatePlrItems(int p)
{
    int i;

    plr[p].InvBody[INVLOC_HEAD]._itype = -1;
    plr[p].InvBody[INVLOC_CHEST]._itype = -1;
    plr[p].InvBody[INVLOC_RING_LEFT]._itype = -1;
    plr[p].InvBody[INVLOC_RING_RIGHT]._itype = -1;
    plr[p].InvBody[INVLOC_HAND_RIGHT]._itype = -1;

    switch (plr[p]._pClass)
    {
    case PC_WARRIOR:
        SetPlrHandItem(&plr[p].InvBody[INVLOC_HAND_LEFT], 1);  // TODO magic number
        SetPlrHandItem(&plr[p].InvBody[INVLOC_HAND_RIGHT], 2); // TODO magic number
        break;
    case PC_ROGUE:
        SetPlrHandItem(&plr[p].InvBody[INVLOC_HAND_LEFT], 3); // TODO magic number
        break;
    case PC_SORCERER:
        SetPlrHandItem(&plr[p].InvBody[INVLOC_HAND_LEFT], 4); // TODO magic number
        break;
    }

    for (i = 0; i < 40; i++) // TODO magic number
    {
        plr[p].InvList[i]._itype = -1;
        plr[p].InvGrid[i] = 0;
    }

    plr[p]._pNumInv = 0;
    CalcPlrItemVals(p);
}

// GetItemSpace	000000000041E58F
// GetBookSpell	000000000041EAAC
// GetStaffPower	000000000041EC27
// GetStaffSpell	000000000041F1E2
// GetItemAttrs	000000000041F43C

// .text:0041FB27
// Generate random number between param1 and param2
int RndPL(int param1, int param2)
{
    return param1 + random_(param2 - param1 + 1);
}

// PLVal	000000000041FB51
// Determine the item's gold cost based off of how well the stat rolled.
// `pv` is the random value for the stat
// `p1` and `p2` are the upper and lower bounds for what `pv` could have been. This means that p1 <= pv <= p2
// `minv` and `maxv` come from the caller of SaveItemPower.
int PLVal(int pv, int p1, int p2, int minv, int maxv)
{
    // The corner cases don't make a lot of sense but I guess they needed something.
    // The item is always at least worth `minv`
    if (p1 == p2)
    {
        return minv;
    }
    if (minv == maxv)
    {
        return minv;
    }
    // Determine the percetange of the `pv` roll between the lower bound `p1` and the uppser bound `p2`. Use that to calculate additional value on top of the minimum.
    // For example, a max roll is worth more than a low roll.
    return minv + (maxv - minv) * (100 * (pv - p1) / (p2 - p1)) / 100;
}

// .text:0041FBC0
// `power` is one of `enum item_effect_type`
void SaveItemPower(int i, int power, int param1, int param2, int minval, int maxval, int multval)
{
    int r, r2;

    r = RndPL(param1, param2);
    switch (power)
    {
    case IPL_TOHIT:
        item[i]._iPLToHit += r;
        break;
    case IPL_TOHIT_CURSE:
        item[i]._iPLToHit -= r;
        break;
    case IPL_DAMP:
        item[i]._iPLDam += r;
        break;
    case IPL_DAMP_CURSE:
        item[i]._iPLDam -= r;
        break;
    case IPL_TOHIT_DAMP:
        r = RndPL(param1, param2);
        item[i]._iPLDam += r;
        if (param1 == 25)
            r2 = RndPL(1, 5);
        if (param1 == 50)
            r2 = RndPL(6, 10);
        if (param1 == 75)
            r2 = RndPL(11, 15);
        if (param1 == 100)
            r2 = RndPL(16, 25);
        if (param1 == 150)
            r2 = RndPL(26, 40);
        if (param1 == 200)
            r2 = RndPL(41, 60);
        if (param1 == 250)
            r2 = RndPL(61, 100);
        item[i]._iPLToHit += r2;
        break;
    case IPL_TOHIT_DAMP_CURSE:
        item[i]._iPLDam -= r;
        if (param1 == 25)
            r2 = RndPL(1, 5);
        if (param1 == 50)
            r2 = RndPL(6, 10);
        item[i]._iPLToHit -= r2;
        break;
    case IPL_ACP:
        item[i]._iPLAC += r;
        break;
    case IPL_ACP_CURSE:
        item[i]._iPLAC -= r;
        break;
    case IPL_FIRERES:
        item[i]._iPLFR += r;
        break;
    case IPL_LIGHTRES:
        item[i]._iPLLR += r;
        break;
    case IPL_MAGICRES:
        item[i]._iPLMR += r;
        break;
    case IPL_ALLRES:
        item[i]._iPLFR += r;
        item[i]._iPLLR += r;
        item[i]._iPLMR += r;
    case IPL_SPLCOST:
        item[i]._iPLSplCost = r;
        break;
    case IPL_SPLDUR:
        item[i]._iPLSplDur = r;
        break;
    case IPL_SPLLVLADD:
        item[i]._iSplLvlAdd = r;
        break;
    case IPL_CHARGES:
        item[i]._iCharges *= param1;
        item[i]._iMaxCharges = item[i]._iCharges;
        break;
    case IPL_STR:
        item[i]._iPLStr += r;
        break;
    case IPL_STR_CURSE:
        item[i]._iPLStr -= r;
        break;
    case IPL_MAG:
        item[i]._iPLMag += r;
        break;
    case IPL_MAG_CURSE:
        item[i]._iPLMag -= r;
        break;
    case IPL_DEX:
        item[i]._iPLDex += r;
        break;
    case IPL_DEX_CURSE:
        item[i]._iPLDex -= r;
        break;
    case IPL_VIT:
        item[i]._iPLVit += r;
        break;
    case IPL_VIT_CURSE:
        item[i]._iPLVit -= r;
        break;
    case IPL_ATTRIBS:
        item[i]._iPLStr += r;
        item[i]._iPLMag += r;
        item[i]._iPLDex += r;
        item[i]._iPLVit += r;
        break;
    case IPL_ATTRIBS_CURSE:
        item[i]._iPLStr -= r;
        item[i]._iPLMag -= r;
        item[i]._iPLDex -= r;
        item[i]._iPLVit -= r;
        break;
    case IPL_GETHIT_CURSE:
        item[i]._iPLGetHit += r;
        break;
    case IPL_GETHIT:
        item[i]._iPLGetHit -= r;
        break;
    case IPL_LIFE:
        item[i]._iPLHP += INT2FP(r);
        break;
    case IPL_LIFE_CURSE:
        item[i]._iPLHP -= INT2FP(r);
        break;
    case IPL_MANA:
        item[i]._iPLMana += INT2FP(r);
        break;
    case IPL_MANA_CURSE:
        item[i]._iPLMana -= INT2FP(r);
        break;
    case IPL_DUR:
        r2 = r * item[i]._iMaxDur / 100;
        item[i]._iMaxDur += r2;
        item[i]._iDurability += r2;
        break;
    case IPL_DUR_CURSE:
        item[i]._iMaxDur -= r * item[i]._iMaxDur / 100;
        if (item[i]._iMaxDur < 1)
            item[i]._iMaxDur = 1;
        item[i]._iDurability = item[i]._iMaxDur;
        break;
    case IPL_INDESTRUCTIBLE:
        item[i]._iDurability = DUR_INDESTRUCTIBLE;
        item[i]._iMaxDur = DUR_INDESTRUCTIBLE;
        break;
    case IPL_LIGHT:
        item[i]._iPLLight += param1;
        break;
    case IPL_LIGHT_CURSE:
        item[i]._iPLLight -= param1;
        break;
    case IPL_FIRE_ARROWS:
        item[i]._iFlags |= ISPL_FIRE_ARROWS;
        break;
    case IPL_DAMMOD:
        item[i]._iPLDamMod += r;
        break;
    case IPL_RNDARROWVEL:
        item[i]._iFlags |= ISPL_RNDARROWVEL;
        break;
    case IPL_SETDAM:
        item[i]._iMinDam = param1;
        item[i]._iMaxDam = param2;
        break;
    case IPL_SETDUR:
        item[i]._iDurability = param1;
        item[i]._iMaxDur = param1;
        break;
    case IPL_DRAINLIFE:
        item[i]._iFlags |= ISPL_DRAINLIFE;
        break;
    }

    // Determine how much the item costs (and how much it sells for)
    item[i]._iIvalue += PLVal(r, param1, param2, minval, maxval);
    item[i]._iIvalue += item[i]._ivalue;
    if (item[i]._iIvalue < 0)
    {
        item[i]._iIvalue = 0;
    }
}

// GetItemPower	00000000004205CD
// GetItemBonus	0000000000420F52

// .text:00421087
void SetupItem(int i)
{
    int it;

    it = ItemCAnimTbl[item[i]._iCurs];
    item[i]._iAnimData = itemanims[it];
    item[i]._iAnimLen = ItemAnimLs[it];
    item[i]._iAnimFrame = 1;
    item[i]._iAnimFlag = TRUE;
    item[i]._iDelFlag = FALSE;
    item[i]._iAnimWidth = 96;
    item[i]._iAnimWidth2 = 16;
    item[i]._iSelFlag = 0;
    item[i]._iIdentified = FALSE;
    item[i]._iPostDraw = FALSE;
}

// RndItem	00000000004211B7
// RndUItem	0000000000421348
// RndAllItems	00000000004214FE
// RndTypeItems	00000000004215AF
// CheckUnique	00000000004216EB
// GetUniqueItem	00000000004218ED
// SpawnUnique	0000000000421CDC
// SpawnItem	0000000000421DAC

// CreateItem	0000000000422056
void CreateItem(int uid, int x, int y)
{
    // TODO
}

// CreateRndItem	0000000000422142
void CreateRndItem(int x, int y)
{
    // TODO
}

// CreateRndUseful	000000000042232B
void CreateRndUseful(int x, int y)
{
    // TODO
}

// CreateTypeItem	000000000042249F
void CreateTypeItem(int x, int y, BOOL onlygood, int itype, int imisc)
{
    // TODO
}

// SpawnRock	0000000000422688
// RespawnItem	0000000000422841
// DeleteItem	000000000042298C
// ProcessItems	00000000004229F1
void ProcessItems()
{
    // TODO
}
// FreeItemGFX	0000000000422C65
void FreeItemGFX()
{
    // TODO
}
// GetItemFrm	0000000000422CC6
// GetItemStr	0000000000422D1A
// DoIdentify	0000000000422E5C
void DoIdentify()
{
    // TODO
}
// RepairItem	0000000000422FF4
// DoRepair	00000000004230B3
void DoRepair()
{
    // TODO
}
// RechargeItem	00000000004232A3
// DoRecharge	0000000000423336
void DoRecharge()
{
    // TODO
}

// .text:0042352F
void PrintItemPower(char plidx, ItemStruct x)
{
    int light;

    switch (plidx)
    {
    case IPL_TOHIT:
    case IPL_TOHIT_CURSE:
        sprintf(tempstr, "To Hit : %+i%%", x._iPLToHit);
        break;
    case IPL_DAMP:
    case IPL_DAMP_CURSE:
        sprintf(tempstr, "Damage : %+i%%", x._iPLDam);
        break;
    case IPL_TOHIT_DAMP:
    case IPL_TOHIT_DAMP_CURSE:
        sprintf(tempstr, "Damage : %+i%%  To Hit : %+i%%", x._iPLDam, x._iPLToHit);
        break;
    case IPL_ACP:
    case IPL_ACP_CURSE:
        sprintf(tempstr, "Armor Class : %+i", x._iPLAC);
        break;
    case IPL_FIRERES:
        sprintf(tempstr, "Resist Fire : %+i%%", x._iPLFR);
        break;
    case IPL_LIGHTRES:
        sprintf(tempstr, "Resist Lightning : %+i%%", x._iPLLR);
        break;
    case IPL_MAGICRES:
        sprintf(tempstr, "Resist General Magic : %+i%%", x._iPLMR);
        break;
    case IPL_ALLRES:
        sprintf(tempstr, "Resist All : %+i%%", x._iPLFR);
        break;
    case IPL_SPLCOST:
        sprintf(tempstr, "Spell cost -%i%%", x._iPLSplCost);
        break;
    case IPL_SPLDUR:
        sprintf(tempstr, "Spell duration +%i%%", x._iPLSplDur);
        break;
    case IPL_SPLLVLADD:
        sprintf(tempstr, "All Spell levels +%i", x._iSplLvlAdd);
        break;
    case IPL_CHARGES:
        strcpy(tempstr, "Extra charges");
        break;
    case IPL_FIREDAM:
        strcpy(tempstr, "Fire hit (NW)");
        break;
    case IPL_LIGHTDAM:
        strcpy(tempstr, "Lightning hit (NW)");
        break;
    case IPL_RANDSPL:
        strcpy(tempstr, "Random spell every 5 min (NW)");
        break;
    case IPL_STR:
    case IPL_STR_CURSE:
        sprintf(tempstr, "Strength : %+i", x._iPLStr);
        break;
    case IPL_MAG:
    case IPL_MAG_CURSE:
        sprintf(tempstr, "Magic : %+i", x._iPLMag);
        break;
    case IPL_DEX:
    case IPL_DEX_CURSE:
        sprintf(tempstr, "Dexterity : %+i", x._iPLDex);
        break;
    case IPL_VIT:
    case IPL_VIT_CURSE:
        sprintf(tempstr, "Vitality : %+i", x._iPLVit);
        break;
    case IPL_ATTRIBS:
    case IPL_ATTRIBS_CURSE:
        sprintf(tempstr, "All attributes : %+i", x._iPLStr);
        break;
    case IPL_GETHIT_CURSE:
    case IPL_GETHIT:
        sprintf(tempstr, "Every hit you recieve : %+i", x._iPLGetHit);
        break;
    case IPL_LIFE:
    case IPL_LIFE_CURSE:
        sprintf(tempstr, "Hit Points : %+i", x._iPLHP >> 6);
        break;
    case IPL_MANA:
    case IPL_MANA_CURSE:
        sprintf(tempstr, "Mana : %+i", x._iPLMana >> 6);
        break;
    case IPL_DUR:
        strcpy(tempstr, "Increase in durability");
        break;
    case IPL_DUR_CURSE:
        strcpy(tempstr, "Decrease in durability");
        break;
    case IPL_INDESTRUCTIBLE:
        strcpy(tempstr, "Infinite durability");
        break;
    case IPL_LIGHT:
        light = 10 * x._iPLLight;
        sprintf(tempstr, "Increases light radius %i%%", light);
        break;
    case IPL_LIGHT_CURSE:
        light = -(10 * x._iPLLight);
        sprintf(tempstr, "Decreases light radius %i%%", light);
        break;
    case IPL_INVIS:
        sprintf(tempstr, "Invisibility (NW)");
        break;
    case IPL_MULT_ARROWS:
        sprintf(tempstr, "Increase arrows per shot (NW)");
        break;
    case IPL_FIRE_ARROWS:
        sprintf(tempstr, "Fire arrows");
        break;
    case IPL_LIGHT_ARROWS:
        sprintf(tempstr, "Lightning arrows (NW)");
        break;
    case IPL_HOMING_ARROWS:
        sprintf(tempstr, "Homing arrows (NW)");
        break;
    case IPL_THORNS:
        strcpy(tempstr, "You get hurt each use (NW)");
        break;
    case IPL_NOMANA:
        strcpy(tempstr, "Lose all mana, no regen (NW)");
        break;
    case IPL_NOHEALPLR:
        strcpy(tempstr, "You can't heal (NW)");
        break;
    case IPL_SCARE:
        strcpy(tempstr, "Scare monster away (NW)");
        break;
    case IPL_DOUBLE_STRIKE:
        strcpy(tempstr, "Attacks square twice (NW)");
        break;
    case IPL_EXP_DAM:
        strcpy(tempstr, "Exponential damage (NW)");
        break;
    case IPL_SEE_INVIS:
        strcpy(tempstr, "See invisible (NW)");
        break;
    case IPL_ABSHALFTRAP:
        strcpy(tempstr, "Half trap damage (NW)");
        break;
    case IPL_KNOCKBACK:
        strcpy(tempstr, "Knock enemies back (NW)");
        break;
    case IPL_NOHEALMON:
        strcpy(tempstr, "Enemy stops healing (NW)");
        break;
    case IPL_DAMMOD:
        sprintf(tempstr, "+%i damage to each hit", x._iPLDamMod);
        break;
    case IPL_SETDAM:
        sprintf(tempstr, "Unusual item damage");
        break;
    case IPL_SETDUR:
        strcpy(tempstr, "Enhanced durability");
        break;
    case IPL_RNDARROWVEL:
        strcpy(tempstr, "Fires random speed arrows");
        break;
    case IPL_DRAINLIFE:
        strcpy(tempstr, "You constantly lose hit points");
        break;
    default:
        strcpy(tempstr, "Another ability (NW)");
        break;
    }
}

// DrawUniqueInfo	0000000000423BA4

// .text:00423E53
void PrintItemDetails(ItemStruct x)
{
    if (x._iMagical != ITEM_QUALITY_UNIQUE && x._iPrePower != -1)
    {
        PrintItemPower(x._iPrePower, x);
        AddPanelString(tempstr, TRUE);
    }

    if (x._iSufPower != -1)
    {
        PrintItemPower(x._iSufPower, x);
        AddPanelString(tempstr, TRUE);
    }

    if (x._iMagical == ITEM_QUALITY_UNIQUE)
    {
        AddPanelString("unique item", TRUE);
        uitemflag = TRUE;
        curruitem = x;
    }

    if (x._iMiscId == IMISC_STAFF && x._iMaxCharges != 0)
    {
        sprintf(tempstr, "Charges : %i/%i", x._iCharges, x._iMaxCharges);
        AddPanelString(tempstr, TRUE);
    }

    if (x._iClass == ICLASS_WEAPON || x._iClass == ICLASS_ARMOR)
    {
        if (x._iMaxDur == 1000)
        {
            strcpy(tempstr, "Indestructible");
        }
        else
        {
            sprintf(tempstr, "Durability : %i/%i", x._iDurability, x._iMaxDur);
        }
        AddPanelString(tempstr, TRUE);
    }

    if (x._iMinStr + x._iMinMag + x._iMinDex)
    {
        strcpy(tempstr, "Required :");
        if (x._iMinStr)
        {
            sprintf(tempstr, "%s %i Str", tempstr, x._iMinStr);
        }
        if (x._iMinMag)
        {
            sprintf(tempstr, "%s %i Mag", tempstr, x._iMinMag);
        }
        if (x._iMinDex)
        {
            sprintf(tempstr, "%s %i Dex", tempstr, x._iMinDex);
        }
        AddPanelString(tempstr, TRUE);
    }

    pinfoflag = TRUE;
}

// PrintItemDur	00000000004240B2

// .text:00424284
void UseItem(int p, int Mid, int spl)
{
    int rnd, j;

    switch (Mid)
    {
    case IMISC_HEAL:
    case IMISC_FOOD:
        j = plr[p]._pMaxHP >> 8;
        j = ((j >> 1) + random_(j)) << 6;
        if (plr[p]._pClass == PC_WARRIOR)
        {
            j <<= 1;
        }
        if (plr[p]._pClass == PC_ROGUE)
        {
            j += j >> 1;
        }
        plr[p]._pHitPoints += j;
        if (plr[p]._pMaxHP < plr[p]._pHitPoints)
        {
            plr[p]._pHitPoints = plr[p]._pMaxHP;
        }
        plr[p]._pHPBase += j;
        if (plr[p]._pHPBase > plr[p]._pMaxHPBase)
        {
            plr[p]._pHPBase = plr[p]._pMaxHPBase;
        }
        drawhpflag = TRUE;
        break;
    case IMISC_FULLHEAL:
        plr[p]._pHitPoints = plr[p]._pMaxHP;
        plr[p]._pHPBase = plr[p]._pMaxHPBase;
        drawhpflag = TRUE;
        break;
    case IMISC_MANA:
        j = plr[p]._pMaxMana >> 8;
        j = ((j >> 1) + random_(j)) << 6;
        if (plr[p]._pClass == PC_SORCERER)
        {
            j <<= 1;
        }
        if (plr[p]._pClass == PC_ROGUE)
        {
            j += j >> 1;
        }
        plr[p]._pMana += j;
        if (plr[p]._pMana > plr[p]._pMaxMana)
        {
            plr[p]._pMana = plr[p]._pMaxMana;
        }
        plr[p]._pManaBase += j;
        if (plr[p]._pManaBase > plr[p]._pMaxManaBase)
        {
            plr[p]._pManaBase = plr[p]._pMaxManaBase;
        }
        drawmanaflag = TRUE;
        break;
    case IMISC_FULLMANA:
        plr[p]._pMana = plr[p]._pMaxMana;
        plr[p]._pManaBase = plr[p]._pMaxManaBase;
        drawmanaflag = TRUE;
        break;
    case IMISC_POTEXP:
        AddPlrExperience(p, plr[p]._pLevel, plr[p]._pExperience);
        break;
    case IMISC_ELIXSTR:
        rnd = random_(3) + 1;
        plr[p]._pStrength += rnd;
        plr[p]._pBaseStr += rnd;
        break;
    case IMISC_ELIXMAG:
        rnd = random_(3) + 1;
        plr[p]._pMagic += rnd;
        plr[p]._pBaseMag += rnd;
        break;
    case IMISC_ELIXDEX:
        rnd = random_(3) + 1;
        plr[p]._pDexterity += rnd;
        plr[p]._pBaseDex += rnd;
        break;
    case IMISC_ELIXVIT:
        rnd = random_(3) + 1;
        plr[p]._pVitality += rnd;
        plr[p]._pBaseVit += rnd;
        break;
    case IMISC_ELIXWEAK:
        rnd = random_(3) + 1;
        plr[p]._pStrength -= rnd;
        plr[p]._pBaseStr -= rnd;
        break;
    case IMISC_ELIXDIS:
        rnd = random_(3) + 1;
        plr[p]._pMagic -= rnd;
        plr[p]._pBaseMag -= rnd;
        break;
    case IMISC_ELIXCLUM:
        rnd = random_(3) + 1;
        plr[p]._pDexterity -= rnd;
        plr[p]._pBaseDex -= rnd;
        break;
    case IMISC_ELIXSICK:
        rnd = random_(3) + 1;
        plr[p]._pVitality -= rnd;
        plr[p]._pBaseVit -= rnd;
        break;
    case IMISC_BOOK:
        plr[p]._pMemSpells |= SPELLBIT(spl);
        plr[p]._pSplLvl[spl]++;
        plr[p]._pMana += spelldata_sManaCost[spl] << 6;
        if (plr[p]._pMana > plr[p]._pMaxMana)
        {
            plr[p]._pMana = plr[p]._pMaxMana;
        }
        plr[p]._pManaBase += spelldata_sManaCost[spl] << 6;
        if (plr[p]._pManaBase > plr[p]._pMaxManaBase)
        {
            plr[p]._pManaBase = plr[p]._pMaxManaBase;
        }
        drawmanaflag = TRUE;
        break;
    }
}

// SmithItemOk	0000000000424C0F
// RndSmithItem	0000000000424CA6
// SpawnSmith	0000000000424D67
// WitchItemOk	0000000000424FC7
// RndWitchItem	0000000000425058
// SpawnWitch	0000000000425119
// RndBoyItem	00000000004253AD
// SpawnBoy	000000000042545B
