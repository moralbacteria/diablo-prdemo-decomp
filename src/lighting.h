#ifndef __LIGHTING_H__
#define __LIGHTING_H__

#include <windows.h>

//
// defines
//

#define MAXLIGHTS 32
#define MAXVISION 32

//
// structs
//

// TODO assert size == 0x30
struct LightListStruct
{
    int _lx;      // current location
    int _ly;      // current location
    int _lradius; // current radius
    int _lid;     // unique identifier, used instead of index into LightList
    int _ldel;
    BOOL _lunflag; // TRUE if _lun* should be applied next lighting update
    int field_18;  // TODO ?
    int _lunx;     // previous light location
    int _luny;     // previous light location
    int _lunr;     // previous light radius
    int _xoff;     // TODO what units does the offset use
    int _yoff;     // TODO what units does the offset use
};

//
// variables
//

extern BOOL lightflag;
// CrawlTable is weird because it is structured data being represented in a flat array.
// See Devilution for one explanation.
// I like to think of them as tree rings or a bullseye: concentric rings starting from inside and going outward.
// Each ring is defined by a number of (x,y) offsets, followed by a list of (x,y) offsets
// These offsets are relative to a location and together will form a ring around that location.
// Since larger rings have more offsets, each ring is a different size in the array.
// Typically this will be indexed by something like CrawlNum, which will specify the offset for each ring.
// I'm guessing that this is a pre-computed alternative to euclidean distance up to a certain integer distance
extern char CrawlTable[];
// vCrawlTable is an array of lines that fire from the origin.
// For vCrawlTable[i][j], i the angle of the line, and iterating over (j, j+1) gives locations along that line.
// i == 0 is 0 degrees; i == 22 is 90 degrees
// Whereas CrawlTable is like ripples on water radiating outwards, vCrawlTable is like firing a laser or shooting an arrow.
extern BYTE vCrawlTable[23][30];
extern BYTE *pLightTbl;
extern LightListStruct LightList[MAXLIGHTS];
extern int numlights;
extern int nextLightId;
extern int numvision;
extern int visionid;

//
// functions
//

void MakeLightTable();
int AddLight(int x, int y, int r);
void AddUnLight(int i);
void DoLighting(int nXPos, int nYPos, int nRadius, int Lnum);
void ToggleLighting();
void ProcessLightList();
void ProcessVisionList();
void DoVision(int, int, int);
void DoUnVision(int, int, int);
void InitLighting();
void ChangeLight(int i, int x, int y, int r);
void ChangeLightOff(int i, int x, int y);
void ChangeLightRadius(int i, int r);
void ChangeVisionRadius(int id, int r);

#endif